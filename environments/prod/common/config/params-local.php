<?php
return [
    'adminEmail' => 'admin@admin.ru',
    'email' => [
        'from' => '',
        'host' => '',
        'username' => '',
        'password' => '',
        'port' => '465',
        'encryption' => 'ssl',
    ],
];
