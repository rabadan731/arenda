<?php

$params = require_once "params-local.php";
$emailConfig = $params['email'];

return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=base_dev',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => $emailConfig['from']
            ],
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => $emailConfig['host'],
                'username' => $emailConfig['username'],
                'password' => $emailConfig['password'],
                'port' => isset($emailConfig['port'])?$emailConfig['port']:'465',
                'encryption' => isset($emailConfig['encryption'])?$emailConfig['encryption']:'ssl',
            ],
        ],
    ],
];
