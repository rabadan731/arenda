<?php
return [
    'adminEmail' => 'admin@admin.ru',
    'email' => [
        'from' => 'admin@admin.ru',
        'host' => 'smtp.yandex.ru',
        'username' => '',
        'password' => '',
        'port' => '465',
        'encryption' => 'ssl',
    ],
];
