<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'main' => [
            'class' => 'modules\main\Module',
            'isBackend' => true
        ],
        'users' => [
            'class' => 'modules\users\Module',
            'isBackend' => true
        ],
        'rbac' => [
            'class' => 'modules\rbac\Module',
            'isBackend' => true
        ],
        'log' => [
            'class' => 'modules\log\Module',
            'isBackend' => true
        ],
        'agency' => [
            'class' => 'modules\agency\Module',
            'isBackend' => true
        ],
        'cars' => [
            'class' => 'modules\cars\Module',
            'isBackend' => true
        ],
        'clients' => [
            'class' => 'modules\clients\Module',
            'isBackend' => true
        ],
        'eav' => [
            'class' => 'modules\eav\Module',
            'isBackend' => true
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'yii2images' => [
            'class' => 'rico\yii2images\Module',
            //be sure, that permissions ok 
            //if you cant avoid permission errors you have to create "images" folder in web root manually and set 777 permissions
            'imagesStorePath' => '@common/files/images/store', //path to origin images
            'imagesCachePath' => '@common/files/images/cache', //path to resized copies
            'graphicsLibrary' => 'GD', //but really its better to use 'Imagick' 
            'placeHolderPath' => '@common/files/noimage.png', // if you want to get placeholder when image not exists, string will be processed by Yii::getAlias
            'waterMark'       => false //'@frontend/web/images/watermark.png'
        ],
    ],
    'components' => [
        'user' => [
            'class' => 'modules\users\components\User',
            'identityClass' => 'modules\users\models\backend\Users',
            'enableAutoLogin' => true,
            'loginUrl' => ['/users/guest/login'],
        ],
        'request' => [
            'baseUrl' => '/backend'
        ],
        'errorHandler' => [
            'errorAction' => 'main/default/error',
        ],
        'view' => [
            'theme' => 'themes\smart\Theme'
        ],
    ],
    'as access' => [
        'class' => 'yii\filters\AccessControl',
        'rules' => [
            [
                'allow' => true,
                'actions' => ['login', 'error', 'captcha'],
                'roles' => ['?'],
            ], [
                'allow' => true,
                'roles' => ['@'],
            ],
        ],
    ],
    'params' => $params,
];