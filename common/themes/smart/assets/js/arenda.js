/**
 * Created by rabadan731 on 14.07.2016.
 */
$(document).ready(function() {

    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    pageSetUp();




});


function get_data_range() {
    var check_in_str = $("#forrent-check_in").val().split(" ");
    var check_out_str = $("#forrent-check_out").val().split(" ");

    var check_in = new Date(
        check_in_str[0].split("-")[0],
        check_in_str[0].split("-")[1],
        check_in_str[0].split("-")[2],
        check_in_str[1].split(":")[0],
        check_in_str[1].split(":")[1]
    );

     var check_out = new Date(
         check_out_str[0].split("-")[0],
         check_out_str[0].split("-")[1],
         check_out_str[0].split("-")[2],
         check_out_str[1].split(":")[0],
         check_out_str[1].split(":")[1]
    );

    return Math.floor(check_out - check_in);

}

function change_price() {

    var seconds = get_data_range();

    var price = $('#forrent-price_day').val();

    var secPrice = price/ 24 / 60 / 60 / 1000;

    var resultPrice = (seconds*secPrice).toFixed(2);

    $('#forrent-price_all').val(isNaN(resultPrice)?0:resultPrice);

}



$('body').on('change', '#forrent-price_day', function () {
    change_price();
});
$('body').on('change', '#forrent-check_out', function () {
    change_price();
});
$('body').on('change', '#forrent-check_in', function () {
    change_price();
});


$('body').on('change', '#forrent-price_all', function () {
    var seconds = get_data_range();
    var price = $('#forrent-price_all').val();
    var secPrice = price/seconds;
    var resultPrice = (24*60*60*1000*secPrice).toFixed(2);
    $('#forrent-price_day').val(isNaN(resultPrice)?0:resultPrice);
});