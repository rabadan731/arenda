<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\modules\agency\models\ForRent;
use yii\widgets\Breadcrumbs;
use themes\smart\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$asset = Yii::$app->assetManager->getPublishedUrl('@themes/smart/assets/');

if (isset($this->params['place'])){
    $this->registerJs('
        var place = $("#place-' . Html::encode($this->params['place']) . '");
        if (place.parent().parent().prop("tagName") == "LI"){
            place.parent().parent().addClass("open");
            place.parent().show();
        }
        place.addClass("active");
    ');
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="smart-style-0">

<!-- #HEADER -->
<header id="header">
    <div id="logo-group">

        <!-- PLACE YOUR LOGO HERE -->
        <span id="logo"> <img src="<?= $asset ?>/img/logo.png" alt="SmartAdmin"> </span>
        <!-- END LOGO PLACEHOLDER -->
    </div>

    <!-- #TOGGLE LAYOUT BUTTONS -->
    <!-- pulled right: nav area -->
    <div class="pull-right">

        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right hidden-lg hidden-md">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->

        <!-- #MOBILE -->
        <!-- Top menu profile link : this shows only when top menu is active -->
        <ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
            <li class="">
                <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
                    <img src="<?= $asset ?>/img/avatars/sunny.png" alt="John Doe" class="online" />
                </a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0"><i class="fa fa-cog"></i> Setting</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#ajax/profile.html" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="toggleShortcut"><i class="fa fa-arrow-down"></i> <u>S</u>hortcut</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="<?= Url::to(['/users/user/logout']) ?>" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout"><i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>
                    </li>
                </ul>
            </li>
        </ul>



        <!-- logout button -->
        <div class="btn-header transparent pull-right">
            <span>
                <a href="<?= Url::to(['/users/user/logout']) ?>" title="Выход">
                    <i class="fa fa-sign-out"></i>
                </a>
            </span>
        </div>
        <!-- end logout button -->
        <div class="btn-header transparent pull-right">
            <?php if (Yii::$app->user->id !== 1 &&
                isset($_COOKIE['admin_reaut']) &&
                $_COOKIE['admin_reaut'] === md5('731'.date("Y-m-d"))
            ) { ?>
                <span>
                    <a href="<?= Url::to(['/agency/agency/auth-admin']) ?>" title="Войти как админ">
                        <i class="fa fa-reply-all"></i> Варнутся к админу
                    </a>
                </span>
            <?php } ?>
        </div>
    </div>
    <!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->

<!-- #NAVIGATION -->
<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS/SASS variables -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as is -->

            <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                <img src="<?= $asset ?>/img/avatars/sunny.png" alt="me" class="online" />
                <span>
                    <?= Yii::$app->user->identity->name ?>
                </span>
                <i class="fa fa-angle-down"></i>
            </a>

        </span>
    </div>
    <!-- end user info -->

    <?= $this->render('nav2'); ?>
</aside>
<!-- END NAVIGATION -->

<!-- #MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="ribbon">

        <a href="<?= Url::to() ?>" class="ribbon-button-alignment">
		    <span class="btn btn-ribbon">
                <i class="fa fa-refresh"></i>
            </span>
        </a>

        <!-- breadcrumb -->
        <?= Breadcrumbs::widget([
            'encodeLabels' => false,
            'homeLink' => [
                'label' => Yii::t('common','Home'),
                'url' => ['/']
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
        ]) ?>
        <!-- end breadcrumb -->

    </div>
    <!-- END RIBBON -->

    <!-- #MAIN CONTENT -->
    <div id="content">

        <div class="row">

            <!-- col -->
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <?php if (isset($this->params['pageTitle'])): ?>
                        <!-- PAGE HEADER -->
                        <?php if (isset($this->params['pageImage'])): ?>
                            <img width="48" src="<?= $this->params['pageImage'] ?>" alt=""/>
                        <?php else: ?>
                            <?php if (isset($this->params['pageIcon'])): ?>
                                <i class="fa-fw fa fa-<?= $this->params['pageIcon'] ?>"></i>
                            <?php endif; ?>
                        <?php endif; ?>

                        <?= ($this->params['pageTitle']); ?>
                    <?php endif; ?>
                </h1>
            </div>

            <?php /*if (isset($this->params['rightHead'])): ?>
                <?=$this->params['rightHead'];?>
            <?php endif; */ ?>

            <?php if ($this->beginCache("sparks", ['duration' => 600])) { ?>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-8">
                <ul id="sparks" class="">
                    <li class="sparks-info">
                        <h5>
                            <?= Yii::t("common", "Payment"); ?>
                            <span class="txt-color-blue">
                                <?= ForRent::paymentForTheMonth(time());?>
                                <i class="fa fa-eur"></i>
                            </span>
                        </h5>
                        <div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
                            <?php
                                $paymentForTheYear = ForRent::paymentForTheYear(time());
                                echo implode(", ",$paymentForTheYear);
                            ?>
                        </div>
                    </li>
                    <li class="sparks-info">
                        <h5>
                            <?= Yii::t("common", "The population"); ?>
                            <span class="txt-color-purple">
                                <i class="fa fa-arrow-circle-up"></i>
                                &nbsp;
                                <?= ForRent::timeForTheMonth(time());?>%
                            </span>
                        </h5>
                        <div class="sparkline txt-color-purple hidden-mobile hidden-md hidden-sm">
                            <?php
                                $timeForTheYear = ForRent::timeForTheYear(time());
                                echo implode(", ",$timeForTheYear);
                            ?>
                        </div>
                    </li>
                    <li class="sparks-info">
                        &nbsp;
                    </li>
                    <?php /* <li class="sparks-info">
                        <h5>
                            <?= Yii::t("common", "Income"); ?>
                            <span class="txt-color-greenDark">
                                <i class="fa fa-shopping-cart"></i>
                                &nbsp;
                                0
                            </span>
                        </h5>
                        <div class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm">
                            0,0,0,0,0,0,0,0,0,0,0,0
                        </div>
                    </li> */ ?> 
                </ul>
            </div>
            <!-- end col -->
            
            <?php $this->endCache(); } ?>

            <div class="col-xs-12">
                <div class="row">

                    <?php if (Yii::$app->session->hasFlash('success')): ?>
                        <div class="jarviswidget">
                            <div>
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?= Yii::$app->session->getFlash('success') ?>
                                </div>
                            </div>
                        </div>
                    <?php elseif (Yii::$app->session->hasFlash('error')): ?>
                        <div class="jarviswidget">
                            <div>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?= Yii::$app->session->getFlash('error') ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?= $content ?>
                </div>
            </div>

        </div>

    </div>

    <!-- END #MAIN CONTENT -->

</div>
<!-- END #MAIN PANEL -->

<!-- #PAGE FOOTER -->
<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white"><?= Yii::$app->name ?> © <?= date('Y') ?></span>
        </div>

        <div class="col-xs-6 col-sm-6 text-right hidden-xs">
            <div class="txt-color-white inline-block">
                <div class="btn-group dropup">
                    <button class="btn btn-xs dropdown-toggle bg-color-blue txt-color-white" data-toggle="dropdown">
                        <i class="fa fa-link"></i> <span class="caret"></span>
                    </button>
                </div>
                <!-- end btn-group-->
            </div>
            <!-- end div-->
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>
<!-- END FOOTER -->

<!-- #SHORTCUT AREA : With large tiles (activated via clicking user name tag)
     Note: These tiles are completely responsive, you can add as many as you like -->
<div id="shortcut">
    <ul>
        <li>
            <a href="<?= Url::to(['/users/default/update', 'id' => Yii::$app->user->id]) ?>"
               class="jarvismetro-tile big-cubes bg-color-blueLight">
                <span class="iconbox">
                    <i class="fa fa-user fa-4x"></i>
                    <span>
                        <?=Yii::t('common','Profile');?>
                    </span>
                </span>
            </a>
            <a href="<?= Url::to(['/users/default/gcalendar']) ?>" class="jarvismetro-tile big-cubes bg-color-greenDark">
                <span class="iconbox">
                    <i class="fa fa-calendar fa-4x"></i>
                    <span>
                        <?=Yii::t('common','Calendar');?>
                    </span>
                </span>
            </a>
            <?php if (Yii::$app->user->can('admin')): ?>
                <a href="<?= Url::to(['/agency/agency-tariff/index']) ?>" class="jarvismetro-tile big-cubes bg-color-blueDark">
                    <span class="iconbox">
                        <i class="fa fa-money fa-4x"></i>
                        <span>
                            <?=Yii::t('common','Tariffs');?>
                        </span>
                    </span>
                </a>
                <a href="<?= Url::to(['/eav/eav-param/index']) ?>" class="jarvismetro-tile big-cubes bg-color-blue">
                    <span class="iconbox">
                        <i class="fa fa-sitemap fa-4x"></i>
                        <span>
                            <?=Yii::t('common','Parameters');?>
                        </span>
                    </span>
                </a>
                <a href="<?= Url::to(['/agency/default/trash']) ?>" class="jarvismetro-tile big-cubes bg-color-red">
                    <span class="iconbox">
                        <i class="fa fa-trash-o fa-4x"></i>
                        <span>
                            <?=Yii::t('common','Trash');?>
                        </span>
                    </span>
                </a>
                <a href="<?= Url::to(['/main/key-storage/index']) ?>"
                   class="jarvismetro-tile big-cubes bg-color-greenLight">
                    <span class="iconbox">
                        <i class="fa fa-gears fa-4x"></i>
                        <span>
                            <?=Yii::t('common','Settings');?>
                        </span>
                    </span>
                </a>
            <?php endif; ?>
        </li>
    </ul>
</div>
<!-- END SHORTCUT AREA -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
