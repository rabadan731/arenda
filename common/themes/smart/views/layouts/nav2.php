<?php

use common\modules\cars\widgets\Menu;

/**
 * Created by PhpStorm.
 * User: rabadan
 * Date: 01.02.2017
 * Time: 13:05
 */

$adminSiteMenu[] = [
    'label' => Yii::t("common", "Home"),
    'url' => ['/'],
    'icon' => 'fa fa-home',
];

$adminSiteMenu[] = Yii::$app->user->can('admin')? [
    'icon' => 'fa fa-building',
    'label' => Yii::t("common", "Agencies"),
    'url' => ['/agency/agency/index'],
] : [
    'icon' => 'fa fa-building-o',
    'label' => Yii::t("common", "My Agency"),
    'url' => ['/agency/agency/view','id'=>Yii::$app->user->identity->agency_id]
];


$adminSiteMenu[] = [
    'label' => Yii::t("common", "Realty"),
    'icon' => 'fa fa-building-o',
    'options' => ['class' => 'treeview'],
    'items' => [
        [
            'icon' => 'fa fa-hotel',
            'label' => Yii::t("common", "Objects fot rent"),
            'url' => ['/agency/rent/index'],
        ],
        [
            'icon' => 'fa fa-university',
            'label' => Yii::t("common", "Objects fot sale"),
            'url' => ['/agency/rent/index-buy'],
        ],
        [
            'icon' => 'fa fa-building-o',
            'label' => Yii::t("common", "Others objects"),
            'url' => ['/agency/rent/index-others'],
        ],
        [
            'icon' => 'fa fa-calendar',
            'label' => Yii::t("common", "Calendar"),
            'url' => ['/agency/for-rent/index'],
        ],
        [
            'icon' => 'fa fa-cogs',
            'label' => Yii::t("common", "Operations"),
            'options' => ['class' => 'treeview'],
            'items' => [
                [
                    'icon' => 'fa fa-plus',
                    'label' => Yii::t("common", "Add object"),
                    'url' => ['/agency/rent/create'],
                ],
                [
                    'icon' => 'fa fa-share',
                    'label' => Yii::t("common", "Import"),
                    'url' => ['/main/import/index'],
                ],
                [
                    'icon' => 'fa fa-reply',
                    'label' => Yii::t("common", "Export"),
                    'url' => ['/main/import/export'],
                ],

            ],
        ],

    ],
];

\modules\cars\Module::registerTranslations();
$adminSiteMenu[] = [
    'label' => Yii::t("cars", "Cars"),
    'icon' => 'fa fa-car',
    'options' => ['class' => 'treeview'],
    'items' => [
        [
            'icon' => 'fa fa-car',
            'label' => Yii::t("cars", "Provisions machines"),
            'url' => ['/cars/car-reserve/index'],
        ],
        [
            'icon' => 'fa fa-car',
            'label' => Yii::t("cars", "Car"),
            'url' => ['/cars/car/index'],
        ],
        [
            'icon' => 'fa fa-wrench',
            'label' => Yii::t("cars", "Params"),
            'options' => ['class' => 'treeview'],
            'items' => [
                [
                    'icon' => 'fa fa-building-o',
                    'label' => Yii::t("cars", "Mark"),
                    'url' => ['/cars/car-mark/index'],
                ],
                [
                    'icon' => 'fa fa-building-o',
                    'label' => Yii::t("cars", "Model"),
                    'url' => ['/cars/car-model/index'],
                ],
            ]
        ],
    ],
];

$adminSiteMenu[] = [
    'icon' => 'fa fa-users',
    'label' => Yii::t("common", "Clients"),
    'url' => ['/clients/clients/index'],
];

$adminSiteMenu[] = [
    'icon' => 'fa fa-wrench',
    'label' => Yii::t("common", "Workers"),
    'url' => ['/agency/workers/index'],
];

$adminSiteMenu[] = [
    'icon' => 'fa fa-inbox',
    'label' => Yii::t("common", "Sent emails"),
    'url' => ['/main/mail/index'],
];


$adminSiteMenu[] = [
    'label' => Yii::t("common", "Users"),
    'icon' => 'fa fa-users',
    'options' => ['class' => 'treeview'],
    'visible' => Yii::$app->user->can('user'),
    'items' => [
        [
            'icon' => 'fa fa-users',
            'label' => Yii::t("common", "Users"),
            'url' => ['/users/default/index'],
        ],
        [
            'icon' => 'fa fa-male',
            'label' => Yii::t("common", "Access rules"),
            'url' => ['/rbac/roles/index'],
            'visible' => Yii::$app->user->can('roles_view')
        ],
        [
            'icon' => 'fa fa-male',
            'label' => Yii::t("common", "Permissions"),
            'url' => ['/rbac/default/index'],
            'visible' => Yii::$app->user->can('permissions_view')
        ],
    ],
];


$adminSiteMenu[] = [
    'icon' => 'fa fa-retweet',
    'label' => Yii::t("common", "Logs action"),
    'url' => ['/log'],
    'visible' => Yii::$app->user->can('log_view')
];


?>

<nav>
    <?= Menu::widget(['items' => $adminSiteMenu]); ?>
</nav>