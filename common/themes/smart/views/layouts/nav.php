<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\modules\agency\models\ForRent;
use yii\widgets\Breadcrumbs;
use themes\smart\assets\AppAsset;

?>
<!-- NAVIGATION : This navigation is also responsive

    To make this navigation dynamic please make sure to link the node
    (the reference to the nav > ul) after page load. Or the navigation
    will not initialize.
    -->
<nav>
    <!--
    NOTE: Notice the gaps after each icon usage <i></i>..
    Please note that these links work a bit different than
    traditional href="" links. See documentation for details.
    -->

    <ul>


        <li>
            <a href="#">
                <i class="fa fa-lg fa-fw fa-building-o"></i>
                <span class="menu-item-parent">Недвижемость</span>
                <b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b>
            </a>
            <ul style="display: none;">
                <?php if (Yii::$app->user->can('admin')): ?>

                    <li id="place-agency">
                        <a href="<?= Url::to(['/agency/agency/index']) ?>">
                            <i class="fa fa-lg fa-fw fa-building"></i>
                            <span class="menu-item-parent"><?=Yii::t('common','Agencies');?></span>
                        </a>
                    </li>

                <?php else: ?>

                    <li id="place-agency">
                        <a href="<?= Url::to(['/agency/agency/view','id'=>Yii::$app->user->identity->agency_id]) ?>">
                            <i class="fa fa-lg fa-fw fa-building-o"></i>
                            <span class="menu-item-parent"><?=Yii::t('common','My Agency');?></span>
                        </a>
                    </li>

                <?php endif; ?>

                <li id="place-rent">
                    <a href="<?= Url::to(['/agency/rent/index']) ?>">
                        <i class="fa fa-lg fa-fw fa-building-o"></i>
                        <span class="menu-item-parent"><?=Yii::t('common','Objects fot rent');?></span>
                    </a>
                </li>


                <li id="place-forRent">
                    <a href="<?= Url::to(['/agency/for-rent/index']) ?>">
                        <i class="fa fa-lg fa-fw fa-calendar"></i>
                        <span class="menu-item-parent"><?=Yii::t('common', 'Calendar');?></span>
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="#">
                <i class="fa fa-lg fa-fw fa-car"></i>
                <span class="menu-item-parent"> Авто </span>
                <b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b>
            </a>
            <ul style="display: none;">
                <li id="place-forRent">
                    <a href="<?= Url::to(['/cars/car/index']) ?>">
                        <i class="fa fa-lg fa-fw fa-car"></i>
                        <span class="menu-item-parent"><?=Yii::t('common', 'Cars');?></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-lg fa-fw fa-car"></i>
                        <span class="menu-item-parent"> Конфигурация </span>
                    </a>
                    <ul style="display: none;">
                        <li id="place-forRent">
                            <a href="<?= Url::to(['/cars/mark/index']) ?>">
                                <i class="fa fa-lg fa-fw fa-calendar"></i>
                                <span class="menu-item-parent"><?=Yii::t('common', 'Mark');?></span>
                            </a>
                        </li>
                        <li id="place-forRent">
                            <a href="<?= Url::to(['/cars/model/index']) ?>">
                                <i class="fa fa-lg fa-fw fa-calendar"></i>
                                <span class="menu-item-parent"><?=Yii::t('common', 'Model');?></span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>

        <li id="place-clients">
            <a href="<?= Url::to(['/agency/clients/index']) ?>">
                <i class="fa fa-lg fa-fw fa-users"></i>
                <span class="menu-item-parent"><?=Yii::t('common','Clients');?></span>
            </a>
        </li>

        <li id="place-workers">
            <a href="<?= Url::to(['/agency/workers/index']) ?>">
                <i class="fa fa-lg fa-fw fa-wrench "></i>
                <span class="menu-item-parent"><?=Yii::t('common','Workers');?></span>
            </a>
        </li>


        <li class="">
            <a href="<?= Url::to(['/main/arenda-mail/index']) ?>">
                <i class="fa fa-lg fa-fw fa-inbox"></i>
                <span class="menu-item-parent"><?= Yii::t("common", "Sent emails"); ?></span>
            </a>
        </li>



        <?php if (Yii::$app->user->can('users')): ?>
            <li>
                <a href="#">
                    <i class="fa fa-lg fa-fw fa-users"></i>
                    <span class="menu-item-parent"><?=Yii::t('common','Users');?></span>
                    <b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b>
                </a>
                <ul style="display: none;">

                    <li id="place-users">
                        <a href="<?= Url::to(['/users']) ?>">
                            <i class="fa fa-lg fa-fw fa-users"></i>
                            <span class="menu-item-parent"><?=Yii::t('common','Users');?></span>
                        </a>
                    </li>
                    <?php if (Yii::$app->user->can('roles_view')): ?>
                        <li id="place-roles">
                            <a href="<?= Url::to(['/rbac/roles']) ?>">
                                <i class="fa fa-lg fa-fw fa-puzzle-piece"></i>
                                <span class="menu-item-parent"><?=Yii::t('common','User roles');?></span>
                            </a>
                        </li>
                    <?php endif ?>

                    <?php if (Yii::$app->user->can('rules_view')): ?>
                        <li id="place-rules">
                            <a href="<?= Url::to(['/rbac/rules']) ?>">
                                <i class="fa fa-lg fa-fw fa-male"></i>
                                <span class="menu-item-parent"><?=Yii::t('common','Access rules');?></span>
                            </a>
                        </li>
                    <?php endif ?>

                    <?php if (Yii::$app->user->can('permissions_view')): ?>
                        <li id="place-rbac">
                            <a href="<?= Url::to(['/rbac']) ?>">
                                <i class="fa fa-lg fa-fw fa-unlock-alt"></i>
                                <span class="menu-item-parent"><?=Yii::t('common','Permissions');?></span>
                            </a>
                        </li>
                    <?php endif ?>
                </ul>
            </li>
        <?php endif ?>
        <?php /* if (
                Yii::$app->user->can('product_view') ||
                Yii::$app->user->can('import_view') ||
                Yii::$app->user->can('moderation_view')
            ): ?>
                <li>
                    <a href="#">
                        <i class="fa fa-lg fa-fw fa-cubes"></i>
                        <span class="menu-item-parent">Товары</span>
                        <b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b>
                    </a>
                    <ul style="display: none;">
                        <?php if (Yii::$app->user->can('product_view')): ?>
                            <li id="place-product-default">
                                <a href="<?= Url::to(['/product']) ?>">Каталог</a>
                            </li>
                        <?php endif; ?>

                        <?php if (Yii::$app->user->can('import_view')): ?>
                            <li id="place-product-import">
                                <a href="<?= Url::to(['/product/import']) ?>">Импорт</a>
                            </li>
                        <?php endif; ?>

                        <?php if (Yii::$app->user->can('moderation_view')): ?>
                            <li id="place-product-moderation">
                                <a href="<?= Url::to(['/product/moderation']) ?>">На модерации</a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if (Yii::$app->user->can('category_crud')): ?>
                <li id="place-product-category">
                    <a href="<?= Url::to(['/product/category']) ?>">
                        <i class="fa fa-fw fa-fw fa-code-fork"></i>
                        <span class="menu-item-parent">Категории</span>
                    </a>
                </li>
            <?php endif; ?>
            */ ?>

        <?php if (Yii::$app->user->can('log_view')): ?>
            <li id="place-log">
                <a href="<?= Url::to(['/log']) ?>">
                    <i class="fa fa-lg fa-fw fa-retweet"></i>
                    <span class="menu-item-parent"><?=Yii::t('common','Logs action');?></span>
                </a>
            </li>
        <?php endif ?>
    </ul>
</nav>