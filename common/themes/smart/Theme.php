<?php

namespace themes\smart;

use yii;
use yii\helpers\Html;

class Theme extends yii\base\Theme
{
    /**
     * @inheritdoc
     */
    public $pathMap = [
        '@backend/views' => '@themes/smart/views',
        '@modules' => '@themes/smart/modules'
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        Yii::$container->set('yii\grid\CheckboxColumn', [
            'options' => [
                'style' => 'width: 30px'
            ]
        ]);

        Yii::$container->set('yii\grid\GridView', [
            'layout' => "{items}\n{pager}"
        ]);

        Yii::$container->set('yii\grid\ActionColumn', [
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    return Html::a('<i class="fa fa-eye"></i>', $url, [
                        'class' => 'btn btn-xs btn-info',
                        'title' => 'Просмотр',
                        'data-pjax' => 0,
                    ]);
                },
                'view2' => function ($url, $model, $key) {
                    $url = str_replace('view2', 'view', $url);
                    return Html::a('<i class="fa fa-eye"></i> '.Yii::t("common","View"), $url, [
                        'class' => 'btn btn-xs btn-info',
                        'title' => Yii::t("common","View"),
                        'data-pjax' => 0,
                    ]);
                },
                'update' => function ($url, $model, $key) {
                    return Html::a('<i class="fa fa-edit"></i>', $url, [
                        'class' => 'btn btn-xs btn-warning',
                        'title' => 'Изменить',
                        'data-pjax' => 0,
                    ]);
                },
                'delete' => function ($url, $model, $key) {
                    return Html::a('<i class="fa fa-trash-o"></i>', $url, [
                        'class' => 'btn btn-xs btn-danger', 'data-method' => 'post',
                        'title' => 'Удалить',
                        'data' => [
                            'pjax' => 0,
                            'confirm' => 'Вы уверены, что хотите удалить этот элемент?'
                        ],
                    ]);
                }
            ],
            'template' => '<div class="text-center">{update} {delete}</div>'
        ]);


        $bundles = Yii::$app->assetManager->bundles;
        if (is_array($bundles)) {
            Yii::$app->assetManager->bundles = array_merge($bundles, $this->getThemeBundles());
        } else {
            Yii::$app->assetManager->bundles = $this->getThemeBundles();
        }
    }

    public function getThemeBundles()
    {
        $bundles = [];

        $bundles['yii\web\JqueryAsset'] = [
            'sourcePath' => '@themes/smart/assets',
            'js' => [
                'js/libs/jquery-2.1.1.min.js'
            ],
        ];

        $bundles['yii\bootstrap\BootstrapAsset'] = [
            'sourcePath' => '@themes/smart/assets',
            'css' => [
                'css/bootstrap.min.css'
            ],
        ];

        $bundles['yii\bootstrap\BootstrapPluginAsset'] = [
            'sourcePath' => '@themes/smart/assets',
            'js' => [
                'js/bootstrap/bootstrap.min.js'
            ],
            'depends' => [
                'yii\web\JqueryAsset',
                'yii\bootstrap\BootstrapAsset',
            ]
        ];

        return $bundles;
    }
}
