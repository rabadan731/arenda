<?php 
 return array (
  'class' => 'yii\\swiftmailer\\Mailer',
  'useFileTransport' => false,
  'messageConfig' => 
  array (
    'charset' => 'UTF-8',
    'from' => 'from',
  ),
  'transport' => 
  array (
    'class' => 'Swift_SmtpTransport',
    'host' => 'host',
    'username' => 'username',
    'password' => 'password',
    'port' => '465',
    'encryption' => 'ssl',
  ),
);