<?php

namespace modules\users\models\backend;

use common\modules\agency\models\Agency;
use common\modules\clients\models\Clients;
use common\modules\agency\models\Rent;
use yii;
use yii\helpers\Html;
use common\helpers\Time;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use modules\log\behaviors\LogBehavior;

/**
 * Class Users
 * @package modules\users\models\backend
 *
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $phone
 * @property string $auth_key
 * @property string $time_reg
 * @property string $role
 * @property string $sex
 * @property string $error_auth
 * @property string $avatar
 * @property string $locale
 * @property string $google_token
 * @property integer $agency_id
 */
class Users extends ActiveRecord implements IdentityInterface
{
    /**
     * Сценарий редактирования профиля
     */
    const SCENARIO_PROFILE = 'profile';

    /**
     * Старый пароль
     * @var string
     */
    public $old_password;
    /**
     * Новый пароль
     * @var string
     */
    public $new_password;
    /**
     * Повторяем новый пароль
     * @var string
     */
    public $new_password_repeat;
    /**
     * Все роли
     * @var array
     */
    private static $roles = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sex = empty($this->sex) ? 'm' : $this->sex;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name'], 'required'],
            [['email'], 'email'],
            [['balance'], 'number'],
            [['phone'], 'string'],
            ['name', 'string', 'min' => 2, 'max' => 50,
                'tooShort' => Yii::t("common", "Minimum length {num} characters", ['num' => 2]),
                'tooLong' => Yii::t("common", "Maximum length {num} characters", ['num' => 50]),
                'when' => function ($model) {
                    return Yii::$app->user->can('users_crud');
                }
            ],
            ['agency_id', 'integer', 'when' => function ($model) {
                return Yii::$app->user->can('agency');
            }
            ],
            [
                'email',
                'unique',
                'targetAttribute' => 'email',
                'message' => Yii::t("common", "This email is already in use"),
//                'when' => function ($model) {
//                    return Yii::$app->user->can('users_crud');
//                }
            ],
            ['sex', 'in', 'range' => ['m', 'w'], 'message' => Yii::t("common", "Specify your gender")],
            [['password'], 'required',
                'when' => function ($model) {
                    return $model->isNewRecord;
                }
            ],
            ['password', 'string', 'min' => 6, 'max' => 50,
                'tooShort' => Yii::t("common", "Minimum length {num} characters", ['num' => 6]),
                'tooLong' => Yii::t("common", "Maximum length {num} characters", ['num' => 50]),
                'when' => function ($model) {
                    return $model->isNewRecord;
                }
            ],
            ['old_password', 'isOldPassword', 'when' => function ($model) {
                return $this->id === Yii::$app->user->getId();
            }],
            ['google_token', 'string'],
            ['new_password', 'string', 'min' => 6, 'max' => 50,
                'tooShort' => Yii::t("common", "Minimum length {num} characters", ['num' => 6]),
                'tooLong' => Yii::t("common", "Maximum length {num} characters", ['num' => 50]),
            ],
            ['new_password_repeat', 'compare', 'compareAttribute' => $this->isNewRecord ? 'password' : 'new_password',
                'when' => function ($model) {
                    return empty($model->new_password) === false || empty($model->password) === false;
                }, 'skipOnEmpty' => false, 'message' => Yii::t("common", "Passwords do not match")],
            ['role', 'roleExists', 'on' => 'profile',
                'when' => function ($model) {
                    return Yii::$app->user->can('users_crud');
                }
            ],
            ['locale', 'default', 'value' => Yii::$app->sourceLanguage],
            ['locale', 'in', 'range' => array_keys(Yii::$app->params['availableLocales'])],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => Yii::t("common", "Email"),
            'password' => Yii::t("common", "Password"),
            'name' => Yii::t("common", "Name"),
            'phone' => Yii::t("common", "Phone"),
            'agency_id' => Yii::t("common", "Agency"),
            'sex' => Yii::t("common", "Sex"),
            'time_reg' => Yii::t("common", "Registration date"),
            'role' => Yii::t("common", "Role"),
            'locale' => Yii::t("common", "System language"),
            'old_password' => Yii::t("common", "Old password"),
            'new_password' => Yii::t("common", "New password"),
            'new_password_repeat' => Yii::t("common", "Password repeat"),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'log' => [
                'class' => LogBehavior::className(),
                'types' => [
                    'update' => function ($model) {
                        /* @var $model self */
                        return "Изменил данные пользователя \"$model->name\"";
                    },
                    'insert' => function ($model) {
                        /* @var $model self */
                        return "Добавил пользователя \"$model->name\"";
                    },
                    'delete' => function ($model) {
                        /* @var $model self */
                        return "Удалил пользователя \"$model->name\"";
                    }
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
            $this->time_reg = Time::real();
        } elseif ($this->isNewRecord === false && $this->new_password) {
            $this->password = Yii::$app->security->generatePasswordHash($this->new_password);
        }

        $this->name = Html::encode($this->name);
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->scenario === self::SCENARIO_PROFILE) {
            $this->saveRole();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return parent::scenarios();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new yii\base\NotSupportedException('findIdentityByAccessToken is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Проверка наличия роли
     * @param $attribute
     * @param $params
     */
    public function roleExists($attribute, $params)
    {
        if (Yii::$app->authManager->getRole($this->$attribute) === null) {
            $this->addError($attribute, Yii::t("common", "This role does not exist"));
        }
    }

    /**
     * Проверяем старый пароль
     * @param $attribute
     * @param $params
     */
    public function isOldPassword($attribute, $params)
    {
        if (!Yii::$app->security->validatePassword($this->old_password, Yii::$app->user->identity->password)) {
            $this->addError('old_password', Yii::t("common", "Invalid old password"));
        }
    }

    /**
     * Получаем все роли
     * @return array
     */
    public static function getRoles()
    {
        if (self::$roles) {
            return self::$roles;
        }

        self::$roles = ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');
        return self::$roles;
    }

    /**
     * Сохраняем роль пользователя
     */
    private function saveRole()
    {
        $auth = Yii::$app->authManager;
        $auth->revokeAll($this->id);

        if (($role = $auth->getRole($this->role)) !== null) {
            $auth->assign($role, $this->id);
        }
    }

    /**
     * Получаем всех пользователей
     * @return array
     */
    public function valuesAll()
    {
        $find = self::find();
        if (!Yii::$app->user->can('admin')) {
            $find->andWhere(['agency_id' => Yii::$app->user->identity->agency_id]);
        }
        return ArrayHelper::map($find->all(), 'id', 'name');
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(
            Clients::className(),
            ['id' => 'client_id']
        )->viaTable(
            'client_manager',
            ['manager_id' => 'id']
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRents()
    {
        return $this->hasMany(
            Rent::className(),
            ['id' => 'rent_id']
        )->viaTable(
            'rent_manager',
            ['manager_id' => 'id']
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        $t = $this->hasOne(Agency::className(), ['id' => 'agency_id']);
        print_r($t);
        die;
    }
}
