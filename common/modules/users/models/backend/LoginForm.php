<?php

namespace modules\users\models\backend;

use common\helpers\Time;
use Yii;
use yii\base\Model;
use yii\validators\Validator;

/**
 * Class LoginForm
 * @package modules\users\models\backend
 *
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $auth_key
 * @property string $time_reg
 * @property string $role
 * @property string $sex
 * @property string $error_auth
 * @property string $avatar
 */
class LoginForm extends Model
{
    /**
     * Логин
     * @var string
     */
    public $email;
    /**
     * Пароль
     * @var string
     */
    public $password;
    /**
     * Запомнить меня
     * @var bool
     */
    public $rememberMe = true;
    /**
     * Капча
     * @var string
     */
    public $verifyCode;
    /**
     * Храним данные пользователя
     * @var object
     */
    public $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            [['email', 'password'], 'userValidate'],
            ['verifyCode', 'required', 'when' => function ($model) {
                return isset($model->user->error_auth) && $model->user->error_auth > 3;
            }
            ],
            ['verifyCode', 'captchaValidate', 'skipOnEmpty' => false, 'when' => function ($model) {
                return isset($model->user->error_auth) && $model->user->error_auth > 3;
            }
            ],
        ];
    }

    /**
     * Проверяем капчу и обновляем счётчик ошибок авторизации
     * @param $attribute string
     * @param $params array
     */
    public function captchaValidate($attribute, $params)
    {
        $validator = Validator::createValidator('captcha', $this, $attribute, [
            'captchaAction' => 'users/guest/captcha'
        ]);

        if ($validator->validate($this->verifyCode) === false) {
            $this->addError($attribute, Yii::t("common", "Invalid code from the picture"));
        } else {
            $this->user->detachBehavior('log');
            $this->user->error_auth = 0;
            $this->user->save(0);
        }
    }

    /**
     * Ищем пользователя по логину и паролю
     * @throws \yii\base\InvalidConfigException
     */
    public function userValidate()
    {
        $auth = Yii::$app->authManager;
        $this->user = Users::find()->where(['email' => $this->email])->one();

        if ($this->user === null) {
            $this->addError('email', Yii::t("common", "The user is not found"));
        } elseif (!Yii::$app->security->validatePassword($this->password, $this->user->password)) {
            $this->user->error_auth++;
            $this->user->save();
            $this->password = null;
            $this->addError('password', Yii::t("common", "Wrong password"));
        } elseif (isset($auth->getPermissionsByUser($this->user->id)['backend_access']) === false) {
            $this->addError('email', Yii::t("common", "Insufficient rights"));
        }
    }

    /**
     * Авторизуем пользователя
     * @return bool
     */
    public function login()
    {
        if ($this->validate()) {
            if (Yii::$app->user->login($this->user, $this->rememberMe ? 864000 : 0)) {
                return true;
            } else {
                $this->addError('email', Yii::t("common", "Authorization error. Try it"));
            }
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t("common", "Email"),
            'password' => Yii::t("common", "Password"),
            'verifyCode' => Yii::t("common", "Code from the picture"),
            'rememberMe' => Yii::t("common", "Remember Me")
        ];
    }
}
