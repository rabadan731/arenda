<?php

namespace modules\users\models;

use yii;

/**
 * This is the model class for table "{{%users_login}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $ip
 * @property string $ua
 * @property integer $created_at
 */
class UsersLogin extends yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users_login}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => Yii::t("common", "Users"),
            'ip' => Yii::t("common", "IP"),
            'ua' => 'User Agent',
            'created_at' => Yii::t("common", "Time"),
        ];
    }
}
