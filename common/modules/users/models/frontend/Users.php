<?php

namespace modules\users\models\frontend;

use modules\users\models\NotSupportedException;
use Yii;
use common\helpers\Time;
use yii\helpers\Html;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;
use abeautifulsite\SimpleImage;

/**
 * Class Users
 * @package modules\users\models\frontend
 *
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $auth_key
 * @property string $time_reg
 * @property string $role
 * @property string $sex
 * @property string $error_auth
 * @property string $avatar
 */
class Users extends ActiveRecord implements IdentityInterface
{
    /**
     * Сценарий редактирования профиля
     */

    const SCENARIO_PROFILE = 'profile';
    /**
     * Сценарий регистрации
     */
    const SCENARIO_REGISTRATION = 'registration';

    /**
     * Путь для сохранения файлов (аватарок)
     */
    const AVATAR_PATH = '@files/users/avatars/';

    /**
     * URL директории с аватарками
     */
    const AVATAR_URL = '@urlFiles/users/avatars/';

    /**
     * URL к аватарке по умолчанию
     */
    const DEFAULT_AVATAR_URL = '@urlFiles/users/avatar.jpg';

    /**
     * Код с картинки
     * @var string
     */
    public $captcha;
    /**
     * Повторяем пароль
     * @var string
     */
    public $password_repeat;
    /**
     * Старый пароль
     * @var string
     */
    public $password_old;
    /**
     * Новый пароль
     * @var string
     */
    public $password_new;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sex = empty($this->sex) ? 'm' : $this->sex;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'email'],
            [['email', 'name', 'password', 'captcha'], 'required',
                'on' => self::SCENARIO_REGISTRATION],
            ['name', 'string', 'min' => 3, 'max' => 50,
                'tooShort' => Yii::t("common", "Minimum length {num} characters", ['num' => 3]),
                'tooLong' => Yii::t("common", "Maximum length {num} characters", ['num' => 50]),
                'on' => self::SCENARIO_REGISTRATION],
            ['password', 'string', 'min' => 6, 'max' => 64,
                'tooShort' => Yii::t("common", "Minimum length {num} characters", ['num' => 6]),
                'tooLong' => Yii::t("common", "Maximum length {num} characters", ['num' => 64]),
                'on' => self::SCENARIO_REGISTRATION],
            ['captcha', 'captcha', 'captchaAction' => 'users/guest/captcha',
                'on' => self::SCENARIO_REGISTRATION],
            [
                'password_repeat',
                'compare',
                'compareAttribute' => 'password',
                'message' => Yii::t("common", "Passwords do not match"),
                'on' => self::SCENARIO_REGISTRATION
            ],
            [
                'email',
                'unique',
                'targetAttribute' => 'email',
                'message' => Yii::t("common", "This email is already in use")
            ],
            ['sex', 'in', 'range' => ['m', 'w'], 'message' => Yii::t("common", "Specify your gender")],
            ['avatar', 'file', 'mimeTypes' => ['image/png', 'image/gif', 'image/jpeg']],
            ['password_old', 'isOldPassword', 'on' => self::SCENARIO_PROFILE],
            ['password_new', 'string', 'min' => 6, 'max' => 64,
                'tooShort' => Yii::t("common", "Minimum length {num} characters", ['num' => 6]),
                'tooLong' => Yii::t("common", "Maximum length {num} characters", ['num' => 64]),
                'on' => self::SCENARIO_PROFILE,
            ],
            ['password_repeat', 'compare', 'compareAttribute' => 'password_new',
                'skipOnEmpty' => false, 'message' => Yii::t("common", "Passwords do not match"),
                'on' => self::SCENARIO_PROFILE,
                'when' => function ($model) {
                    return empty($model->password_new) === false;
                }
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => Yii::t("common", "Email"),
            'password' => Yii::t("common", "Password"),
            'name' => Yii::t("common", "Name"),
            'sex' => Yii::t("common", "Sex"),
            'time_reg' => Yii::t("common", "Registration date"),
            'role' => Yii::t("common", "Role"),
            'locale' => Yii::t("common", "System language"),
            'password_old' => Yii::t("common", "Old password"),
            'password_new' => Yii::t("common", "New password"),
            'password_repeat' => Yii::t("common", "Password repeat"),
            'avatar' => Yii::t("common", "Picture"),
            'captcha' => Yii::t("common", "Code from the picture"),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->scenario == self::SCENARIO_PROFILE) {
            $this->uploadAvatar();
        }

        if ($this->isNewRecord) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
            $this->time_reg = Time::real();
        } elseif ($this->password_new && $this->password_old) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password_new);
        }

        $this->name = Html::encode($this->name);
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return parent::scenarios();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new \yii\base\NotSupportedException('findIdentityByAccessToken is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Проверяем старый пароль
     * @param $attribute
     * @param $params
     */
    public function isOldPassword($attribute, $params)
    {
        if (!Yii::$app->security->validatePassword($this->{$attribute}, Yii::$app->user->identity->password)) {
            $this->addError($attribute, Yii::t("common", "Invalid old password"));
        }
    }

    /**
     * Полный путь к аватарке
     * @param bool $small Уменьшиная копия
     * @param bool $exists Делать проверку на наличие файла или нет
     * @return string
     */
    public function getPathAvatar($small = false, $exists = true)
    {
        $path = Yii::getAlias(self::AVATAR_PATH) .
            ($small ? 'small_' : '') .
            ($this->avatar ? $this->avatar : $this->oldAttributes['avatar']);
        return is_file($path) || $exists === false ? $path : null;
    }

    /**
     * URL к аватарке
     * @param bool $small Уменьшиная копия
     * @return string
     */
    public function getUrlAvatar($small = false)
    {
        if (is_file($this->getPathAvatar($small))) {
            return Yii::getAlias(self::AVATAR_URL) . ($small ? 'small_' : '') . $this->avatar;
        }
        return Yii::getAlias(self::DEFAULT_AVATAR_URL);
    }

    /**
     * Удаление аватара
     */
    public function deleteAvatar($save = false)
    {
        if (is_file($this->getPathAvatar())) {
            unlink($this->getPathAvatar());
        }
        if (is_file($this->getPathAvatar(true))) {
            unlink($this->getPathAvatar(true));
        }
        $this->avatar = null;
        if ($save) {
            $this->save(0);
        }
    }

    /**
     * Загружаем аватар
     * @throws \Exception
     */
    private function uploadAvatar()
    {
        $avatar = UploadedFile::getInstance($this, 'avatar');
        if ($avatar !== null) {
            $this->deleteAvatar();
            $this->avatar = uniqid() . '.jpg';
            if ($avatar->saveAs($this->getPathAvatar(false, false))) {
                $small = new SimpleImage($this->getPathAvatar());
                $small->thumbnail(120, 150);
                $small->save($this->getPathAvatar(true, false), 100, $avatar->extension);
            }
        }
    }
}
