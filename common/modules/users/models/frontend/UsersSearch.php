<?php

namespace modules\users\models\frontend;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\users\models\frontend\Users;

/**
 * Class UsersSearch
 * @package modules\users\models\frontend
 *
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $auth_key
 * @property string $time_reg
 * @property string $role
 * @property string $sex
 * @property string $error_auth
 * @property string $avatar
 */
class UsersSearch extends Users
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['sex', 'in', 'range' => ['m', 'w'], 'message' => Yii::t("common", "Specify gender")],
            [['name', 'avatar'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Создаем критерии поиска
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->avatar) {
            $query->andFilterWhere(['!=', 'avatar', '0']);
        }

        $query->andFilterWhere([
            'id' => $this->id
        ]);

        $query->andFilterWhere([
            'sex' => $this->sex
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
