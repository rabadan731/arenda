<?php

namespace modules\users\models\frontend;

use Yii;
use modules\users\models\backend\LoginForm as Model;

/**
 * Class LoginForm
 * @package modules\users\models\frontend
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $auth_key
 * @property string $time_reg
 * @property string $role
 * @property string $sex
 * @property integer $error_auth
 * @property string $avatar
 */
class LoginForm extends Model
{
    /**
     * @inheritdoc
     */
    public function userValidate()
    {
        $auth = Yii::$app->authManager;
        $this->user = Users::find()->where(['email' => $this->email])->one();

        if ($this->user === null) {
            $this->addError('email', Yii::t("common", "The user is not found"));
        } elseif (!Yii::$app->security->validatePassword($this->password, $this->user->password)) {
            $this->user->error_auth++;
            $this->user->save();
            $this->password = null;
            $this->addError('password', Yii::t("common", "Wrong password"));
        }
    }
}
