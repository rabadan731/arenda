<?php

use yii\db\Migration;
use common\helpers\Time;

class m150515_130844_create_table_users extends Migration
{
    /**
     * @var string
     */
    public $table = '{{%users}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id'            => $this->primaryKey(),
            'email'         => $this->string(250)->notNull(),
            'password'      => $this->string(250)->notNull(),
            'name'          => $this->string(250)->notNull(),
            'avatar'        => $this->string(250),
            'phone'         => $this->string(250),
            'auth_key'      => $this->string(250),
            'time_reg'      => $this->integer(),
            'role'          => $this->string(50),
            'sex'           => $this->string(1),
            'balance'       => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'error_auth'    => $this->integer(3),
            'google_token'  => $this->text(),
            'locale'        => $this->string(32)->notNull(),
            'agency_id'     => $this->integer()->defaultValue(0),
        ]);

        $this->createAdmin();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }

    /**
     * Добавляем администратора
     * Данные для входа email: admin@site.com password: admin
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function createAdmin()
    {
        $this->insert($this->table, [
            'email' => 'admin@site.com',
            'password' => Yii::$app->security->generatePasswordHash('admin'),
            'name' => 'Администратор',
            'phone' => '112',
            'time_reg' => Time::real(),
            'role' => 'admin',
            'locale' => Yii::$app->language,
            'sex' => 'm',
        ]);

        Yii::$app->authManager->removeAllAssignments();

        $adminRole = Yii::$app->authManager->getRole('admin');
        Yii::$app->authManager->assign($adminRole, 1);
    }
}
