<?php

namespace modules\users\components;

use yii;
use modules\users\models\UsersLogin;
use modules\users\models\backend\Users;

/**
 * Class User
 * @package modules\users\components
 */
class User extends yii\web\User
{
    /**
     * @inheritdoc
     */
    public function beforeLogin($identity, $cookieBased, $duration)
    {
        return parent::beforeLogin($identity, $cookieBased, $duration);
    }

    /**
     * @inheritdoc
     */
    public function afterLogin($identity, $cookieBased, $duration)
    {
        /* @var $identity Users */
        $model = new UsersLogin();
        $model->user_id = $identity->id;
        $model->ip = Yii::$app->request->userIP;
        $model->ua = Yii::$app->request->userAgent;
        $model->created_at = time();
        $model->save();

        parent::afterLogin($identity, $cookieBased, $duration);
    }
}
