<?php

/* @var $this yii\web\View */
/* @var $model \modules\users\models\backend\Users */

$this->title = Yii::t("common", "Users");
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'users';
$this->params['place'] = 'users';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t("common", "Add");

$this->render('_form', [
    'model' => $model,
])
?>

<div class="jarviswidget">
    <div>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>