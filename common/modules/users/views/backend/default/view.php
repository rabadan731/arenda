<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\widgets\DetailView;
use modules\log\models\Log;

/* @var $this yii\web\View */
/* @var $model \modules\users\models\backend\Users */
/* @var $logDataProvider \yii\data\ActiveDataProvider */
/* @var $loginDataProvider \yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Users');
$this->params['pageIcon'] = 'users';
$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'users';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>

<div class="jarviswidget">
    <div>
        <?php if (Yii::$app->user->can('users_crud') || Yii::$app->user->can('user', ['id' => $model->id])) : ?>
            <p>
                <?= Html::a(Yii::t('common', 'Change'), ['update', 'id' => $model->id], [
                    'class' => 'btn btn-warning'
                ]) ?>
                <?php if (Yii::$app->user->can('users_crud')) : ?>
                    <?= Html::a(Yii::t("common", "Delete"), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('common', 'Are you sure you want to remove this user?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                <?php endif; ?>
            </p>
        <?php endif; ?>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'email',
                [
                    'attribute' => 'sex',
                    'value' => $model->sex === 'w' ? Yii::t('common', 'Female') : Yii::t('common', 'Male'),
                ],
                'time_reg:datetime',
            ],
        ]) ?>

        <?php /*
        <h2><?= Yii::t("common", "Google calendar"); ?></h2>
        <?php if (empty($model->google_token)) {

        } else {

        } */
        ?>

        <h2><?= Yii::t("common", "Action"); ?></h2>
        <?= GridView::widget([
            'dataProvider' => $logDataProvider,
            'columns' => [
                'description',
                [
                    'attribute' => 'type',
                    'format' => 'html',
                    'filter' => (new Log())->valueTypes(),
                    'value' => function ($model) {
                        /* @var $model Log */
                        $title = isset($model->valueTypes()[$model->type]) ? $model->valueTypes()[$model->type] :
                            Yii::t("common", "Unknown");

                        switch ($model->type) {
                            case Log::TYPE_INSERT:
                                return '<span class="btn btn-success btn-xs btn-block">' . $title . '</span>';
                                break;
                            case Log::TYPE_UPDATE:
                                return '<span class="btn btn-primary btn-xs btn-block">' . $title . '</span>';
                                break;
                            case Log::TYPE_DELETE:
                                return '<span class="btn btn-danger btn-xs btn-block">' . $title . '</span>';
                                break;
                            default:
                                return '<span class="btn btn-xs btn-block">' . $title . '</span>';
                                break;
                        }
                    },
                ],
                'created_at:datetime',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '<div class="text-center">{view}</div>',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            $url = Url::to(['/log/default/view', 'id' => $model->id]);
                            return Html::a('<i class="fa fa-eye"></i>', $url, [
                                'class' => 'btn btn-xs btn-info',
                                'title' => Yii::t("common", "View"),
                                'data-pjax' => 0,
                            ]);
                        },
                    ]
                ],
            ],
        ]); ?>

        <h2><?= Yii::t("common", "Authorization"); ?></h2>
        <?= GridView::widget([
            'dataProvider' => $loginDataProvider,
            'columns' => [
                'ip',
                'ua',
                'created_at:datetime'
            ],
        ]); ?>
    </div>
</div>