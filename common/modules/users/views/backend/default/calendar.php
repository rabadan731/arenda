<?php
/**
 * Created by PhpStorm.
 * User: rabadan731
 * Date: 15.06.2016
 * Time: 15:24
 */


$optParams = array(
    'maxResults' => 10,
    'orderBy' => 'startTime',
    'singleEvents' => true,
    'timeMin' => date('c'),
);
$results = $service->events->listEvents('primary', $optParams);

echo '<div class="jarviswidget"><pre>';
print_r($results->items);
echo '</pre></div>';
