<?php
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t("common", "Users");
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'users';
$this->params['place'] = 'users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jarviswidget">

    <div>
        <?php if (Yii::$app->user->can('users_crud')) : ?>
            <p>
                <?= Html::a(Yii::t("common", "Add"), ['create'], ['class' => 'btn btn-primary']) ?>
            </p>
        <?php endif; ?>

        <?php Pjax::begin() ?>

        <?= $this->render('_view', [
            'dataProvider' => $dataProvider
        ]) ?>

        <?php Pjax::end() ?>
    </div>

</div>
