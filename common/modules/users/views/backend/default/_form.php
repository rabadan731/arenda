<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\modules\agency\models\Agency;

/* @var $this yii\web\View */
/* @var $model \modules\users\models\backend\Users */
/* @var $form yii\widgets\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<div class="jarviswidget">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
    </div>



    <?php if (Yii::$app->user->can('users_crud')) : ?>
        <div class="form-group">
            <?= $form->field($model, 'email')->textInput(['maxlength' => 250]) ?>
        </div>
    <?php endif; ?>

    <?php if ($model->id === Yii::$app->user->getId()) : ?>
        <div class="form-group">
            <?= $form->field($model, 'old_password')->passwordInput(['maxlength' => 50]) ?>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <?= $form->field($model, $model->isNewRecord ? 'password' : 'new_password')
            ->passwordInput(['maxlength' => 50]) ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'new_password_repeat')->passwordInput(['maxlength' => 50]) ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'sex')->dropDownList([
            'm' => Yii::t("common", "Male"),
            'w' => Yii::t("common", "Female"),
        ]); ?>
    </div>
    <div class="form-group">
        <?php echo $form->field($model, 'locale')->dropDownlist(Yii::$app->params['availableLocales']) ?>
    </div>


    <?php if (Yii::$app->user->can('users_crud')) : ?>
        <div class="form-group">
            <?= $form->field($model, 'role')->dropDownList($model->getRoles()); ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->user->can('users_crud')) : ?>
        <div class="form-group">
            <?= $form->field($model, 'agency_id')->dropDownList(Agency::getAgencies()); ?>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t("common", "Save"), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
