<?php

/* @var $this yii\web\View */
/* @var $model \modules\users\models\backend\Users */

$this->title = Yii::t("common", "Users");
$this->params['pageIcon'] = 'users';
$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'users';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t("common", "Change");
?>

<div class="jarviswidget">
    <div>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>