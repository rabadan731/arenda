<?php

use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \modules\users\models\backend\LoginForm; */

$this->title = Yii::t("common", "Authorization");
?>

<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => [
        'class' => 'smart-form client-form'
    ]
]); ?>
    <header>
        <?= Yii::t('common', 'Authorization'); ?>
    </header>

    <fieldset>

        <section>
            <?= $form->beginField($model, 'email') ?>
            <?= Html::activeLabel($model, 'email') ?>
            <label class="input">
                <i class="icon-append fa fa-envelope-o"></i>
                <?= Html::activeTextInput($model, 'email') ?>
                <b class="tooltip tooltip-top-right">
                    <i class="fa fa-envelope-o txt-color-teal"></i>
                    <?= Yii::t('common', 'Enter your email'); ?>
                </b>
            </label>
            <?= Html::error($model, 'email', ['class' => 'help-block help-block-error']) ?>
            <?= $form->endField() ?>
        </section>

        <section>
            <?= $form->beginField($model, 'password') ?>
            <?= Html::activeLabel($model, 'password') ?>
            <label class="input">
                <i class="icon-append fa fa-lock"></i>
                <?= Html::activePasswordInput($model, 'password') ?>
                <b class="tooltip tooltip-top-right">
                    <i class="fa fa-lock txt-color-teal"></i> <?= Yii::t('common', 'Enter your password'); ?></b>
            </label>
            <?= Html::error($model, 'password', ['class' => 'help-block help-block-error']) ?>
            <?= $form->endField() ?>
        </section>

        <?php if (isset($model->user->error_auth) && $model->user->error_auth >= 3) : ?>
            <section>
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'captchaAction' => 'guest/captcha',
                    'template' => '<div class="form-group input-group">
                                        {input}
                                        <span class="input-group-addon">{image}</span>
                                    </div>',
                ]) ?>
            </section>
        <?php endif; ?>
    </fieldset>
    <footer>
        <?= Html::submitButton(Yii::t("common", "Sign me in"), [
            'class' => 'btn btn-primary',
            'name' => 'login-button'
        ]) ?>
    </footer>
<?php ActiveForm::end(); ?>