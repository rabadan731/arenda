<?php

use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel modules\users\models\frontend\UsersSearch */

$this->title = Yii::t("common", "Users");
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="col-lg-10">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            [
                'attribute' => 'sex',
                'value' => function ($model, $key, $index, $column) {
                    return $model['sex'] === 'w' ? Yii::t("common", "Female") : Yii::t("common", "Male");
                },
            ],
            'time_reg:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<div class="text-center">{view}</div>'
            ]
        ],
    ]) ?>
</div>

<div class="col-lg-2 panel panel-info panel-heading">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($searchModel, 'name') ?>

    <?= $form->field($searchModel, 'sex')->radioList([
        'm' => Yii::t("common", "Male"),
        'w' => Yii::t("common", "Female"),
    ]) ?>

    <?= $form->field($searchModel, 'avatar')->checkboxList([
        '1' => Yii::t("common", "Only with the avatar")
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t("common", "Search"), ['class' => 'btn btn-primary col-lg-12 col-xs-12']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>