<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \modules\users\models\frontend\Users */

$this->title = $model->name;
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t("common", "Users"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>

<div class="col-lg-12">
    <div class="pull-left">
        <a href="<?= $model->getUrlAvatar() ?>">
            <img src="<?= $model->getUrlAvatar() ?>" alt="<?= $model->name ?>"/>
        </a>
    </div>
    <div class="col-lg-12">
        <div>
            <strong>
                <?= Yii::t("common", "Login"); ?>
                :
            </strong>
            <?= $model->name ?>
        </div>
        <div>
            <strong>
                <?= Yii::t("common", "Sex"); ?>
                :
            </strong>
            <?= $model->sex == 'w' ? Yii::t("common", "Female") : Yii::t("common", "Male") ?>
        </div>
        <div>
            <strong>
                <?= Yii::t("common", "Registration date"); ?>
                :
            </strong>
            <?= Yii::$app->formatter->asDatetime($model->time_reg) ?>
        </div>
    </div>
</div>