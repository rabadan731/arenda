<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \modules\users\models\frontend\LoginForm; */

$this->title = Yii::t("common", "Authorization");
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-4">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'email') ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?php if (isset($model->user->error_auth) && $model->user->error_auth >= 3) : ?>
            <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                'captchaAction' => 'guest/captcha',
                'template' => '<div class="form-group input-group">
                                   {input}
                                   <span class="input-group-addon">{image}</span>
                               </div>',
            ]) ?>
        <?php endif; ?>

        <?= $form->field($model, 'rememberMe')->checkbox([1 => Yii::t("common", "Remember Me")]) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t("common", "Sign me in"), [
                'class' => 'btn btn-primary',
                'name' => 'login-button'
            ]) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>