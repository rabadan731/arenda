<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $model \modules\users\models\frontend\Users */

$this->title = Yii::t("common", "Signup");
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row col-lg-5">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'email')->textInput(['maxlength' => 50]) ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'password')->passwordInput(['maxlength' => 64]) ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'password_repeat')->passwordInput(['maxlength' => 64]) ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'captcha')->widget(Captcha::className(), [
            'captchaAction' => '/users/guest/captcha',
            'template' => '
                <div class="form-group input-group input-group-lg">
                    {input}
                    <span class="input-group-addon">{image}</span>
                </div>',
        ]) ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'sex')->dropDownList([
            'm' => Yii::t("common", "Male"),
            'w' => Yii::t("common", "Female"),
        ]); ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t("common", "Save"), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>