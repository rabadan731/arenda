<?php

namespace modules\users\controllers\backend;

use common\components\GoogleApiClient;
use modules\log\models\Log;
use yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use modules\log\models\LogSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use modules\users\models\UsersLogin;
use modules\users\models\backend\Users;
use modules\users\models\backend\UsersSearch;

class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'gcalendar'],
                        'roles' => ['backend_access']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['users_crud'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('user', ['id' => Yii::$app->request->get('id')]);
                        }
                    ]
                ],
            ]
        ]);
    }

    /**
     * Выводим всех пользователей
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        if (Yii::$app->user->can('admin')) {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        } else {
            $dataProvider = $searchModel->searchAgency(
                Yii::$app->user->identity->agency_id,
                Yii::$app->request->queryParams
            );
        }


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Выводим всех пользователей
     * @return mixed
     */
    public function actionGcalendar($code = null)
    {
        $thisUrl = yii\helpers\Url::to([]);
        $user = $this->findModel(Yii::$app->user->id);
        $client = Yii::$app->googleApi->getClient();
        /** @var \Google_Client $client */

        //Если у клиента не установлен токен
        if (empty($user->google_token)) {
            $client->setRedirectUri((Yii::$app->request->hostInfo) . $thisUrl);
            if (is_null($code)) {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();

                return $this->renderContent(yii\helpers\Html::a('Google', $authUrl));
            } else {
                $accessToken = $client->authenticate($code);
                $user->google_token = $accessToken;
                $user->save(false, ['google_token']);
                $this->redirect($thisUrl);
            }
            //Если у клиента установлен токен
        } else {
            $client->setAccessToken($user->google_token);

            //Если токен устарел, обновляем
            if ($client->isAccessTokenExpired()) {
                $client->refreshToken($client->getRefreshToken());
                $user->google_token = $client->getAccessToken();
                $user->save(false, ['google_token']);
            }
        }

        $service = new \Google_Service_Calendar($client);

        return $this->render("calendar", ['service' => $service]);
    }

    /**
     * Выводим информацию определёного юзера
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $logDataProvider = new ActiveDataProvider([
            'query' => Log::find()->where(['user_id' => $id])->orderBy('id DESC')
        ]);
        $loginDataProvider = new ActiveDataProvider([
            'query' => UsersLogin::find()->where(['user_id' => $id])->orderBy('id DESC')
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'logDataProvider' => $logDataProvider,
            'loginDataProvider' => $loginDataProvider
        ]);
    }

    /**
     * Создание пользователя
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();
        $model->scenario = $model::SCENARIO_PROFILE;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Изменение пользователя
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = $model::SCENARIO_PROFILE;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t("common", "Data saved successfully"));
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Удаление пользователя
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Поиск пользователя, если не найден возвращаем 404 ошибку
     * @param integer $id
     * @return \modules\users\models\backend\Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (Yii::$app->user->id == $id) {
            return Yii::$app->user->identity;
        }

        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
