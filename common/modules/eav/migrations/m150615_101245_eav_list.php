<?php

use yii\db\Schema;
use yii\db\Migration;

class m150615_101245_eav_list extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%eav_param_options}}', [
            'option_id'      => $this->primaryKey(),
            'param_id'       => $this->integer()->notNull(),
            'title'          => $this->string(256)->notNull(),
        ], $tableOptions);


    }

    public function safeDown()
    {
        $this->dropTable('{{%eav_param_options}}');
    }

}
