<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\eav\models\EavParamValue */
?>
<div class="eav-param-value-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'object_table',
            'object_id',
            'param_id',
            'param_type',
            'value',
            'dimension',
        ],
    ]) ?>

</div>
