<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\modules\eav\models\EavParamOptions;

/** @var $paramid int */

/**
 * Created by PhpStorm.
 * User: rabadan
 * Date: 25.04.2017
 * Time: 14:30
 */

$data = new \yii\data\ArrayDataProvider([
    'allModels' => EavParamOptions::find()
        ->andWhere(['param_id'=>$paramid])
        ->all(),
    'pagination' => false
]);

echo \yii\grid\GridView::widget([
    'id'=>'crud-datatable-list',
    'dataProvider' => $data,
    'columns' => [
        'option_id',
        'title',
        [
            'label'=>'action',
            'format' => 'raw',
            'value'=>function ($data) {
                return Html::button(Yii::t("common", "Delete"), [
                    'id' => 'delete-list-param-'.$data->option_id,
                    'class' => 'delete-list-param',
                    'data' => [
                        'option_id' => 'delete-list-param-'.$data->option_id,
                        'url' => Url::to(['delete-list-param', 'id' => $data->option_id]),
                    ],
                ]);
            },
        ],
    ]
]);
