<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\modules\eav\models\EavParam;

/* @var $this yii\web\View */
/* @var $model common\modules\eav\models\EavParam */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="eav-param-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'param_type')->dropDownList(EavParam::getTypes(), [
        'prompt'=>Yii::t("common","-- Select --")
    ]); ?>

    <?= $form->field($model, 'dimension')->textInput(['maxlength' => true]) ?>

    <?php if ($model->param_type == $model::EAV_TYPE_LIST) { ?>
        <hr />
        <h4>Список параметров</h4>
        <div>
            <div id="param_list">
                <?= $this->render('_param_list', ['paramid' => $model->id]); ?>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <?= Html::textInput('newParamList', null, [
                        'class'=> 'form-control',
                        'id' => 'create-param-input'
                    ]); ?>
                </div>
                <div class="col-md-4">
                    <?= Html::button(Yii::t("common", "Add"), [
                        'class'=> 'btn btn-default create-param-button',
                        'data' => [
                            'paramid' => $model->id,
                            'url' => Url::to(['create-list-param'])
                        ]
                    ]); ?>
                </div>
            </div>
        </div>
    <?php } ?>


    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
