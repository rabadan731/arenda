<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\eav\models\EavParam */
?>
<div class="eav-param-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'category',
            'paramTypeValue',
            'dimension',
        ],
    ]) ?>

</div>
