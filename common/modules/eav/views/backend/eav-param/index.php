<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\eav\models\search\EavParamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $searchListModel common\modules\eav\models\search\EavListSearch */
/* @var $dataListProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('common', 'Parameters of object');
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'eav';
$this->params['place'] = 'eav';

//$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

<?php $js = <<< JS
    $('body').on('click', '.create-param-button', function () {
        
        $.ajax({
           url: $(this).data('url'),
           data: { paramid: $(this).data('paramid'), title: $("#create-param-input").val() },
           type: 'post',
           cache: false,         
           success: function (response) {
              $("#param_list").html(response);
              $("#create-param-input").val("");
           }
        });
    });

    $('body').on('click', '.delete-list-param', function () {
        parent = $(this).parent().parent();
        if (confirm("Delete?")) {
            $.ajax({
               url: $(this).data('url'),
               type: 'post',
               cache: false,         
               success: function (response) {
                   if (response.success) {
                       parent.remove();  
                   } else {
                       alert('ERROR DELETE');
                   }
               }
            });
        }         
    });



    
JS;
$this->registerJs($js);
?>

<div class="eav-param-index padding-10">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                    ['role'=>'modal-remote','title'=> 'Create new Eav Params','class'=>'btn btn-default']).
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid'])
                ],
            ],          
            'striped' => true,
            'condensed' => true,
            'responsive' => true,          
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="glyphicon glyphicon-list"></i> ' . Yii::t('common', 'Parameters of object'),
                'after' => '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>

<div class="eav-param-index padding-10">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable-list',
            'dataProvider' => $dataListProvider,
            'filterModel' => $searchListModel,
            'columns' => require(__DIR__.'/../eav-list/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/eav/eav-list/create'],
                        ['data-pjax'=>1, 'role'=>'modal-remote','title'=> 'Create new Eav Params','class'=>'btn btn-default']).
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid'])
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="glyphicon glyphicon-list"></i> ' . Yii::t('common', 'Kit parameters'),
                'after' => '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

