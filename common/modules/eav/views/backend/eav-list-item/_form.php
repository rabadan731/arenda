<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\eav\models\EavListItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="eav-list-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'list_id')->textInput() ?>

    <?= $form->field($model, 'param_id')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
