<?php

namespace common\modules\eav\models;

use Yii;
use common\modules\eav\models\query\EavParamQuery;
use kartik\touchspin\TouchSpin;
use yii\helpers\Html;

/**
 * This is the model class for table "eav_param".
 *
 * @property integer $id
 * @property string $title
 * @property string $category
 * @property integer $param_type
 * @property string $dimension
 *
 * @property EavParamValue[] $eavParamValues
 */

class EavParam extends \yii\db\ActiveRecord
{
    const EAV_TYPE_STRING = 0;
    const EAV_TYPE_INTEGER = 2;
    const EAV_TYPE_BOOLEAN = 4;
    const EAV_TYPE_LIST = 6;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eav_param';
    }

    /**
     * @param null $type
     * @return array|mixed|null
     */
    public static function getTypes($type = null)
    {
        $types = [
            self::EAV_TYPE_BOOLEAN => Yii::t("common", "Boolean"),
            self::EAV_TYPE_INTEGER => Yii::t("common", "Number"),
            self::EAV_TYPE_STRING => Yii::t("common", "String"),
            self::EAV_TYPE_LIST => Yii::t("common", "List"),
        ];

        if (is_null($type)) {
            return $types;
        } else {
            if (array_key_exists($type, $types)) {
                return $types[$type];
            }
            return $type;
        }
    }

    /**
     * @return array|mixed|null
     */
    public function getParamTypeValue()
    {
        return $this->getTypes($this->param_type);
    }

    /**
     * @param null $paramValue
     * @return string
     */
    public function getValueForm($paramValue = null)
    {
        $paramValue = is_null($paramValue)?(new EavParamValue()):$paramValue;
        $attrName = is_null($paramValue) ?
            Html::getInputName($paramValue, 'value') :
            "EavParamValue[{$paramValue->id}][value]";

        switch ($this->param_type) {
            case EavParam::EAV_TYPE_BOOLEAN :
                return Html::checkbox($attrName, $paramValue->value);
                break;
            case EavParam::EAV_TYPE_INTEGER :
                return TouchSpin::widget([
                    'name' => $attrName,
                    'value'=> $paramValue->value
                ]);break;
            case EavParam::EAV_TYPE_LIST :
                return Html::dropDownList(
                    $attrName,
                    $paramValue->value,
                    EavParamOptions::getListOnParam($this->id)
                );break;
            default : return Html::textInput($attrName, $paramValue->value);
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'category', 'param_type'], 'required'],
            [['param_type'], 'integer'],
            [['title', 'category', 'dimension'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'title' => Yii::t('common', 'Title'),
            'category' => Yii::t('common', 'Category'),
            'param_type' => Yii::t('common', 'Parameter type'),
            'paramTypeValue' => Yii::t('common', 'Parameter type'),
            'dimension' => Yii::t('common', 'Dimension'),
        ];
    }

    /**
     * @param array $notList
     * @return array
     */
    public static function getListDropdown($notList = [])
    {
        $params = self::find()->andWhere(['not in', 'id', $notList])->orderBy('category, title')->all();
        $result = [];
        foreach ($params as $param) {
            $result[$param['category']][$param['id']] = $param['title'];
            if (!empty($param['dimension'])) {
                $result[$param['category']][$param['id']] .= " (" . ($param['dimension']) . ")";
            }
        }
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEavParamValues()
    {
        return $this->hasMany(EavParamValue::className(), ['param_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return EavParamQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EavParamQuery(get_called_class());
    }


    /*************************************** *************************************************/
    /**
     * @return bool
     */
    public function beforeDelete()
    {
        foreach ($this->getEavParamValues()->all() as $eavParam) {
            $eavParam->delete();
        }
        return parent::beforeDelete();
    }
    /******************************************** **********************************************/
}
