<?php

namespace common\modules\eav\models;

use Yii;
use common\modules\eav\models\query\EavParamOptionsQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "eav_param_options".
 *
 * @property integer $option_id
 * @property integer $param_id
 * @property string $title
 */
class EavParamOptions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eav_param_options';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['param_id', 'title'], 'required'],
            [['param_id'], 'integer'],
            [['title'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id' => Yii::t('common', 'Option ID'),
            'param_id' => Yii::t('common', 'Param ID'),
            'title' => Yii::t('common', 'Title'),
        ];
    }

    /**
     * @inheritdoc
     * @return EavParamOptionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EavParamOptionsQuery(get_called_class());
    }

    /**
     * @param $paramID
     * @return array|EavParamOptions[]
     */
    public static function getListOnParam($paramID)
    {
        $find = self::find()->asArray();
        $find->andWhere(['param_id'=>$paramID]);
        $result = $find->all();
        if (count($result)) {
            $result = ArrayHelper::map($result, 'option_id', 'title');
        }

        return $result;
    }
}
