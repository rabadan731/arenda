<?php

namespace common\modules\eav\models;

use Yii;

/**
 * This is the model class for table "eav_param_value".
 *
 * @property integer $id
 * @property string $object_table
 * @property integer $object_id
 * @property integer $param_id
 * @property integer $param_type
 * @property string $value
 * @property string $dimension
 *
 * @property EavParam $param
 */
class EavParamValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eav_param_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_table', 'object_id', 'param_id', 'value'], 'required'],
            [['object_id', 'param_id', 'param_type'], 'integer'],
            [['object_table', 'value', 'dimension'], 'string', 'max' => 256],
            [
                ['param_id'], 
                'exist', 
                'skipOnError' => true, 
                'targetClass' => EavParam::className(), 
                'targetAttribute' => ['param_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'object_table' => Yii::t('common', 'Object Table'),
            'object_id' => Yii::t('common', 'Object ID'),
            'param_id' => Yii::t('common', 'Param ID'),
            'param_type' => Yii::t('common', 'Param Type'),
            'value' => Yii::t('common', 'Value'),
            'dimension' => Yii::t('common', 'Dimension'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParam()
    {
        return $this->hasOne(EavParam::className(), ['id' => 'param_id']);
    }


    //обновление значений, при добавление параметров
    public static function updateList($post)
    {
        foreach ($post as $id => $data) {
            $model = self::findOne($id);
            if ($model !== null) {
                switch ($model->param->param_type) {
                    case EavParam::EAV_TYPE_BOOLEAN :
                        $defaultValue = '0';
                        break;
                    case EavParam::EAV_TYPE_INTEGER :
                        $defaultValue = '0';
                        break;
                    default : $defaultValue = '-';
                }
                $model->dimension = isset($data['dimension'])?$data['dimension']:"";
                $model->value = isset($data['value'])?$data['value']:$defaultValue;
                if (!$model->save()) {
                    echo '<pre>'.print_r($model->errors, 1).'</pre>';die;
                }
            }
        }
    }

    /**
     * @inheritdoc
     * @return \common\modules\eav\models\query\EavParamValueQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\eav\models\query\EavParamValueQuery(get_called_class());
    }
}
