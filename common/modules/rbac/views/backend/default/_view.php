<?php
use yii\grid\GridView;

$columns = [];

if (Yii::$app->user->can('permissions_crud')) {
    $columns[] = ['class' => 'yii\grid\CheckboxColumn'];
}

$columns[] = [
    'label' => Yii::t("common", "Alias"),
    'attribute' => 'name'
];
$columns[] = [
    'label' => Yii::t("common", "Description"),
    'attribute' => 'description'
];

if (Yii::$app->user->can('permissions_crud')) {
    $columns[] = [
        'class' => 'yii\grid\ActionColumn'
    ];
}

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns
]);
