<?php

/* @var $this yii\web\View */
/* @var $model \modules\rbac\models\PermissionsForm */

$this->title = Yii::t("common", "Permissions");
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'unlock-alt';
$this->params['place'] = 'rbac';
$this->params['breadcrumbs'][] = ['label' => Yii::t("common", "Permissions"), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t("common", "Add");
?>

<div class="jarviswidget">
    <div>
        <?= $this->render('_form', [
            'model' => $model
        ]) ?>
    </div>
</div>
