<?php

use yii\grid\GridView;

/* @var $dataProvider \yii\data\ActiveDataProvider */

$columns = [];

if (Yii::$app->user->can('rules_crud')) {
    $columns[] = ['class' => 'yii\grid\CheckboxColumn'];
}

$columns[] = [
    'attribute' => 'name',
    'label' => Yii::t("common", "Title"),
    'value' => 'name'
];

if (Yii::$app->user->can('rules_crud')) {
    $columns[] = [
        'class' => 'yii\grid\ActionColumn',
        'template' => '<div class="text-center">{delete}</div>'
    ];
}

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns,
]);
