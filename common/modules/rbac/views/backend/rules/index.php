<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t("common", "Access rules");
$this->params['pageIcon'] = 'male';
$this->params['place'] = 'rules';
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="jarviswidget">
    <div>
        <?= Html::beginForm(['delete-multiple']); ?>

        <?php if (Yii::$app->user->can('rules_crud')) : ?>
            <p>
                <?= Html::a(Yii::t("common", "Add"), ['create'], ['class' => 'btn btn-primary']) ?>
                <?= Html::submitButton(Yii::t("common", "Delete"), ['class' => 'btn btn-danger']) ?>
            </p>
        <?php endif; ?>

        <?= $this->render('_view', [
            'dataProvider' => $dataProvider
        ]) ?>

        <?= Html::endForm(); ?>
    </div>
</div>
