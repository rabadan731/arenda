<?php

use yii\grid\GridView;

/* @var $dataProvider \yii\data\ActiveDataProvider */

$columns = [];

if (Yii::$app->user->can('roles_crud')) {
    $columns[] = ['class' => 'yii\grid\CheckboxColumn'];
}

$columns[] = [
    'attribute' => 'name',
    'label' => Yii::t("common", "Alias"),
    'value' => 'name'
];

$columns[] = [
    'attribute' => 'description',
    'label' => Yii::t("common", "Description"),
    'value' => 'description'
];

if (Yii::$app->user->can('roles_crud')) {
    $columns[] = ['class' => 'yii\grid\ActionColumn'];
}

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns,
]);
