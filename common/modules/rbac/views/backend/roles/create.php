<?php

/* @var $this yii\web\View */
/* @var $model \modules\rbac\models\RolesForm */

$this->title = Yii::t("common", "User roles");
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'puzzle-piece';
$this->params['place'] = 'roles';
$this->params['breadcrumbs'][] = ['label' => Yii::t("common", "User roles"), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t("common", "Add");
?>

<div class="jarviswidget">
    <div>
        <?= $this->render('_form', [
            'model' => $model
        ]) ?>
    </div>

    