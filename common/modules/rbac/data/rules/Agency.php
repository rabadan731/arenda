<?php

namespace modules\rbac\data\rules;

use Yii;
use yii\rbac\Rule;

/**
 * Class Agency
 * @package modules\rbac\data\rules
 */
class Agency extends Rule
{
    /**
     * @var string
     */
    public $name = 'Agency';

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        if (Yii::$app->user->can('admin')) {
            return true;
        } else {
            return isset($params['id']) ? Yii::$app->user->identity->agency_id == $params['id'] : true;
        }
    }
}
