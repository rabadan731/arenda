<?php
return [
    'admin' => [
        'type' => 1,
        'description' => Yii::t("common", "Administrator"),
        'ruleName' => 'Author',
        'children' => [
            'permissions_crud',
            'permissions_view',
            'users_crud',
            'roles_crud',
            'roles_view',
            'rules_crud',
            'rules_view',
            'backend_access',
            'log_view',
            'user',
            'manager',
            'agency',
        ],
    ],
    'agency' => [
        'type' => 1,
        'description' => Yii::t('common', 'Agency'),
        'ruleName' => 'Agency',
        'children' => [
            'backend_access',
            'user',
            'manager',
            'log_view'
        ],
    ],
    'manager' => [
        'type' => 1,
        'description' => Yii::t('common', 'Manager'),
        'ruleName' => 'Agency',
        'children' => [
            'backend_access',
        ],
    ],
    'user' => [
        'type' => 1,
        'description' => Yii::t("common", "User"),
        'ruleName' => 'Author',
    ],
    'permissions_crud' => [
        'type' => 2,
        'description' => Yii::t("common", "CRUD access Rights"),
    ],
    'permissions_view' => [
        'type' => 2,
        'description' => Yii::t("common", "Viewing access rights"),
    ],
    'users_crud' => [
        'type' => 2,
        'description' => Yii::t("common", "CRUD Users"),
    ],
    'roles_crud' => [
        'type' => 2,
        'description' => Yii::t("common", "CRUD Roles"),
    ],
    'roles_view' => [
        'type' => 2,
        'description' => Yii::t("common", "Viewing roles"),
    ],
    'rules_crud' => [
        'type' => 2,
        'description' => Yii::t("common", "CRUD access rules"),
    ],
    'rules_view' => [
        'type' => 2,
        'description' => Yii::t("common", "The view access rules"),
    ],
    'backend_access' => [
        'type' => 2,
        'description' => Yii::t("common", "Access in admin area"),
    ],
    'log_view' => [
        'type' => 2,
//    'product_view' => [
//        'type' => 2,
//        'description' => 'Просмотр каталога товаров',
//    ],
//    'product_crud' => [
//        'type' => 2,
//        'description' => 'CRUD Каталога товаров',
//    ],
//    'category_crud' => [
//        'type' => 2,
//        'description' => 'CRUD Категорий',
//    ],
//    'import_view' => [
//        'type' => 2,
//        'description' => 'Просмотр импортируемых товаров',
//    ],
//    'import_create' => [
//        'type' => 2,
//        'description' => 'Импорт товаров',
//    ],
//    'moderation_view' => [
//        'type' => 2,
//        'description' => 'Просмотр товаров на модерации',
//    ],
//    'moderation_crud' => [
//        'type' => 2,
//        'description' => 'CRUD Модерации товаров',
//    ],
        'description' => Yii::t("common", "Viewing the actions of the administration"),
    ],
];
