<?php

use yii\db\Migration;

class m160519_104505_agency_init extends Migration
{

    /**
     * @var string
     */
    public $table = '{{%agency}}';
    public $table_tariff = '{{%agency_tariff}}';
    public $table_pay = '{{%agency_pay_log}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {

        $this->createTable($this->table_tariff, [
            'id'            => $this->primaryKey(),
            'title'         => $this->string(256)->notNull(),
            'month'         => $this->integer(3),
            'price'         => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'status'        => $this->integer(1),
            'delete'        => $this->integer(1)->defaultValue(0), //статус удален/не удален
        ]);

        $this->createTable($this->table, [
            'id'            => $this->primaryKey(),
            'title'         => $this->string(256)->notNull(),
            'logo'          => $this->string(512),
            'watermark'     => $this->string(512),
            'balance'       => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'status'        => $this->integer(1),
            'tariff_id'     => $this->integer(11),
            'date_begin'    => $this->integer(11),
            'date_end'      => $this->integer(11),
            'delete'        => $this->integer(1)->defaultValue(0), //статус удален/не удален
        ]);

        $this->createIndex('idx_agency_tariff_id', $this->table, 'tariff_id');
        $this->addForeignKey('fk_agency_tariff_items', $this->table, 'tariff_id', $this->table_tariff, 'id');

        $this->createTable($this->table_pay, [
            'id'            => $this->primaryKey(),
            'agency_id'     => $this->integer(11)->notNull(),
            'tariff_id'     => $this->integer(11)->notNull(),
            'date_pay'      => $this->integer(11),
            'price'         => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'delete'        => $this->integer(1)->defaultValue(0), //статус удален/не удален
        ]);

        $this->createIndex('idx_tariff_agency_id', $this->table_pay, 'agency_id');
        $this->addForeignKey('fk_tariff_agency_items', $this->table_pay, 'agency_id', $this->table, 'id', 'cascade', 'cascade');

        $this->createIndex('idx_tariff_tariff_id', $this->table_pay, 'tariff_id');
        $this->addForeignKey('fk_tariff_tariff_items', $this->table_pay, 'tariff_id', $this->table_tariff, 'id', 'cascade', 'cascade');

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_agency_tariff_items', $this->table);
        $this->dropForeignKey('fk_tariff_agency_items', $this->table_pay);
        $this->dropForeignKey('fk_tariff_tariff_items', $this->table_pay);

        $this->dropTable($this->table_pay);
        $this->dropTable($this->table_tariff);
        $this->dropTable($this->table);
    }

}
