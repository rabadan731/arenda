<?php

use yii\db\Migration;

class m160521_094840_workers extends Migration
{
    /**
     * @var string
     */
    public $table = '{{%workers}}';
    public $table_jobs = '{{%worker_jobs}}';
    public $table_payments = '{{%worker_payments}}';
    public $agency = '{{%agency}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {

        $this->createTable($this->table, [
            'id'            => $this->primaryKey(),
            'worker_type'   => $this->integer(3),
            'fio'           => $this->string(512)->notNull(),
            'photo'         => $this->string(512),
            'email'         => $this->string(256),
            'phone'         => $this->string(256),
            'balance'       => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'agency_id'     => $this->integer(11),
            'created_by'     => $this->integer(),
            'updated_by'     => $this->integer(),
            'status'        => $this->integer(1),
            'delete'        => $this->integer(1)->defaultValue(0), //статус удален/не удален
        ]);

        $this->createIndex('idx_workers_agency_id', $this->table, 'agency_id');
        $this->addForeignKey('fk_workers_agency_items', $this->table, 'agency_id', $this->agency, 'id', 'cascade', 'cascade');


        $this->createTable($this->table_jobs, [
            'id'            => $this->primaryKey(),
            'worker_id'     => $this->integer(11)->notNull(),
            'address_text'  => $this->string(512),
            'address_map'   => $this->string(512),
            'need_to'       => $this->text(), // что нужно сделать\
            'price'         => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'date_event'    => $this->integer(11), //дата события
            'date_sent'     => $this->integer(11), //дата отправки уведомления
            'status'        => $this->integer(1)->defaultValue(0), //статус заявки
            'delete'        => $this->integer(1)->defaultValue(0), //статус удален/не удален
        ]);

        $this->createIndex('idx_worker_jobs_worker_id', $this->table_jobs, 'worker_id');
        $this->addForeignKey('fk_worker_jobs_worker_items', $this->table_jobs, 'worker_id', $this->table, 'id', 'cascade', 'cascade');


        $this->createTable($this->table_payments, [
            'id'            => $this->primaryKey(),
            'worker_id'     => $this->integer(11)->notNull(),
            'title'         => $this->string(512)->notNull(),
            'date_invoice'  => $this->integer(11),
            'price'         => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'paid'          => $this->integer(1)->defaultValue(0),
            'for_rent_id'   => $this->integer(11),
            'delete'        => $this->integer(1)->defaultValue(0), //статус удален/не удален
        ]);

        $this->createIndex('idx_workers_payments_worker_id', $this->table_payments, 'worker_id');
        $this->addForeignKey('fk_workers_payments_worker_items', $this->table_payments, 'worker_id', $this->table, 'id', 'cascade', 'cascade');




    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_workers_payments_worker_items', $this->table_payments);
        $this->dropForeignKey('fk_workers_agency_items', $this->table);
        $this->dropForeignKey('fk_worker_jobs_worker_items', $this->table_jobs);

        $this->dropTable($this->table_payments);
        $this->dropTable($this->table_jobs);
        $this->dropTable($this->table);
    }

}
