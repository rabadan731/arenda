<?php

use yii\db\Migration;

class m160522_211009_rental extends Migration
{
    public $table = '{{%rent}}';
    public $table_men = '{{%rent_manager}}';
    public $table_agency = '{{%rent_agency}}';
    public $table_rented = '{{%for_rent}}';
    public $tableRentedImportService = '{{%rent_import_services}}';
    public $tableRentedImportList = '{{%rent_import_list}}';
    public $agency = '{{%agency}}';
    public $clients = '{{%clients}}';


    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id'            => $this->primaryKey(),
            'title'         => $this->string(512)->notNull(),
            'description'   => $this->text(),

            'address_text'  => $this->string(512),
            'address_map'   => $this->string(512),
            'latitude'      => $this->string(128),
            'longitude'     => $this->string(128),

            'fio'           => $this->string(512),
            'photo'         => $this->string(512),
            'email'         => $this->string(256),
            'phone'         => $this->string(256),

            'hash'         => $this->string(35),

            'contract_date_begin'  => $this->integer(11),
            'contract_date_end'    => $this->integer(11),

            'contract_month_price' => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'contract_week_price'  => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'contract_day_price'   => $this->decimal(10,2)->notNull()->defaultValue(0.00),

            'price_month'   => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'price_week'    => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'price_day'     => $this->decimal(10,2)->notNull()->defaultValue(0.00),

            'agency_id'     => $this->integer(11),
            'import'        => $this->integer(1),
            'created_by'    => $this->integer(),
            'updated_by'    => $this->integer(),
            'status'        => $this->integer(1),
            'img_sort'      => $this->string(256),
            'delete'        => $this->integer(1)->defaultValue(0), //статус удален/не удален
        ]);

        $this->createIndex('idx_rent_agency_id', $this->table, 'agency_id');
        $this->addForeignKey('fk_rent_agency_items', $this->table, 'agency_id', $this->agency, 'id', 'cascade', 'cascade');

        $this->createTable($this->table_rented, [
            'id'            => $this->primaryKey(),
            'client_id'     => $this->integer(11)->notNull(),
            'rent_id'       => $this->integer(11)->notNull(),

            'check_in'      => $this->integer(11),
            'check_out'     => $this->integer(11),

            'comment'       => $this->text(),

            'price_all'     => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'price_day'     => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'price_deposit' => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'price_prepay'  => $this->decimal(10,2)->notNull()->defaultValue(0.00),

            'worker_agent'  => $this->integer(11),
            'worker_driver' => $this->integer(11),
            'worker_cleaner'=> $this->integer(11),
            'worker_master' => $this->integer(11),

            'agency_id'     => $this->integer(11),
            'import'        => $this->integer(1),
            'status'        => $this->integer(1)->defaultValue(0),
            'reservations'  => $this->integer(1)->defaultValue(0),
            'delete'        => $this->integer(1)->defaultValue(0), //статус удален/не удален
        ]);

        $this->createIndex('idx_for_rent_worker_agent', $this->table_rented, 'worker_agent');
        $this->createIndex('idx_for_rent_worker_driver', $this->table_rented, 'worker_driver');
        $this->createIndex('idx_for_rent_worker_cleaner', $this->table_rented, 'worker_cleaner');
        $this->createIndex('idx_for_rent_worker_master', $this->table_rented, 'worker_master');

        $this->createIndex('idx_for_rent_agency_id', $this->table_rented, 'agency_id');
        $this->addForeignKey('fk_for_rent_agency_items', $this->table_rented, 'agency_id', $this->agency, 'id', 'cascade', 'cascade');

        $this->createIndex('idx_for_rent_client_id', $this->table_rented, 'client_id');
        $this->addForeignKey('fk_for_rent_client_items', $this->table_rented, 'client_id', $this->clients, 'id', 'cascade', 'cascade');

        $this->createIndex('idx_for_rent_rent_id', $this->table_rented, 'rent_id');
        $this->addForeignKey('fk_for_rent_rent_items', $this->table_rented, 'rent_id', $this->table, 'id', 'cascade', 'cascade');
 

        $this->createTable($this->table_men, [
            'id'             => $this->primaryKey(),
            'rent_id'        => $this->integer()->notNull(),
            'manager_id'     => $this->integer()->notNull(),
            'access_type'    => $this->integer(1)->defaultValue(1),
        ]);

        $this->createIndex(  'idx_client_rent_rent_id', $this->table_men, 'rent_id');
        $this->addForeignKey('fk_client_rent_rent_items', $this->table_men, 'rent_id', $this->table, 'id', 'cascade', 'cascade');
        $this->createIndex(  'idx_client_rent_manager_id', $this->table_men, 'manager_id');
        $this->addForeignKey('fk_client_rent_manager_items', $this->table_men, 'manager_id', '{{%users}}', 'id', 'cascade', 'cascade');

        $this->createTable($this->table_agency, [
            'id'             => $this->primaryKey(),
            'rent_id'        => $this->integer()->notNull(),
            'agency_id'      => $this->integer()->notNull(),
            'access_type'    => $this->integer(1)->defaultValue(1),
        ]);

        $this->createIndex(  'idx_client_rent_agency_rent_id', $this->table_agency, 'rent_id');
        $this->addForeignKey('fk_client_rent_agency_rent_items', $this->table_agency, 'rent_id', $this->table, 'id', 'cascade', 'cascade');
        $this->createIndex(  'idx_client_rent_agency_agency_id', $this->table_agency, 'agency_id');
        $this->addForeignKey('fk_client_rent_agency_agency_items', $this->table_agency, 'agency_id', $this->agency, 'id', 'cascade', 'cascade');

        $this->createTable($this->tableRentedImportService, [
            'service_id'     => $this->primaryKey(),
            'rent_id'        => $this->integer()->notNull(),
            'agency_id'      => $this->integer()->notNull(),
            'title'          => $this->string(256)->notNull(),
            'import_url'     => $this->string(1024)->notNull(),
            'import_date'    => $this->string(1024),
        ]);

        $this->createIndex(  'idx_tableRentedImportService_agency_id', $this->tableRentedImportService, 'agency_id');
        $this->createIndex(  'idx_tableRentedImportService_rent_id', $this->tableRentedImportService, 'rent_id');
        $this->addForeignKey('fk_tableRentedImportService_rent_items', $this->tableRentedImportService, 'rent_id', $this->table, 'id', 'cascade', 'cascade');



        $this->createTable($this->tableRentedImportList, [
            'id'             => $this->primaryKey(),
            'rent_id'        => $this->integer()->notNull(),
            'agency_id'      => $this->integer()->notNull(),
            'service_id'     => $this->integer()->notNull(),
            'uid'            => $this->string(512),
            'date_import'    => $this->integer(),
            'date_from'      => $this->integer()->notNull(),
            'date_to'        => $this->integer()->notNull(),
        ]);

        $this->createIndex(  'idx_tableRentedImportList_service_id', $this->tableRentedImportList, 'service_id');
        $this->addForeignKey('fk_tableRentedImportList_service_id', $this->tableRentedImportList, 'service_id', $this->tableRentedImportService, 'service_id', 'cascade', 'cascade');
        $this->createIndex(  'idx_tableRentedImportList_agency_id', $this->tableRentedImportList, 'agency_id');
        $this->createIndex(  'idx_tableRentedImportList_rent_id', $this->tableRentedImportList, 'rent_id');
        $this->addForeignKey('fk_tableRentedImportList_rent_items', $this->tableRentedImportList, 'rent_id', $this->table, 'id', 'cascade', 'cascade');




    }

    public function safeDown()
    {

        $this->dropForeignKey('fk_tableRentedImportList_rent_items', $this->tableRentedImportList);
        $this->dropForeignKey('fk_tableRentedImportList_service_id', $this->tableRentedImportList);
        $this->dropTable($this->tableRentedImportList);

        $this->dropForeignKey('fk_tableRentedImportService_rent_items', $this->tableRentedImportService);
        $this->dropTable($this->tableRentedImportService);


        $this->dropForeignKey('fk_client_rent_agency_rent_items', $this->table_agency);
        $this->dropForeignKey('fk_client_rent_agency_agency_items', $this->table_agency);
        $this->dropTable($this->table_agency);

        $this->dropForeignKey('fk_client_rent_rent_items', $this->table_men);
        $this->dropForeignKey('fk_client_rent_manager_items', $this->table_men);
        $this->dropTable($this->table_men);

        $this->dropForeignKey('fk_for_rent_rent_items', $this->table_rented);
        $this->dropForeignKey('fk_for_rent_agency_items', $this->table_rented);
        $this->dropForeignKey('fk_for_rent_client_items', $this->table_rented);
        $this->dropTable($this->table_rented);

        $this->dropForeignKey('fk_rent_agency_items', $this->table);
        $this->dropTable($this->table);
    }

}
