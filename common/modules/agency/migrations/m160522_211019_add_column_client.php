<?php

use yii\db\Migration;

class m160522_211019_add_column_client extends Migration
{


    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn("{{rent_import_list}}", "client_id", $this->integer());
        $this->addColumn("{{clients}}", "import_md5", $this->string(128));
    }

    public function safeDown()
    {
        $this->dropColumn("{{clients}}", "import_md5");
        $this->dropColumn("{{rent_import_list}}", "client_id");
    }

}
