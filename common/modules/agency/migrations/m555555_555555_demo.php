<?php

use yii\db\Migration;

class m555555_555555_demo extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {

        $agency = "{{%agency}}";
        $tariff = "{{%agency_tariff}}";
        $users = "{{%users}}";

        $agencyRole = Yii::$app->authManager->getRole('agency');
        $managerRole = Yii::$app->authManager->getRole('manager');
        $phm = Yii::$app->security->generatePasswordHash('manager');
        for ($i = 1; $i <= 3; $i++) {
            $this->insert($agency, [
                'title' => "Agency demo #{$i}",
                'balance' => 1000,
                'status' => 1,
            ]);
            


            $user = new \modules\users\models\backend\Users([
                'email' => "agency{$i}@site.com",
                'password' => Yii::$app->security->generatePasswordHash('agency'),
                'name' => "Агент демо {$i}",
                'phone' => '112',
                'time_reg' => \common\helpers\Time::real(),
                'role' => 'agency',
                'sex' => 'm',
                'locale' => Yii::$app->language,
                'agency_id' => $i,
            ]);
            $user->scenario = $user::SCENARIO_PROFILE;
            $user->save(false);
           // Yii::$app->authManager->assign($agencyRole, $user->id);

            for ($j = 1; $j <= 5; $j++) {
                $user = new \modules\users\models\backend\Users([
                    'email' => "manager{$i}_{$j}@site.com",
                    'password' => $phm,
                    'name' => "Менеджер демо {$i} - {$j}",
                    'phone' => '112',
                    'time_reg' => \common\helpers\Time::real(),
                    'role' => 'manager',
                    'sex' => 'm',
                    'locale' => Yii::$app->language,
                    'agency_id' => $i,
                ]);
                $user->scenario = $user::SCENARIO_PROFILE;
                $user->save(false);
                //Yii::$app->authManager->assign($managerRole, $user->id);
            }


            $this->batchInsert('{{%workers}}', [
                'worker_type', 'fio', 'email', 'phone', 'balance', 'agency_id', 'created_by', 'updated_by', 'status',
            ], [
                ['3', "Имя Фамилия Отчество {$i}_1", 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['3', "Имя Фамилия Отчество {$i}_4", 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['3', "Имя Фамилия Отчество {$i}_7", 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['6', "Имя Фамилия Отчество {$i}_2", 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['6', "Имя Фамилия Отчество {$i}_5", 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['6', "Имя Фамилия Отчество {$i}_8", 'email@email.ru', '112', '0', $i, '2', '2', '1'],
               ['7', "Имя Фамилия Отчество {$i}_10", 'email@email.ru', '112', '0', $i, '2', '2', '1'],
               ['7', "Имя Фамилия Отчество {$i}_11", 'email@email.ru', '112', '0', $i, '2', '2', '1'],
               ['7', "Имя Фамилия Отчество {$i}_12", 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['9', "Имя Фамилия Отчество {$i}_3", 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['9', "Имя Фамилия Отчество {$i}_6", 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['9', "Имя Фамилия Отчество {$i}_9", 'email@email.ru', '112', '0', $i, '2', '2', '1'],
            ]);



            $rents = [];
            for ($j = 1; $j < 5; $j++) {
                $rents[] = [
                    'Объект аренды №' . ($j) . ' агенства №' . ($i),
                    'Владелец №' . ($j) . ' Фамилия Отчество',
                    'email@email.ru',
                    '112',
                    '30',
                    '5',
                    $i,
                    '2',
                    '2',
                    '1',
                    'Описание',
                    '55.7558260,37.6173000',
                    'Москва',
                    \common\helpers\Time::real(),
                    (\common\helpers\Time::real() + (50 * 31 * 24 * 60 * 60)),
                    '100'
                ];
            }
            
            $this->batchInsert('{{%rent}}', [
                'title', 'fio', 'email', 'phone', 'price_week', 'price_day', 'agency_id',
                'created_by', 'updated_by', 'status', 'description', 'address_map', 'address_text',
                'contract_date_begin', 'contract_date_end', 'contract_month_price',
            ], $rents);
        }


        $this->batchInsert($tariff, ['title', 'month', 'price', 'status',], [
            ['title' => 'На 1 месяц', 'month' => 1, 'price' => 100, 'status' => 1],
            ['title' => 'На 3 месяц', 'month' => 3, 'price' => 290, 'status' => 1],
            ['title' => 'На пол года', 'month' => 6, 'price' => 550, 'status' => 1],
            ['title' => 'На 1 год', 'month' => 12, 'price' => 1000, 'status' => 1],
        ]);
    }
}
