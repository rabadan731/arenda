<?php

use yii\db\Migration;

class m160522_211020_add_column_rent extends Migration
{
    /**
     * Use safeUp/safeDown to run migration code within a transaction
     */
    public function safeUp()
    {

        $this->addColumn("{{rent}}", "type_rent", $this->integer());

        $this->addColumn("{{rent}}", "status_rent", $this->integer());
        $this->addColumn("{{rent}}", "status_buy", $this->integer());
        $this->addColumn(
            "{{rent}}",
            "price_sale",
            $this->decimal(10,2)->notNull()->defaultValue(0.00)
        );

        \common\modules\agency\models\Rent::updateAll(['status_rent' => 1]);
    }

    /**
     * Use safeUp/safeDown to run migration code within a transaction
     */
    public function safeDown()
    {
        $this->dropColumn("{{rent}}", "price_sale");
        $this->dropColumn("{{rent}}", "status_buy");
        $this->dropColumn("{{rent}}", "status_rent");
    }

}
