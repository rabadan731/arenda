<?php

namespace modules\agency;

use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package modules\main
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->urlManager->addRules([
            'agency/rent/export/<id:\d+>/<hash>' => 'agency/rent/export'
        ], false);
    }
}
