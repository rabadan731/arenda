<?php

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('common', 'Trash');
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'trash-o';
$this->params['place'] = 'trash';
$this->params['breadcrumbs'][] = $this->title;

\modules\cars\Module::registerTranslations();

/* @var $dataProviderAgency yii\data\ActiveDataProvider */
/* @var $dataProviderAgencyTariff yii\data\ActiveDataProvider */
/* @var $dataProviderClients yii\data\ActiveDataProvider */
/* @var $dataProviderClientPayments yii\data\ActiveDataProvider */
/* @var $dataProviderRent yii\data\ActiveDataProvider */
/* @var $dataProviderForRent yii\data\ActiveDataProvider */
/* @var $dataProviderWorkers yii\data\ActiveDataProvider */
/* @var $dataProviderWorkerJobs yii\data\ActiveDataProvider */

?>
<div class="jarviswidget">
    <div class="agency-index">

        <?php if ($dataProviderAgency->count > 0) { ?>
            <h2><?= Yii::t("common", "Agencies"); ?></h2>
            <?= GridView::widget([
                'dataProvider' => $dataProviderAgency,

                'columns' => [
                    'id',
                    'title',
                    'balance',
                    //'status',
                    // 'date_begin',
                    // 'date_end',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'options' => ['style' => 'width:265px;'],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            return ['agency/' . $action, 'id' => $model->id];
                        },
                        'template' => '<div class="text-center">{view} {undelete}</div>',
                        'buttons' => [
                            'undelete' => function ($url, $model, $key) {
                                return Html::a(Yii::t('common', 'To restore the object'), $url, [
                                    'class' => 'btn btn-danger btn-sm ',
                                    'data' => [
                                        'confirm' => Yii::t('common', 'To restore the object'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                            'view' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-eye"></i> ' , $url, [
                                    'class' => 'btn btn-info btn-sm ',
                                    'title' => Yii::t("common", "View"),
                                    'data-pjax' => 0,
                                ]);
                            },
                        ]
                    ],
                ],
            ]); ?>
            <hr/>
        <?php } ?>


        <?php if ($dataProviderAgencyTariff->count > 0) { ?>
            <h2><?= Yii::t("common", "Tariff"); ?></h2>
            <?= GridView::widget([
                'dataProvider' => $dataProviderAgencyTariff,

                'columns' => [
                    'id',
                    'title',
                    'month',
                    'price',
                    'status',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'options' => ['style' => 'width:265px;'],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            return ['agency-tariff/' . $action, 'id' => $model->id];
                        },
                        'template' => '<div class="text-center">{view} {undelete}</div>',
                        'buttons' => [
                            'undelete' => function ($url, $model, $key) {
                                return Html::a(Yii::t('common', 'To restore the object'), $url, [
                                    'class' => 'btn btn-danger btn-sm ',
                                    'data' => [
                                        'confirm' => Yii::t('common', 'To restore the object'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                            'view' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-eye"></i> ' , $url, [
                                    'class' => 'btn btn-info btn-sm ',
                                    'title' => Yii::t("common", "View"),
                                    'data-pjax' => 0,
                                ]);
                            },
                        ]
                    ],
                ],
            ]); ?>
            <hr/>
        <?php } ?>

        <?php if ($dataProviderClients->count > 0) { ?>
            <h2><?= Yii::t("common", "Clients"); ?></h2>
            <?= GridView::widget([
                'dataProvider' => $dataProviderClients,

                'columns' => [
                    'id',
                    [
                        'attribute' => 'photo',
                        'filter' => false,
                        'format' => 'html',
                        'value' => function ($data) {
                            return Html::img($data->getUrlAvatar(true), ['style' => 'width:25px']);
                        },
                        'options' => ['style' => 'width:50px']
                    ],
                    'fio',
                    'email:email',
                    'phone',
                    'balance',
                    'agency.title',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'options' => ['style' => 'width:265px;'],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            return ['/clients/clients/' . $action, 'id' => $model->id];
                        },
                        'template' => '<div class="text-center">{view} {undelete}</div>',
                        'buttons' => [
                            'undelete' => function ($url, $model, $key) {
                                return Html::a(Yii::t('common', 'To restore the object'), $url, [
                                    'class' => 'btn btn-danger btn-sm ',
                                    'data' => [
                                        'confirm' => Yii::t('common', 'To restore the object'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                            'view' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-eye"></i> ' , $url, [
                                    'class' => 'btn btn-info btn-sm ',
                                    'title' => Yii::t("common", "View"),
                                    'data-pjax' => 0,
                                ]);
                            },
                        ]
                    ],
                ],
            ]); ?>
            <hr/>
        <?php } ?>

        <?php /*
        <h2><?= Yii::t("common", "Client Payments"); ?></h2>
        <?= GridView::widget([
            'dataProvider' => $dataProviderClientPayments,

            'columns' => [
                'id',
                'client_id',
                'title',
                'date_invoice',
                'price',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'urlCreator' => function ($action, $model, $key, $index) {
                        return ['agency/' . $action, 'id' => $model->id];
                    },
                    'template' => '<div class="text-center"> {view2} </div>'
                ],
            ],
        ]); ?>
        <hr /> */ ?>

        <?php if ($dataProviderRent->count > 0) { ?>
            <h2><?= Yii::t("common", "Objects fot rent"); ?></h2>
            <?= GridView::widget([
                'dataProvider' => $dataProviderRent,

                'columns' => [
                    'id',
                    'title',
                    'address_text',
                    'email:email',
                    'phone',
                    'price_day',
                    'agency.title',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'options' => ['style' => 'width:265px;'],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            return ['rent/' . $action, 'id' => $model->id];
                        },
                        'template' => '<div class="text-center">{view} {undelete}</div>',
                        'buttons' => [
                            'undelete' => function ($url, $model, $key) {
                                return Html::a(Yii::t('common', 'To restore the object'), $url, [
                                    'class' => 'btn btn-danger btn-sm ',
                                    'data' => [
                                        'confirm' => Yii::t('common', 'To restore the object'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                            'view' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-eye"></i> ' , $url, [
                                    'class' => 'btn btn-info btn-sm ',
                                    'title' => Yii::t("common", "View"),
                                    'data-pjax' => 0,
                                ]);
                            },
                        ]
                    ],
                ],
            ]); ?>
            <hr/>
        <?php } ?>

        <?php if ($dataProviderForRent->count > 0) { ?>
            <h2><?= Yii::t("common", "Objects in rent"); ?></h2>
            <?= GridView::widget([
                'dataProvider' => $dataProviderForRent,

                'columns' => [
                    'id',
                    'rent_id',
                    'check_in:date',
                    'check_out:date',
                    'price_all',
                    'price_day',
                    'price_deposit',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'options' => ['style' => 'width:265px;'],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            return ['for-rent/' . $action, 'id' => $model->id];
                        },
                        'template' => '<div class="text-center">{view} {undelete}</div>',
                        'buttons' => [
                            'undelete' => function ($url, $model, $key) {
                                return Html::a(Yii::t('common', 'To restore the object'), $url, [
                                    'class' => 'btn btn-danger btn-sm ',
                                    'data' => [
                                        'confirm' => Yii::t('common', 'To restore the object'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                            'view' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-eye"></i> ' , $url, [
                                    'class' => 'btn btn-info btn-sm ',
                                    'title' => Yii::t("common", "View"),
                                    'data-pjax' => 0,
                                ]);
                            },
                        ]
                    ],
                ],
            ]); ?>
            <hr/>
        <?php } ?>

        <?php if ($dataProviderWorkers->count > 0) { ?>
            <h2><?= Yii::t("common", "Workers"); ?></h2>
            <?= GridView::widget([
                'dataProvider' => $dataProviderWorkers,

                'columns' => [
                    'id',
                    [
                        'attribute' => 'photo',
                        'filter' => false,
                        'format' => 'html',
                        'value' => function ($data) {
                            return Html::img($data->getUrlAvatar(true), ['style' => 'width:25px']);
                        },
                        'options' => ['style' => 'width:50px']
                    ],
                    [
                        'attribute' => 'worker_type',
                        'value' => 'workerTitle',
                        'filter' => \common\grid\TypeWorkerEnum::listData(),
                    ],
                    'fio',
                    'email:email',
                    'phone',
                    'balance',
                    'agency.title',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'options' => ['style' => 'width:265px;'],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            return ['workers/' . $action, 'id' => $model->id];
                        },
                        'template' => '<div class="text-center">{view} {undelete}</div>',
                        'buttons' => [
                            'undelete' => function ($url, $model, $key) {
                                return Html::a(Yii::t('common', 'To restore the object'), $url, [
                                    'class' => 'btn btn-danger btn-sm ',
                                    'data' => [
                                        'confirm' => Yii::t('common', 'To restore the object'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                            'view' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-eye"></i> ' , $url, [
                                    'class' => 'btn btn-info btn-sm ',
                                    'title' => Yii::t("common", "View"),
                                    'data-pjax' => 0,
                                ]);
                            },
                        ]
                    ],
                ],
            ]); ?>
            <hr/>
        <?php } ?>

        <?php if ($dataProviderWorkerJobs->count > 0) { ?>
            <h2><?= Yii::t("common", "Worker Jobs"); ?></h2>
            <?= GridView::widget([
            'dataProvider' => $dataProviderWorkerJobs,

            'columns' => [
                'id',
                'worker_id',
                'address_text',
                'address_map',
                'need_to:ntext',
                'price',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'options' => ['style' => 'width:265px;'],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        return ['worker-jobs/' . $action, 'id' => $model->id];
                    },
                    'template' => '<div class="text-center">{view} {undelete}</div>',
                    'buttons' => [
                        'undelete' => function ($url, $model, $key) {
                            return Html::a(Yii::t('common', 'To restore the object'), $url, [
                                'class' => 'btn btn-danger btn-sm ',
                                'data' => [
                                    'confirm' => Yii::t('common', 'To restore the object'),
                                    'method' => 'post',
                                ],
                            ]);
                        },
                        'view' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-eye"></i> ' , $url, [
                                'class' => 'btn btn-info btn-sm ',
                                'title' => Yii::t("common", "View"),
                                'data-pjax' => 0,
                            ]);
                        },
                    ]
                ],
            ],
        ]); ?>
            <hr/>
        <?php } ?>



        <?php if ($dataProviderCars->count > 0) { ?>
            <h2><?= Yii::t("cars", "Cars"); ?></h2>
            <?= GridView::widget([
                'dataProvider' => $dataProviderCars,

                'columns' => [
                    [
                        'attribute' => 'car_mark_id',
                        'value' => function ($data) {
                            return $data->carMark->title;
                        },
                        'filter' => \common\modules\cars\models\CarMark::getList()
                    ],
                    [
                        'attribute'=>'car_model_id',
                        'value' => function ($data) {
                            return $data->carModel->title;
                        },
                        'filter' => \common\modules\cars\models\CarModel::getList()
                    ],
                    'year',
                    'number',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'options' => ['style' => 'width:265px;'],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            /** @var $model \common\modules\cars\models\Car */
                            return ['/cars/car/' . $action, 'id' => $model->car_id];
                        },
                        'template' => '<div class="text-center">{view} {undelete}</div>',
                        'buttons' => [
                            'undelete' => function ($url, $model, $key) {
                                return Html::a(Yii::t('common', 'To restore the object'), $url, [
                                    'class' => 'btn btn-danger btn-sm ',
                                    'data' => [
                                        'confirm' => Yii::t('common', 'To restore the object'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                            'view' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-eye"></i> ' , $url, [
                                    'class' => 'btn btn-info btn-sm ',
                                    'title' => Yii::t("common", "View"),
                                    'data-pjax' => 0,
                                ]);
                            },
                        ]
                    ],
                ],
            ]); ?>
            <hr/>
        <?php } ?>

        <?php if ($dataProviderReserve->count > 0) { ?>
            <h2><?= Yii::t("cars", "Car Reserves"); ?></h2>
            <?= GridView::widget([
                'dataProvider' => $dataProviderReserve,

                'columns' => [
                    'car.carTitle',
                    'phone',
                    'date_begin:datetime',
                    'date_end:datetime',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'options' => ['style' => 'width:265px;'],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            /** @var $model \common\modules\cars\models\CarReserve */
                            return ['/cars/car-reserve/' . $action, 'id' => $model->reserve_id];
                        },
                        'template' => '<div class="text-center">{view} {undelete}</div>',
                        'buttons' => [
                            'undelete' => function ($url, $model, $key) {
                                return Html::a(Yii::t('common', 'To restore the object'), $url, [
                                    'class' => 'btn btn-danger btn-sm ',
                                    'data' => [
                                        'confirm' => Yii::t('common', 'To restore the object'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                            'view' => function ($url, $model, $key) {
                                return Html::a('<i class="fa fa-eye"></i> ' , $url, [
                                    'class' => 'btn btn-info btn-sm ',
                                    'title' => Yii::t("common", "View"),
                                    'data-pjax' => 0,
                                ]);
                            },
                        ]
                    ],
                ],
            ]); ?>
            <hr/>
        <?php } ?>

    </div>
</div>
