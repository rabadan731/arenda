<?php

use common\modules\clients\models\ClientPayments;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;

/** @var $clientPaymentsProvider \yii\data\ActiveDataProvider */
/** @var $id integer */

$model = new ClientPayments();

$js = <<< JS
$('body').on('beforeSubmit', 'form#form-payments', function () {

     var form = $(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }

     // submit form
     $.ajax({
          url: form.attr('action')+'&save=1',
          type: 'post',
          data: new FormData(this), // Data sent to server, a set of key/value pairs
          contentType: false,       // The content type used when sending data to the server.
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false 

          success: function (response) {
               if (response.success) {
                   $.pjax.reload({container:"#list-pay"}); 
               } else {
                   alert(response.error_text);
               }
          }
     });
     return false;
});
JS;
$this->registerJs($js);

Pjax::begin(['id' => 'list-pay', 'timeout' => 10000]);
?>

    <div>
        <?= GridView::widget([
            'dataProvider' => $clientPaymentsProvider,
            'columns' => [
                'title',
                'date_invoice:date',
                'price',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'urlCreator' => function ($action, $model, $key, $index) {
                        return ['client-payments/' . $action, 'id' => $model->id];
                    },
                    'template' => '<div class="text-center">  {update} {delete}</div>'
                ],
            ],
        ]);
        ?>

        <?php $form = ActiveForm::begin([
            'id' => 'form-payments',
            'action' => \yii\helpers\Url::to(['add-pay', 'id' => $id])
        ]); ?>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']); ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
<?php Pjax::end();
