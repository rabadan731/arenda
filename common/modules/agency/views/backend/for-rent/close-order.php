<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
$js = <<< JS
$('body').on('beforeSubmit', 'form#form-payments', function () {

     var form = $(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }

     // submit form
     $.ajax({
          url: form.attr('action')+'&save=1',
          type: 'post',
          data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
          contentType: false,       // The content type used when sending data to the server.
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false 

          success: function (response) {
               if (response.success) {
                   $.pjax.reload({container:"#list-pay"}); 
               } else {
                   alert(response.error_text);
               }
          }
     });
     return false;


});
JS;
$this->registerJs($js);

/* @var $this yii\web\View */
/* @var $result Int */
/* @var $model common\modules\agency\models\ForRent */
/* @var $rent common\modules\agency\models\Rent */
/* @var $clientPaymentsProvider \yii\data\ActiveDataProvider */
/* @var $modelPay \common\modules\clients\models\ClientPayments */
/* @var $payment \common\modules\clients\models\ClientPayments */
$rent = $model->rent;
$this->title = Yii::t('common', 'Close the order');

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'close-order';

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Objects fot rent'), 'url' => ['rent/index']];
$this->params['breadcrumbs'][] = ['label' => $rent->address_text, 'url' => ['rent/view', 'id' => $rent->id]];

$this->params['breadcrumbs'][] = ['label' => Yii::$app->formatter->asDate($model->check_in) . ' - ' .
    Yii::$app->formatter->asDate($model->check_out), 'url' => ['for-rent/view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;


?>

<div class="rent-form" id="content">
    <div class="row">
        <div class="jarviswidget">
            <div class="widget-body">
                <?php Pjax::begin(['id'=>'list-pay','timeout' => 10000 ]); ?>
                <div id="productForm" class="form-horizontal bv-form">


                    <fieldset>
                        <?php
                        $modelPay->title = $result < 0 ? Yii::t('common', 'Payment for rent'):
                            Yii::t('common', 'Refund of a deposit');
                        $modelPay->price = -$result;
                        $form = ActiveForm::begin([
                            'id' => 'form-payments',
                            'action' => \yii\helpers\Url::to(['add-pay', 'id' => $model->id])
                        ]); ?>
                        <div class="col-xs-5 col-xs-offset-3">
                            <h4><b>Добавить платеж</b></h4>
                            <div class="form-group has-feedback row" style="margin: 0 0px">

                                <div class="col-xs-12">
                                    <?= $form->field($modelPay, 'title')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-xs-9 smart-form">
                                    <?= $form->field($modelPay, 'price', [
                                        'template' => '{label}<label class="input">
                                                      <i class="icon-append fa fa-eur"></i>{input}</label>'
                                    ])->widget(\yii\widgets\MaskedInput::className(), [
                                        'clientOptions' => [
                                            'alias' => 'decimal',
                                            'autoGroup' => true
                                        ],
                                    ]) ?>
                                </div>
                                <div class="col-xs-3 " style="padding-top: 24px">
                                    <?= Html::submitButton(Yii::t('common', 'Add'), ['class' => 'btn btn-primary']) ?>
                                </div>

                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </fieldset>
                    <hr />
                    <?php /* foreach ($clientPaymentsProvider->getModels() as $payment ) { $result +=$payment->price; ?>
                            <fieldset>
                                <div class="form-group has-feedback">
                                    <label class="col-xs-2 col-lg-4 control-label"><?= $payment->title; ?></label>
                                    <div class="col-xs-6 col-lg-4 inputGroupContainer">
                                        <div class="input-group">
                                            <span class="input-group-addon">€</span>
                                            <input type="text"
                                                   class="form-control"
                                                   name="amount"
                                                   disabled
                                                   data-bv-field="amount"
                                                   value="<?= $payment->price; ?>">
                                        </div>
                                </div>
                            </fieldset>
                        <?php } */ ?>

                    <div class="row">
                        <div class="col-xs-5 col-xs-offset-3 ">
                            <?php echo \yii\grid\GridView::widget([
                                'dataProvider' => $clientPaymentsProvider,
                                'columns' => [
                                    'title',
                                    'date_invoice:datetime',
                                    'price',
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'urlCreator' => function ($action, $model, $key, $index) {
                                            return ['/clients/client-payments/' . $action, 'id' => $model->id];
                                        },
                                        'template' => '<div class="text-center">  {update} {delete}</div>'
                                    ],
                                ],
                            ]);
                            ?>
                        </div>

                        <div class="col-xs-4"></div>
                    </div>

                    <hr/>
                    <fieldset>
                        <div class="form-group has-feedback">
                            <label class="col-xs-2 col-lg-4 control-label"> Итого </label>
                            <div class="col-xs-6 col-lg-4 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon">€</span>
                                    <input type="text"
                                           class="form-control"
                                           name="amount"
                                           disabled
                                           data-bv-field="amount"
                                           value="<?= $result; ?>">
                                </div>
                            </div>
                    </fieldset>
                    <hr/>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                            <?= Html::button(
                                                '<i class="fa fa-check-square-o"></i>' .
                                                Yii::t("common", "Close the order"),
                                                [
                                                    'class' => 'btn btn-'.($result == 0 ? "success" : "danger"),
                                                    'data' => [
                                                        'confirm' => ($result == 0 ? "":(
                                                        Yii::t('common', 'The deposit amount is not the same')."! ")
                                                        ). Yii::t('common', 'Do you really want to close the order?'),
                                                        'method' => 'post',
                                                    ],
                                                ]
                                            ); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
