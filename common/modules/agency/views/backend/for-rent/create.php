<?php

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\ForRent */
/* @var $rent common\modules\agency\models\Rent */

$this->title = Yii::t('common', 'Delivery of object').': '.$rent->title;

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'сlients';

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Objects fot rent'), 'url' => ['rent/index']];
$this->params['breadcrumbs'][] = ['label' => $rent->title, 'url' => ['rent/view','id'=>$rent->id]];
$this->params['breadcrumbs'][] = $this->title;

$model->price_all = empty($model->price_all)?$model->price_day:$model->price_all;

?>
<div id="content" >
    <div class="for-rent-create">

    <?= $this->render('_form', [
        'model' => $model,
        'rent' => $rent,
    ]) ?>

    </div>
</div>
