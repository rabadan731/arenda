<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\ForRent */
/* @var $rent  common\modules\agency\models\Rent */

$this->title = Yii::t('common', 'Update {modelClass}: ', [
    'modelClass' =>  Yii::t('common', 'Objects in rent'),
]) . $model->id;

$rent = $model->rent;
$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'сlients';

$this->params['breadcrumbs'][] = ['label' =>  Yii::t('common', 'Objects in rent'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $rent->title, 'url' => ['rent/view', 'id' => $rent->id]];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->formatter->asDate($model->check_in) . ' - ' .
    Yii::$app->formatter->asDate($model->check_out), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="jarviswidget">
    <div class="for-rent-update">

    <?= $this->render('_form', [
        'model' => $model,
        'rent' => $rent,
    ]) ?>

    </div>
</div>
