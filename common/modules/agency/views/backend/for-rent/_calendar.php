<?php
\themes\fullcalendar\Scheduler::register($this);

$this->registerCss(
    "#calendar { 
		margin: 50px auto;
	}"
);


$js = <<< JS
$(function() { // document ready

		$('#calendar').fullCalendar({		
			aspectRatio: 1.8,
			lang: $('#calendar').data("lang"),
			height : 'auto',
			header: {
				left: 'today prev,next myCustomButton',
				center: 'title',
				right: 'timelineWeekMy,timelineMonthMy,timelineYearMy'
			},
			defaultView: 'timelineMonthMy',
            views: {
                timelineWeekMy: {
                    type: 'timelineMonth',
                    duration: { days: 10 },
                    buttonText: $('#calendar').data("text-week")
                },
                timelineMonthMy: {
                    type: 'timelineMonth',
                    duration: { days: 31 },
                    buttonText: $('#calendar').data("text-month")
                },
                timelineYearMy: {
                    type: 'timelineYear',
                    duration: { days: 365 },
                    buttonText: $('#calendar').data("text-year")
                },
                
            },
            
			schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
			resourceAreaWidth: '25%',
			resourceLabelText: 'Rent',
			resources: {$resources},
			events: '{$ajaxEvents}',
			 
		});
	
	});
JS;
$this->registerJs($js);


?>
<div data-lang="<?= substr(Yii::$app->language, 0, 2); ?>"
     data-text-month="<?= Yii::t("common", "Month"); ?>"
     data-text-year="<?= Yii::t("common", "Year"); ?>"
     data-text-week="<?= Yii::t("common", "Week"); ?>" id='calendar'></div>
