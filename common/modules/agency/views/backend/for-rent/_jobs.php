<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\agency\models\WorkerJobs;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;

/**
 * @var $modelWJ WorkerJobs
 * @var $workers []
 */

$modelWJ->date_event = date(
    "Y-m-d H:i",
    is_null($modelWJ->date_event) ? time() : (
        is_int($modelWJ->date_event) ? $modelWJ->date_event : strtotime($modelWJ->date_event)
    )
);

$form = ActiveForm::begin(['options' => [
    'class' => ''
]]);

?>

<?= $form->field($modelWJ, 'worker_id')->dropDownList($workers, ['prompt' => Yii::t('common', '-- Select --')]) ?>
<?= $form->field($modelWJ, 'address_text')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($modelWJ, 'lat')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($modelWJ, 'lng')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 text-center">
            <label><?= Yii::t("common", "Query coordinate"); ?></label>
            <?= Html::button(Yii::t("common", "Find at the address"), [
                'class' => 'btn btn-default',
                'data-address' => 'workerjobs-address_text',
                'data-lat' => 'workerjobs-lat',
                'data-lng' => 'workerjobs-lng',
                'id' => 'find-coordinate'
            ]) ?>
        </div>
    </div>

<?php echo $this->render('_map', ['lat' => $modelWJ->getLat(), 'lng' => $modelWJ->getLng()]); ?>
    <br/>
<?= $form->field($modelWJ, 'need_to')->textarea(['rows' => 2]) ?>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?php echo DateTimePicker::widget([
                    'model' => $modelWJ,
                    'attribute' => 'date_event',
                    'options' => ['placeholder' => Yii::t("common", "Date event")],
                    'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd HH:ii',
                        'autoclose' => true,
                    ],
                ]); ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="smart-form">
                <?= $form->field(
                    $modelWJ,
                    'price',
                    ['template' => '{label}<label class="input">
                                       <i class="icon-append fa fa-eur"></i>{input}</label>']
                )->widget(\yii\widgets\MaskedInput::className(), [
                    'clientOptions' => [
                        'alias' => 'decimal',
                        'autoGroup' => true
                    ]
                ]) ?>
            </div>
        </div>
    </div>
    <p>
        <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>
    </p>
<?php ActiveForm::end();
