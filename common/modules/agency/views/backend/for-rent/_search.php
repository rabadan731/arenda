<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\search\ForRentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="for-rent-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'client_id') ?>

    <?= $form->field($model, 'check_in') ?>

    <?= $form->field($model, 'check_out') ?>

    <?= $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'price_all') ?>

    <?php // echo $form->field($model, 'price_day') ?>

    <?php // echo $form->field($model, 'price_deposit') ?>

    <?php // echo $form->field($model, 'worker_agent') ?>

    <?php // echo $form->field($model, 'worker_driver') ?>

    <?php // echo $form->field($model, 'worker_cleaner') ?>

    <?php // echo $form->field($model, 'agency_id') ?>

    <?php // echo $form->field($model, 'import') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('common', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
