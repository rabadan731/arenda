<?php

use common\modules\agency\models\ForRent;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\ForRent */

if ($model->status == ForRent::STATUS_OPEN) {?>
    <div class="alert alert-<?=$model->statusText; ?> alert-block">
        <h4 class="alert-heading"><?= Yii::t("common", "The order is open"); ?></h4>
        <?= \yii\helpers\Html::a(
            Yii::t("common", "Close the order"),
            ['close-order', 'id'=>$model->id],
            ['style'=>'margin-top: 10px;', 'class'=>"btn btn-sm btn-{$model->statusText}"]
        ); ?>
    </div>
<?php }