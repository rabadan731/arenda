<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $resources [] */
/* @var $searchModel common\modules\agency\models\search\ForRentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Objects in rent');

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'сlients';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="jarviswidget">
    <div class="for-rent-index">

        <br />
        <br />
        <?=$this->render('_calendar', [
            'resources'=>$resources,
            'ajaxEvents'=>\yii\helpers\Url::to(['jsoncalendar']),
        ]); ?>
        
    </div>
</div>
