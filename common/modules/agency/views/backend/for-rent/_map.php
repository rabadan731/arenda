<?php

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\overlays\Marker;

/**
 * @var $lat float
 * @var $lng float
 */

$coord = new LatLng(['lat' => $lat, 'lng' => $lng]);
$map = new Map([
    'center' => $coord,
    'zoom' => 17,
]);
$eventDrag = new \dosamigos\google\maps\Event([
    'trigger'=>'drag',
    'js'=> 'document.getElementById($(\'#find-coordinate\').data("lat")).value = this.position.lat(); 
        document.getElementById($(\'#find-coordinate\').data("lng")).value = this.position.lng();']);

$marker = new Marker([
    'position' => $coord,
    'draggable' => true,
    'events'=>[$eventDrag]
]);

$map->addOverlay($marker);
$map->width = '100%';
$map->height = "150";

$js = <<< JS
google.maps.event.addDomListener(
    document.getElementById('find-coordinate'), 'click', function(event){

        var address_text = $("#"+$(this).data("address"));
        var address_lat = $("#"+$(this).data("lat"));
        var address_lng = $("#"+$(this).data("lng"));

        $.ajax({
            url: '/backend/agency/default/geocode/?address='+address_text.val(),
            type: 'get',
            cache: false,
            success: function (response) {
                address_lat.val(response.lat[0]);
                address_lng.val(response.lng[0]);
                latlng = new google.maps.LatLng(response.lat[0],response.lng[0]);
                {$marker->getName()}.setPosition(latlng);
                {$map->getName()}.setCenter(latlng);
            }
        });
        
         
    }
);
$('body').on('click', '#find-coordinate', function () {
});
JS;

$map->appendScript($js);
echo $map->display();
