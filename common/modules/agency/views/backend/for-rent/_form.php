<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\datetime\DateTimePicker;
use common\modules\clients\models\Clients;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\ForRent */
/* @var $form yii\bootstrap\ActiveForm */

$model->check_in = date("Y-m-d H:i", is_null($model->check_in) ? time() :
    (is_int($model->check_in) ? $model->check_in : strtotime($model->check_in)));
$model->check_out = date("Y-m-d H:i", is_null($model->check_out) ? time() + (24 * 60 * 60) :
    (is_int($model->check_out) ? $model->check_out : strtotime($model->check_out)));

$addClientTitle = Yii::t('common', 'Add client');

$js = <<< JS
    // Modal Link
    $('#add-client-form').click(function() {
        $("#dialog-message").dialog('open');
        return false;
    });
    
    $("#dialog-message").dialog({
        autoOpen : false,
        modal : true,
        title : '{$addClientTitle}'
    });
    
    
    $('body').on('beforeSubmit', 'form#clients-form', function () {

     $("#clients-form-button").attr("disabled", "disabled");
     var form = $(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
         $("#clients-form-button").attr("disabled", null);
          return false;
     }
  
     
     
     $.ajax({
          url: '/backend/clients/clients/create/?save=1&agency_id={$model->agency_id}',
          type: 'post',
          data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
          contentType: false,       // The content type used when sending data to the server.
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false 
          success: function (response) {
               if (response.success) {
                  
                    $("#forrent-client_id").append(
                        $('<option value="'+response.at.id+'">'+response.at.fio+'</option>')
                    );
                    $("#forrent-client_id [value='"+response.at.id+"']").attr("selected", "selected");
                    
               } else {
                   if (response.find) {
                      $("#forrent-client_id [value='"+(response.at.id)+"']").attr("selected", "selected");
                   } else {
                      alert("error save client"); 
                   }
               }
               $("#dialog-message").dialog("close");
               $("#clients-form-button").attr("disabled", null);
          }
     });
     return false;
});
    
JS;
$this->registerJs($js);

?>


<div class="rent-form">

    <?php $form = ActiveForm::begin(['options' => [
        'class' => ''
    ]]); ?>

    <?php if (count($model->errors)) {
        echo $form->errorSummary($model);
    } ?>

    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-6">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-colorbutton="false"
                 data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

                <header role="heading">

                    <h2> <?= Yii::t('common', 'Information about object'); ?> </h2>

                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="">

                    <!-- widget content -->
                    <div class="widget-body ">

                        <div class="btn-group" data-toggle="buttons" id="radio-status-rent">
                            <?= Html::radio(
                                Html::getInputName($model, 'reservations'),
                                $model->reservations == $model::RESERVATIONS_TEMP,
                                [
                                    'labelOptions' => [
                                        'class' => 'btn btn-warning' .
                                            ($model->reservations == $model::RESERVATIONS_TEMP ? " active" : "")
                                    ],
                                    'label' => Yii::t("common", "Pre-booking"),
                                    'value' => $model::RESERVATIONS_TEMP
                                ]
                            ); ?>

                            <?= Html::radio(
                                Html::getInputName($model, 'reservations'),
                                $model->reservations == $model::RESERVATIONS_YES,
                                [
                                    'labelOptions' => [
                                        'class' => 'btn btn-success' .
                                            ($model->reservations == $model::RESERVATIONS_YES ? " active" : "")
                                    ],
                                    'label' => Yii::t("common", "Confirmed reservations"),
                                    'value' => $model::RESERVATIONS_YES
                                ]
                            ); ?>

                            <?= Html::radio(
                                Html::getInputName($model, 'reservations'),
                                $model->reservations == $model::RESERVATIONS_DELETE,
                                [
                                    'labelOptions' => [
                                        'class' => 'btn btn-danger' .
                                            ($model->reservations == $model::RESERVATIONS_DELETE ? " active" : "")
                                    ],
                                    'label' => Yii::t("common", "Reservation canceled"),
                                    'value' => $model::RESERVATIONS_DELETE
                                ]
                            ); ?>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                <?= $form->field($model, 'client_id')->dropDownList(
                                    Clients::getList(),
                                    ['prompt' => Yii::t("common", "-- Select --")]
                                ) ?>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <br/>
                                    <?= Html::button(Yii::t('common', 'Create new client'), [
                                        // other options
                                        'id' => 'add-client-form',
                                        'class' => 'btn btn-success',
//                                        'data' => [
//                                            'toggle' => 'modal',
//                                            'target' => '#addClientForm',
//                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-colorbutton="false"
                 data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

                <header role="heading">

                    <h2> <?= Yii::t('common', 'Lease'); ?> </h2>

                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="">

                    <!-- widget content -->
                    <div class="widget-body ">
                        <?= $this->render('/rent/_calendar', [
                            'resources' => null,
                            'ajaxEvents' => \yii\helpers\Url::to(['jsoncalendar', 'id' => $model->rent_id]),
                        ]); ?>

                        <div
                                class="form-group <?= (isset($model->errors['check_in'])) ? 'has-error' : ''; ?>"
                                style="    margin-bottom: 45px;"
                        >
                            <label class="control-label">
                                <?= Yii::t("common", "Date of entry and departure"); ?>
                            </label>
                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo DateTimePicker::widget([
                                        'model' => $model,
                                        'attribute' => 'check_in',
                                        'options' => ['placeholder' => Yii::t("common", "Check in")],
                                        'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                                        'pluginOptions' => [
                                            'format' => 'yyyy-mm-dd HH:ii',
                                            'autoclose' => true,
                                        ],
                                        'pluginEvents' => [
                                            "changeDate" => "function(e) { 
                                                od = new Date($('#forrent-check_out').val());
                                                if (od < e.date) {
                                                    newDate = new Date(e.date.getTime() + (((24*60)-e.date.getTimezoneOffset())*60*1000));
                                                    var year    = newDate.getUTCFullYear();
                                                    var month   = newDate.getUTCMonth() + 1;
                                                    var day     = newDate.getUTCDate();
                                                    var hours   = newDate.getUTCHours();
                                                    var minutes = newDate.getUTCMinutes();
                                                    $('#forrent-check_out').val(year + \"-\" + month + \"-\" + day + 
                                                    \" \" + hours + \":\" + minutes);
                                                }
                                                change_price();
                                            }",
                                        ]
                                    ]); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php echo DateTimePicker::widget([
                                        'model' => $model,
                                        'attribute' => 'check_out',
                                        'options' => ['placeholder' => Yii::t("common", "Check out")],
                                        'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                                        'pluginOptions' => [
                                            'format' => 'yyyy-mm-dd HH:ii',
                                            'autoclose' => true,
                                        ],
                                        'pluginEvents' => [
                                            "changeDate" => "function(e) { 
                                                od = new Date($('#forrent-check_in').val());
                                                if (od > e.date) {
                                                    newDate = new Date(e.date.getTime() - (((24*60)+e.date.getTimezoneOffset())*60*1000));
                                                    var year    = newDate.getUTCFullYear();
                                                    var month   = newDate.getUTCMonth() + 1;
                                                    var day     = newDate.getUTCDate();
                                                    var hours   = newDate.getUTCHours();
                                                    var minutes = newDate.getUTCMinutes();
                                                    $('#forrent-check_in').val(
                                                        year + \"-\" + month + \"-\" + day + 
                                                        \" \" + hours + \":\" + minutes
                                                    );
                                                }
                                                change_price();
                                            }",
                                        ]
                                    ]); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- WIDGET END -->


        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-colorbutton="false"
                 data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

                <header role="heading">

                    <h2> <?= Yii::t('common', 'Rent price'); ?> </h2>

                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="">

                    <!-- widget content -->
                    <div class="widget-body ">

                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="smart-form">
                                    <?= $form->field($model, 'price_all', [
                                        'template' => '{label}<label class="input">
                                                          <i class="icon-append fa fa-eur"></i>{input}</label>'
                                    ])->widget(\yii\widgets\MaskedInput::className(), [
                                        'clientOptions' => [
                                            'alias' => 'decimal',
                                            'autoGroup' => true
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="smart-form">
                                    <?= $form->field($model, 'price_day', [
                                        'template' => '{label}<label class="input">
                                                     <i class="icon-append fa fa-eur"></i>{input}</label>'
                                    ])->widget(\yii\widgets\MaskedInput::className(), [
                                        'clientOptions' => [
                                            'alias' => 'decimal',
                                            'autoGroup' => true
                                        ]
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <?php if ($model->isNewRecord) { ?>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <div class="smart-form">
                                        <?= $form->field($model, 'price_deposit', [
                                            'template' => '{label}<label class="input">
                                                         <i class="icon-append fa fa-eur"></i>{input}</label>'
                                        ])->widget(\yii\widgets\MaskedInput::className(), [
                                            'clientOptions' => [
                                                'alias' => 'decimal',
                                                'autoGroup' => true
                                            ],
                                        ]) ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="smart-form">
                                        <?= $form->field($model, 'price_prepay', [
                                            'template' => '{label}<label class="input">
                                                          <i class="icon-append fa fa-eur"></i>{input}</label>'
                                        ])->widget(\yii\widgets\MaskedInput::className(), [
                                            'clientOptions' => [
                                                'alias' => 'decimal',
                                                'autoGroup' => true
                                            ],
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

                        <?= $form->field($model, 'import')->widget(\kartik\switchinput\SwitchInput::classname(), [
                            'pluginOptions' => [
                                'onText' => 'Импортировать объект',
                                'offText' => 'НЕ импортировать объект',
                            ]
                        ]); ?>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- WIDGET END -->

    </div>


    <?php ActiveForm::end(); ?>

</div>


<div id="dialog-message" title="Dialog Simple Title">
    <?= $this->render('../../../../clients/views/backend/clients/_form', [
        'model' => new Clients(['agency_id' => $model->agency_id])
    ]); ?>
</div><!-- #dialog-message -->
