<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\overlays\Marker;
use common\grid\TypeWorkerEnum;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\ForRent */
/* @var $rent common\modules\agency\models\Rent */
 
$rent = $model->rent;
$client = $model->client;
$this->title = Yii::$app->formatter->asDate($model->check_in) . ' - ' . Yii::$app->formatter->asDate($model->check_out);

$this->params['pageTitle'] = $this->title;
if ($model->status==10) {
    $this->params['pageTitle'] .= ' &nbsp; <span class="label label-success">';
    $this->params['pageTitle'] .= Yii::t("common", "The order is close");
    $this->params['pageTitle'] .= '</span>';
}

$this->params['place'] = 'сlients';

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Objects fot rent'), 'url' => ['rent/index']];
$this->params['breadcrumbs'][] = ['label' => $rent->title, 'url' => ['rent/view', 'id' => $rent->id]];

$this->params['breadcrumbs'][] = $this->title;
?>


<div class="rent-form" id="content">

    <?php if ($model->delete) {
        echo $this->render('/_delete', ['model'=>$model]);
    } elseif ($model->reservations == $model::RESERVATIONS_DELETE) {
        ?>
    <div class="alert alert-danger" role="alert">
        <?= Yii::t("common", "Reservation was canceled"); ?>
    </div>
        <?php
    } else {
        echo $this->render('_status', ['model'=>$model]);
    } ?>
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">


            <div class="jarviswidget" data-widget-togglebutton="false" data-widget-editbutton="false"
                 data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false"
                 role="widget">

                <header role="heading">

                    <ul class="nav nav-tabs in" id="myTab">
                        <li class="active">
                            <a data-toggle="tab" href="#s1" aria-expanded="true">
                                <i class="fa fa-building"></i>
                                <span class="hidden-mobile hidden-tablet">
                                    <?= Yii::t("common", "General information"); ?>
                                </span>
                            </a>
                        </li>

                        <li class="">
                            <a data-toggle="tab" href="#s2" aria-expanded="true">
                                <i class="fa fa-building"></i>
                                <span class="hidden-mobile hidden-tablet">
                                    <?= Yii::t("common", "Client Information"); ?>
                                </span>
                            </a>
                        </li>


                        <li class="">
                            <a data-toggle="tab" href="#s3" aria-expanded="true">
                                <i class="fa fa-building"></i>
                                <span class="hidden-mobile hidden-tablet">
                                    <?= Yii::t("common", "Client payments"); ?>
                                </span>
                            </a>
                        </li>

                        <li class="">

                        </li>
                    </ul>

                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div class="no-padding" role="content">
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">

                    </div>
                    <!-- end widget edit box -->

                    <div class="widget-body">
                        <!-- content -->
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade active in padding-10 no-padding-bottom" id="s1">
                                <p>
                                    <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], [
                                        'class' => 'btn btn-primary'
                                    ]) ?>
                                    <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
                                        'class' => 'btn btn-danger',
                                        'data' => [
                                            'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                                            'method' => 'post',
                                        ],
                                    ]) ?>
                                </p>
                                <?= DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [
                                        'check_in:datetime',
                                        'check_out:datetime',
                                        'price_all',
                                        'price_day',
                                        'price_prepay',
                                        'price_deposit',
                                        'rent.address_text',
                                    ],
                                ]) ?>
                                <?php

                                $coord = new LatLng(['lat' => $rent->latitude, 'lng' => $rent->longitude]);
                                $map = new Map([
                                    'center' => $coord,
                                    'zoom' => 17,
                                ]);
                                $marker = new Marker(['position' => $coord]);
                                $map->addOverlay($marker);
                                $map->width = '100%';
                                $map->height = "150";
                                echo $map->display();
                                ?>
                                <h5><?= Yii::t("common", "Comment"); ?>:</h5>
                                <p><?= $model->comment; ?></p>
                            </div>
                            <div class="tab-pane fade padding-10" id="s2">
                                <?php if (!is_null($client)) {?>
                                    <?php if ($client->getUrlAvatar(true) !== null) : ?>
                                        <img class="pull-left"
                                             src="<?= $client->getUrlAvatar(true) ?>"
                                             alt="<?= $client->fio ?>" />
                                    <?php endif; ?>
                                    <br/><br/>
                                    <?= DetailView::widget([
                                        'model' => $client,
                                        'attributes' => [
                                            'fio',
                                            'email:email',
                                            'phone',
                                            'balance',
                                        ],
                                    ]) ?>
                                <?php } ?>
                            </div>
                            <div class="tab-pane fade padding-10" id="s3">
                                <?=$this->render('_payments', [
                                    'clientPaymentsProvider'=>$clientPaymentsProvider,
                                    'id'=>$model->id
                                ]);?>
                            </div>
                            <div class="tab-pane fade" id="s4">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </article>


        <article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">

            <div class="jarviswidget" data-widget-colorbutton="false" data-widget-editbutton="false"
                 data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false"
                 data-widget-custombutton="false" data-widget-sortable="false">
                <header role="heading">
                    <h2> <?= Yii::t('common', 'Workers'); ?> </h2>
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>


<div class="for-rent-view">
<?php
$workers = \common\modules\agency\models\Workers::getList($model);
if (!empty($model->workerAgent)) {
    echo '<h2 class="no-margin">'.(Yii::t("common", "Agent")).'</h2>';
    echo DetailView::widget([
        'model' => $model->workerAgent,
        'attributes' => [
            'worker.fio',
            'address_text',
            'need_to',
            'price',
            'date_event:datetime',
        ],
    ]);
} else {
    if (isset($workers[9])) {
        $workers[TypeWorkerEnum::getByKey(9)] = $workers[9];
    }
    echo Yii::t("common", "No agent");
}
if (isset($workers[9])) {
    unset($workers[9]);
}
?>
<hr/>
<?php if (!empty($model->workerCleaner)) {
    echo '<h2 class="no-margin">'.(Yii::t("common", "Cleaner")).'</h2>';
    echo DetailView::widget([
        'model' => $model->workerCleaner,
        'attributes' => [
            'worker.fio',
            'address_text',
            'need_to',
            'price',
            'date_event:datetime',
        ],
    ]);
} else {
    if (isset($workers[3])) {
        $workers[TypeWorkerEnum::getByKey(3)] = $workers[3];
    }
    echo Yii::t("common", "No employee cleaning");
}
if (isset($workers[3])) {
    unset($workers[3]);
}
?>
<hr/>
<?php if (!empty($model->workerDriver)) {
    echo '<h2 class="no-margin">' . (Yii::t("common", "Driver")) . '</h2>';
    echo DetailView::widget([
        'model' => $model->workerDriver,
        'attributes' => [
            'worker.fio',
            'address_text',
            'need_to',
            'price',
            'date_event:datetime',
        ],
    ]);
} else {
    if (isset($workers[6])) {
        $workers[TypeWorkerEnum::getByKey(6)] = $workers[6];
    }
    echo Yii::t("common", "No driver");
}
unset($workers[6]); ?>
<hr/>
<?php if (!empty($model->workerMaster)) {
    echo '<h2 class="no-margin">' . (Yii::t("common", "Master")) . '</h2>';
    echo DetailView::widget([
        'model' => $model->workerMaster,
        'attributes' => [
            'worker.fio',
            'address_text',
            'need_to',
            'price',
            'date_event:datetime',
        ],
    ]);
} else {
    if (isset($workers[7])) {
        $workers[TypeWorkerEnum::getByKey(7)] = $workers[7];
    }
    echo Yii::t("common", "No master");
}
if (isset($workers[7])) {
    unset($workers[7]);
}
?>
                    <br/>
                    <br/>
                </div>
            </div>

            <?php if (count($workers)) { ?>
                <div class="jarviswidget" data-widget-colorbutton="false" data-widget-editbutton="false"
                     data-widget-togglebutton="false" data-widget-deletebutton="false"
                     data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false">
                    <header role="heading">
                        <h2> <?= Yii::t('common', 'Add worker'); ?> </h2>
                        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                    </header>
                    <div class="for-rent-view">
                        <?= $this->render('_jobs', ['workers' => $workers, 'modelWJ' => $modelWJ]); ?>
                    </div>
                </div>
            <?php } ?>

        </article>
    </div>
</div>
