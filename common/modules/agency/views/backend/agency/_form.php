<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\modules\agency\models\AgencyTariff;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\Agency */
/* @var $form yii\widgets\ActiveForm */
/* @var $user \modules\users\models\backend\Users */

?>

<div class="agency-create">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">


        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">

            <?php if (count($model->errors)) { ?>
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-colorbutton="false"
                     data-widget-editbutton="false" data-widget-custombutton="false" role="widget">


                    <!-- widget div-->
                    <div role="">


                        <!-- widget content -->
                        <div class="widget-body ">
                            <?= $form->errorSummary($model); ?>
                        </div>
                    </div>
                </div>
            <?php } ?>


            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-colorbutton="false"
                 data-widget-editbutton="false" data-widget-custombutton="false" role="widget">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header role="heading">

                    <h2> <?= Yii::t('common', 'Information about object'); ?> </h2>

                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="">


                    <!-- widget content -->
                    <div class="widget-body ">


                        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                        <div class="form-group">
                            <?php if ($model->getUrlAvatar(true) !== null) : ?>
                                <?php if (!$model->isNewRecord) : ?>
                                    <img src="<?= $model->getUrlAvatar(true) ?>" alt="<?= $model->title ?>"/>
                                    <div>
                                        &raquo;
                                        <a href="<?= Url::to(['delete-avatar', 'id' => $model->id]) ?>">
                                            <?= Yii::t("common", "Remove avatar"); ?>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?= $form->field($model, 'logo')->fileInput() ?>
                        </div>

                        <div class="form-group">
                            <?php if ($model->watermarkUrl !== null) : ?>
                                <?php if (!$model->isNewRecord) : ?>
                                    <img src="<?= $model->watermarkUrl ?>" alt="<?= $model->title ?>"/>
                                    <div>
                                        &raquo;
                                        <a href="<?= Url::to(['delete-watermark', 'id' => $model->id]) ?>">
                                            <?= Yii::t("common", "Remove watermark"); ?>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?= $form->field($model, 'watermark')->fileInput() ?>
                        </div>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>
                        </div>


                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>

        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-6">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" >

                <header>
                    <h2> <?= Yii::t('common', 'User'); ?> </h2>
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div class="widget-body ">
                        <div class="form-group">
                            <?= $form->field($user, 'name')->textInput(['maxlength' => 50]) ?>
                        </div>

                        <div class="form-group">
                            <?= $form->field($user, 'email', ['enableAjaxValidation' => true])->textInput([
                                'maxlength' => 250
                            ]) ?>
                        </div>

                        <?php if ($user->id === Yii::$app->user->getId()) : ?>
                            <div class="form-group">
                                <?= $form->field($user, 'old_password')->passwordInput(['maxlength' => 50]) ?>
                            </div>
                        <?php endif; ?>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= $form->field($user, $user->isNewRecord ? 'password' : 'new_password')
                                        ->passwordInput(['maxlength' => 50]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= $form->field($user, 'new_password_repeat')->passwordInput([
                                        'maxlength' => 50
                                    ]) ?>
                                </div>
                            </div>
                        </div>

                        <?= $form->field($user, 'sex')->dropDownList([
                            'm' => Yii::t("common", "Men"),
                            'w' => Yii::t("common", "Women"),
                        ]); ?>

                        <?php echo $form->field($user, 'locale')->dropDownlist(Yii::$app->params['availableLocales']) ?>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

            <?php if (Yii::$app->user->can("admin")) : ?>
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-sortable">

                    <header>
                        <h2> <?= Yii::t('common', 'Payment'); ?> </h2>
                        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                    </header>

                    <!-- widget div-->
                    <div>
                        <!-- widget content -->
                        <div class="widget-body">
                            <div class="smart-form">
                                <?= $form->field($model, 'balance', [
                                    'template' => '{label}<label class="input">
                                                      <i class="icon-append fa fa-eur"></i>{input}</label>'
                                ])->widget(\yii\widgets\MaskedInput::className(), [
                                    'clientOptions' => [
                                        'alias' => 'decimal',
                                        'autoGroup' => true
                                    ],
                                ]) ?>
                            </div>

                            <br/>

                            <?= $form->field($model, 'tariff_id')->dropDownList(AgencyTariff::getList(1), [
                                'prompt' => Yii::t("common", "-- Select --")
                            ]) ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </article>
        <!-- end widget -->
    </div>


    <?php ActiveForm::end(); ?>
</div>
