<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $user \modules\users\models\backend\Users */
/* @var $model common\modules\agency\models\Agency */

$this->title = Yii::t('common', 'Update {modelClass}: ', [
        'modelClass' => 'Agency',
    ]) . $model->title;

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'agency';
$this->params['pageIcon'] = 'building';

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Agencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>


<div class="agency-create padding-10">

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user,
    ]) ?>

</div> 