<?php

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\Agency */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('common', 'Payment history');
$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'agency';

$this->params['pageImage'] = $model->getUrlAvatar(true);

$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Payment history');
?>

<div class="jarviswidget">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'tariff.tprice',
            'date_pay:datetime',
            'price',
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
