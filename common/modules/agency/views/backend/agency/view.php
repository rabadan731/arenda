<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\Agency */
/* @var $searchModel common\modules\agency\models\search\RentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataAgencyProvider yii\data\ActiveDataProvider */
/* @var $dataMangerProvider yii\data\ActiveDataProvider */
/* @var $dataClientsProvider yii\data\ActiveDataProvider */
/* @var $dataWorkersProvider yii\data\ActiveDataProvider */
/* @var $forRentProvider yii\data\ActiveDataProvider */


$this->title = $model->title;

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'agency';
$this->params['breadcrumbs'][] = $this->title;

$errorText = Yii::t("common", "Error saving data");

$js = <<< JS
    $("#dialog-new-user").dialog({
        autoOpen : false,
        modal : true,
        width: 600,
        title : 'Новый пользователь'
    });
    $("#dialog-set-tariff").dialog({
        autoOpen : false,
        modal : true,
        width: 600,
        title : 'Установить тариф'
    });
    
    $('#dialog-set-tariff-button').click(function() {
        $("#dialog-set-tariff").dialog('open');
        return false;
    });
    
    $('#dialog-new-user-button').click(function() {
        $("#dialog-new-user").dialog('open');
        return false;
    });
    
    
    
$('body').on('beforeSubmit', 'form.add_user', function () {

     var form = $(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }

     // submit form
     $.ajax({
          url: form.attr('action')+'&save=1',
          type: 'post',
          data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
          contentType: false,       // The content type used when sending data to the server.
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false 

          success: function (response) {
               if (response.success) {
                    $("#dialog-new-user").dialog('close');
                    $.pjax.reload({container:"#pjax-users"});
                    $(".modal-backdrop").remove();
               } else {
                   alert('$errorText');
               }
          }
     });
     return false;


});

$('body').on('beforeSubmit', 'form#clients-form', function () {

     var form = $(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }
     // submit form
     $.ajax({
          url: '/backend/clients/clients/create/?save=1&agency_id=$model->id',
          type: 'post',
          data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
          contentType: false,       // The content type used when sending data to the server.
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false 
          success: function (response) {
               if (response.success) {
                    $("#addClient").modal('hide');
                    $.pjax.reload({container:"#pjax-clients"});
                    $(".modal-backdrop").remove();
               } else {
                   alert('$errorText');
               }
          }
     });
     return false;
});

$('body').on('beforeSubmit', 'form#worker-form', function () {

     var form = $(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }
     // submit form
     $.ajax({
          url: '/backend/agency/workers/create/?save=1&agency_id=$model->id',
          type: 'post',
          data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
          contentType: false,       // The content type used when sending data to the server.
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false 
          success: function (response) {
               if (response.success) {
                    $("#addWorker").modal('hide');
                    $.pjax.reload({container:"#pjax-workers"});
                    $(".modal-backdrop").remove();
               } else {
                   alert('$errorText');
               }
          }
     });
     return false;
});


JS;
$this->registerJs($js);


?>


    <div class="agency-form" id="content">
        <?php if ($model->delete) { ?>
            <?php echo $this->render('/_delete', ['model' => $model]); ?>
        <?php } ?>

        <div class="jarviswidget" data-widget-togglebutton="false" data-widget-editbutton="false"
             data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false"
             role="widget">

            <header role="heading">

                <ul class="nav nav-tabs in" id="myTab">
                    <li class="active">
                        <a data-toggle="tab" href="#s1" aria-expanded="true">
                            <i class="fa fa-building"></i>
                        <span class="hidden-mobile hidden-tablet">
                            <?= Yii::t('common', 'Objects fot rent'); ?>
                        </span>
                        </a>
                    </li>

                    <li class="">
                        <a data-toggle="tab" href="#s5" aria-expanded="false">
                            <i class="fa fa-database"></i>
                            <span class="hidden-mobile hidden-tablet"><?= Yii::t("common", "Open orders"); ?></span>
                            <span class="badge bg-color-red"><?= $forRentProvider->count; ?></span>
                        </a>
                    </li>

                    <li class="">
                        <a data-toggle="tab" href="#s2" aria-expanded="false">
                            <i class="fa fa-calendar"></i>
                            <span class="hidden-mobile hidden-tablet">
                                <?= Yii::t("common", "Payment information"); ?>
                            </span>
                        </a>
                    </li>


                    <li class="">
                        <a data-toggle="tab" href="#s3" aria-expanded="false">
                            <i class="fa fa-cogs"></i>
                            <span class="hidden-mobile hidden-tablet"><?= Yii::t("common", "Permissions"); ?></span>
                        </a>
                    </li>

                </ul>

                <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

            <!-- widget div-->
            <div class="no-padding" role="content">

                <div class="widget-body">
                    <!-- content -->
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade active in padding-10 no-padding-bottom" id="s1">
                            <h3><?= Yii::t("common", "Free"); ?></h3>
                            <?php Pjax::begin(); ?>
                            <?= $this->render('/rent/_list', [
                                'dataProvider' => $freeProvider,
                                'searchModel' => false
                            ]); ?>
                            <?php Pjax::end(); ?>


                            <h3><?= Yii::t("common", "Freed after 3 days"); ?></h3>
                            <?php Pjax::begin(); ?>
                            <?= $this->render('/rent/_list', [
                                'dataProvider' => $soonProvider,
                                'searchModel' => false
                            ]); ?>
                            <?php Pjax::end(); ?>
                        </div>
                        <!-- end s1 tab pane -->

                        <div class="tab-pane fade padding-10" id="s2">
                            <h3><?= Yii::t("common", "Payment information"); ?></h3>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'balance',
        'tariff.title',
    ],
]); ?>
<?php if ($model->tariff_id !== null) {
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tariffDateRange',
            'tariffDaysLeft',
        ],
    ]);
} ?>
                            <br/>
                            <p>
<?php if (Yii::$app->user->can("agency") && is_null($model->tariff_id)) {
//    echo Html::button(Yii::t('common', 'Set tarif'), [
//        // other options
//        'class' => 'btn btn-success btn-sm',
//        'data' => [
//            'toggle' => 'modal',
//            'target' => '#setTariff',
//        ],
//    ]);
    echo Html::button(Yii::t('common', 'Set tarif'), [
        'id' => 'dialog-set-tariff-button',
        'class' => 'btn btn-success btn-sm'
    ]);
} ?>

<?php if (Yii::$app->user->can("agency", ['id' => $model->id]) ||
    Yii::$app->user->can("admin")) {
    echo Html::a(Yii::t('common', 'Payment history'), [
            'pay-log',
            'id' => $model->id
        ], ['class' => 'btn btn-default btn-sm']) . " ";
    echo Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], [
        'class' => 'btn btn-primary btn-sm'
    ]);
} ?>

<?php if (Yii::$app->user->can("admin")) {
    echo Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger btn-sm',
        'data' => [
            'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]);
} ?>
                            </p>
                        </div>
                        <!-- end s2 tab pane -->


                        <div class="tab-pane fade padding-10" id="s3">
                            <?php Pjax::begin(['id' => 'pjax-users']); ?>
                             
                            <h3><?= Yii::t("common", "Managers"); ?></h3>
                            <?= $this->render('_users', [
                                'dataProvider' => $dataMangerProvider
                            ]) ?>

                            <p>
                                <?= Html::button(Yii::t('common', 'Add user'), [
                                    // other options
                                    'class' => 'btn btn-success btn-sm',
                                    'id' => 'dialog-new-user-button'
//                                    'data' => [
//                                        'toggle' => 'modal',
//                                        'target' => '#addManager',
//                                    ],
                                ]) ?>
                            </p>
                            <?php Pjax::end(); ?>
                        </div>
                        <!-- end s3 tab pane -->

                        <div class="tab-pane fade padding-10" id="s5">
                            <?= \yii\grid\GridView::widget([
                                'dataProvider' => $forRentProvider,
                                'columns' => [
                                    'rent_id',
                                    'check_in:date',
                                    'check_out:date',
                                    'price_all',
                                    'price_day',
                                    'price_deposit',
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'options' => ['style' => 'width:115px;'],
                                        'template' => '<div class="text-center">{rent} </div>',
                                        'buttons' => [
                                            'rent' => function ($url, $model, $key) {
                                                return Html::a(
                                                    '<i class="fa fa-check-square"></i> ' .
                                                    (Yii::t('common', 'Close the order')),
                                                    ['for-rent/close-order', 'id' => $model['id']],
                                                    [
                                                        'class' => 'btn btn-xs btn-success',
                                                        'title' => Yii::t('common', 'Close the order'),
                                                        'data-pjax' => 0,
                                                        'target' => '_blank'
                                                    ]
                                                );
                                            },
                                        ]
                                    ],
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'options' => ['style' => 'width:175px;'],
                                        'urlCreator' => function ($action, $model, $key, $index) {
                                            return ['for-rent/' . $action, 'id' => $model->id];
                                        },
                                        'template' => '<div class="text-center"> {view2} {update} {delete}</div>'
                                    ],
                                ],
                            ]); ?>
                        </div>
                        <!-- end s5 tab pane -->
                    </div>
                    <!-- end content -->
                </div>
            </div>
            <!-- end widget div -->
        </div>
    </div>

<div id="dialog-new-user" title="<?=Yii::t('common', 'New Agency');?>">
    <?= $this->render('_new_user_form', ['model' => $new_user]); ?>
</div>

<div id="dialog-set-tariff" title="<?=Yii::t('common', 'Set tariff');?>">
<?= $this->render('_form_tariff', ['model' => $model]); ?>
</div>
