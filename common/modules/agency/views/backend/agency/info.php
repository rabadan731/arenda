<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $new_user \modules\users\models\backend\Users */
/* @var $model common\modules\agency\models\Agency */
/* @var $dataAgencyProvider yii\data\ActiveDataProvider */
/* @var $dataMangerProvider yii\data\ActiveDataProvider */
/* @var $dataClientsProvider yii\data\ActiveDataProvider */
/* @var $dataWorkersProvider yii\data\ActiveDataProvider */

$this->title = $model->title;

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'agency';
$this->params['pageIcon'] = 'building';

$this->params['pageImage'] = $model->getUrlAvatar(true);

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Agencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$errorText = Yii::t("common", "Error saving data");

$js = <<< JS

$('body').on('beforeSubmit', 'form.add_user', function () {

     var form = $(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }

     // submit form
     $.ajax({
          url: form.attr('action')+'&save=1',
          type: 'post',
          data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
          contentType: false,       // The content type used when sending data to the server.
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false 

          success: function (response) {
               if (response.success) {
                    $("#addManager").modal('hide');
                    $.pjax.reload({container:"#pjax-users"});
                    $(".modal-backdrop").remove();
               } else {
                   alert('$errorText');
               }
          }
     });
     return false;


});

$('body').on('beforeSubmit', 'form#clients-form', function () {

     var form = $(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }
     // submit form
     $.ajax({
          url: '/backend/clients/clients/create/?save=1&agency_id=$model->id',
          type: 'post',
          data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
          contentType: false,       // The content type used when sending data to the server.
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false 
          success: function (response) {
               if (response.success) {
                    $("#addClient").modal('hide');
                    $.pjax.reload({container:"#pjax-clients"});
                    $(".modal-backdrop").remove();
               } else {
                   alert('$errorText');
               }
          }
     });
     return false;
});

$('body').on('beforeSubmit', 'form#worker-form', function () {

     var form = $(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }
     // submit form
     $.ajax({
          url: '/backend/agency/workers/create/?save=1&agency_id=$model->id',
          type: 'post',
          data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
          contentType: false,       // The content type used when sending data to the server.
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false 
          success: function (response) {
               if (response.success) {
                    $("#addWorker").modal('hide');
                    $.pjax.reload({container:"#pjax-workers"});
                    $(".modal-backdrop").remove();
               } else {
                   alert('$errorText');
               }
          }
     });
     return false;
});


JS;
$this->registerJs($js);

?>
<div class="jarviswidget">
    <div class="row">
        <div class="col-md-6">
            <h3><?= Yii::t("common", "Payment information"); ?></h3>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'balance',
        'tariff.title',
    ],
]); ?>

<?php if ($model->tariff_id !== null) {
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tariffDateRange',
            'tariffDaysLeft',
        ],
    ]);
} ?>

            <br/>
            <p>
<?php if (Yii::$app->user->can("agency") && is_null($model->tariff_id)) {
    echo Html::button(Yii::t('common', 'Set tariff'), [
        // other options
        'class' => 'btn btn-success btn-sm',
        'data' => [
            'toggle' => 'modal',
            'target' => '#setTariff',
        ],
    ]);
} ?>

<?php if (Yii::$app->user->can("agency", ['id' => $model->id]) || Yii::$app->user->can("admin")) {
    echo Html::a(Yii::t('common', 'Pay log'), ['pay-log', 'id' => $model->id], [
            'class' => 'btn btn-default btn-sm'
        ]) . " ";
    echo Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], [
        'class' => 'btn btn-primary btn-sm'
    ]);
} ?>
<?php if (Yii::$app->user->can("admin")) {
    echo Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger btn-sm',
        'data' => [
            'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ]);
} ?>
            </p>

            <br/>
        </div>
        <div class="col-md-6">

        </div>
    </div>
</div>


<?php
Modal::begin(['id' => 'addManager', 'header' => '<b>' . Yii::t('common', 'New Agency') . '</b>']);
echo $this->render('_new_user_form', ['model' => $new_user]);
Modal::end();
Modal::begin(['id' => 'setTariff', 'header' => '<b>' . Yii::t('common', 'Set tariff') . '</b>']);
echo $this->render('_form_tariff', ['model' => $model]);
Modal::end();

