<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\modules\agency\models\Agency;

/* @var $this yii\web\View */
/* @var $model \modules\users\models\backend\Users */
/* @var $form yii\widgets\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<div class="jarviswidget">

    <?php $form = ActiveForm::begin(['options'=>['class'=>'add_user']]); ?>

    <div class="form-group">
        <?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>
    </div>


    <div class="form-group">
        <?= $form->field($model, 'email', ['enableAjaxValidation' => true])->textInput(['maxlength' => 250]) ?>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => 50]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'new_password_repeat')->passwordInput(['maxlength' => 50]) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?= $form->field($model, 'sex')->dropDownList([
                    'm' => Yii::t("common", "Men"),
                    'w' => Yii::t("common", "Women"),
                ]); ?>
            </div>
        </div>
        <div class="col-md-6">
            <?php echo $form->field($model, 'locale')->dropDownlist(Yii::$app->params['availableLocales']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Add'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
