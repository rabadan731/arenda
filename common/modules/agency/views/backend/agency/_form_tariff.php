<?php

use yii\helpers\Html;
use common\modules\agency\models\AgencyTariff;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\Agency */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agency-form-tariff">
<?php foreach (AgencyTariff::find()->all() as $tariff) {
    $params = [
        'class' => 'btn btn-primary btn-sm'
    ];

    if ($model->balance<$tariff->price) {
        $params['disabled'] = 'disabled';
    } else {
        $params['data'] = [
            'confirm' => $tariff->title .' - '.$tariff->price,
            'method' => 'post',
        ];
    }

    $url = ($model->balance>=$tariff->price)?['set-tariff', 'id' => $model->id,'tariff_id'=>$tariff->id]:"#";

    echo Html::a($tariff->title.' - '.$tariff->price, $url, $params).'<br /><br />';
} ?>
</div>
