<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\agency\models\search\AgencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Agencies');
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'building';
$this->params['place'] = 'agency';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="jarviswidget">
    <div class="agency-index">

        <p>
            <?= Html::a(Yii::t('common', 'Add Agency'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'title', 
                    'status', 
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'options' => ['style'=>'width:115px;'],
                        'template' => '<div class="text-center">{auth} </div>',
                        'buttons' => [
                            'auth' => function ($url, $model, $key) {
                                return Html::a(
                                    '<i class="fa fa-mail-forward"></i> ' . Yii::t('common', 'Authorization'),
                                    ['reauth', 'id' => $model['id']],
                                    ['class' => 'btn btn-xs btn-success', 'data-pjax' => 0]
                                );
                            },
                        ]
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '<div class="text-center"> {view} {update} {delete}</div>'
                    ],
                ],
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
