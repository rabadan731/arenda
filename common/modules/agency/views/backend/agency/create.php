<?php

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\Agency */
/* @var $user \modules\users\models\backend\Users */

$this->title = Yii::t('common', 'Add Agency');

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'agency';
$this->params['pageIcon'] = 'building';

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Agencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="agency-create padding-10">

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user,
    ]) ?>

</div>