<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'style' => 'width: 30px;',
        ]
    ],
    'name',
    'email',
    [
        'attribute' => 'sex',
        'value' => function ($model, $key, $index, $column) {
            return $model['sex'] === 'w' ? Yii::t("common", "Women") : Yii::t("common", "Men");
        },
    ],
];

if (Yii::$app->user->can('users_crud')) {
    $columns[] = [
        'class' => 'yii\grid\ActionColumn',
        'urlCreator' => function ($action, $model, $key, $index) {
            return ['/users/default/' . $action, 'id' => $model->id];
        },
        'template' => '<div style="width:135px;" class="text-center">{view} {update} {unlink}</div>',
        'buttons' => [
            'view' => function ($url, $model, $key) {
                return Html::a('<i class="fa fa-eye"></i>', $url, [
                    'class' => 'btn btn-xs btn-info',
                    'title' => Yii::t("common", "View"),
                    'data-pjax' => 0,
                    'target' => '_blank'
                ]);
            },
            'update' => function ($url, $model, $key) {
                return Html::a('<i class="fa fa-edit"></i>', $url, [
                    'class' => 'btn btn-xs btn-warning',
                    'title' => Yii::t("common", "Change"),
                    'data-pjax' => 0,
                    'target' => '_blank'
                ]);
            },
            'unlink' => function ($url, $model, $key) {
                return Html::a('<i class="fa fa-trash-o"></i> ' . Yii::t("common", "To decouple"), [
                    'unlink',
                    'user_id' => $model->id
                ], [
                    'class' => 'btn btn-xs btn-danger',
                    'data-method' => 'post',
                    'title' => Yii::t("common", "To decouple"),
                    'data' => [
                        'confirm' => Yii::t("common", "Are you sure you want to unlink this user?")
                    ],
                ]);
            }
        ]
    ];
} else {
    $columns[] = [
        'class' => 'yii\grid\ActionColumn',
        'urlCreator' => function ($action, $model, $key, $index) {
            return ['/users/default/' . $action, 'id' => $model->id];
        },
        'template' => '<div class="text-center">{view}</div>',
        'buttons' => [
            'view' => function ($url, $model, $key) {
                return Html::a('<i class="fa fa-eye"></i>', $url, [
                    'class' => 'btn btn-xs btn-info',
                    'title' => Yii::t("common", "View"),
                    'data-pjax' => 0,
                    'target' => '_blank'
                ]);
            },
        ]
    ];
}
Pjax::begin();
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns
]);
Pjax::end();
