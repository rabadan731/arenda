<?php

/* @var $this yii\web\View */
/* @var $eavParamDropDown [] */
/* @var $model common\modules\agency\models\Rent */

$this->title = Yii::t('common', 'Add object');

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'rent';

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Rents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rent-create">
    <div id="content" style="opacity: 1;">
        
        <?php echo $this->render('_form', [
            'model' => $model,
            'eavParamDropDown' => $eavParamDropDown,
        ])  ?>
    </div>
</div>
