<?php

use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\modules\agency\models\Agency;
use common\components\images\widgets\Uploadifive;
use common\modules\eav\models\EavList;
use kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\Rent */
/* @var $form yii\widgets\ActiveForm */
/* @var $eavParam [] */

$model->contract_date_begin = date(
    "Y-m-d",
    empty($model->contract_date_begin) ? time() : strtotime($model->contract_date_begin)
);
$model->contract_date_end = date(
    "Y-m-d",
    empty($model->contract_date_end) ? time() : strtotime($model->contract_date_end)
);

$js = <<< JS
$('body').on('change', '#img_sort', function () {

    $.ajax({
           url: $(this).data('url'),
           type: 'post',
           data: {img_sort:$(this).val()},
           cache: false,             // To unable request pages to be cached
           success: function (response) {
        if (response.success) {
            $.pjax.reload({container:"#list-images"}); 
              } else {
            alert(response.text);
        }
    }
        });
     
});

$('body').on('dblclick', '#img_sort-sortable img', function () {
    if (confirm($(this).data('confirm-delete'))) {
        $.ajax({
           url: $(this).data('delete-url'),
           type: 'post',
           cache: false,             // To unable request pages to be cached
           processData: false,    
           success: function (response) {
            if (response.success) {
                $.pjax.reload({container:"#list-images"}); 
              } else {
                alert(response.text);
            }
        }
        });
     }
});

JS;

$this->registerJs($js);
?>

<div class="rent-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <?php if (count($model->errors)) { ?>
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-colorbutton="false"
                 data-widget-editbutton="false" data-widget-custombutton="false" role="widget">


                <!-- widget div-->
                <div role="">


                    <!-- widget content -->
                    <div class="widget-body ">
                        <?= $form->errorSummary($model); ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" data-widget-colorbutton="false"
                 data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

                <header role="heading">
                    <h2> <?= Yii::t('common', 'Information about object'); ?> </h2>
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="">


                    <!-- widget content -->
                    <div class="widget-body ">

                        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'description')->textarea(['row' => 4]) ?>

                        <fieldset>

                            <?= $form->field($model, 'address_text')->textInput(['maxlength' => true]) ?>

                            <div class="row">
                                <div class="col-md-4">
                                    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-4">
                                    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-4 text-center">
                                    <label><?= Yii::t("common", "Query coordinate"); ?></label>
                                    <?= Html::button(Yii::t("common", "Find at the address"), [
                                        'class' => 'btn btn-default',
                                        'data-address' => 'rent-address_text',
                                        'data-lat' => 'rent-latitude',
                                        'data-lng' => 'rent-longitude',
                                        'id' => 'find-coordinate'
                                    ]) ?>
                                </div>
                            </div>

                            <?php echo $this->render('_map', ['lat' => $model->latitude, 'lng' => $model->longitude]); ?>

                        </fieldset>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-colorbutton="false"
                 data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

                <header role="heading">

                    <h2> <?= Yii::t('common', 'Prices for delivery to customers'); ?> </h2>

                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="">

                    <!-- widget content -->
                    <div class="widget-body ">

                        <fieldset>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="smart-form">
                                        <?= $form->field($model, 'price_month', [
                                            'template' => '{label}<label class="input">
                                                           <i class="icon-append fa fa-eur"></i>{input}</label>'
                                        ])->widget(\yii\widgets\MaskedInput::className(), [
                                            'clientOptions' => [
                                                'alias' => 'decimal',
                                                'autoGroup' => true
                                            ],
                                        ]) ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="smart-form">
                                        <?= $form->field($model, 'price_week', [
                                            'template' => '{label}<label class="input">
                                                           <i class="icon-append fa fa-eur"></i>{input}</label>'
                                        ])->widget(\yii\widgets\MaskedInput::className(), [
                                            'clientOptions' => [
                                                'alias' => 'decimal',
                                                'autoGroup' => true
                                            ],
                                        ]) ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="smart-form">
                                        <?= $form->field($model, 'price_day', [
                                            'template' => '{label}<label class="input">
                                                           <i class="icon-append fa fa-eur"></i>{input}</label>'
                                        ])->widget(\yii\widgets\MaskedInput::className(), [
                                            'clientOptions' => ['alias' => 'decimal', 'autoGroup' => true]
                                        ]); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="smart-form">
                                        <?= $form->field($model, 'price_sale', [
                                            'template' => '{label}<label class="input">
                                                           <i class="icon-append fa fa-eur"></i>{input}</label>'
                                        ])->widget(\yii\widgets\MaskedInput::className(), [
                                            'clientOptions' => ['alias' => 'decimal', 'autoGroup' => true]
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <!-- end widget content -->
                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-colorbutton="false"
                 data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

                <header role="heading">

                    <h2> Системные поля </h2>

                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="">

                    <!-- widget content -->
                    <div class="widget-body ">

                        <fieldset>
                            <label><?= Yii::t("common", "Import"); ?></label>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'import')->widget(
                                        SwitchInput::classname(), [
                                            'pluginOptions' => [
                                                'onText' => 'Импортировать объект',
                                                'offText' => 'НЕ импортировать объект'
                                            ]
                                        ]
                                    ); ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'status_rent')
                                        ->widget(SwitchInput::classname(), ['pluginOptions' => [
                                            'onText' => '&nbsp; &nbsp; &nbsp; Сдавать объект &nbsp; &nbsp; &nbsp;',
                                            'offText' => 'Объект НЕ сдается'
                                        ]
                                    ]); ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                            <?= $form->field($model, 'status_buy')->widget(SwitchInput::classname(), [
                                'pluginOptions' => [
                                    'onText' => 'Объект продается',
                                    'offText' => 'Объект НЕ продается',
                                ]
                            ]); ?>
                                </div>
                            </div>


                            <?php if (Yii::$app->user->can('admin')) { ?>
                                <?= $form->field($model, 'agency_id')->dropDownList(
                                    Agency::getAgencies()
                                ); ?>
                            <?php } else { ?>
                                <?= $form->field($model, 'agency_id')->hiddenInput([
                                    'value'=>Yii::$app->user->identity->agency_id
                                ])->label(false) ?>
                            <?php } ?>
                            <hr/>


                        </fieldset>
                    </div>
                    <!-- end widget content -->
                </div>
                <!-- end widget div -->
            </div>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('common', 'Save'), [
                    'class' => 'btn btn-primary  btn-lg'
                ]); ?>
            </div>
            <!-- end widget -->
        </article>
        <!-- WIDGET END -->



        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-sortable"
                 id="wid-id-0"
                 data-widget-colorbutton="false"
                 data-widget-editbutton="false"
                 data-widget-custombutton="false"
                 role="widget">

                <header role="heading">
                    <h2> <?= Yii::t('common', 'Information about the owner'); ?> </h2>
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="">

                    <!-- widget content -->
                    <div class="widget-body ">

                        <fieldset>
                            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'email')
                                        ->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'phone')
                                        ->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <!-- end widget content -->
                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-sortable">

                <header>

                    <h2> <?= Yii::t('common', 'Information about the contract'); ?> </h2>

                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="">
                    <!-- widget content -->
                    <div class="widget-body ">
                        <fieldset>
                            <div>
                                <?php
                                echo '<label class="control-label">';
                                echo Yii::t("common", "Select date range");
                                echo '</label>';
                                echo DatePicker::widget([
                                    'model' => $model,
                                    'attribute' => 'contract_date_begin',
                                    'attribute2' => 'contract_date_end',
                                    'options' => ['placeholder' => 'Start date'],
                                    'options2' => ['placeholder' => 'End date'],
                                    'type' => DatePicker::TYPE_RANGE,
                                    'form' => $form,
                                    'pluginOptions' => [
                                        'format' => 'yyyy-mm-dd g:i',
                                        'autoclose' => true,
                                    ]
                                ]); ?>
                            </div>
                        </fieldset>
                        <br/>
                        <br/>
                        <div class="row">
                            <div class="smart-form col-lg-4 col-md-6">
                                <?= $form->field(
                                    $model,
                                    'contract_month_price',
                                    [
                                        'template' => '{label}<label class="input">
<i class="icon-prepend fa fa-eur"></i>{input}</label>'
                                    ]
                                )->widget(\yii\widgets\MaskedInput::className(), [
                                    'clientOptions' => [
                                        'alias' => 'decimal',
                                        'autoGroup' => true
                                    ],
                                ]) ?>
                                <br/>
                            </div>

                            <div class="smart-form col-lg-4 col-md-6">
                                <?= $form->field(
                                    $model,
                                    'contract_week_price',
                                    [
                                        'template' => '{label}<label class="input">
                                                            <i class="icon-prepend fa fa-eur"></i>{input}</label>'
                                    ]
                                )->widget(\yii\widgets\MaskedInput::className(), [
                                    'clientOptions' => [
                                        'alias' => 'decimal',
                                        'autoGroup' => true
                                    ],
                                ]) ?>
                                <br/>
                            </div>

                            <div class="smart-form col-lg-4 col-md-6">
                                <?= $form->field(
                                    $model,
                                    'contract_day_price',
                                    [
                                        'template' => '{label}<label class="input">
                                                            <i class="icon-prepend fa fa-eur"></i>{input}</label>'
                                    ]
                                )->widget(\yii\widgets\MaskedInput::className(), [
                                    'clientOptions' => [
                                        'alias' => 'decimal',
                                        'autoGroup' => true
                                    ],
                                ]) ?>
                                <br/>
                            </div>
                        </div>
                    </div>
                    <!-- end widget content -->
                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

            <?php if (!$model->isNewRecord) { ?>
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

                    <header role="heading">

                        <h2> <?= Yii::t('common', 'Photo'); ?> </h2>

                        <span class="jarviswidget-loader">
                            <i class="fa fa-refresh fa-spin"></i>
                        </span>
                    </header>

                    <!-- widget div-->
                    <div>
                        <!-- widget content -->
                        <div class="widget-body">

                            <div class="row">
                                <div class="col-md-8">

                                    <?php
                                    Pjax::begin(['id' => 'list-images', 'timeout' => 1000]);
                                    $items = array_map(function ($item) {
                                        return ['content' => Html::img(
                                            $item->getUrl("x125"),
                                            ['data' =>
                                                [
                                                    'delete-url' => \yii\helpers\Url::to([
                                                        'del-img',
                                                        'id' => $item->itemId,
                                                        'alias' => $item->urlAlias
                                                    ]),
                                                    'confirm-delete' =>
                                                        Yii::t("common", "Are you sure you want to delete this item?")
                                                ]
                                            ]
                                        )];
                                    }, $model->getImages());

                                    echo \kartik\sortinput\SortableInput::widget([
                                        'name' => 'img_sort',
                                        'items' => $items,
                                        'value' => $model->img_sort,
                                        'options' => [
                                            'id' => 'img_sort',
                                            'data' => [
                                                'url' => \yii\helpers\Url::to(['set-sort', 'id' => $model->id])
                                            ]
                                        ],
                                        'sortableOptions' => [
                                            'type' => \kartik\sortable\Sortable::TYPE_GRID,
                                        ]
                                    ]);

                                    Pjax::end();
                                    ?>
                                    <p class="padding-10">
                                        <i class="fa fa-exclamation-triangle"></i>
                                        <?= Yii::t("common", "Double click on photo - it removes"); ?>
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <?php echo Uploadifive::widget([
                                        'tableAttribute' => 'rent',
                                        'idAttribute' => $model->id,
                                        'uploadScriptUrl' => 'rent/upload-img/?id=' . ($model->id),
                                        'onComplete' => "$.pjax.reload({container:'#list-images',async:false});",
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                        <!-- end widget content -->
                    </div>
                    <!-- end widget div -->
                </div>
                <!-- end widget -->

                <?php \yii\widgets\Pjax::begin(['id' => ('pjax_params_advansed_val'),'timeout' => 10000]); ?>
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-sortable">

                    <header>
                        <h2> <?= Yii::t('common', 'Parameters of object'); ?> </h2>
                    </header>

                    <!-- widget div-->
                    <div role="">
                        <!-- widget content -->
                        <div class="widget-body ">
                            <?php echo \yii\grid\GridView::widget([
                                'dataProvider' => $eavParam['provider'],
                                'columns' => [
                                    'param.title',
                                    [
                                        'attribute' => 'value',
                                        'format'=>'raw',
                                        'value'=> function ($model, $index, $widget) {
                                            return ($model->param->getValueForm($model));
                                        },
                                    ],
                                    [
                                        'attribute' => 'dimension',
                                        'format'=>'raw',
                                        'value'=> function ($model, $index, $widget) {
                                            return Html::textInput(
                                                'EavParamValue['.($model->id).'][dimension]',
                                                $model->dimension,
                                                ['class'=>'w100','maxlength' => true]
                                            );
                                        },
                                    ],
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'template'=>'{delete}',
                                        'buttons' => [
                                            'delete' => function ($url, $model) {
                                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                                    null,
                                                    [
                                                        'title' => Yii::t('yii', 'Delete'),
                                                        'data-pjax'=>'0',
                                                        'onclick' => '
                                                        if (confirm("' . Yii::t("common", "Are you sure?") . '")) {
                                                            $.ajax({
                                                                type     : "POST",
                                                                dataType : "json",
                                                                data : { delete_id : "'.($model->id).'", "'.
                                                            (Yii::$app->request->csrfParam).'" : "'.
                                                            (Yii::$app->request->getCsrfToken()).'" } ,
                                                                url  : "'.(Url::to(['delete-eav-value'])).'",
                                                                success  : function(data) {
                                                                    if (data == "GOOD") {
                                                                        $.pjax.reload({
                                                                            container:"#pjax_params_advansed_val",
                                                                            async:false
                                                                        });
                                                                    } else {
                                                                        alert("Ошибка удаления: "+data);
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    ',
                                                    ]
                                                );
                                            }
                                        ]
                                    ]
                                ],
                            ]); ?>


                            <?php if (count($eavParamDropDown)) { ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="smart-form">
                                            <label>
                                                <?= Yii::t("common", "Parameter"); ?>
                                            </label>
                                            <?php echo Html::dropDownList('eav-param-value', null, $eavParamDropDown, [
                                                'id' => 'eavParamValueParamId',
                                                'class' => 'form-control'
                                            ]) ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <br />
                                        <?php echo Html::a(Yii::t('common', 'Create'),
                null,
                [
                    'title' => Yii::t('common', 'Create'),
                    'class' => 'btn btn-primary',
                    'data-pjax'=>'0',
                    'onclick' => '
                        $.ajax({
                            type     : "POST",
                            dataType : "json",
                            data : {
                                model_id : "'.($model->id).'",
                                param_id : $("#eavParamValueParamId").val(), 
                                "'.(Yii::$app->request->csrfParam).'" : "'.(Yii::$app->request->getCsrfToken()).'"
                            },
                            url  : "'.(Url::to(['create-eav-value'])).'",
                            success  : function(data) {
                                if (data == "GOOD") {
                                    $.pjax.reload({container:"#pjax_params_advansed_val",async:false});
                                    $("#eavParamValueParamId").val("");
                                } else {
                                    alert("Ошибка сохранения: "+data);
                                }
                            }
                        });
                    ',
                ]
            );  ?>
                                    </div>
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="smart-form">
                                            <label>
                                                <?= Yii::t('common', 'Kit parameters'); ?>
                                            </label>
                                            <?php echo Html::dropDownList('eav-param-value', null, EavList::getDropDownList(), [
                                                'id' => 'eavParamKitId',
                                                'class' => 'form-control'
                                            ]) ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <br />
            <?php echo Html::a(Yii::t('common', 'Create'),
                null,
                [
                    'title' => Yii::t('common', 'Create'),
                    'class' => 'btn btn-primary',
                    'data-pjax'=>'0',
                    'onclick' => '
                        $.ajax({
                            type     : "POST",
                            dataType : "json",
                            data : {
                                model_id : "'.($model->id).'",
                                param_id : $("#eavParamKitId").val(), 
                                "'.(Yii::$app->request->csrfParam).'" : "'.(Yii::$app->request->getCsrfToken()).'"
                            },
                            url  : "'.(Url::to(['create-eav-kit'])).'",
                            success  : function(data) {
                                if (data == "GOOD") {
                                    $.pjax.reload({container:"#pjax_params_advansed_val",async:false});
                                    $("#eavParamKitId").val("");
                                } else {
                                    alert("Ошибка сохранения: "+data);
                                }
                            }
                        });
                    ',
                ]
            );  ?>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <?= Yii::t("common", "All parameters have been added"); ?>
                            <?php } ?>
                        </div>
                        <!-- end widget content -->
                    </div>
                    <!-- end widget div -->
                </div>
                <!-- end widget -->
                <?php \yii\widgets\Pjax::end(); ?>
            <?php } else { ?>
                <p class="padding-10">
                    <i class="fa fa-exclamation-triangle"></i>
                    <?= Yii::t("common", "To add photos after you save the object"); ?>
                </p>
            <?php } ?>
        </article>
        <!-- WIDGET END -->
    </div>
    <?php ActiveForm::end(); ?>
</div>
