<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $title string */
/* @var $searchModel common\modules\agency\models\search\RentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataProviderMy yii\data\ActiveDataProvider */
/* @var $dataProviderOthers yii\data\ActiveDataProvider */

$this->title = $title;

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'rent';

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jarviswidget">
    <div class="rent-index">


        <h2><?=Yii::t('common', 'My objects');?></h2>
        <?php Pjax::begin(); ?>
        <?=$this->render('_list', [
            'dataProvider' => $dataProviderMy, 
            'searchModel'=>false, 
            'showStatus' => true
        ]);?>
      
        <h2><?=Yii::t('common', 'Objects other agencies');?></h2>
        <?=$this->render('_list', [
            'dataProvider' => $dataProviderOthers,
            'searchModel'=> false,
            'showStatus' => true
        ]);?>


        <?php Pjax::end(); ?>
    </div>
</div>




















