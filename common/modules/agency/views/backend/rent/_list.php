<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $dataProvider yii\data\ActiveDataProvider */

$columns = [
    'title',
    [
        'attribute' => 'status',
        'filter' => false,
        'options' => ['style' => 'width:165px;'],
        'format' => 'html',
        'value' => function ($data) {
            return $data->statusLabel;
        },
    ],
    'address_text',
    'price_day',
    'author.name',
    'agency.title',
];

$columns[] = [
    'class' => 'yii\grid\ActionColumn',
    'options' => ['style' => 'width:115px;'],
    'template' => '<div class="text-center">{rent} </div>',
    'buttons' => [
        'rent' => function ($url, $model, $key) {
            /** @var $model \common\modules\agency\models\Rent */
            return $model->status_rent ? Html::a(
                '<i class="fa fa-building"></i> ' . (Yii::t('common', 'Pass the object')),
                ['for-rent/create', 'id' => $model['id']],
                [
                    'class' => 'btn btn-xs btn-success',
                    'title' => Yii::t('common', 'Pass the object'),
                    'data-pjax' => 0,
                    'target' => '_blank'
                ]
            ) : "";
        },
    ]
];

$columns[] = [
    'class' => 'yii\grid\ActionColumn',
    'urlCreator' => function ($action, $model, $key, $index) {
        return ["/agency/rent/{$action}", 'id' => $model->id];
    },
    'options' => ['style' => 'width:175px;'],
    'template' => '<div class="text-center"> {view2} {update} {delete}</div>'
];

if (!isset($showStatus) || $showStatus === false) {
    unset($columns[1]);
} ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns
]);
