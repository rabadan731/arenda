<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\main\models\Mail */
/* @var $rent \common\modules\agency\models\Rent */
/* @var $form yii\widgets\ActiveForm */

$this->title = $rent->title . Yii::t("common","Send email");

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'rent-email';

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Objects fot rent'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $rent->title, 'url' => ['view', 'id' => $rent->id]];
$this->params['breadcrumbs'][] = Yii::t("common","Send email");

?>

<hr />
<div class="padding-10">
    <div class="row">
        <div class="col-md-4">
            <div class="jarviswidget">
                <div class="arenda-mail-form padding-10">

                    <?php if ($model->status == $model::STATUS_SENT) { ?>
                        <div class="alert alert-success" role="alert">
                            <h4>
                                <strong>
                                    <?= Yii::t("common", "Email sent"); ?>
                                </strong>
                                (<?=Yii::$app->formatter->asDatetime($model->data_sent, "long");?>)
                            </h4>
                        </div>
                    <?php } else {
                        if ($model->status == $model::STATUS_ERROR_SENT) { ?>
                            <div class="alert alert-warning" role="alert">
                                <h4>
                                    <strong>
                                        <?= Yii::t("common", "Error sending email"); ?>
                                    </strong>
                                    <br />
                                    <?= Yii::t("common", "Attempt to send"); ?> №<?=$model->data_try_count;?>
                                    <br />
                                    <?=Yii::$app->formatter->asDatetime($model->data_try_sent, "long");?><br />
                                </h4>
                            </div>
                        <?php }
                        if ($model->status == $model::STATUS_DRAFT) { ?>
                            <div class="alert alert-info" role="alert">
                                <h4>
                                    <strong>
                                        <?= Yii::t("common", "New email"); ?>
                                    </strong>
                                </h4>
                            </div>
                        <?php } ?>

                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($model, 'to')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'from')->textInput(['disabled' => 'disabled']) ?>

                        <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('common', 'Sent'),['class'=>'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <label>Email</label>
            <iframe style="border: 1px solid #000" width="100%" height="600" src="<?=\yii\helpers\Url::to(['email-html','id'=>$rent->id]);?>" frameborder="0" allowfullscreen>yy</iframe>
        </div>
    </div>

</div>
