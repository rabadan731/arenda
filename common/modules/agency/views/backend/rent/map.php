<?php

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\Rent */

$this->title = Yii::t("common", "Map").$model->title;

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'rent';

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Map'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


echo $this->render("_map", [
    'lat'=>$model->latitude,
    'lng'=>$model->longitude,
    'h' => '500'
]);