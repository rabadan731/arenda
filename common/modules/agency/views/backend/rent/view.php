<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\agency\models\ForRent;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\overlays\Marker;
use yii\grid\GridView;
use yii\widgets\Pjax;
use newerton\fancybox\FancyBox;
use common\modules\eav\models\EavParam;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\Rent */
/* @var $newService common\modules\agency\models\RentImportServices */
/* @var $forRentProvider yii\data\ActiveDataProvider */
/* @var $forRentProviderDelete yii\data\ActiveDataProvider */
/* @var $eavParamProvider yii\data\ActiveDataProvider */
/* @var $importServices yii\data\ActiveDataProvider */
/* @var $rentLogExchangeProvider yii\data\ActiveDataProvider */

$this->title = $model->title;

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'rent';

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Objects fot rent'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$errorText = Yii::t("common", "Error saving data");

$confirmDelete = Yii::t("common", "Are you sure you want to unlink this user?");

$js = <<< JS

$('body').on('beforeSubmit', 'form#importLinkForm', function () {
     var form = $(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }
     // submit form
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
          contentType: false,       // The content type used when sending data to the server.
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false 

          success: function (response) {
               if (response.success) { 
                    $(':input','#importLinkForm').not(':button, :submit, :reset, :hidden').val('');
                    $.pjax.reload({container:"#import-link-pjax-list"}); 
               } else {
                   alert('{$errorText}');
               }
          }
     });
     return false;
});

$('body').on('click', '#manager-add', function () {
var managerId = parseInt($("#no-manager-list").val());
    if (!isNaN(managerId)) {
        $.ajax({
          url: '/backend/agency/rent/set-manager/?id={$model->id}&manager_id='+managerId,
          type: 'post',
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false 
          success: function (response) {
          console.log(response);
               if (response.success) {
                $.pjax.reload({container:"#list-managers"}); 
               } else {
                   alert(response.text);
               }
          }
     });
    }
});

$('body').on('click', '.unset-manager', function () {
     if (confirm("$confirmDelete")) {
        $.ajax({
           url: $(this).data('url'),
           type: 'post',
           cache: false,             // To unable request pages to be cached
           processData:false,        // To send DOMDocument or non processed data file it is set to false 
           success: function (response) {
           console.log(response);
              if (response.success) {
                 $.pjax.reload({container:"#list-managers"}); 
              } else {
                 alert(response.text);
              }
           }
        });
     }
});

$('body').on('click', '#agency-add', function () {
    var agencyId = parseInt($("#no-agency-list").val());
    if (!isNaN(agencyId)) {
        $.ajax({
          url: '/backend/agency/rent/set-agency/?id={$model->id}&agency_id='+agencyId,
          type: 'post',
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false 
          success: function (response) {
          console.log(response);
               if (response.success) {
                $.pjax.reload({container:"#list-managers"}); 
               } else {
                   alert(response.text);
               }
          }
     });
    }
});

$('body').on('click', '.unset-agency', function () {
     if (confirm("$confirmDelete")) {
        $.ajax({
           url: $(this).data('url'),
           type: 'post',
           cache: false,             // To unable request pages to be cached
           processData:false,        // To send DOMDocument or non processed data file it is set to false 
           success: function (response) {
           console.log(response);
              if (response.success) {
                 $.pjax.reload({container:"#list-managers"}); 
              } else {
                 alert(response.text);
              }
           }
        });
     }
});

$('body').on('click', '.delete-import-service', function () {
     if (confirm("$confirmDelete")) {
        $.ajax({
           url: $(this).data('url'),
           type: 'post',
           cache: false,             // To unable request pages to be cached
           processData:false,        // To send DOMDocument or non processed data file it is set to false 
           success: function (response) {
           console.log(response);
              if (response.success) {
                 $.pjax.reload({container:"#import-link-pjax-list"}); 
              } else {
                 alert(response.text);
              }
           }
        });
     }
});

$('body').on('click', '.import-service', function () {
    th = $(this);
    th.children("i").addClass("fa-spin");
    $.ajax({
       url: $(this).data('url'),
       type: 'post',
       cache: false,             // To unable request pages to be cached
       processData:false,        // To send DOMDocument or non processed data file it is set to false 
       success: function (response) {
          if (response.success === false) {
             th.children("i").removeClass("fa-spin");
             th.removeClass("btn-success");
             th.addClass("btn-danger");
          }  
       }
    });
});

JS;
$this->registerJs($js);
?>


<div class="rent-form" id="content">

    <?php if ($model->delete) { ?>
        <?php echo $this->render('/_delete', ['model' => $model]); ?>
    <?php } ?>

    <p>
        <?php if ($model->status_rent) { ?>
        <?= Html::a("<i class='fa fa-key'></i> " . Yii::t('common', 'Pass the object'), [
            'for-rent/create',
            'id' => $model->id
        ], ['class' => 'btn btn-success']) ?>
        <?php } ?>

        <?= Html::a("<i class='fa fa-envelope-o'></i> &nbsp;" . Yii::t('common', 'Send email'),
            ['email', 'id' => $model->id],
            ['class' => 'btn btn-info']
        ); ?>

        <?php if ((Yii::$app->user->can('admin')) || (Yii::$app->user->can('agency', [
                'id' => $model->agency_id
            ]))
        ) { ?>
            <?= Html::a(
                Yii::t('common', 'Update'),
                ['update', 'id' => $model->id],
                ['class' => 'btn btn-primary']
            ); ?>
            <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        <?php } ?>
    </p>

    <div class="jarviswidget" data-widget-togglebutton="false" data-widget-editbutton="false"
         data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">
        <header>
            <ul class="nav nav-tabs in" id="myTab">
                <li class="active">
                    <a data-toggle="tab" href="#s1" aria-expanded="true">
                        <i class="fa fa-building"></i>
                        <span class="hidden-mobile hidden-tablet">
                            <?= Yii::t("common", "Information about object"); ?>
                        </span>
                    </a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#s2" aria-expanded="false">
                        <i class="fa fa-picture-o"></i>
                        <span class="hidden-mobile hidden-tablet">
                            <?= Yii::t("common", "Photos"); ?>
                        </span>
                    </a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#s3" aria-expanded="false">
                        <i class="fa fa-calendar"></i>
                        <span class="hidden-mobile hidden-tablet">
                            <?= Yii::t("common", "History of delivery"); ?>
                        </span>
                    </a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#s4" aria-expanded="false">
                        <i class="fa fa-cogs"></i>
                        <span class="hidden-mobile hidden-tablet">
                            <?= Yii::t("common", "Permissions"); ?>
                        </span>
                    </a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#s5" aria-expanded="false">
                        <i class="fa fa-exchange"></i>
                        <span class="hidden-mobile hidden-tablet">
                            <?= Yii::t("common", "Exchange to employment"); ?>
                        </span>
                    </a>
                </li>
            </ul>
            <span class="jarviswidget-loader">
                <i class="fa fa-refresh fa-spin"></i>
            </span>
        </header>

        <!-- widget div-->
        <div class="no-padding">
            <div class="widget-body">
                <!-- content -->
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in padding-10 no-padding-bottom" id="s1">
                        <?php if ($model->status_rent) { ?>
                        <div class="padding-10">
                            <?= $this->render('_calendar', [
                                'resources' => null,
                                'ajaxEvents' => \yii\helpers\Url::to(['jsoncalendar', 'id' => $model->id]),
                            ]); ?>
                        </div>
                        <hr/>
                            <label>Адресс:</label>
                            <span><?=$model->address_text;?></span>
                            <a href="<?= \yii\helpers\Url::to(['map', 'id'=> $model->id]);?>"
                               class="btn btn-sm btn-default"
                               target="_blank">
                                Показать на карте
                            </a>
                        <hr/>
                        <?php } ?>
                        <div class="row">
                            <div class="col-md-6">
                                <?php
                                $attributePrice = [];
                                if ($model->status_buy) {
                                    $attributePrice[] = 'price_sale';
                                }
                                if ($model->status_rent) {
                                    $attributePrice[] = 'price_month';
                                    $attributePrice[] = 'price_week';
                                    $attributePrice[] = 'price_day';
                                }
                                echo DetailView::widget([
                                    'model' => $model,
                                    'attributes' => $attributePrice
                                ]) ?>

                                <label><?= Yii::t("common", "Parameters of object"); ?></label>
                                <?php echo \yii\grid\GridView::widget([
                                    'dataProvider' => $eavParamProvider,
                                    'columns' => [
                                        'param.title',
                                        [
                                            'attribute' => 'value',
                                            'format' => 'html',
                                            'value' => function ($model, $index, $widget) {
                                                switch ($model->param->param_type) {
                                                    case EavParam::EAV_TYPE_BOOLEAN :
                                                        return $model->value ? Yii::t("common", "Yes") : Yii::t("common", "No");
                                                        break;
                                                    default :
                                                        return "$model->value $model->dimension";
                                                }
                                            },
                                        ],
                                    ],
                                ]); ?>
                            </div>
                            <div class="col-md-6">



                                <label><?= Yii::t("common", "Information about the owner"); ?></label>
                                <?= DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [
                                        'fio',
                                        'email:email',
                                        'phone',
                                    ],
                                ]) ?>
                                <label><?= Yii::t("common", "Information about the contract"); ?></label>
                                <?= DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [
                                        'contract_date_begin:date',
                                        'contract_date_end:date',
                                        'contract_month_price',
                                    ],
                                ]) ?>
                            </div>

                        </div>
                    </div>
                    <!-- end s1 tab pane -->

                    <div class="tab-pane fade padding-10" id="s2">
                        <?php
                        echo FancyBox::widget([
                            'target' => 'a[rel=fancybox]',
                            'helpers' => true,
                            'mouse' => true,
                            'config' => [
                                'maxWidth' => '90%',
                                'maxHeight' => '90%',
                                'playSpeed' => 7000,
                                'padding' => 0,
                                'fitToView' => false,
                                'width' => '70%',
                                'height' => '70%',
                                'autoSize' => false,
                                'closeClick' => false,
                                'openEffect' => 'elastic',
                                'closeEffect' => 'elastic',
                                'prevEffect' => 'elastic',
                                'nextEffect' => 'elastic',
                                'closeBtn' => false,
                                'openOpacity' => true,
                                'helpers' => [
                                    'title' => ['type' => 'float'],
                                    'buttons' => [],
                                    'thumbs' => ['width' => 68, 'height' => 50],
                                    'overlay' => [
                                        'css' => [
                                            'background' => 'rgba(0, 0, 0, 0.8)'
                                        ]
                                    ]
                                ],
                            ]
                        ]);

                        foreach ($model->getImages() as $img) {
                            echo Html::a(Html::img($img->getUrl("x225")), $img->getUrl(), [
                                'rel' => 'fancybox',
                                'class' => ' padding-10'
                            ]);
                        }
                        ?>
                    </div>
                    <!-- end s2 tab pane -->

                    <div class="tab-pane fade" id="s3">
                        <div class="padding-10">
                            <?= \yii\grid\GridView::widget([
                                'dataProvider' => $forRentProvider,
                                'columns' => [
                                    [
                                        'attribute' => 'statusText',
                                        'filter' => false,
                                        'options' => ['style' => 'width:165px;'],
                                        'format' => 'html',
                                        'value' => function ($data) {
                                            return "<span class='label label-{$data->statusText}'>" .
                                            (($data->status == ForRent::STATUS_OPEN) ?
                                                Yii::t("common", "The order is open") :
                                                Yii::t("common", "The order is close")
                                            ) .
                                            "</span>";
                                        },
                                    ],
                                    'check_in:date',
                                    'check_out:date',
                                    'price_all',
                                    'price_day',
                                    'price_deposit',
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'urlCreator' => function ($action, $model, $key, $index) {
                                            return ['for-rent/' . $action, 'id' => $model->id];
                                        },
                                        'template' => '<div class="text-center"> {view2} {update} {delete}</div>'
                                    ],
                                ],
                            ]); ?>


                            <?= \yii\grid\GridView::widget([
                                'dataProvider' => $forRentProviderDelete,
                                'columns' => [
                                    'check_in:date',
                                    'check_out:date',
                                    'price_all',
                                    'price_day',
                                    'price_deposit',
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'urlCreator' => function ($action, $model, $key, $index) {
                                            return ['for-rent/' . $action, 'id' => $model->id];
                                        },
                                        'template' => '<div class="text-center"> {view2} {update} {delete}</div>'
                                    ],
                                ],
                            ]); ?>
                        </div>
                    </div>
                    <!-- end s3 tab pane -->

                    <div class="tab-pane fade" id="s4">
                        <div class="padding-10">

                            <?php Pjax::begin(['id' => 'list-managers', 'timeout' => 10000]); ?>
                            <div class="row">
                                <div class="col-md-7">
                                    <h3 class="no-margin"><?= Yii::t("common", "Managers"); ?></h3>
                                    <?= GridView::widget([
                                        'dataProvider' => $managersProvider,
                                        'columns' => [
                                            [
                                                'attribute' => 'id',
                                                'options' => [
                                                    'style' => 'width: 30px;',
                                                ]
                                            ],
                                            [
                                                'attribute' => 'agency_id',
                                                'value' => function ($model, $key, $index, $column) {
                                                    return \common\modules\agency\models\Agency::getTitle($model['agency_id']);
                                                },
                                            ],
                                            'name',
                                            'email',
                                            [
                                                'attribute' => 'sex',
                                                'value' => function ($model, $key, $index, $column) {
                                                    return $model['sex'] === 'w' ? Yii::t("common", "Women") : Yii::t("common", "Men");
                                                },
                                            ],
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'urlCreator' => function ($action, $model, $key, $index) {
                                                    return ['/users/default/' . $action, 'id' => $model['id']];
                                                },
                                                'template' => '<div class="text-center">{unlink} {view}</div>',
                                                'buttons' => [
                                                    'view' => function ($url, $model, $key) {
                                                        return Html::a('<i class="fa fa-eye"></i>', $url, [
                                                            'class' => 'btn btn-xs btn-info',
                                                            'title' => Yii::t("common", "View"),
                                                            'data-pjax' => 0,
                                                            'target' => '_blank'
                                                        ]);
                                                    },
                                                    'unlink' => function ($url, $manager, $key) use ($model) {
                                                        return Html::a(
                                                            '<i class="fa fa-trash-o"></i> ' .
                                                            Yii::t("common", "To decouple"),
                                                            'javascript:void(0);',
                                                            [
                                                                'class' => 'btn btn-xs btn-danger unset-agency',
                                                                'data-pjax' => 0,
                                                                'data-url' => \yii\helpers\Url::to([
                                                                    'unset-manager',
                                                                    'id' => $model->id,
                                                                    'user_id' => $manager['id']
                                                                ])
                                                            ]
                                                        );
                                                    }
                                                ]
                                            ]
                                        ],
                                    ]); ?>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <!-- end widget content -->
                                            <?= Html::dropDownList('noManager', null, $noManagers, [
                                                'prompt' => Yii::t("common", "Select Manager"),
                                                'class' => 'form-control',
                                                'id' => 'no-manager-list'
                                            ]); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?= Html::button(
                                                Yii::t("common", "Add"),
                                                ['id' => 'manager-add', 'class' => 'form-control btn btn-success']
                                            ); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php if (Yii::$app->user->can('agency', ['id' => $model->agency_id])) { ?>
                                    <div class="col-md-5">
                                        <h3 class="no-margin"><?= Yii::t("common", "Other agencies"); ?></h3>
                                        <?= GridView::widget([
                                            'dataProvider' => $rentAgencyProvider,
                                            'columns' => [
                                                'agency.title',
                                                [
                                                    'class' => 'yii\grid\ActionColumn',
                                                    'urlCreator' => function ($action, $model, $key, $index) {
                                                        return ['/users/default/' . $action, 'id' => $model['id']];
                                                    },
                                                    'template' => '<div class="text-center">{unlink} {view}</div>',
                                                    'buttons' => [
                                                        'view' => function ($url, $model, $key) {
                                                            return Html::a('<i class="fa fa-eye"></i>', $url, [
                                                                'class' => 'btn btn-xs btn-info',
                                                                'title' => Yii::t("common", "View"),
                                                                'data-pjax' => 0,
                                                                'target' => '_blank'
                                                            ]);
                                                        },
                                                        'unlink' => function ($url, $manager, $key) use ($model) {
                                                            return Html::a(
                                                                '<i class="fa fa-trash-o"></i> ' .
                                                                Yii::t("common", "To decouple"),
                                                                'javascript:void(0);',
                                                                [
                                                                    'class' => 'btn btn-xs btn-danger unset-agency',
                                                                    'data-pjax' => 0,
                                                                    'data-url' => \yii\helpers\Url::to([
                                                                        'unset-agency',
                                                                        'id' => $model->id,
                                                                        'agency_id' => $manager['agency_id']
                                                                    ])
                                                                ]
                                                            );
                                                        }
                                                    ]
                                                ]
                                            ],
                                        ]); ?>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <!-- end widget content -->
                                                <?= Html::dropDownList('noAgency', null, $othersAgency, [
                                                    'prompt' => Yii::t("common", "Choose the Agency"),
                                                    'class' => 'form-control',
                                                    'id' => 'no-agency-list'
                                                ]); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <?= Html::button(Yii::t("common", "Add"), [
                                                    'id' => 'agency-add',
                                                    'class' => 'form-control btn btn-success'
                                                ]); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <?php Pjax::end(); ?>
                            <hr/>
                        </div>
                    </div>
                    <!-- end s4 tab pane -->

                    <div class="tab-pane fade" id="s5">
                        <div class="padding-10">
                            <h3>
                                <?= Yii::t("common", "Link to export data"); ?>
                            </h3>
                            <div class="alert alert-info">
                                <i class="fa-fw fa fa-long-arrow-right"></i>
                                <strong>
                                    <?=Html::a(
                                        $model->getExportUrl(),
                                        $model->getExportUrl(),
                                        [
                                            'target' => '_blank'
                                        ]
                                    );?>
                                </strong>
                            </div>
                            <hr />
                            <h3>
                                <?= Yii::t("common", "Import data"); ?>
                            </h3>

                            <?php Pjax::begin(['id'=>"import-link-pjax-list", "timeout"=> 10000]); ?>
                                <?= GridView::widget([
                                    'dataProvider' => $importServices,
                                    'columns' => [
                                        'title',
                                        'import_url',
                                        'import_date:datetime',
                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '<div class="text-center">{reload} {unlink}</div>',
                                            'buttons' => [
                                                'reload' => function ($url, $service, $key) use ($model) {
                                                    return Html::a(
                                                        '<i class="fa fa-refresh"></i> ' .
                                                        Yii::t("common", "Reload"),
                                                        'javascript:void(0);',
                                                        [
                                                            'class' => 'btn btn-xs btn-success import-service',
                                                            'data-pjax' => 0,
                                                            'data-url' => \yii\helpers\Url::to([
                                                                'import-service-reload',
                                                                'id' => $service['service_id'],
                                                            ])
                                                        ]
                                                    );
                                                },
                                                'unlink' => function ($url, $service, $key) use ($model) {
                                                    return Html::a(
                                                        '<i class="fa fa-trash-o"></i> ' .
                                                        Yii::t("common", "Delete"),
                                                        'javascript:void(0);',
                                                        [
                                                            'class' => 'btn btn-xs btn-danger delete-import-service',
                                                            'data-pjax' => 0,
                                                            'data-url' => \yii\helpers\Url::to([
                                                                'delete-import-service',
                                                                'id' => $service['service_id'],
                                                            ])
                                                        ]
                                                    );
                                                },
                                            ]
                                        ]
                                    ]
                                ]); ?>
                            <?php Pjax::end(); ?>

                            <?php $form = ActiveForm::begin([
                                'id' => 'importLinkForm',
                                'action' => \yii\helpers\Url::to(['create-import-service', 'id'=>$model->id])
                            ]); ?>

                                <div class="row">
                                    <div class="col-md-4">
                                        <?= $form->field($newService, 'title')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-8">
                                        <?= $form->field($newService, 'import_url')->textInput(['maxlength' => true]) ?>
                                    </div>
                                </div>

                                <?= Html::submitButton(Yii::t("common","Add"), [
                                    'class' => 'btn btn-primary'
                                ]); ?>

                            <?php ActiveForm::end(); ?>

                            <hr />

                            <div>
                                <h3>
                                    <?= Yii::t("common", "Log"); ?>
                                </h3>
                                <?php Pjax::begin(['id'=>"pjax-import-list", "timeout"=> 10000]); ?>
                                <?= GridView::widget([
                                    'dataProvider' => $rentLogExchangeProvider,
                                    'columns' => [
                                        'service.title',
                                        'ip',
                                        'text',
                                        'created_at:datetime'
                                    ],
                                ]); ?>
                                <?php Pjax::end(); ?>
                            </div>
                        </div>
                    </div>
                    <!-- end s4 tab pane -->
                </div>
                <!-- end content -->
            </div>
        </div>
        <!-- end widget div -->
    </div>
</div>
