<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$columns = [
    [
        'attribute' => 'id',
        'options' => [
            'style' => 'width: 30px;',
        ]
    ],
    [
        'attribute' => 'agency_id',
        'value' => function ($model, $key, $index, $column) {
             return \common\modules\agency\models\Agency::getTitle($model['agency_id']);
        },
    ],
    'name',
    'email',

    [
        'attribute' => 'sex',
        'value' => function ($model, $key, $index, $column) {
            return $model['sex'] === 'w' ? Yii::t("common", "Women") : Yii::t("common", "Men");
        },
    ],
];


$columns[] = [
    'class' => 'yii\grid\ActionColumn',
    'urlCreator' => function ($action, $model, $key, $index) {
        return ['/users/default/' . $action, 'id' => $model['id']];
    },
    'template' => '<div class="text-center">{unlink} {view}</div>',
    'buttons' => [
        'view' => function ($url, $model, $key) {
            return Html::a('<i class="fa fa-eye"></i>', $url, [
                'class' => 'btn btn-xs btn-info',
                'title' => Yii::t("common", "View"),
                'data-pjax' => 0,
                'target' => '_blank'
            ]);
        },
        'unlink' => function ($url, $manager, $key) use ($model) {
            if (Yii::$app->user->can('admin')) {
                return Html::a(
                    '<i class="fa fa-trash-o"></i> ' . Yii::t("common", "To decouple"),
                    ['unset-manager', 'id'=>$model->id, 'user_id'=>$manager['id']],
                    [
                        'class' => 'btn btn-xs btn-danger unset-manager',
                        'data-method' => 'post',
                        'title' => Yii::t("common", "To decouple"),
                        'data' => [
                            'confirm' => Yii::t("common", "Are you sure you want to unlink this user?")
                        ]
                    ]
                );
            } else {
                return '';
            }
        }
    ]
];

Pjax::begin();
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns
]);
Pjax::end();
