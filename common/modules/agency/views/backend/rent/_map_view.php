<?php
/**
 * Created by PhpStorm.
 * User: rabadan
 * Date: 21.04.2017
 * Time: 10:55
 */


$coord = new LatLng(['lat' => $model->latitude, 'lng' => $model->longitude]);
$map = new Map([
    'center' => $coord,
    'zoom' => 17,
]);
$marker = new Marker(['position' => $coord]);
$map->addOverlay($marker);
$map->width = '100%';
$map->height = "350";
echo $map->display();
