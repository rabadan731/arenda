<?php

/* @var $this yii\web\View */
/* @var $eavParam [] */
/* @var $eavParamDropDown [] */
/* @var $model common\modules\agency\models\Rent */

$this->title = Yii::t('common', 'Update {modelClass}: ', [
    'modelClass' => 'Rent',
]) . $model->id;

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'rent';

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Rents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="rent-update">
    <div id="content">
        <?= $this->render('_form', [
            'model' => $model,
            'eavParam' => $eavParam,
            'eavParamDropDown' => $eavParamDropDown,
        ]) ?>
    </div>

</div>
