<?php
\themes\fullcalendar\Scheduler::register($this);

$this->registerCss(
    "#calendar { 
		margin: 50px auto;
	}"
);


$js = <<< JS

$(function() { // document ready

		$('#fullcalendar').fullCalendar({		
			aspectRatio: 1.8,
			height : 160,
			header: {
				left: 'today prev,next',
				center: 'title',
				right: 'timelineWeekMy,timelineMonthMy,timelineYearMy'
			},
			defaultView: 'timelineMonthMy',
             views: {
                    timelineWeekMy: {
                        type: 'timelineMonth',
                        duration: { days: 10 },
                        buttonText: $('#fullcalendar').data("text-week")
                    },
                    timelineMonthMy: {
                        type: 'timelineMonth',
                        duration: { days: 31 },
                        buttonText: $('#fullcalendar').data("text-month")
                    },
                    timelineYearMy: {
                        type: 'timelineYear',
                        duration: { days: 365 },
                        buttonText: $('#fullcalendar').data("text-year")
                    },
                    
                },
			schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
			resourceAreaWidth: '25%',
			resourceLabelText: 'Rent',
			events: '{$ajaxEvents}'
		});
	
	});
JS;
$this->registerJs($js);


?>
<div data-text-month="<?= Yii::t("common", "Month"); ?>"
     data-text-year="<?= Yii::t("common", "Year"); ?>"
     data-text-week="<?= Yii::t("common", "Week"); ?>" id="fullcalendar"></div>
<br />