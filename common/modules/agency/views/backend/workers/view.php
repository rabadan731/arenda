<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\Workers */
/* @var $jobsProvider \yii\data\ArrayDataProvider */
/* @var $workerPaymentsProvider \yii\data\ArrayDataProvider */

$this->title = $model->fio;
$this->params['pageTitle'] = $model->fio . " <small> " . $model->workerTitle . "</small>";
$this->params['place'] = 'workers';

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Workers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rent-form" id="content">

    <?php if ($model->delete) { ?>
        <?php echo $this->render('/_delete', ['model'=>$model]); ?>
    <?php } ?>
    
    <div class="jarviswidget" data-widget-togglebutton="false" data-widget-editbutton="false"
         data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false"
         role="widget">


        <header role="heading">

            <ul class="nav nav-tabs in" id="myTab">
                <li class="active">
                    <a data-toggle="tab" href="#s1" aria-expanded="true">
                        <i class="fa fa-building"></i>
                        <span class="hidden-mobile hidden-tablet"><?= Yii::t("common", "General information"); ?></span>
                    </a>
                </li>

                <li class="">
                    <a data-toggle="tab" href="#s2" aria-expanded="false">
                        <i class="fa fa-calendar"></i>
                        <span class="hidden-mobile hidden-tablet"><?= Yii::t('common', 'To-do list'); ?></span>
                    </a>
                </li>

                <li class="">
                    <a data-toggle="tab" href="#s3" aria-expanded="false">
                        <i class="fa fa-eur"></i>
                        <span class="hidden-mobile hidden-tablet"><?= Yii::t('common', 'Payment information'); ?></span>
                    </a>
                </li>
            </ul>

            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
        </header>

        <!-- widget div-->
        <div class="no-padding" role="content">
            <div class="widget-body">
                <!-- content -->
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in padding-10 no-padding-bottom" id="s1">
                        <?php if ($model->getUrlAvatar(true) !== null) { ?>
                            <img src="<?= $model->getUrlAvatar(true) ?>" alt="<?= $model->fio ?>" />
                        <?php } ?>
                        <br/><br/>
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'workerTitle',
                                'email:email',
                                'phone',
                                'balance',
                            ],
                        ]) ?>
                        <br/><br/>
                        <p>
                            <?= Html::a(
                                Yii::t('common', 'Update'),
                                ['update', 'id' => $model->id],
                                ['class' => 'btn btn-primary']
                            ); ?>
                            <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </p>
                    </div>
                    <div class="tab-pane fade padding-10" id="s2">

                        <?= GridView::widget([
                            'dataProvider' => $jobsProvider,
                            'columns' => [
                                'address_text',
                                'need_to:ntext',
                                'price',
                                'date_event:datetime',
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'urlCreator' => function ($action, $model, $key, $index) {
                                        return ['worker-jobs/' . $action, 'id' => $model->id];
                                    },
                                    'template' => '<div class="text-center"> {view2}  </div>'
                                ],
                            ],
                        ]); ?>
                    </div>
                    <div class="tab-pane fade padding-10" id="s3">
                        <?= GridView::widget([
                            'dataProvider' => $workerPaymentsProvider,
                            'columns' => [
                                'title',
                                'date_invoice:datetime',
                                'price',
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
