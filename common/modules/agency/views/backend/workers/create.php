<?php

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\Workers */

$this->title = Yii::t('common', 'Add Worker');
$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'workers';
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Workers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
 
<div class="workers-create padding-10">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div> 