<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\modules\agency\models\Agency;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\Workers */
/* @var $form yii\widgets\ActiveForm */

$model->balance = is_null($model->balance)?0:$model->balance;
?>

<div class="workers-form">
    <?php $form = ActiveForm::begin(['id'=>'worker-form']); ?>
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-sm-12 col-md-12 col-lg-6">
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-sortable">

                    <header>
                        <h2> <?= Yii::t('common', 'User'); ?> </h2>
                        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                    </header>

                    <!-- widget div-->
                    <div>
                        <!-- widget content -->
                        <div class="widget-body ">

                            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

                            <div class="form-group">
                                <?php if ($model->getUrlAvatar(true) !== null && !$model->isNewRecord) {?>
                                    <img src="<?= $model->getUrlAvatar(true) ?>" alt="<?= $model->fio ?>"/>
                                    <div>
                                        &raquo;
                                        <a href="<?= Url::to(['delete-avatar', 'id'=>$model->id]) ?>">
                                            <?= Yii::t("common", "Remove avatar"); ?>
                                        </a>
                                    </div>
                                <?php } ?>
                                <?= $form->field($model, 'photo')->fileInput() ?>
                            </div>

                            <?php if (!Yii::$app->request->isAjax) { ?>
                                <div class="form-group">
                                    <?= Html::submitButton(Yii::t('common', 'Save'), [
                                        'class'=>'btn btn-primary'
                                    ]) ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-sm-12 col-md-12 col-lg-6">
                <div class="jarviswidget jarviswidget-sortable">

                    <header>
                        <h2> <?= Yii::t('common', 'User'); ?> </h2>
                        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                    </header>

                    <!-- widget div-->
                    <div>
                        <!-- widget content -->
                        <div class="widget-body ">

                            <?= $form->field($model, 'worker_type')
                                ->dropDownList(\common\grid\TypeWorkerEnum::listData(), [
                                    'prompt'=> Yii::t('common', '-- Select --')
                                ]); ?>

                            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                            <?php if (Yii::$app->user->can('admin')) { ?>
                                <?= $form->field($model, 'agency_id')->dropDownList(Agency::getAgencies()) ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    <?php ActiveForm::end(); ?>
</div>





</div>
