<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\Workers */

$this->title = Yii::t('common', 'Update {modelClass}: ', [
        'modelClass' => Yii::t('common', 'Worker'),
    ]) . $model->fio;
$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'workers';
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Workers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fio, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>


<div class="workers-update padding-10">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>