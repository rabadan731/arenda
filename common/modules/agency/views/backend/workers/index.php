<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\agency\models\search\WorkersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Workers');
$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'workers';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="jarviswidget">
    <div class="workers-index"> 
        <p>
            <?= Html::a(Yii::t('common', 'Add worker'), ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('common', 'Payment of salaries'), ['payments'], ['class' => 'btn btn-primary']) ?>
        </p>
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'photo',
                    'filter' => false,
                    'format' => 'html',
                    'value' => function ($data) {
                        return Html::img($data->getUrlAvatar(true), ['style' => 'width:25px']);
                    },
                    'options' => ['style' => 'width:50px']
                ],
                [
                    'attribute' => 'worker_type',
                    'value' => 'workerTitle',
                    'filter' => \common\grid\TypeWorkerEnum::listData(),
                ],
                'fio',
                'email:email',
                'phone',
                'balance',
                // 'agency_id',
                // 'status',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'options' => ['style' => 'width:180px'],
                    'template' => '<div class="text-center"> {view2} {update} {delete}</div>'
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
