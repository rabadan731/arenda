<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\agency\models\search\WorkersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Payment of salaries');
$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'workers-payments';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="jarviswidget">
    <div class="workers-index">

        <?php $form = ActiveForm::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'id',
                    'options' => ['style' => 'width:50px'],
                ],
                [
                    'attribute' => 'photo',
                    'filter' => false,
                    'format' => 'html',
                    'value' => function ($data) {
                        return Html::img($data->getUrlAvatar(true), ['style' => 'width:25px']);
                    },
                    'options' => ['style' => 'width:50px']
                ],
                'fio',
                [
                    'attribute' => 'worker_type',
                    'value' => 'workerTitle',
                    'filter' => \common\grid\TypeWorkerEnum::listData(),
                ],

                'balance',
                [
                    'attribute' => 'id',
                    'filter' => false,
                    'format' => 'raw',
                    'value' => function ($data) {
                        return "<div class='smart-form'><label class='input'><i class='icon-append fa fa-eur'></i>" .
                        MaskedInput::widget([
                            'name' => "worker[" . ($data->id) . "]",
                            'clientOptions' => [
                                'alias' => 'decimal',
                                'autoGroup' => true
                            ],
                        ]) . "</label></div>";
                    },
                ],
            ],
        ]); ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('common', 'To pay'), ['class' => 'btn btn-primary pull-right']) ?>
        </div>
        <br/>
        <br/>

        <?php ActiveForm::end(); ?>
        <br/>

    </div>
</div>