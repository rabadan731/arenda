<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\AgencyTariff */

$this->title = Yii::t('common', 'Update {modelClass}: ', [
    'modelClass' => 'Agency Tariff',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Agency Tariffs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="agency-tariff-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
