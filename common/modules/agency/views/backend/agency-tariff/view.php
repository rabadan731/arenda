<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\AgencyTariff */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Agency Tariffs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rent-form" id="content">

    <?php if ($model->delete) { ?>
        <?php echo $this->render('/_delete', ['model'=>$model]); ?>
    <?php } ?>

    <div class="jarviswidget" >



    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'month',
            'price',
            'status'
        ],
    ]) ?>

    </div>
</div>
