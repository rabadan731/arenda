<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\agency\models\search\AgencyTariffSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Agency Tariffs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rent-form" id="content">
    <div class="jarviswidget" >

    <p>
        <?= Html::a(Yii::t('common', 'Create Agency Tariff'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'title',
                'month',
                'price',
                'status',
                // 'delete',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '<div class="text-center"> {view2} {update} {delete}</div>'
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
    </div>
</div>
