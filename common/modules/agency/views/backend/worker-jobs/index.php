<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\agency\models\search\WorkerJobsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Worker Jobs');

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'worker-jobs';

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="jarviswidget">
    <div class="rent-index">
 
    <p>
        <?= Html::a(Yii::t('common', 'Create Worker Jobs'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'worker_id',
                'address_text',
                'address_map',
                'need_to:ntext',
                // 'price',
                // 'date_event',
                // 'date_sent',
                // 'status',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
    </div>
</div>
