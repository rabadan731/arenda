<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\widgets\DetailView;
use common\modules\agency\models\Workers;
use common\grid\TypeWorkerEnum;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\WorkerJobs */
/* @var $form yii\widgets\ActiveForm */

$model->date_event = date("Y-m-d", is_null($model->date_event) ? time() :
    (is_int($model->date_event) ? $model->date_event : strtotime($model->date_event)));

$workers = Workers::getList($model);

if (empty($model->workerAgent)) {
    $workers[TypeWorkerEnum::getByKey(9)] = $workers[9];
}

if (empty($model->workerCleaner)) {
    $workers[TypeWorkerEnum::getByKey(3)] = $workers[3];
}

if (empty($model->workerDriver)) {
    $workers[TypeWorkerEnum::getByKey(6)] = $workers[6];
}

unset($workers[9]);
unset($workers[3]);
unset($workers[6]);

?>

    <div class="worker-jobs-form">

        <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'worker_id')->dropDownList($workers, ['prompt' => Yii::t('common', '-- Select --')]) ?>
    <?= $form->field($model, 'address_text')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'lat')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'lng')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 text-center">
            <label><?= Yii::t("common", "Query coordinate"); ?></label>
            <?= Html::button(Yii::t("common", "Find at the address"), [
                'class' => 'btn btn-default',
                'data-address' => 'workerjobs-address_text',
                'data-lat' => 'workerjobs-lat',
                'data-lng' => 'workerjobs-lng',
                'id' => 'find-coordinate'
            ]) ?>
        </div>
    </div>

    <?php echo $this->render('/for-rent/_map', ['lat' => $model->getLat(), 'lng' => $model->getLng()]); ?>
    <br/>
    <?= $form->field($model, 'need_to')->textarea(['rows' => 2]) ?>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?php
                echo DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'date_event',
                    'options' => ['placeholder' => Yii::t("common", "Date event")],
                    //  'type' => DatePicker::TYPE_RANGE,
                    'form' => $form,
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                    ],
                    'pluginEvents' => [
                        //        "changeDate" => "",
                    ]
                ]); ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="smart-form">
                <?= $form->field(
                    $model,
                    'price',
                    ['template' => '{label}<label class="input"><i class="icon-append fa fa-eur"></i>{input}</label>']
                )->widget(\yii\widgets\MaskedInput::className(), [
                    'clientOptions' => [
                        'alias' => 'decimal',
                        'autoGroup' => true
                    ],
                ]); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
