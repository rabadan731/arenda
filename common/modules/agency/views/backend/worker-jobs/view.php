<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\WorkerJobs */

$this->title = Yii::t("common", "Job on") . ' ' . Yii::$app->formatter->asDate($model->date_event);

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'rent';

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Workers'), 'url' => ['workers/index']];
$this->params['breadcrumbs'][] = ['label' => $model->worker->fio, 'url' => ['workers/view','id'=>$model->worker_id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rent-form" id="content">

<?php if ($model->delete) {
    echo $this->render('/_delete', ['model' => $model]);
} ?>
    
    <div class="row">
        
        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false"
                 data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

                <header role="heading">
                    <h2> <?= Yii::t('common', 'Information'); ?> </h2>
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>
                <!-- widget div-->
                <div role="">
                    <!-- widget content -->
                    <div class="widget-body ">
                        <p>
                            <?=Html::activeLabel($model, 'need_to');?>
                            :
                            <?= $model->need_to; ?>
                        </p>
                        <hr />
                        <p>
                            <?=Html::activeLabel($model, 'price');?>
                            :
                            <?=$model->price;?>
                            <b>
                                <i class="fa fa-eur"></i>
                            </b>
                        </p>
                        <hr />
                        <p>
                            <?=Html::activeLabel($model, 'date_event');?>
                            :
                            <?=YIi::$app->formatter->asDate($model->date_event);?>
                        </p>
                    </div>
                    <!-- end widget content -->

                    <p>
                        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], [
                            'class' => 'btn btn-primary'
                        ]) ?>
                        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                    </p>

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false"
                 data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

                <header role="heading">
                    <h2> <?= Yii::t('common', 'Address'); ?> </h2>
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>
                <!-- widget div-->
                <div role="">
                    <!-- widget content -->
                    <div class="widget-body ">
                        <p>
                            <?=Html::activeLabel($model, 'address_text');?>
                            :
                            <?=$model->address_text;?>
                        </p>
                        <?php echo $this->render('/rent/_map', [
                            'lat' => $model->getLat(),
                            'lng' => $model->getLng(),
                            'h'=>300
                        ]); ?>
                    </div>
                    <!-- end widget content -->
                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->
        </article>
        <!-- WIDGET END -->


        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false"
                 data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

                <header role="heading">
                    <h2> <?= Yii::t('common', 'Worker'); ?> </h2>
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>
                <!-- widget div-->
                <div role="">
                    <!-- widget content -->
                    <div class="widget-body ">
                        <?php if ($model->worker->getUrlAvatar(true) !== null) { ?>
                            <img src="<?= $model->worker->getUrlAvatar(true) ?>" alt="<?= $model->worker->fio ?>" />
                        <?php } ?>
                        <br/><br/>
                        <?= DetailView::widget([
                            'model' => $model->worker,
                            'attributes' => [
                                'workerTitle',
                                'email:email',
                                'phone',
                                'balance',
                            ],
                        ]) ?>
                    </div>
                    <!-- end widget content -->
                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

        </article>
        <!-- WIDGET END -->
    </div>
</div>