<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\agency\models\WorkerJobs */

$this->title = Yii::t('common', 'Update {modelClass}: ', [
    'modelClass' => 'Worker Jobs',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Worker Jobs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'worker-jobs-update';

?>
<div class="jarviswidget">
    <div class="worker-jobs-update">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
