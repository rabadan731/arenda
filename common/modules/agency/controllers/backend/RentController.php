<?php

namespace modules\agency\controllers\backend;

use common\modules\agency\models\Agency;
use common\modules\agency\models\RentAgency;
use common\modules\agency\models\RentImportList;
use common\modules\agency\models\RentImportServices;
use common\modules\agency\models\search\ForRentSearch;
use common\modules\agency\models\search\RentAgencySearch;
use common\modules\agency\models\ForRent;
use common\modules\agency\models\RentManager;
use common\modules\agency\models\Rent;
use common\modules\agency\models\search\RentSearch;
use common\modules\eav\models\EavList;
use common\modules\eav\models\EavParam;
use common\modules\eav\models\EavParamValue;
use common\modules\eav\models\search\EavParamValueSearch;
use common\modules\log\models\LogExchange;
use common\modules\main\models\KeyStorageItem;
use common\modules\main\models\Mail;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use rico\yii2images\models\Image;
use themes\fullcalendar\models\Event;
use modules\users\models\backend\Users;
use Yii;

/**
 * RentController implements the CRUD actions for Rent model.
 */
class RentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'undelete' => ['POST'],
                    'unset-manager' => ['POST'],
                    'set-manager' => ['POST'],
                    'create-import-link' => ['POST'],
                    'delete-import-link' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'index-buy',
                            'index-others',
                            'view',
                            'map',
                            'email',
                            'email-html',
                            'create-import-service',
                            'delete-import-service',
                            'import-service-reload',
                            'jsoncalendar',
                            'create'
                        ],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'set-manager',
                            'set-sort',
                            'unset-manager',
                            'set-agency',
                            'unset-agency',
                            'delete',
                            'update',
                            //                           'create-eav-value',
                            //                           'delete-eav-value',
                            'upload-img',
                            'del-img',
                        ],
                        'matchCallback' => function ($rule, $action) {
                            $m = $this->findModel(Yii::$app->getRequest()->get('id'));
                            if (is_null($m)) {
                                return false;
                            } else {
                                return true;
                            }
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'set-agency',
                            'unset-agency',
                            'delete',
                            'update',
                            'fixll',
                            'create-eav-value',
                            'delete-eav-value',
                            'create-eav-kit',
                            'undelete',
                        ],
                        'roles' => ['admin'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all Rent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RentSearch();
        $queryParams = Yii::$app->request->queryParams;
        $queryParams['RentSearch']['status_rent'] = 1;

        $dataProviderMy = $searchModel->searchMy($queryParams);
        $dataProviderOthers = $searchModel->searchOthers($queryParams);

        return $this->render('index', [
            'title' => Yii::t('common', 'The object is for rent'),
            'searchModel' => $searchModel,
            'dataProviderMy' => $dataProviderMy,
            'dataProviderOthers' => $dataProviderOthers,
        ]);
    }

    /**
     * Lists all Rent models.
     * @return mixed
     */
    public function actionIndexBuy()
    {
        $searchModel = new RentSearch();
        $queryParams = Yii::$app->request->queryParams;
        $queryParams['RentSearch']['status_buy'] = 1;
        $dataProviderMy = $searchModel->searchMy($queryParams);
        $dataProviderOthers = $searchModel->searchOthers($queryParams);

        return $this->render('index', [
            'title' => Yii::t('common', 'The object is for Sale'),
            'searchModel' => $searchModel,
            'dataProviderMy' => $dataProviderMy,
            'dataProviderOthers' => $dataProviderOthers,
        ]);
    }

    /**
     * Lists all Rent models.
     * @return mixed
     */
    public function actionIndexOthers()
    {
        $searchModel = new RentSearch();
        $queryParams = Yii::$app->request->queryParams;
        $queryParams['RentSearch']['status_rent'] = 0;
        $queryParams['RentSearch']['status_buy'] = 0;
        $dataProviderMy = $searchModel->searchMy($queryParams);
        $dataProviderOthers = $searchModel->searchOthers($queryParams);

        return $this->render('index', [
            'title' => Yii::t('common', 'Others objects'),
            'searchModel' => $searchModel,
            'dataProviderMy' => $dataProviderMy,
            'dataProviderOthers' => $dataProviderOthers,
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionEmailHtml($id)
    {
        $model = Rent::find()->andWhere(['rent.id' => $id])->one();

        return $this->renderPartial('/../../../main/views/backend/mail/tpl/rent', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @param null $mailId
     * @param int $error
     * @return string
     */
    public function actionEmail($id, $mailId = null, $error = 0)
    {
        $model = Rent::find()->andWhere(['rent.id' => $id])->one();
        $mail = is_null($mailId) ? (new Mail()) : (Mail::findOne((int)$mailId));
        $mail->from = KeyStorageItem::getValue("email.mail", 'email');
//        if (empty($mail->from)) {
//            $mail->from = Yii::$app->user->identity->email;
//        }
        if (empty($mail->subject)) {
            $mail->subject = $model->title;
        }

        $mail->html = $this->renderPartial('/../../../main/views/backend/mail/tpl/rent', [
            'model' => $model
        ]);

        if ($mail->load(Yii::$app->request->post())) {
            if ($mail->save()) {
                $mail->beforeSentMail();
                if ($mail->sent()) {
                    $mail->goodSentMail();
                    $this->redirect(['email', 'id' => $id, 'mailId' => $mail->id]);
                } else {
                    $mail->badSentMail();
                    $this->redirect(['email', 'id' => $id, 'mailId' => $mail->id]);
                }
            }
        }

        return $this->render('email', [
            'model' => $mail,
            'rent' => $model
        ]);
    }

    /**
     * Displays a single Rent model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $queryManagers = $model->getManagers();
        $managersProvider = new ActiveDataProvider([
            'query' => $queryManagers
        ]);

        $queryUsers = Users::find();
        $queryUsers->andWhere([
            'agency_id' => $model->agency_id,
            'role' => 'manager',
        ]);
        $ids = ArrayHelper::getColumn($queryManagers->asArray()->all(), 'id');
        $queryUsers->andWhere(['not in', 'id', $ids]);


        $searchModel = new RentAgencySearch();
        $searchModel->rent_id = $model->id;
        $rentAgencyProvider = $searchModel->search([]);


        $forRentSearch = new ForRentSearch();
        $forRentSearch->rent_id = $id;
        $forRentProvider = $forRentSearch->search([]);


        $forRentProviderDelete = $forRentSearch->searchReservationsDelete($id);


        $idList = ArrayHelper::getColumn($rentAgencyProvider->query->all(), 'agency_id');
        $idList[] = $model->agency_id;

        $othersAgencyQuery = Agency::find()->select('id,title')->where(['not in', 'id', $idList]);
        $othersAgency = ArrayHelper::map($othersAgencyQuery->all(), 'id', 'title');

        $eavParamSearch = new EavParamValueSearch();
        $eavParamProvider = $eavParamSearch->searchParam($model->className(), $model->id);


        $importServices = new ActiveDataProvider([
            'query' => RentImportServices::find()->andWhere(['rent_id' => $model->id])
        ]);

        $newService = new RentImportServices();
        $newService->rent_id = $model->id;
        $newService->agency_id = $model->agency_id;

        $rentLogExchangeProvider = new ActiveDataProvider([
            'query' => LogExchange::find()->andWhere(['rent_id' => $model->id])->orderBy("created_at DESC")
        ]);


        return $this->render('view', [
            'model' => $model,
            'forRentProvider' => $forRentProvider,
            'forRentProviderDelete' => $forRentProviderDelete,
            'managersProvider' => $managersProvider,
            'noManagers' => ArrayHelper::map($queryUsers->all(), 'id', 'name'),
            'othersAgency' => $othersAgency,
            'rentAgencyProvider' => $rentAgencyProvider,
            'eavParamProvider' => $eavParamProvider,
            'importServices' => $importServices,
            'newService' => $newService,
            'rentLogExchangeProvider' => $rentLogExchangeProvider
        ]);
    }

    /**
     * @param $id
     * @return array|Response
     */
    public function actionCreateImportService($id)
    {
        $rent = $this->findModel($id);
        $model = new RentImportServices();
        $model->load(Yii::$app->request->post());
        $model->rent_id = $rent->id;
        $model->agency_id = $rent->agency_id;
        $saveResult = $model->save();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => $saveResult, 'errors' => $model->errors];
        }

//        echo '<pre>' . print_r([
//                'success' => $saveResult,
//                'errors' => $model->errors,
//                'isAjax' => Yii::$app->request->isAjax,
//            ], 1) . '</pre>';

        return $this->redirect(['view', 'id' => $rent->id]);
    }

    /**
     * @param $id
     * @param null $start
     * @param null $end
     * @return array
     */
    public function actionJsoncalendar($id, $start = null, $end = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $models = ForRent::find()->andWhere(['for_rent.rent_id' => $id])->date($start, $end);

        // echo print_r($models->where);
        $events = array();

        foreach ($models->all() as $model) {
            //Testing
            $event = new Event();

            if ($model->reservations == $model::RESERVATIONS_TEMP) {
                $event->color = "#ababab";
            } else {
                if ($model->check_out <= time() && $model->status == ForRent::STATUS_OPEN) {
                    $event->color = "#e47979";
                }
                if ($model->status == ForRent::STATUS_CLOSE) {
                    $event->color = "#5cb85c";
                }
            }

            $event->id = $model->id;
            $event->resourceId = $model->rent_id;
            $event->allDay = false;
            $event->title = $model->rent->title . ' (' . date('Y-m-d', $model->check_in) .
                ' - ' . date('Y-m-d', $model->check_out) . ')';
            $event->start = date('Y-m-d H:i', $model->check_in);
            $event->end = date('Y-m-d H:i', $model->check_out);
            $event->url = Url::to(['for-rent/view', 'id' => $model->id]);
            $event->className = "calendar_item";
            $events[] = $event;
        }

        $importList = RentImportList::find()->andWhere(['rent_id' => $id])->all();

        foreach ($importList as $importItem) {
            /** @var $importItem RentImportList */
            $event = new Event();

            $event->id = "import_{$importItem->id}";
            $event->resourceId = $importItem->rent_id;
            $event->allDay = true;
            $event->title = "{$importItem->client->fio} ({$importItem->service->title}";
            $event->start = date('Y-m-d H:i', $importItem->date_from);
            $event->end = date('Y-m-d H:i', $importItem->date_to);
            //$event->url = Url::to(['for-rent/view', 'id' => $importItem->rent_id]);
            $event->className = "calendar_item";
            $events[] = $event;
        }

        return $events;
    }

    /**
     * @param $id
     * @return bool
     * @throws BadRequestHttpException
     */
    public function actionUploadImg($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $file = UploadedFile::getInstanceByName('file_upload');
            $newName = Inflector::slug($file->baseName, '_') . "_" . uniqid() . '.' . $file->extension;
            $savePath = Yii::getAlias("@runtime/") . $newName;
            $file->saveAs($savePath);

            $waterMarkSystem = KeyStorageItem::getValue('watermark');
            if (!is_null($waterMarkSystem)) {
                $waterMarkSystem = ltrim($waterMarkSystem, '/');
                //echo Yii::getAlias("@frontend/web/{$waterMarkSystem}");
                $this->setWaterMark($savePath, "@frontend/web/{$waterMarkSystem}", false);
            }

            $waterMarkAgency = $model->agency->getWatermarkPath();
            if (!is_null($waterMarkAgency)) {
                $this->setWaterMark($savePath, $waterMarkAgency, true);
            }

            $model->attachImage($savePath);

            if (is_null($model->img_sort)) {
                $imgSortArray = [];
                $imgSortArray[] = 0;
            } else {
                $imgSortArray = explode(',', $model->img_sort);
                $imgSortArray[] = count($imgSortArray);
            }
            $model->img_sort = implode(',', $imgSortArray);
            $model->save(false, ['img_sort']);

            unlink($savePath);

            return true;
        } else {
            throw new BadRequestHttpException('Only POST is allowed');
        }
    }

    /**
     * @param $id
     * @return string
     */
    public function actionMap($id)
    {
        $model = $this->findModel($id);
        return $this->render('map', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionSetSort($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $model->img_sort = Yii::$app->request->post('img_sort');
            if ($model->save(false, ['img_sort'])) {
                return ['success' => true];
            } else {
                return ['success' => false, 'text' => Yii::t('common', 'Error save data')];
            }
        } else {
            throw new BadRequestHttpException('Only POST is allowed');
        }
    }

    public function actionDelImg($id, $alias)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $image = Image::find()->where(['urlAlias' => $alias])->one();
        if (!is_null($image)) {
            $model->removeImage($image);
            if ($model->img_sort == '0') {
                $model->img_sort = null;
            } else {
                $imgSortArray = explode(',', $model->img_sort);
                unset($imgSortArray[array_search(max($imgSortArray), $imgSortArray)]);
                $model->img_sort = implode(',', $imgSortArray);
            }
            $model->save(false, ['img_sort']);
            return ['success' => true];
        } else {
            return ['success' => false, 'text' => Yii::t('common', 'Error delete image')];
        }
    }

    /**
     * Creates a new Rent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Rent();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->user->identity->role == 'manager') {
                $rm = new RentManager();
                $rm->rent_id = $model->id;
                $rm->manager_id = Yii::$app->user->id;
                $rm->save();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {

            $paramsIds = EavParamValue::find()
                ->select('param_id')
                ->distinct()
                ->object($model->className(), $model->id)
                ->asArray()
                ->all();
            $paramsIds = ArrayHelper::getColumn($paramsIds, 'param_id');
            $eavParamDropDown = EavParam::getListDropdown($paramsIds);

            return $this->render('create', [
                'model' => $model,
                'eavParamDropDown' => $eavParamDropDown,
            ]);
        }
    }

    /**
     * Updates an existing Rent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $eavParamSearch = new EavParamValueSearch();

        //Загрузка изображений
        $eavParam['model'] = new EavParamValue();
        $eavParam['search'] = $eavParamSearch;
        $eavParam['provider'] = $eavParamSearch->searchParam($model->className(), $model->id);

        $paramsIds = EavParamValue::find()
            ->select('param_id')
            ->distinct()
            ->object($model->className(), $model->id)
            ->asArray()
            ->all();
        $paramsIds = ArrayHelper::getColumn($paramsIds, 'param_id');
        $eavParamDropDown = EavParam::getListDropdown($paramsIds);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $paramsObject = Yii::$app->request->post("EavParamValue");
            if (!is_null($paramsObject)) {
                EavParamValue::updateList($paramsObject);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'eavParam' => $eavParam,
                'eavParamDropDown' => $eavParamDropDown,
            ]);
        }
    }

    /**
     * Deletes an existing Rent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->myDelete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionDeleteImportService($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = RentImportServices::find()->where(['service_id' => $id])->one();
        if ($model !== null) {

            if ($model->delete()) {
                return ['success' => true];
            } else {
                return ['success' => false, 'text' => Yii::t("common", "Error saving data")];
            }

        } else {
            throw new NotFoundHttpException('The requested page does not exist..');
        }
    }

    /**
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionImportServiceReload($id)
    {
        $model = RentImportServices::find()->where(['service_id' => $id])->one();
        if ($model !== null) {
            if ($model->importCalendar()) {
                return $this->redirect(['view', 'id' => $model->rent_id]);
            } else {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ['success' => false];
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist..');
        }
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionUndelete($id)
    {
        $this->findModel($id)->unDelete();

        Yii::$app->session->setFlash('success', Yii::t("common", "The object is restored"));

        return $this->redirect(['view', 'id' => $id, '#' => 's5']);
    }

    /**
     * Finds the Rent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Rent::find()->where(['rent.id' => $id])->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist..');
        }
    }

    /**
     * Displays a single Clients model.
     * @param integer $id
     * @return mixed
     */
    public function actionUnsetManager($id, $user_id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $this->findModel($id);
        $model = RentManager::find()->cm($id, $user_id)->one();
        if (!is_null($model)) {
            if ($model->delete()) {
                return ['success' => true];
            } else {
                return ['success' => false, 'text' => Yii::t("common", "Error saving data")];
            }
        }
        return ['success' => false, 'text' => Yii::t("common", "Error")];
    }

    /**
     * @param $id
     * @param $manager_id
     * @return array
     */
    public function actionSetManager($id, $manager_id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $this->findModel($id);
        $model = RentManager::find()->cm($id, $manager_id)->one();
        if (is_null($model)) {
            $model = new RentManager();
            $model->rent_id = $id;
            $model->manager_id = $manager_id;
            if ($model->save()) {
                return ['success' => true];
            } else {
                return ['success' => false, 'text' => Yii::t("common", "Error saving data")];
            }
        }
        return ['success' => false, 'text' => Yii::t("common", "Error")];
    }


    /**
     * Displays a single Clients model.
     * @param integer $id
     * @return mixed
     */
    public function actionUnsetAgency($id, $agency_id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (Yii::$app->user->identity->role == 'manager') {
            return ['success' => false, 'text' => Yii::t("common", "Access is denied")];
        } else {
            $rent = $this->findModel($id);
            $model = RentAgency::find()->cm($id, $agency_id)->one();
            if (is_null($model)) {
                return ['success' => false, 'text' => Yii::t("common", "No access")];
            } else {
                if ($model->delete()) {
                    return ['success' => true];
                } else {
                    return ['success' => false, 'text' => Yii::t("common", "Error saving data")];
                }
            }
        }
    }

    /**
     * @param integer $id ,$user_id
     * @return mixed
     */
    public function actionSetAgency($id, $agency_id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (Yii::$app->user->identity->role == 'manager') {
            return ['success' => false, 'text' => Yii::t("common", "Access is denied")];
        } else {
            $model = RentAgency::find()->cm($id, $agency_id)->one();

            if (is_null($model)) {
                $model = new RentAgency();
                $model->rent_id = $id;
                $model->agency_id = $agency_id;
                if ($model->save()) {
                    return ['success' => true];
                } else {
                    return ['success' => false, 'text' => Yii::t("common", "Error saving data")];
                }
            }
        }
        return ['success' => false, 'text' => Yii::t("common", "Unknown error")];
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionDeleteEavValue()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = EavParamValue::findOne((int)$_POST['delete_id']);
        if (($model) !== null) {
            if ($model->delete()) {
                return "GOOD";
            } else {
                return "ERROR DELETE";
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return string
     */
    public function actionCreateEavValue()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if ($this->addParamToRent((int)$_POST['model_id'], (int)$_POST['param_id'])) {
            return "GOOD";
        } else {
            return "ERROR SAVE";
        }
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCreateEavKit()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $list = EavList::findOne((int)$_POST['param_id']);
        if (is_null($list)) throw new NotFoundHttpException('The requested page does not exist. Param not found');
        foreach ($list->eavListItems as $listItem) {
            $this->addParamToRent((int)$_POST['model_id'], $listItem->param_id);
        }

        return "GOOD";
    }

    /**
     * @param $rentId
     * @param $paramId
     * @return bool
     * @throws NotFoundHttpException
     */
    public function addParamToRent($rentId, $paramId)
    {
        $rent = Rent::findOne((int)$rentId);
        $param = EavParam::findOne((int)$paramId);
        if (is_null($rent) || is_null($param)) {
            throw new NotFoundHttpException('The requested page does not exist. Param or EavParam not found');
        }

        $model = EavParamValue::find()->andWhere([
            'param_id' => $param->id,
            'object_table' => Rent::className(),
            'object_id' => $rent->id
        ])->one();

        if (!is_null($model)) {
            return true;
        }

        $model = new EavParamValue();
        $model->object_table = Rent::className();
        $model->object_id = $rent->id;
        $model->param_id = $param->id;

        switch ($param->param_type) {
            case EavParam::EAV_TYPE_BOOLEAN :
                $model->value = '0';
                break;
            case EavParam::EAV_TYPE_INTEGER :
                $model->value = '0';
                break;
            default :
                $model->value = '-';
        }
        $model->dimension = $param->dimension;
        return $model->save();
    }

    /**
     * @param $id
     * @return string
     */
    public function actionGetFormParam($id)
    {
        $param = EavParam::findOne((int)$id);
        return $param->getValueForm();
    }

    /**
     * @param $imagePath
     * @param $waterMark
     * @param bool $right
     * @throws Exception
     */
    public function setWaterMark($imagePath, $waterMark, $right = true)
    {
        if (!file_exists(Yii::getAlias($imagePath))) {
            throw new Exception('Image Path not detected!');
        }

        if (!file_exists(Yii::getAlias($waterMark))) {
            throw new Exception('WaterMark not detected!');
        }

        $image = new \abeautifulsite\SimpleImage($imagePath);

        $wmMaxWidth = intval($image->get_width() * 0.4);
        $wmMaxHeight = intval($image->get_height() * 0.4);

        $waterMarkPath = Yii::getAlias($waterMark);

        $waterMark = new \abeautifulsite\SimpleImage($waterMarkPath);

        if (
            $waterMark->get_height() > $wmMaxHeight
            or
            $waterMark->get_width() > $wmMaxWidth
        ) {

            $waterMarkPath = $this->getModule()->getCachePath() . DIRECTORY_SEPARATOR .
                pathinfo($waterMark)['filename'] .
                $wmMaxWidth . 'x' . $wmMaxHeight . '.' .
                pathinfo($waterMark)['extension'];

            //throw new Exception($waterMarkPath);
            if (!file_exists($waterMarkPath)) {
                $waterMark->fit_to_width($wmMaxWidth);
                $waterMark->save($waterMarkPath, 100);
                if (!file_exists($waterMarkPath)) {
                    throw new Exception('Cant save watermark to ' . $waterMarkPath . '!!!');
                }
            }

        }

        if ($right) {
            $image->overlay($waterMarkPath, "bottom right", .5, -10, -10);
        } else {
            $image->overlay($waterMarkPath, "bottom left", .5, 10, 10);
        }

        $image->save($imagePath, 100);
    }
}
