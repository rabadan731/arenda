<?php

namespace modules\agency\controllers\backend;

use common\helpers\Time;
use common\modules\agency\models\ClientPayments;
use common\modules\agency\models\search\WorkerJobsSearch;
use common\modules\agency\models\search\WorkerPaymentsSearch;
use common\modules\agency\models\WorkerPayments;
use Yii;
use common\modules\agency\models\Workers;
use common\modules\agency\models\search\WorkersSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * WorkersController implements the CRUD actions for Workers model.
 */
class WorkersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'undelete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Workers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WorkersSearch();
        $params = Yii::$app->request->queryParams;
        if (!Yii::$app->user->can('admin')) {
            $params['WorkersSearch']['agency_id'] = Yii::$app->user->identity->agency_id;
        }

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Workers models.
     * @return mixed
     */
    public function actionPayments()
    {

        $post = Yii::$app->request->post();

        if (Yii::$app->request->isPost) {
            if (isset($post['worker'])) {
                array_map(function ($id, $pay) {
                    $worker = Workers::findOne($id);
                    if (!is_null($worker) && $pay != 0) {
                        //Платежка
                        $newPayment = new WorkerPayments();
                        $newPayment->worker_id = $worker->id;
                        $newPayment->title = Yii::t('common', 'Payment of salaries');
                        $newPayment->price = -$pay;
                        $newPayment->date_invoice = Time::real();
                        if ($newPayment->save()) {
                            $worker->balance = $worker->balance - $pay;
                            $worker->save(false, ['balance']);
                        }
                    }
                }, array_keys($post['worker']), $post['worker']);
            }
        }

        $query = Workers::find();
        if (!Yii::$app->user->can('admin')) {
            $query->agency(Yii::$app->user->identity->agency_id);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);


        return $this->render('payments', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Workers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $searchModel = new WorkerJobsSearch();
        $searchModel->worker_id = $model->id;
        $jobsProvider = $searchModel->search([]);

        $searchModelPayments = new WorkerPaymentsSearch();
        $searchModelPayments->worker_id = $model->id;
        $workerPaymentsProvider = $searchModelPayments->search([]);


        return $this->render('view', [
            'model' => $model,
            'jobsProvider' => $jobsProvider,
            'workerPaymentsProvider' => $workerPaymentsProvider
        ]);
    }

    /**
     * Creates a new Workers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $save
     * @param integer $agency_id
     * 
     * @return mixed
     */
    public function actionCreate($save = 0, $agency_id = null)
    {
        $model = new Workers();

        //ajax
        if (!is_null($agency_id) && Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (!Yii::$app->user->can("admin") && Yii::$app->user->identity->agency_id !== $agency_id) {
                $model->agency_id = Yii::$app->user->identity->agency_id;
            }
            if ($save == 0) {
                return ActiveForm::validate($model);
            } else {
                return ['success' => $model->save(), 'errors' => $model->errors, 'at' => $model->attributes];
            }
        }


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Workers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Workers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->myDelete();

        return $this->redirect(['index']);
    }

    public function actionUndelete($id)
    {
        $this->findModel($id)->unDelete();

        Yii::$app->session->setFlash('success', Yii::t("common", "The object is restored"));

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Finds the Workers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Workers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (Yii::$app->user->can('admin')) {
            $model = Workers::findOne($id);
        } else {
            $model = Workers::find()->where(['id' => $id])->thisAgency()->one();
        }

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Удаление аватара
     * @return \yii\web\Response
     */
    public function actionDeleteAvatar($id)
    {
        $model = $this->findModel($id);
        $model->deleteAvatar(true);
        return $this->redirect(['update', 'id' => $id]);
    }
}
