<?php

namespace modules\agency\controllers\backend;

use common\modules\agency\models\search\AgencySearch;
use common\modules\agency\models\search\AgencyTariffSearch;
use common\modules\cars\models\Car;
use common\modules\cars\models\CarReserve;
use common\modules\cars\models\search\CarReserveSearch;
use common\modules\cars\models\search\CarSearch;
use common\modules\clients\models\search\ClientPaymentsSearch;
use common\modules\clients\models\search\ClientsSearch;
use common\modules\agency\models\search\ForRentSearch;
use common\modules\agency\models\search\RentSearch;
use common\modules\agency\models\search\WorkerJobsSearch;
use common\modules\agency\models\search\WorkersSearch;
use yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Product model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            Url::remember();
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
////                    [
////                        'allow' => true,
////                        'actions' => ['index', 'view'],
////                        'roles' => ['product_view']
////                    ],
////                    [
////                        'allow' => true,
////                        'actions' => ['create', 'update', 'delete'],
////                        'roles' => ['product_crud'],
////                    ],
//                ],
//            ]
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionTrash()
    {
        $modelAgency = new AgencySearch();
        $dataProviderAgency = $modelAgency->searchDelete();

        $searchAgencyTariff = new AgencyTariffSearch();
        $dataProviderAgencyTariff = $searchAgencyTariff->searchDelete();

        $searchClientPayments = new ClientPaymentsSearch();
        $dataProviderClientPayments = $searchClientPayments->searchDelete();

        $searchClients = new ClientsSearch();
        $dataProviderClients = $searchClients->searchDelete();

        $searchForRent = new ForRentSearch();
        $dataProviderForRent = $searchForRent->searchDelete();

        $searchRent = new RentSearch();
        $dataProviderRent = $searchRent->searchDelete();

        $searchWorkerJobs = new WorkerJobsSearch();
        $dataProviderWorkerJobs = $searchWorkerJobs->searchDelete();

        $searchWorkers = new WorkersSearch();
        $dataProviderWorkers = $searchWorkers->searchDelete();

        $searchCars = new CarSearch();
        $dataProviderCars = $searchCars->searchDelete();

        $searchReserve = new CarReserveSearch();
        $dataProviderReserve = $searchReserve->searchDelete();



        return $this->render('trash', [
            'dataProviderAgency' => $dataProviderAgency,
            'dataProviderAgencyTariff' => $dataProviderAgencyTariff,
            'dataProviderClientPayments' => $dataProviderClientPayments,
            'dataProviderClients' => $dataProviderClients,
            'dataProviderForRent' => $dataProviderForRent,
            'dataProviderRent' => $dataProviderRent,
            'dataProviderWorkerJobs' => $dataProviderWorkerJobs,
            'dataProviderWorkers' => $dataProviderWorkers,
            'dataProviderCars' => $dataProviderCars,
            'dataProviderReserve' => $dataProviderReserve,
        ]);
    }


    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionGeocode($address = null)
    {
        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;

        if (!empty($address)) {
            $xml = simplexml_load_file("http://maps.google.com/maps/api/geocode/xml?address={$address}&sensor=false");
            // Если геокодировать удалось, то записываем в БД
            $status = $xml->status;

            if ($status == 'OK') {
                return [
                    'lat' => $xml->result->geometry->location->lat,
                    'lng' => $xml->result->geometry->location->lng
                ];
            }
        }

        return ['lat' => 0, 'lng' => 0];
    }
}
