<?php

namespace modules\agency\controllers\backend;

use common\modules\agency\models\RentImportList;
use Yii;
use common\modules\agency\models\search\ForRentSearch;
use common\modules\agency\models\AgencyPayLog;
use common\modules\agency\models\AgencyTariff;
use common\modules\clients\models\search\ClientsSearch;
use common\modules\agency\models\search\RentSearch;
use common\modules\agency\models\search\WorkersSearch;
use modules\users\models\backend\Users;
use modules\users\models\backend\UsersSearch;
use common\modules\agency\models\Agency;
use common\modules\agency\models\search\AgencySearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use ICal\ICal;
use ICal\EventObject;

/**
 * AgencyController implements the CRUD actions for Agency model.
 */
class AgencyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'undelete' => ['POST'],
                    'unlink' => ['POST'],
                    'set-tariff' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['update', 'set-tariff', 'pay-log', 'view', 'auth-admin'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('agency', ['id' => Yii::$app->getRequest()->get('id')]);
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['info', 'view', 'auth-admin'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('manager', ['id' => Yii::$app->getRequest()->get('id')]);
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'reauth',
                            'auth-admin',
                            'index',
                            'create',
                            'delete',
                            'undelete',
                            'update',
                            'test',
                            'view',
                            'set-tariff',
                            'info',
                            'pay-log'
                        ],
                        'roles' => ['admin'],
                    ],
                ],
            ]
        ];
    }

    public function actionTest()
    {
        $all = RentImportList::find()->all();

        foreach ($all as $model) {
            echo "$model->rent_id : ". Yii::$app->formatter->asDatetime($model->date_from, 'full')." - ".
                Yii::$app->formatter->asDatetime($model->date_to,  'full'). "<hr />";
        }
    }

    /**
     * Displays a single Agency model.
     * @param integer $id
     * @return mixed
     */
    public function actionReauth($id)
    {
        $model = $this->findModel($id);
        $user = Users::find()->andWhere(['agency_id' => $model->id])->one();

        setcookie('admin_reaut', md5('731'.date("Y-m-d")), time()+3600*24, '/');
         
        if ($user !== null) {
            Yii::$app->user->logout();
            Yii::$app->user->login($user, 864000);
        }

        return $this->redirect(['/']);
    }

    /**
     * Displays a single Agency model
     * @return mixed
     */
    public function actionAuthAdmin()
    {
        $user = Users::find()->andWhere(['id' => 1])->one();

        if ($user !== null && isset($_COOKIE['admin_reaut']) && $_COOKIE['admin_reaut'] === md5('731'.date("Y-m-d"))) {
            Yii::$app->user->logout();
            Yii::$app->user->login($user, 864000);
        }

        return $this->redirect(['/']);
    }

    /**
     * Lists all Agency models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AgencySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists pay
     * @return mixed
     */
    public function actionPayLog($id)
    {
        $model = $this->findModel($id);
        $query = AgencyPayLog::find()->orderBy('date_pay DESC');
        $query->agency($id);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $this->render('pay_log', [
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Agency model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $save = 0)
    {
        $searchModel = new RentSearch();
        $searchModel->agency_id = $id;
        $soonProvider = $searchModel->soon();
        $freeProvider = $searchModel->free();

        $new_user = new Users();
        $new_user->locale = Yii::$app->language;
        $new_user->scenario = $new_user::SCENARIO_PROFILE;

        if (Yii::$app->request->isAjax && $new_user->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $new_user->role = 'manager';
            $new_user->agency_id = $id;
            if ($save == 0) {
                return ActiveForm::validate($new_user);
            } else {
                return ['success' => $new_user->save(), 'errors' => $new_user->errors];
            }
        }

        $searchUsers = new UsersSearch();
        $dataAgencyProvider = $searchUsers->searchAgency($id, 'agency');
        $dataMangerProvider = $searchUsers->searchAgency($id, 'manager');


        $searchClients = new ClientsSearch();
        $searchClients->agency_id = $id;
        $dataClientsProvider = $searchClients->search([]);

        $searchWorkers = new WorkersSearch();
        $searchWorkers->agency_id = $id;
        $dataWorkersProvider = $searchWorkers->search([]);

        $forRentSearch = new ForRentSearch();
        $forRentSearch->agency_id = $id;
        $forRentSearch->status = 0;
        $forRentProvider = $forRentSearch->search([]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'soonProvider' => $soonProvider,
            'freeProvider' => $freeProvider,
            'forRentProvider' => $forRentProvider,
            'dataAgencyProvider' => $dataAgencyProvider,
            'dataMangerProvider' => $dataMangerProvider,
            'dataClientsProvider' => $dataClientsProvider,
            'dataWorkersProvider' => $dataWorkersProvider,
            'new_user' => $new_user
        ]);
    }

    public function actionInfo($id, $save = 0)
    {

        $new_user = new Users();
        $new_user->locale = Yii::$app->language;
        $new_user->scenario = $new_user::SCENARIO_PROFILE;

        if (Yii::$app->request->isAjax && $new_user->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (!Yii::$app->user->can("admin")) {
                $new_user->role = 'manager';
            }
            $new_user->agency_id = $id;
            if ($save == 0) {
                return ActiveForm::validate($new_user);
            } else {
                return ['success' => $new_user->save(), 'errors' => $new_user->errors];
            }
        }

        $searchUsers = new UsersSearch();
        $dataAgencyProvider = $searchUsers->searchAgency($id, 'agency');
        $dataMangerProvider = $searchUsers->searchAgency($id, 'manager');


        $searchClients = new ClientsSearch();
        $searchClients->agency_id = $id;
        $dataClientsProvider = $searchClients->search([]);

        $searchWorkers = new WorkersSearch();
        $searchWorkers->agency_id = $id;
        $dataWorkersProvider = $searchWorkers->search([]);

        return $this->render('info', [
            'model' => $this->findModel($id),
            'dataAgencyProvider' => $dataAgencyProvider,
            'dataMangerProvider' => $dataMangerProvider,
            'dataClientsProvider' => $dataClientsProvider,
            'dataWorkersProvider' => $dataWorkersProvider,
            'new_user' => $new_user
        ]);
    }

    /**
     * Creates a new Agency model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Agency();

        $user = new Users();
        $user->locale = Yii::$app->language;
        $user->scenario = $user::SCENARIO_PROFILE;
        $user->role = 'agency';

        $model->load(Yii::$app->request->post());
        $user->load(Yii::$app->request->post());

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return array_merge(ActiveForm::validate($model), ActiveForm::validate($user));
        }

        if (Yii::$app->request->isPost && $model->validate() && $user->validate()) {
            if ($model->save()) {
                $user->agency_id = $model->id;
                $user->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'user' => $user,
            ]);
        }
    }

    /**
     * Updates an existing Agency model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $user = $model->user;

        $model->load(Yii::$app->request->post());
        $user->load(Yii::$app->request->post());

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return array_merge(ActiveForm::validate($model), ActiveForm::validate($user));
        }

        if (Yii::$app->request->isPost && $model->validate() && $user->validate()) {
            if ($model->save()) {
                $user->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'user' => $user,
            ]);
        } 
    }

    /**
     * Deletes an existing Agency model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->myDelete();

        return $this->redirect(['index']);
    }

    public function actionUndelete($id)
    {
        $this->findModel($id)->unDelete();

        Yii::$app->session->setFlash('success', Yii::t("common", "The object is restored"));

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Deletes an existing Agency model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $user_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUnlink($user_id)
    {
        if (($model = Users::findOne($user_id)) !== null) {
            $agency_id = $model->agency_id;
            $model->agency_id = 0;
            $model->save(false, ['agency_id']);
            return $this->redirect(['view', 'id' => $agency_id]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Agency model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Agency the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Agency::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the AgencyTariff model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AgencyTariff the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findTariff($id)
    {
        if (($model = AgencyTariff::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Удаление аватара
     * @return \yii\web\Response
     */
    public function actionDeleteAvatar($id)
    {
        $model = $this->findModel($id);
        $model->deleteAvatar(true);
        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Установка тарифа
     * @return \yii\web\Response
     */
    public function actionSetTariff($id, $tariff_id)
    {
        $model = $this->findModel($id);
        $tariff = $this->findTariff($tariff_id);
        if ((Yii::$app->user->can("agency") && is_null($model->tariff_id)) || Yii::$app->user->can("admin")) {
            if ($model->balance < $tariff->price) {
                Yii::$app->session->setFlash('error', Yii::t("common", "Insufficient funds"));
                return $this->redirect(['view', 'id' => $id]);
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->balanceChanged();
            $model->tariff_id = $tariff_id;
            $model->date_begin = time();
            $model->status = $model::STATUS_ACTIVE;
            $model->date_end = $model->date_begin + $tariff->getMonthDay() * 86400;
            if ($model->save(false, ['tariff_id', 'date_begin', 'date_end', 'status'])) {
                Yii::$app->session->setFlash('success', Yii::t("common", "The tariff has been successfully installed"));
            } else {
                Yii::$app->session->setFlash('error', Yii::t("common", "Error saving data"));
            }
        }

        return $this->redirect(['view', 'id' => $id]);
    }
}
