<?php

namespace modules\agency\controllers\backend;

use Yii;
use common\helpers\Time;
use common\modules\clients\models\ClientPayments;
use common\modules\clients\models\Clients;
use common\modules\clients\models\search\ClientPaymentsSearch;
use common\modules\agency\models\WorkerJobs;
use common\modules\agency\models\RentImportList;
use common\modules\agency\models\WorkerPayments;
use common\modules\agency\models\Rent;
use common\modules\agency\models\ForRent;
use themes\fullcalendar\models\Event;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ForRentController implements the CRUD actions for ForRent model.
 */
class ForRentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            Url::remember();
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'undelete' => ['POST'],
                    'add-pay' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ForRent models.
     * @return mixed
     */
    public function actionIndex()
    {

        $models = Rent::find();
 
        $resources = array();

        foreach ($models->all() as $model) {
            $resources[] = ['id' => $model->id, 'title' => $model->title];
        }

        $resources = json_encode($resources);

        return $this->render('index', [
            'resources' => $resources,
        ]);
    }

    /**
     * @param null $start
     * @param null $end
     * @param null $_
     * @param null $id
     * @return array
     */
    public function actionJsoncalendar($start = null, $end = null, $_ = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $models = ForRent::find()->date($start, $end);
        if ($id !== null) {
            $models->andWhere(['for_rent.rent_id' => $id]);
        }
 
        $events = array();

        foreach ($models->all() as $model) {
            //Testing
            $event = new Event();
            $event->id = $model->id;
            if ($model->reservations == $model::RESERVATIONS_TEMP) {
                $event->color = "#ababab";
            } else {
                if ($model->check_out <= time() && $model->status == ForRent::STATUS_OPEN) {
                    $event->color = "#e47979";
                }
                if ($model->status == ForRent::STATUS_CLOSE) {
                    $event->color = "#5cb85c";
                }
            }
            $event->resourceId = $model->rent_id;
            $event->allDay = false;
            $event->title = $model->rent->title . ' (' . date('Y-m-d', $model->check_in) . ' - ' .
                date('Y-m-d', $model->check_out) . ')';
            $event->start = date('Y-m-d H:i', $model->check_in);
            $event->end = date('Y-m-d H:i', $model->check_out);
            $event->url = Url::to(['view', 'id' => $model->id]);
            $event->className = "calendar_item";
            $events[] = $event;
        }

        $importList = RentImportList::find()->all();

        foreach ($importList as $importItem) {
            /** @var $importItem RentImportList */
            $event = new Event();

            $event->id = "import_{$importItem->id}";
            $event->resourceId = $importItem->rent_id;
            $event->allDay = true;
            $event->title =
                (is_null($importItem->client)?"":$importItem->client->fio).
                (is_null($importItem->service)?"":" ({$importItem->service->title})");
            $event->start = date('Y-m-d H:i', $importItem->date_from);
            $event->end = date('Y-m-d H:i', $importItem->date_to);
            //$event->url = Url::to(['for-rent/view', 'id' => $importItem->rent_id]);
            $event->className = "calendar_item";
            $events[] = $event;
        }

        return $events;
    }

    /**
     * Displays a single ForRent model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelWJ = new WorkerJobs();

        /**
         * @var $model ForRent
         */

        if ($modelWJ->load(Yii::$app->request->post()) && $modelWJ->validate()) {
            $modelWJ->address_map = implode(",", [
                str_replace(',', '.', $modelWJ->lat),
                str_replace(',', '.', $modelWJ->lng)
            ]);

            if ($modelWJ->save()) {
                $newPayment = new WorkerPayments();
                $newPayment->worker_id = $modelWJ->worker_id;
                $newPayment->title = Yii::t('common', 'Payment information') . ' №' . ($model->id) . "  " .
                    (Yii::$app->formatter->asDate($model->check_in) . ' - ' .
                        Yii::$app->formatter->asDate($model->check_out));
                $newPayment->price = $modelWJ->price;
                $newPayment->for_rent_id = $model->id;
                $newPayment->date_invoice = Time::real();
                $newPayment->save();

                switch ($modelWJ->worker->worker_type) {
                    case 3:
                        $model->worker_cleaner = $modelWJ->id;
                        $model->save(false, ['worker_cleaner']);
                        break;
                    case 6:
                        $model->worker_driver = $modelWJ->id;
                        $model->save(false, ['worker_driver']);
                        break;
                    case 7:
                        $model->worker_master = $modelWJ->id;
                        $model->save(false, ['worker_master']);
                        break;
                    case 9:
                        $model->worker_agent = $modelWJ->id;
                        $model->save(false, ['worker_agent']);
                        break;
                }

                Yii::$app->session->setFlash('success', Yii::t("common", "Good save"));
                $modelWJ = new WorkerJobs();
                $this->redirect(['view', 'id' => $id]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t("common", "Error save"));
            }
        }

        if (is_null($modelWJ->address_map)) {
            $modelWJ->address_map = $model->rent->address_map;
            $modelWJ->lat = $modelWJ->getLat();
            $modelWJ->lng = $modelWJ->getLng();
        }

        if (is_null($modelWJ->address_text)) {
            $modelWJ->address_text = $model->rent->address_text;
        }

        if (is_null($modelWJ->date_event)) {
            $modelWJ->date_event = $model->check_in;
        }

        $clientPaymentsSearchModel = new ClientPaymentsSearch();
        $clientPaymentsSearchModel->for_rent_id = $model->id;
        $clientPaymentsSearchModel->client_id = $model->client_id;
        $clientPaymentsProvider = $clientPaymentsSearchModel->search([]);

        return $this->render('view', [
            'model' => $model,
            'modelWJ' => $modelWJ,
            'clientPaymentsProvider' => $clientPaymentsProvider,
        ]);
    }

    /**
     * Creates a new ForRent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $id
     * @return string|Response
     */
    public function actionCreate($id)
    {
        $rent = $this->findModelRent($id);
        $model = new ForRent();
        $model->rent_id = $rent->id;
        $model->price_day = $rent->price_day;
        $model->agency_id = $rent->agency_id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $client = $model->client;
            if (!is_null($client)) {

                $client->balance = $client->balance +
                    $model->price_deposit +
                    $model->price_prepay -
                    $model->price_all;
                if ($client->save(false, ['balance'])) {
                    //Платежка
                    $newPayment = new ClientPayments();
                    $newPayment->client_id = $model->client_id;
                    $newPayment->for_rent_id = $model->id;
                    $newPayment->title = Yii::t('common', 'Rent price');
                    $newPayment->price = -$model->price_all;
                    $newPayment->date_invoice = Time::real();
                    $newPayment->paid = -1;
                    $newPayment->save();

                    //Депозит
                    $newPayment = new ClientPayments();
                    $newPayment->client_id = $model->client_id;
                    $newPayment->for_rent_id = $model->id;
                    $newPayment->title = Yii::t('common', 'Deposit');
                    $newPayment->price = $model->price_deposit;
                    $newPayment->date_invoice = Time::real();
                    $newPayment->save();

                    //предоплата
                    $newPayment = new ClientPayments();
                    $newPayment->client_id = $model->client_id;
                    $newPayment->for_rent_id = $model->id;
                    $newPayment->title = Yii::t('common', 'Prepayment');
                    $newPayment->price = $model->price_prepay;
                    $newPayment->date_invoice = Time::real();
                    $newPayment->save();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'rent' => $rent,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $old_price = $model->price_all;
        $payment = ClientPayments::find()->andWhere([
            'client_id' => $model->client_id,
            'for_rent_id' => $model->id,
            'price' => -$old_price,
            'paid' => -1,
        ])->one();

        if (is_null($payment)) {
            throw new NotFoundHttpException('No load Pay: ' . $old_price);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $client = $model->client;
            if (!is_null($client)) {
                $client->balance = $client->balance + $model->price_all - $old_price;
                if ($client->save(false, ['balance'])) {
                    $payment->price = -$model->price_all;
                    $payment->save();
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,

            ]);
        }
    }

    /**
     * Updates an existing ForRent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionAddPay($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $client = $model->client;
        if (is_null($client)) {
            return ['success' => false, 'error_text' => Yii::t("common", "Client not found")];
        }

        //Платежка
        $newPayment = new ClientPayments();
        if ($newPayment->load(Yii::$app->request->post())) {
            $newPayment->price = is_null($newPayment->price) ? 0 : $newPayment->price;
            $newPayment->client_id = $model->client_id;
            $newPayment->for_rent_id = $model->id;
            $newPayment->date_invoice = Time::real();
            if ($newPayment->save()) {
                /** @var $client Clients */
                $client->balance += $newPayment->price;
                $client->save(false, ['balance']);
                return ['success' => true];
            } else {
                return ['success' => false, 'error_text' => Yii::t("common", "Error saving data")];
            }
        }

        return ['success' => false, 'error_text' => Yii::t("common", "Error load data")];
    }

    /**
     * Deletes an existing ForRent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $model->myDelete();

        return $this->redirect(['rent/view', 'id' => $model->rent_id]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionUndelete($id)
    {
        $model = $this->findModel($id);

        $model->unDelete();

        Yii::$app->session->setFlash('success', Yii::t("common", "The object is restored"));

        return $this->redirect(['rent/view', 'id' => $model->rent_id]);
    }

    /**
     * @param $id
     * @return array|ForRent|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (Yii::$app->user->can('admin')) {
            $model = ForRent::findOne($id);
        } else {
            $model = ForRent::find()->where(['for_rent.id' => $id])->thisAgency()->one();
        }

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the ForRent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelRent($id)
    {
        $model = Rent::find()->where(['rent.id' => $id])->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionCloseOrder($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->status = ForRent::STATUS_CLOSE;
            $model->save(false, ['status']);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $modelPay = new ClientPayments();

        $clientPaymentsSearchModel = new ClientPaymentsSearch();
        $clientPaymentsSearchModel->for_rent_id = $model->id;
        $clientPaymentsSearchModel->client_id = $model->client_id;
        $clientPaymentsProvider = $clientPaymentsSearchModel->search([], 'id ASC');

        $result = ClientPayments::find()->andWhere([
            'client_payments.for_rent_id' => $model->id,
            'client_payments.client_id' => $model->client_id,
            'client_payments.delete' => 0
        ])->sum('price');

        return $this->render('close-order', [
            'model' => $model,
            'result' => $result,
            'modelPay' => $modelPay,
            'clientPaymentsProvider' => $clientPaymentsProvider
        ]);
    }
}
