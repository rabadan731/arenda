<?php

namespace modules\agency\controllers\frontend;

use common\modules\agency\models\ForRent;
use common\modules\agency\models\RentImportList;
use common\modules\agency\models\RentImportServices;
use common\modules\log\models\LogExchange;
use Yii;
use common\modules\agency\models\Rent;
use yii\helpers\Inflector;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Jsvrcek\ICS\Model\Calendar;
use Jsvrcek\ICS\Model\CalendarEvent;
use Jsvrcek\ICS\Model\Relationship\Attendee;
use Jsvrcek\ICS\Model\Relationship\Organizer;

use Jsvrcek\ICS\Utility\Formatter;
use Jsvrcek\ICS\CalendarStream;
use Jsvrcek\ICS\CalendarExport;




/**
 * RentController implements the CRUD actions for Rent model.
 */
class RentController extends Controller
{
    public function actionTest($t=null)
    {
        return $this->renderContent("Good: {$t}");
    }

    /**
     * Finds the Rent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionExport($id=1, $hash='2370262d11eef76bd36ceeafd7840425')
    {
        $model = Rent::find()->andWhere([
            'rent.id' => $id,
            'rent.hash' => $hash,
        ])->one();

        if ($model !== null) {

            $timeZone = new \DateTimeZone(Yii::$app->formatter->timeZone);

            $calendar = new Calendar();
            $title = Inflector::slug($model->title);
            $calendar->setProdId("-//{$title}//CalendarExchangeEmployment//EN");

            array_map(function ($item) use ($calendar, $timeZone, $model) {
                /** @var $item ForRent */
                $event = new CalendarEvent();
                $event->setDescription("WEBSITE({$model->agency->title})");
                $event->setAllDay(true);
                $event->setStart(new \DateTime(date("Y-m-d H:i:s", $item->check_in), $timeZone))
                    ->setEnd(new \DateTime(date("Y-m-d H:i:s", mktime(
                        23, 59, 59,
                        date("m", $item->check_out),
                        date("d", $item->check_out),
                        date("Y", $item->check_out)
                    )), $timeZone))
                    ->setSummary($item->id)
                    ->setUid("for_rent_{$item->id}");
                $calendar->addEvent($event);
            }, $model->getForRent()->andWhere(['>=','check_out',time()])->all());


            array_map(function ($item) use ($calendar, $timeZone) {
                /** @var $item RentImportList */
                $event = new CalendarEvent();
                $event->setDescription("{$item->service->title}");
                $event->setAllDay(true);
                $event->setStart(new \DateTime(date("Y-m-d H:i:s", $item->date_from), $timeZone))
                    ->setEnd(new \DateTime(date("Y-m-d H:i:s", $item->date_to), $timeZone))
                    ->setSummary($item->id)
                    ->setUid("rent_import_{$item->id}");
                $calendar->addEvent($event);
            }, RentImportList::find()
                ->andWhere(['rent_id' => $model->id])
                ->andWhere(['>=','date_to',time()])
                ->all());

            $calendarExport = new CalendarExport(new CalendarStream, new Formatter());
            $calendarExport->addCalendar($calendar);

            // LOG
            $textExport = "Export rent: {$model->title} ({$model->id})";
            $log = LogExchange::find()->andWhere([
                'rent_id' => $model->id,
                'ip' => Yii::$app->request->userIP,
                'ua' => Yii::$app->request->userAgent,
                'text' => $textExport,
            ])->one();

            if (is_null($log)) {
                $log = new LogExchange();
                $log->ip = Yii::$app->request->userIP;
                $log->ua = Yii::$app->request->userAgent;
                $log->url = Yii::$app->request->referrer;
                $log->rent_id = $model->id;
                $log->text = $textExport;
                $log->service_id = null;
            }
            $log->created_at = time();
            $log->save();

            return $calendarExport->getStream();

        } else {
            throw new NotFoundHttpException('The requested page does not exist..');
        }
    }

}
