<?php

namespace common\modules\agency\models;

use Yii;

/**
 * This is the model class for table "agency_pay_log".
 *
 * @property integer $id
 * @property integer $agency_id
 * @property integer $tariff_id
 * @property integer $date_pay
 * @property string $price
 * @property integer $delete
 *
 * @property Agency $agency
 * @property AgencyTariff $tariff
 */
class AgencyPayLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agency_pay_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agency_id', 'tariff_id'], 'required'],
            [['agency_id', 'tariff_id', 'date_pay','delete'], 'integer'],
            [['price'],'number'],
            [
                ['agency_id'],
                'exist', 'skipOnError' => true,
                'targetClass' => Agency::className(),
                'targetAttribute' => ['agency_id' => 'id']
            ],
            [
                ['tariff_id'],
                'exist', 'skipOnError' => true,
                'targetClass' => AgencyTariff::className(),
                'targetAttribute' => ['tariff_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'agency_id' => Yii::t('common', 'Agency ID'),
            'tariff_id' => Yii::t('common', 'Tariff'),
            'tariff.tprice' => Yii::t('common', 'Tariff'),
            'date_pay' => Yii::t('common', 'Date Pay'),
            'price' => Yii::t('common', 'Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'agency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(AgencyTariff::className(), ['id' => 'tariff_id']);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\query\AgencyPayLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\agency\models\query\AgencyPayLogQuery(get_called_class());
    }

    public function myDelete()
    {
        $this->delete = 1;
        return $this->save(false, ['delete']);
    }

    public function unDelete()
    {
        $this->delete = 0;
        return $this->save(false, ['delete']);
    }
}
