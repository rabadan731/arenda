<?php

namespace common\modules\agency\models;

use modules\log\behaviors\LogBehavior;
use Yii;

/**
 * This is the model class for table "worker_jobs".
 *
 * @property integer $id
 * @property integer $worker_id
 * @property string $address_text
 * @property string $address_map
 * @property string $need_to
 * @property string $price
 * @property integer $date_event
 * @property integer $date_sent
 * @property integer $status
 * @property integer $delete
 *
 * @property Workers $worker
 */
class WorkerJobs extends \yii\db\ActiveRecord
{

    public $lat;
    public $lng;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'worker_jobs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worker_id'], 'required'],
            [['price','lat','lng'], 'number'],
            [['price','lat','lng'], 'default','value'=>0],
            [['worker_id', 'status'], 'integer'],
            [['date_event', 'date_sent'], 'safe'],
            [['status','lat','lng'],  'default','value'=>0],
            [['need_to'], 'string'],
            [['address_text', 'address_map'], 'string', 'max' => 512],
            [
                ['worker_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Workers::className(),
                'targetAttribute' => ['worker_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                    => Yii::t('common', 'ID'),
            'worker_id'             => Yii::t('common', 'Worker'),
            'worker.fio'            => Yii::t('common', 'Worker'),
            'address_text'          => Yii::t('common', 'Address'),
            'address_map'           => Yii::t('common', 'Address Map'),
            'need_to'               => Yii::t('common', 'Need to make'),
            'price'                 => Yii::t('common', 'Price'),
            'date_event'            => Yii::t('common', 'Date event'),
            'date_sent'             => Yii::t('common', 'Date sent'),
            'status'                => Yii::t('common', 'Status'),
            'lat'                   => Yii::t('common', 'Latitude'),
            'lng'                   => Yii::t('common', 'Longitude'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Workers::className(), ['id' => 'worker_id']);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\query\WorkerJobsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\agency\models\query\WorkerJobsQuery(get_called_class());
    }

    public function getLat()
    {
        if (empty($this->address_map)) {
            return null;
        }

        $latLng = explode(',', $this->address_map);
        return array_shift($latLng);
    }

    public function getLng()
    {
        if (empty($this->address_map)) {
            return null;
        }

        $latLng = explode(',', $this->address_map);
        return array_pop($latLng);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'log' => [
                'class' => LogBehavior::className(),
                'types' => [
                    'update' => function ($m) {
                        /* @var $m self */
                        return "Изменил рабочего \"{$m->worker->fio}\" задание \"{$m->need_to}\"";
                    },
                    'insert' => function ($m) {
                        /* @var $m self */
                        return "Добавил рабочему \"{$m->worker->fio}\" задание \"{$m->id}\"";
                    },
                    'delete' => function ($m) {
                        /* @var $m self */
                        return "Удалил задание \"{$m->id}\" у рабочего \"{$m->worker->fio}\" ";
                    }
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $this->date_event = is_int($this->date_event)?$this->date_event:strtotime($this->date_event);
        $this->date_sent = is_int($this->date_sent)?$this->date_sent:strtotime($this->date_sent);

        return parent::beforeSave($insert);
    }

    public function myDelete()
    {
        $this->delete = 1;
        return $this->save(false, ['delete']);
    }

    public function unDelete()
    {
        $this->delete = 0;
        return $this->save(false, ['delete']);
    }
}
