<?php

namespace common\modules\agency\models;

use Yii;

/**
 * This is the model class for table "rent_agency".
 *
 * @property integer $id
 * @property integer $rent_id
 * @property integer $agency_id
 * @property integer $access_type
 *
 * @property Agency $agency
 * @property Rent $rent
 */
class RentAgency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rent_agency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rent_id', 'agency_id'], 'required'],
            [['rent_id', 'agency_id', 'access_type'], 'integer'],
            [
                ['agency_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Agency::className(),
                'targetAttribute' => ['agency_id' => 'id']
            ],
            [
                ['rent_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Rent::className(),
                'targetAttribute' => ['rent_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'rent_id' => Yii::t('common', 'Rent ID'),
            'agency_id' => Yii::t('common', 'Agency ID'),
            'access_type' => Yii::t('common', 'Access Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'agency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRent()
    {
        return $this->hasOne(Rent::className(), ['id' => 'rent_id']);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\query\RentAgencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\agency\models\query\RentAgencyQuery(get_called_class());
    }
}
