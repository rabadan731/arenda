<?php

namespace common\modules\agency\models;

use modules\log\behaviors\LogBehavior;
use Yii;

/**
 * This is the model class for table "worker_payments".
 *
 * @property integer $id
 * @property integer $worker_id
 * @property string $title
 * @property integer $date_invoice
 * @property string $price
 * @property integer $paid
 * @property integer $for_rent_id
 * @property integer $delete
 *
 * @property Workers $worker
 */
class WorkerPayments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'worker_payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worker_id', 'title'], 'required'],
            [['worker_id', 'date_invoice', 'paid', 'for_rent_id'], 'integer'],
            [['price'], 'number'],
            [['title'], 'string', 'max' => 512],
            [
                ['worker_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Workers::className(),
                'targetAttribute' => ['worker_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => Yii::t('common', 'ID'),
            'worker_id'     => Yii::t('common', 'Worker ID'),
            'title'         => Yii::t('common', 'Title'),
            'date_invoice'  => Yii::t('common', 'Date Invoice'),
            'price'         => Yii::t('common', 'Price'),
            'paid'          => Yii::t('common', 'Paid'),
            'for_rent_id'   => Yii::t('common', 'For Rent ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Workers::className(), ['id' => 'worker_id']);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\query\WorkerPaymentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\agency\models\query\WorkerPaymentsQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'log' => [
                'class' => LogBehavior::className(),
                'types' => [
                    'update' => function ($m) {
                        /* @var $m self */
                        return "Изменил оплату \"{$m->title}\" ID:{$m->id} сумма: {$m->price}";
                    },
                    'insert' => function ($m) {
                        /* @var $m self */
                        return "Добавил оплату \"{$m->title}\" ID:{$m->id} сумма: {$m->price}";
                    },
                    'delete' => function ($m) {
                        /* @var $m self */
                        return "Удалил оплату \"{$m->title}\" ID:{$m->id} сумма: {$m->price}";
                    }
                ]
            ]
        ];
    }

    public function myDelete()
    {
        $this->delete = 1;
        return $this->save(false, ['delete']);
    }

    public function unDelete()
    {
        $this->delete = 0;
        return $this->save(false, ['delete']);
    }
}
