<?php

namespace common\modules\agency\models;

use common\modules\clients\models\Clients;
use modules\users\models\backend\Users;
use Yii;
use abeautifulsite\SimpleImage;
use modules\log\behaviors\LogBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "agency".
 *
 * @property integer $id
 * @property string $title
 * @property string $logo
 * @property string $watermark
 * @property string $watermarkPath
 * @property string $balance
 * @property integer $status
 * @property integer $delete
 * @property integer $tariff_id
 * @property integer $date_begin
 * @property integer $date_end
 * @property boolean $balance_editing
 *
 * @property AgencyTariff $tariff
 *
 * @property AgencyPayLog[] $agencyPayLogs
 * @property Clients[] $clients
 * @property ForRent[] $forRents
 * @property Rent[] $rents
 * @property Workers[] $workers
 * @property Users $user
 */
class Agency extends \yii\db\ActiveRecord
{
    private $balance_editing = false;
    /**
     * Путь для сохранения файлов (аватарок)
     */
    const AVATAR_PATH = '@files/agency/avatars/';
    const WATERMARK_PATH = '@files/agency/watermark/';

    /**
     * URL директории с аватарками
     */
    const AVATAR_URL = '@urlFiles/agency/avatars/';
    const WATERMARK_URL = '@urlFiles/agency/watermark/';


    /**
     * URL к аватарке по умолчанию
     */
    const DEFAULT_AVATAR_URL = '@urlFiles/noimage.png';

    const STATUS_BLOCK = 9;
    const STATUS_NEW = 0;
    const STATUS_ACTIVE = 5;
    const STATUS_END_PAY = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['delete'], 'integer'],
            [
                ['status', 'date_begin', 'date_end', 'tariff_id'],
                'integer',
                'when' => function ($model) {
                    if (Yii::$app->user->can('admin')) {
                        return true;
                    }
                    return $model->balance_editing;
                }
            ],
            [['tariff_id'], 'validateTariff'],
            [
                ['balance'],
                'number',
                'when' => function ($model) {
                    if (Yii::$app->user->can('admin')) {
                        return true;
                    }
                    return $model->balance_editing;
                }
            ],
            [['balance'], 'default', 'value' => 0],
            [['title'], 'string', 'max' => 256],
            [['logo', 'watermark'], 'file', 'mimeTypes' => ['image/png', 'image/gif', 'image/jpeg']],
        ];
    }


    /**
     * @param $attribute
     * @param $params
     * @return bool
     */
    public function validateTariff($attribute, $params)
    {
        if ($this->tariff === $this->getOldAttribute('tariff_id') &&
            $this->balance === $this->getOldAttribute('balance')) {
            return true;
        }

        $tariff = $this->tariff;
        if ($tariff !== null) {
            if ($tariff->price > $this->balance) {
                $this->addError($attribute, Yii::t('common', 'Insufficient funds'));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'title' => Yii::t('common', 'Title'),
            'logo' => Yii::t('common', 'Logo'),
            'balance' => Yii::t('common', 'Balance'),
            'status' => Yii::t('common', 'Status'),
            'date_begin' => Yii::t('common', 'Date Begin'),
            'date_end' => Yii::t('common', 'Date End'),
            'tariff_id' => Yii::t('common', 'Tariff'),
            'tariff.title' => Yii::t('common', 'Tariff'),
            'tariffDateRange' => Yii::t('common', 'Tariff period'),
            'tariffDaysLeft' => Yii::t('common', 'Days left'),
        ];
    }

    /**
     * @return String
     */
    public function getTariffDateRange()
    {
        return
            Yii::t("common", "With") .
            " " .
            Yii::$app->formatter->asDate($this->date_begin) .
            " " .
            Yii::t("common", "at") .
            " " .
            Yii::$app->formatter->asDate($this->date_end);
    }

    /**
     * @return String
     */
    public function getTariffDaysLeft()
    {
        return ceil(($this->date_end - time()) / 86400);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\query\AgencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\agency\models\query\AgencyQuery(get_called_class());
    }

    /**
     * Получаем все роли
     * @return array
     */
    public static function getAgencies()
    {
        $all_agency = []; // Yii::t("common", "No agencies")
        foreach (ArrayHelper::map(self::find()->orderBy('title')->all(), 'id', 'title') as $k=>$v) {
            $all_agency[$k] = $v;
        }
        return $all_agency;
    }

    /**
     * Полный путь к аватарке
     * @param bool $small Уменьшиная копия
     * @param bool $exists Делать проверку на наличие файла или нет
     * @return string
     */
    public function getPathAvatar($small = false, $exists = true)
    {
        $path = Yii::getAlias(self::AVATAR_PATH) .
            ($small ? 'small_' : '') .
            ($this->logo ? $this->logo : $this->getOldAttr('logo'));
        return is_file($path) || $exists === false ? $path : null;
    }

    public function getOldAttr($attribute)
    {
        return isset($this->oldAttributes[$attribute])?$this->oldAttributes[$attribute]:null;
    }

    /**
     * URL к аватарке
     * @param bool $small Уменьшиная копия
     * @return string
     */
    public function getUrlAvatar($small = false)
    {
        if (is_file($this->getPathAvatar($small))) {
            return Yii::getAlias(self::AVATAR_URL) . ($small ? 'small_' : '') . $this->logo;
        }
        return Yii::getAlias(self::DEFAULT_AVATAR_URL);
    }

    /**
     * Удаление аватара
     */
    public function deleteAvatar($save = false)
    {
        if (is_file($this->getPathAvatar())) {
            unlink($this->getPathAvatar());
        }
        if (is_file($this->getPathAvatar(true))) {
            unlink($this->getPathAvatar(true));
        }
        $this->logo = null;
        if ($save) {
            $this->save(0);
        }
    }

    /**
     * Удаление аватара
     */
    public function deleteWatermark($save = false)
    {
        if (is_file(Yii::getAlias(self::WATERMARK_PATH) . $this->watermark)) {
            unlink(Yii::getAlias(self::WATERMARK_PATH) . $this->watermark);
        }
        $this->watermark = null;
        if ($save) {
            $this->save(0);
        }
    }

    /**
     * Загружаем аватар
     * @throws \Exception
     */
    private function uploadAvatar()
    {
        FileHelper::createDirectory(Yii::getAlias(self::AVATAR_PATH));
        $avatar = UploadedFile::getInstance($this, 'logo');
        if ($avatar !== null) {
            $this->deleteAvatar();
            $this->logo = uniqid() . '.jpg';
            if ($avatar->saveAs($this->getPathAvatar(false, false))) {
                $small = new SimpleImage($this->getPathAvatar());
                $small->thumbnail(120, 150);
                $small->save($this->getPathAvatar(true, false), 100, $avatar->extension);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!empty(UploadedFile::getInstance($this, 'logo'))) {
            $this->uploadAvatar();
        } else {
            $this->logo = $this->getOldAttr('logo');
        }

        $watermarkUploadedFile = UploadedFile::getInstance($this, 'watermark');
        if (!empty($watermarkUploadedFile)) {
            if ($watermarkUploadedFile !== null) {
                $this->deleteWatermark();
                $this->watermark = uniqid() . '.jpg';
                FileHelper::createDirectory(Yii::getAlias(self::WATERMARK_PATH));
                $watermarkUploadedFile->saveAs(Yii::getAlias(self::WATERMARK_PATH) . $this->watermark);
            }
        } else {
            $this->watermark = $this->getOldAttr('watermark');
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return null|string
     */
    public function getWatermarkUrl()
    {
        return is_null($this->watermark)?null:(Yii::getAlias(self::WATERMARK_URL) . $this->watermark);
    }

    /**
     * @return null|string
     */
    public function getWatermarkPath()
    {
        return is_null($this->watermark)?null:(Yii::getAlias(self::WATERMARK_PATH) . $this->watermark);
    }

    /**
     *
     */
    public function balanceChanged()
    {
        $this->balance_editing = true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(AgencyTariff::className(), ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgencyPayLogs()
    {
        return $this->hasMany(AgencyPayLog::className(), ['agency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Clients::className(), ['agency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForRents()
    {
        return $this->hasMany(ForRent::className(), ['agency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRents()
    {
        return $this->hasMany(Rent::className(), ['agency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkers()
    {
        return $this->hasMany(Workers::className(), ['agency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['agency_id' => 'id']);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'log' => [
                'class' => LogBehavior::className(),
                'types' => [
                    'update' => function ($model) {
                        /* @var $model self */
                        return "Изменил данные агенства \"{$model->title}\" ID:{$model->id}";
                    },
                    'tariff' => function ($model) {
                        /* @var $model self */
                        return "Изменил тариф агенства \"{$model->title}\" ID:{$model->id} 
                        Тариф:{$model->tariff->title}";
                    },
                    'insert' => function ($model) {
                        /* @var $model self */
                        return "Добавил агенство \"{$model->title}\" ID:{$model->id}";
                    },
                    'delete' => function ($model) {
                        /* @var $model self */
                        return "Удалил агенство \"{$model->title}\" ID:{$model->id}";
                    }
                ]
            ]
        ];
    }

    /**
     * @param $id
     * @return string
     */
    public static function getTitle($id)
    {
        $find = self::findOne($id);
        if ($find !== null) {
            return $find->title;
        } else {
            return 'no load id: ' . $id;
        }
    }

    /**
     * @return bool
     */
    public function myDelete()
    {
        $this->delete = 1;
        return $this->save(false, ['delete']);
    }

    /**
     * @return bool
     */
    public function unDelete()
    {
        $this->delete = 0;
        return $this->save(false, ['delete']);
    }
}
