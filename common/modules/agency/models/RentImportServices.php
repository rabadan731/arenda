<?php

namespace common\modules\agency\models;

use common\modules\clients\models\Clients;
use common\modules\log\models\LogExchange;
use ICal\EventObject;
use ICal\ICal;
use Yii;
use yii\web\NotFoundHttpException;
use common\modules\agency\models\query\RentImportServicesQuery;

/**
 * This is the model class for table "rent_import_services".
 *
 * @property integer $service_id
 * @property integer $rent_id
 * @property integer $agency_id
 * @property string $title
 * @property string $import_url
 * @property string $import_date
 *
 * @property RentImportList[] $rentImportLists
 * @property Rent $rent
 */
class RentImportServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rent_import_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rent_id', 'agency_id', 'title', 'import_url'], 'required'],
            [['rent_id', 'agency_id'], 'integer'],
            [['import_url'], 'url'],
            [['title'], 'string', 'max' => 256],
            [['import_date'], 'string', 'max' => 1024],
            [
                ['rent_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Rent::className(),
                'targetAttribute' => ['rent_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'service_id' => Yii::t('common', 'Service ID'),
            'rent_id' => Yii::t('common', 'Rent ID'),
            'agency_id' => Yii::t('common', 'Agency ID'),
            'title' => Yii::t('common', 'Title'),
            'import_url' => Yii::t('common', 'Import Url'),
            'import_date' => Yii::t('common', 'Import Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRentImportLists()
    {
        return $this->hasMany(RentImportList::className(), ['service_id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRent()
    {
        return $this->hasOne(Rent::className(), ['id' => 'rent_id']);
    }

    /**
     * @inheritdoc
     * @return RentImportServicesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RentImportServicesQuery(get_called_class());
    }

    /**
     * @return bool
     */
    public function importCalendar()
    {
        $fileContent = $this->curlLoadFile($this->import_url);
        if ($fileContent === false) {
            return false;
        }

        $iCal = new ICal();
        $iCal->initString($fileContent);



        RentImportList::deleteAll([
            'rent_id' => $this->rent_id,
            'agency_id' => $this->agency_id,
            'service_id' => $this->service_id,
        ]);

        array_map(function ($event) {

            $md5 = md5($event->description.$event->summary);
            $phone = $this->getPhoneOnText($event->description);

            if ($phone === false) {
                $client = Clients::find()->andWhere(['import_md5'=>$md5])->one();
            } else {
                $client = Clients::find()->andWhere(['phone'=>$phone])->one();
            }

            if (is_null($client)) {
                $client = new Clients();
                $client->agency_id = $this->agency_id;
                $client->fio = $event->summary;
                $client->phone = $phone;
                $client->import_md5 = $md5;
                $client->save(false);
            }

            $model = new RentImportList();
            $model->rent_id = $this->rent_id;
            $model->agency_id = $this->agency_id;
            $model->uid = $event->uid;
            $model->service_id = $this->service_id;
            $model->date_import = time();
            $model->date_from = strtotime($event->dtstart);
            $model->date_to   = strtotime($event->dtend);
            $model->client_id = $client->id;



            if ($model->save()) {
                $this->import_date = time();
                $this->save(false, ['import_date']);
            }

        }, $iCal->events());

        $log = new LogExchange();

        if (Yii::$app->id == "app-console") {
            $log->ip = "127.0.0.1";
            $log->ua = "Console";
            $log->url = "";
        } else {
            $log->ip = Yii::$app->request->userIP;
            $log->ua = Yii::$app->request->userAgent;
            $log->url = Yii::$app->request->referrer;
        }

        $log->created_at = time();
        $log->rent_id = $this->rent_id;
        $log->service_id = $this->service_id;
        $log->text = "Import rent ({$this->rent_id})";
//            $log->description' => Yii::t('common', 'Description'),
        $log->save();

        return true;
    }

    public function getPhoneOnText($text)
    {
        $data = explode("\n", $text);

        $data = array_filter($data, function($item) {
            return strpos(mb_strtolower($item), 'phone') === 0;
        });

        if (count($data)) {
            $data = trim(trim(trim(trim(array_shift($data), 'PHONE')), ":"));
        } else {
            $data = false;
        }

        return $data;
    }

    /**
     * @param $importUrl
     * @return bool|mixed
     *
     */
    private function curlLoadFile($importUrl)
    {
        $ch = curl_init($importUrl);

        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36");

        $headers = array
        (
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*;q=0.8',
            'Accept-Language: ru,en-us;q=0.7,en;q=0.3',
            'Accept-Encoding: deflate',
            'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7'
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_COOKIEJAR, sys_get_temp_dir() . "/my_cookies.txt");
        curl_setopt($ch, CURLOPT_COOKIEFILE, sys_get_temp_dir() . "/my_cookies.txt");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch); // выполняем запрос curl

        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if ($code == 200) {
            return $result;
        }

        return false;
    }
}
