<?php

namespace common\modules\agency\models;

use modules\log\behaviors\LogBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "agency_tariff".
 *
 * @property integer $id
 * @property string $title
 * @property integer $month
 * @property string $price
 * @property integer $status
 * @property integer $delete
 *
 * @property Agency[] $agencies
 * @property AgencyPayLog[] $agencyPayLogs
 */
class AgencyTariff extends \yii\db\ActiveRecord
{
    const MONTH_DAYS = 30;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agency_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['price'], 'number'],
            [['month', 'status', 'delete'], 'integer'],
            [['title'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'title' => Yii::t('common', 'Title'),
            'month' => Yii::t('common', 'Month'),
            'price' => Yii::t('common', 'Price'),
            'status' => Yii::t('common', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgencies()
    {
        return $this->hasMany(Agency::className(), ['tariff_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgencyPayLogs()
    {
        return $this->hasMany(AgencyPayLog::className(), ['tariff_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\query\AgencyTariffQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\agency\models\query\AgencyTariffQuery(get_called_class());
    }

    public function getPriceDay()
    {
        return $this->price / $this->getMonthDay();
    }

    public function getMonthDay()
    {
        return $this->month * self::MONTH_DAYS;
    }


    /**
     * Лист клиентов
     * @param ForRent $get_model Уменьшиная копия
     * @return array
     */
    public static function getList($andPrice = false)
    {
        $find = self::find();


        if ($andPrice) {
            $list = [];
            foreach ($find->all() as $item) {
                $list[$item->id] = $item->title . ' (' . ($item->price) . " €)";
            }

            return $list;
        } else {
            return ArrayHelper::map($find->select('id,title')->all(), 'id', 'title');
        }
    }


    public function getTprice()
    {
        return $this->title . ' - ' . $this->price;
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'log' => [
                'class' => LogBehavior::className(),
                'types' => [
                    'update' => function ($model) {
                        /* @var $model self */
                        return "Изменил тариф \"{$model->title}\" ID:{$model->id}";
                    },
                    'insert' => function ($model) {
                        /* @var $model self */
                        return "Добавил тариф \"{$model->title}\" ID:{$model->id}";
                    },
                    'delete' => function ($model) {
                        /* @var $model self */
                        return "Удалил тариф \"{$model->title}\" ID:{$model->id}";
                    }
                ]
            ]
        ];
    }


    public function myDelete()
    {
        $this->delete = 1;
        return $this->save(false, ['delete']);
    }

    public function unDelete()
    {
        $this->delete = 0;
        return $this->save(false, ['delete']);
    }
}
