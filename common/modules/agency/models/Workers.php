<?php

namespace common\modules\agency\models;

use abeautifulsite\SimpleImage;
use common\grid\TypeWorkerEnum;
use modules\log\behaviors\LogBehavior;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "workers".
 *
 * @property integer $id
 * @property integer $worker_type
 * @property string $workerTitle
 * @property string $fio
 * @property string $photo
 * @property string $email
 * @property string $phone
 * @property string $balance
 * @property integer $agency_id
 * @property integer $status
 * @property integer $delete
 *
 * @property WorkerJobs $workerJobs
 * @property Agency $agency
 */
class Workers extends \yii\db\ActiveRecord
{
    /**
     * Путь для сохранения файлов (аватарок)
     */
    const AVATAR_PATH = '@files/workers/avatars/';

    /**
     * URL директории с аватарками
     */
    const AVATAR_URL = '@urlFiles/workers/avatars/';

    /**
     * URL к аватарке по умолчанию
     */
    const DEFAULT_AVATAR_URL = '@urlFiles/workers/avatar.jpg';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'workers';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio','worker_type'], 'required'],
            [['email'], 'email'],
            [['worker_type', 'agency_id', 'status'], 'integer'],
            [['balance'], 'number'],
            [['fio'], 'string', 'max' => 512],
            ['photo', 'file', 'mimeTypes' => ['image/png', 'image/gif', 'image/jpeg']],
            [['email', 'phone'], 'string', 'max' => 256],
            [
                ['agency_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Agency::className(),
                'targetAttribute' => ['agency_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'worker_type' => Yii::t('common', 'Worker type'),
            'workerTitle' => Yii::t('common', 'Worker type'),
            'fio' => Yii::t('common', 'Fio'),
            'photo' => Yii::t('common', 'Photo'),
            'email' => Yii::t('common', 'Email'),
            'phone' => Yii::t('common', 'Phone'),
            'balance' => Yii::t('common', 'Balance'),
            'agency_id' => Yii::t('common', 'Agency'),
            'status' => Yii::t('common', 'Status'),
        ];
    }


    public function getWorkerTitle()
    {
        return TypeWorkerEnum::getByKey($this->worker_type);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerJobs()
    {
        return $this->hasOne(WorkerJobs::className(), ['worker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'agency_id']);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\query\WorkersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\agency\models\query\WorkersQuery(get_called_class());
    }

    /**
     * Полный путь к аватарке
     * @param bool $small Уменьшиная копия
     * @param bool $exists Делать проверку на наличие файла или нет
     * @return string
     */
    public function getPathAvatar($small = false, $exists = true)
    {
        $path = Yii::getAlias(self::AVATAR_PATH) .
            ($small ? 'small_' : '') .
            ($this->photo ? $this->photo : $this->getOldAttr('photo'));
        return is_file($path) || $exists === false ? $path : null;
    }

    /**
     * URL к аватарке
     * @param bool $small Уменьшиная копия
     * @return string
     */
    public function getUrlAvatar($small = false)
    {
        if (is_file($this->getPathAvatar($small))) {
            return Yii::getAlias(self::AVATAR_URL) . ($small ? 'small_' : '') . $this->photo;
        }
        return Yii::getAlias(self::DEFAULT_AVATAR_URL);
    }

    /**
     * Удаление аватара
     */
    public function deleteAvatar($save = false)
    {
        if (is_file($this->getPathAvatar())) {
            unlink($this->getPathAvatar());
        }
        if (is_file($this->getPathAvatar(true))) {
            unlink($this->getPathAvatar(true));
        }
        $this->photo = null;
        if ($save) {
            $this->save(0);
        }
    }

    /**
     * Загружаем аватар
     * @throws \Exception
     */
    private function uploadAvatar()
    {
        $avatar = UploadedFile::getInstance($this, 'photo');
        if ($avatar !== null) {
            $this->deleteAvatar();
            $this->photo = uniqid() . '.jpg';
            if ($avatar->saveAs($this->getPathAvatar(false, false))) {
                $small = new SimpleImage($this->getPathAvatar());
                $small->thumbnail(120, 150);
                $small->save($this->getPathAvatar(true, false), 100, $avatar->extension);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!Yii::$app->user->can("admin")) {
            $this->agency_id = Yii::$app->user->identity->agency_id;
        }

        if (!empty(UploadedFile::getInstance($this, 'photo'))) {
            $this->uploadAvatar();
        } else {
            $this->photo = $this->getOldAttr('photo');
        }
        return parent::beforeSave($insert);
    }

    public function getOldAttr($attribute)
    {
        return isset($this->oldAttributes[$attribute])?$this->oldAttributes[$attribute]:null;
    }

    /**
     * Лист клиентов
     * @param ForRent $get_model Уменьшиная копия
     * @return array
     */
    public static function getList($get_model, $worker_type_id = null)
    {
        $find = self::find();


        if (isset($get_model->agency_id)) {
            if ($get_model->isNewRecord) {
                if (!Yii::$app->user->can("admin")) {
                    $find->thisAgency();
                }
            } else {
                $find->andWhere(['agency_id'=>$get_model->agency_id]);
            }
        }

        if (!is_null($worker_type_id)) {
            $find->type($worker_type_id);
            return ArrayHelper::map($find->all(), 'id', 'fio');
        } else {
            return ArrayHelper::map($find->all(), 'id', 'fio', 'worker_type');
        }
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'log' => [
                'class' => LogBehavior::className(),
                'types' => [
                    'update' => function ($model) {
                        /* @var $model self */
                        return "Изменил рабочего \"{$model->fio}\" ID:{$model->id}";
                    },
                    'insert' => function ($model) {
                        /* @var $model self */
                        return "Добавил рабочего \"{$model->fio}\" ID:{$model->id}";
                    },
                    'delete' => function ($model) {
                        /* @var $model self */
                        return "Удалил рабочего \"{$model->fio}\" ID:{$model->id}";
                    }
                ]
            ]
        ];
    }


    public function myDelete()
    {
        $this->delete = 1;
        return $this->save(false, ['delete']);
    }

    public function unDelete()
    {
        $this->delete = 0;
        return $this->save(false, ['delete']);
    }
}
