<?php

namespace common\modules\agency\models;

use common\modules\clients\models\Clients;
use modules\log\behaviors\LogBehavior;
use Yii;
use yii\helpers\ArrayHelper;
use common\modules\agency\models\query\ForRentQuery;

/**
 * This is the model class for table "for_rent".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $rent_id
 * @property integer $check_in
 * @property integer $check_out
 * @property string $comment
 * @property integer $price_all
 * @property integer $price_day
 * @property integer $price_deposit
 * @property integer $price_prepay
 * @property integer $workerAgent
 * @property integer $workerDriver
 * @property integer $workerCleaner
 * @property integer $workerMaster
 * @property integer $worker_agent
 * @property integer $worker_driver
 * @property integer $worker_cleaner
 * @property integer $worker_master
 * @property integer $agency_id
 * @property integer $import
 * @property integer $status
 * @property integer $statusText
 * @property integer $reservations
 * @property integer $delete
 *
 * @property Agency $agency
 * @property Clients $client
 * @property Rent $rent
 */
class ForRent extends \yii\db\ActiveRecord
{

    const STATUS_CLOSE = 10;
    const STATUS_OPEN = 0;

    const RESERVATIONS_TEMP = 0;
    const RESERVATIONS_YES = 7;
    const RESERVATIONS_DELETE = 9;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'for_rent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'rent_id'], 'required'],
            [[
                'client_id',
                'rent_id',
                'worker_agent',
                'worker_driver',
                'worker_cleaner',
                'worker_master',
                'agency_id',
                'import',
                'reservations',
                'status'
            ], 'integer'],
            [['comment'], 'string'],
            [['check_in', 'check_out'], 'default', 'value' => null],
            [['check_in', 'check_out'], 'validateRent'],
            [['price_all', 'price_day', 'price_deposit',], 'number'],
            [['price_all', 'price_day', 'price_deposit', 'price_prepay'], 'default', 'value' => 0],
            [
                ['agency_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Agency::className(),
                'targetAttribute' => ['agency_id' => 'id']
            ],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Clients::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
            [
                ['rent_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Rent::className(),
                'targetAttribute' => ['rent_id' => 'id']
            ],
        ];
    }

    public function validateRent($attribute, $params)
    {
        $find = self::find();
        if (!$this->isNewRecord) {
            $find->andWhere(['<>', 'for_rent.id', $this->id]);
        }

        $result = $find->andWhere(['for_rent.rent_id' => $this->rent_id])
            ->date($this->check_in, $this->check_out, true)->notDeleted()->one();

        $findImport = RentImportList::find();
        $resultImport = $findImport->andWhere(['rent_id' => $this->rent_id])
            ->date($this->check_in, $this->check_out, true)->one();


        if (!is_null($result) || !is_null($resultImport)) {
            $this->addError($attribute, Yii::t("common", "During this period, the object is busy"));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'client_id' => Yii::t('common', 'Client'),
            'rent' => Yii::t('common', 'Rent'),
            'client.fio' => Yii::t('common', 'Client'),
            'check_in' => Yii::t('common', 'Check in'),
            'check_out' => Yii::t('common', 'Check out'),
            'comment' => Yii::t('common', 'Comment'),
            'price_all' => Yii::t('common', 'Price for the entire period'),
            'price_day' => Yii::t('common', 'Price per day'),
            'price_prepay' => Yii::t('common', 'Prepayment'),
            'price_deposit' => Yii::t('common', 'Deposit'),
            'worker_agent' => Yii::t('common', 'Responsible agent'),
            'worker_driver' => Yii::t('common', 'Responsible driver'),
            'worker_cleaner' => Yii::t('common', 'Responsible for cleaning'),
            'worker_master' => Yii::t('common', 'Responsible for cleaning'),
            'workerAgent.fio' => Yii::t('common', 'Responsible agent'),
            'workerDriver.fio' => Yii::t('common', 'Responsible driver'),
            'workerCleaner.fio' => Yii::t('common', 'Responsible for cleaning'),
            'workerMaster.fio' => Yii::t('common', 'Responsible for master'),
            'agency_id' => Yii::t('common', 'Agency ID'),
            'import' => Yii::t('common', 'Import'),
            'status' => Yii::t('common', 'Status'),
            'reservations' => Yii::t('common', 'Reservations'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'agency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRent()
    {
        return $this->hasOne(Rent::className(), ['id' => 'rent_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerDriver()
    {
        return $this->hasOne(WorkerJobs::className(), ['id' => 'worker_driver']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerAgent()
    {
        return $this->hasOne(WorkerJobs::className(), ['id' => 'worker_agent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerCleaner()
    {
        return $this->hasOne(WorkerJobs::className(), ['id' => 'worker_cleaner']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerMaster()
    {
        return $this->hasOne(WorkerJobs::className(), ['id' => 'worker_master']);
    }


    /**
     * @inheritdoc
     * @return ForRentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ForRentQuery(get_called_class());
    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!Yii::$app->user->can("admin")) {
            $this->agency_id = Yii::$app->user->identity->agency_id;
        }

        $this->dateToTime();

        return parent::beforeSave($insert);
    }


    public function dateToTime()
    {
        $this->check_in = is_int($this->check_in) ? $this->check_in : strtotime($this->check_in);
        $this->check_out = is_int($this->check_out) ? $this->check_out : strtotime($this->check_out);
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'log' => [
                'class' => LogBehavior::className(),
                'types' => [
                    'update' => function ($model) {
                        /* @var $model self */
                        return "Изменил запись о сдаче объекта \"{$model->rent->address_text} 
                        - ($model->rent_id)\" клиенту \"{$model->client->fio}\"";
                    },
                    'insert' => function ($model) {
                        /* @var $model self */
                        return "Сдал объект \"{$model->rent->address_text} 
                        ($model->rent_id)\" клиенту \"{$model->client->fio}\"";
                    },
                    'delete' => function ($model) {
                        /* @var $model self */
                        return "Удалил запись о сдаче объекта \"{$model->rent->address_text} 
                        ($model->rent_id)\" клиенту \"{$model->client->fio}\"";
                    }
                ]
            ]
        ];
    }


    public static function getList($get_model)
    {
        $find = self::find();

        if (isset($get_model->agency_id)) {
            if ($get_model->isNewRecord) {
                if (!Yii::$app->user->can("admin")) {
                    $find->thisAgency();
                }
            } else {
                $find->andWhere(['clients.agency_id' => $get_model->agency_id]);
            }
        }

        return ArrayHelper::map($find->all(), 'id', 'id');
    }

    public function myDelete()
    {
        $this->delete = 1;
        return $this->save(false, ['delete']);
    }

    public function unDelete()
    {
        $this->delete = 0;
        return $this->save(false, ['delete']);
    }

    public static function paymentForTheMonth($time)
    {

        $start_period = mktime(0, 0, 0, date("m", $time), 1, date("Y", $time));
        $end_period = mktime(0, 0, 0, date("m", $time) + 1, 1, date("Y", $time));

        $find = self::find();
        if (!\Yii::$app->user->can("admin")) {
            $find->thisAgency();
        }

        $find->datetime($start_period, $end_period);

        $sum = 0;
        foreach ($find->all() as $item) {
            $priceSecond = ($item->price_all / ($item->check_out - $item->check_in));

            $sum += $priceSecond * (
                    ($item->check_out < $end_period ? $item->check_out : $end_period) -
                    ($item->check_in > $start_period ? $item->check_in : $start_period)
                );
        }

        return round($sum);
    }

    public static function timeForTheMonth($time)
    {

        $start_period = mktime(0, 0, 0, date("m", $time), 1, date("Y", $time));
        $end_period = mktime(0, 0, 0, date("m", $time) + 1, 1, date("Y", $time));

        $find = self::find();
        $rentCount = Rent::find();
        if (!\Yii::$app->user->can("admin")) {
            $find->thisAgency();
            $rentCount->thisAgency();
        }

        $find->datetime($start_period, $end_period);

        $period = $end_period - $start_period;
        $period_def = $period *= $rentCount->count();

        foreach ($find->all() as $item) {
            $period -=
                (
                    ($item->check_out < $end_period ? $item->check_out : $end_period) -
                    ($item->check_in > $start_period ? $item->check_in : $start_period)
                );
        }

        return (($period_def == 0) ? 0 : (100 - round($period * 100 / ($period_def))));
    }


    public static function timeForTheYear($time)
    {
        $result = [];

        for ($i = 11; $i >= 0; $i--) {
            $result[] = self::timeForTheMonth(mktime(0, 0, 0, date("m", $time) - $i, 1, date("Y", $time)));
        }

        return $result;
    }


    public static function paymentForTheYear($time)
    {
        $result = [];

        for ($i = 11; $i >= 0; $i--) {
            $result[] = self::paymentForTheMonth(mktime(0, 0, 0, date("m", $time) - $i, 1, date("Y", $time)));
        }

        return $result;
    }

    public function getStatusText()
    {
        return $this->check_out <= time()?"danger":($this->check_in > time()?"success":"warning");
    }
    
    public function getCheckInDate()
    {
        return date("Y-m-d H:i:s", $this->check_in);
    }
    
    public function getCheckOutDate()
    {
        return date("Y-m-d H:i:s", $this->check_out);
    }
    
}
