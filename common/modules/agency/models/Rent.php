<?php

namespace common\modules\agency\models;

use common\modules\agency\models\query\RentQuery;
use common\modules\eav\models\search\EavParamValueSearch;
use Yii;
use abeautifulsite\SimpleImage;
use modules\log\behaviors\LogBehavior;
use modules\users\models\backend\Users;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "rent".
 *
 * @property integer $id
 * @property string $description
 * @property string $address_text
 * @property string $address_map
 * @property string $latitude
 * @property string $longitude
 * @property string $fio
 * @property string $photo
 * @property string $email
 * @property string $title
 * @property string $hash
 * @property string $phone
 * @property integer $contract_date_begin
 * @property integer $contract_date_end
 * 
 * @property string $contract_month_price
 * @property string $contract_week_price
 * @property string $contract_day_price
 * 
 * @property string $price_month
 * @property string $price_week
 * @property string $price_day
 * @property string $price_sale
 *
 * @property integer $agency_id
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $import
 * @property integer $status
 * @property integer $status_rent
 * @property integer $status_buy
 * @property integer $delete
 * @property string $img_sort
 *
 * @property Agency $agency
 */
class Rent extends \yii\db\ActiveRecord
{
    public $lat;
    public $lng;

    const STATUS_FREE = 0;
    const STATUS_FREE3DAY = 3;
    const STATUS_BUSY = 5;

    /**
     * Путь для сохранения файлов (аватарок)
     */
    const AVATAR_PATH = '@files/rent/avatars/';

    /**
     * URL директории с аватарками
     */
    const AVATAR_URL = '@urlFiles/rent/avatars/';

    /**
     * URL к аватарке по умолчанию
     */
    const DEFAULT_AVATAR_URL = '@urlFiles/rent/placeHolder.png';
    
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            'log' => [
                'class' => LogBehavior::className(),
                'types' => [
                    'update' => function ($model) {
                        /* @var $model self */
                        return "Изменил объект аренды \"{$model->address_text}\" ID:{$model->id}";
                    },
                    'insert' => function ($model) {
                        /* @var $model self */
                        return "Добавил объект аренды \"{$model->address_text}\" ID:{$model->id}";
                    },
                    'delete' => function ($model) {
                        /* @var $model self */
                        return "Удалил объект аренды \"{$model->address_text}\" ID:{$model->id}";
                    }
                ]
            ],
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(Users::className(), ['id' => 'updated_by']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'agency_id'], 'required'],
            [['description'], 'string'],
            [[
                'contract_month_price',
                'contract_week_price',
                'contract_day_price',
                'price_sale',
                'price_month',
                'price_week',
                'price_day',
            ], 'number'],
            [[
                'agency_id',
                'import',
                'status',
                'status_buy',
                'status_rent',
                'created_by',
                'updated_by'
            ], 'integer'],
            [['address_text', 'address_map', 'fio', 'photo', 'title'], 'string', 'max' => 512],
            [['email', 'phone', 'img_sort'], 'string', 'max' => 256],
            [['latitude', 'longitude'], 'string', 'max' => 128],
            [['hash'], 'string', 'max' => 35],
            [[
                'contract_month_price',
                'contract_week_price',
                'contract_day_price',
                'price_month',
                'price_week',
                'price_day',
                'latitude',
                'longitude'
            ], 'default', 'value' => 0],
            [['contract_date_begin', 'contract_date_end', 'agency_id'], 'default', 'value' => null],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'title' => Yii::t('common', 'Title'),
            'created_by' => Yii::t('common', 'Created by'),
            'author.name' => Yii::t('common', 'Author'),
            'updated_by' => Yii::t('common', 'Updated by'),
            'description' => Yii::t('common', 'Description'),
            'address_text' => Yii::t('common', 'Address'),
            'address_map' => Yii::t('common', 'Address Map'),
            'fio' => Yii::t('common', 'Fio'),
            'photo' => Yii::t('common', 'Photo'),
            'email' => Yii::t('common', 'Email'),
            'phone' => Yii::t('common', 'Phone'),
            'hash' => Yii::t('common', 'Hash'),
            'contract_date_begin' => Yii::t('common', 'Contract start date'),
            'contract_date_end' => Yii::t('common', 'Contract end date'),
            'contract_month_price' => Yii::t('common', 'Monthly price for contract'),
            'contract_week_price' => Yii::t('common', 'Weekly price for contract'),
            'contract_day_price' => Yii::t('common', 'Daily price for contract'),
            'price_month' => Yii::t('common', 'Monthly price'),
            'price_week' => Yii::t('common', 'Weekly price'),
            'price_sale' => Yii::t('common', 'Price sale'),
            'price_day' => Yii::t('common', 'Daily price'),
            'agency_id' => Yii::t('common', 'Agency'),
            'agency.title' => Yii::t('common', 'Agency'),
            'import' => Yii::t('common', 'Perform object import'),
            'status' => Yii::t('common', 'Status'), 
            'status_buy' => Yii::t('common', 'The object is for Sale'),
            'status_rent' => Yii::t('common', 'The object is for rent'),
            'latitude' => Yii::t('common', 'Latitude'),
            'longitude' => Yii::t('common', 'Longitude'),
        ];
    }

    /****************************      joins       ***************************************/
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'agency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForRent()
    {
        return $this->hasMany(ForRent::className(), ['rent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRentManagers()
    {
        return $this->hasMany(RentManager::className(), ['rent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManagers()
    {
        return $this->hasMany(
            Users::className(),
            ['id' => 'manager_id']
        )->viaTable(
            'rent_manager',
            ['rent_id' => 'id']
        );
    }

    /**
     * @return \yii\data\ActiveDataProvider
     */
    public function getEavParamProvider()
    {
        $eavParamSearch = new EavParamValueSearch();
        return $eavParamSearch->searchParam($this->className(), $this->id);
    }
    /*********************************** *******************************************/
    
    /**
     * @inheritdoc
     * @return RentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RentQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!Yii::$app->user->can("admin")) {
            $this->agency_id = Yii::$app->user->identity->agency_id;
        }

        if (!empty(UploadedFile::getInstance($this, 'photo'))) {
            $this->uploadAvatar();
        } else {
            $this->photo = $this->getOldAttr('photo');
        }

        if ($this->lat !== null && $this->lng !== null) {
            $this->address_map = implode(",", [
                str_replace(',', '.', $this->lat),
                str_replace(',', '.', $this->lng)
            ]);
        }

        if (empty($this->hash)) {
            $this->hash = md5(print_r($this->attributes, 1).time());
        }

        $this->contract_date_begin = strtotime($this->contract_date_begin);
        $this->contract_date_end = strtotime($this->contract_date_end);

        return parent::beforeSave($insert);
    }

    /**
     * @param $attribute
     * @return null
     */
    public function getOldAttr($attribute)
    {
        return isset($this->oldAttributes[$attribute])?$this->oldAttributes[$attribute]:null;
    }

    /**
     * @return string
     */
    public function getExportUrl()
    {
        if (is_null($this->hash)) {
            $this->save(false, ['hash']);
        }

        $baseUrl = Yii::$app->urlManager->getBaseUrl();

        Yii::$app->urlManager->setBaseUrl('');
        $url = Yii::$app->urlManager->createAbsoluteUrl([
            "/agency/rent/export",
            'id' => $this->id,
            'hash' => $this->hash,
        ]);
        Yii::$app->urlManager->setBaseUrl($baseUrl);
        return $url;
    }

    /**
     * Полный путь к аватарке
     * @param bool $small Уменьшиная копия
     * @param bool $exists Делать проверку на наличие файла или нет
     * @return string
     */
    public function getPathAvatar($small = false, $exists = true)
    {
        $path = Yii::getAlias(self::AVATAR_PATH) .
            ($small ? 'small_' : '') .
            ($this->photo ? $this->photo : $this->getOldAttr('photo'));
        return is_file($path) || $exists === false ? $path : null;
    }

    /**
     * URL к аватарке
     * @param bool $small Уменьшиная копия
     * @return string
     */
    public function getUrlAvatar($small = false)
    {
        if (is_file($this->getPathAvatar($small))) {
            return Yii::getAlias(self::AVATAR_URL) .
                ($small ? 'small_' : '') .
                $this->photo;
        }
        return Yii::getAlias(self::DEFAULT_AVATAR_URL);
    }

    /**
     * Удаление аватара
     */
    public function deleteAvatar($save = false)
    {
        if (is_file($this->getPathAvatar())) {
            unlink($this->getPathAvatar());
        }
        if (is_file($this->getPathAvatar(true))) {
            unlink($this->getPathAvatar(true));
        }
        $this->photo = null;
        if ($save) {
            $this->save(0);
        }
    }

    /**
     * Загружаем аватар
     * @throws \Exception
     */
    private function uploadAvatar()
    {
        FileHelper::createDirectory(Yii::getAlias(self::AVATAR_PATH));
        $avatar = UploadedFile::getInstance($this, 'photo');
        if ($avatar !== null) {
            $this->deleteAvatar();
            $this->photo = uniqid() . '.jpg';
            if ($avatar->saveAs($this->getPathAvatar(false, false))) {
                $small = new SimpleImage($this->getPathAvatar());
                $small->thumbnail(120, 150);
                $small->save($this->getPathAvatar(true, false), 100, $avatar->extension);
            }
        }
    }

    /**
     * Лист объектов
     * @param ForRent $get_model Уменьшиная копия
     * @return array
     */
    public static function getList($get_model)
    {
        $find = self::find();

        if (isset($get_model->agency_id)) {
            if ($get_model->isNewRecord) {
                if (!Yii::$app->user->can("admin")) {
                    $find->thisAgency();
                }
            } else {
                $find->andWhere(['agency_id' => $get_model->agency_id]);
            }
        }

        return ArrayHelper::map($find->all(), 'id', 'address_text');
    }

    /**
     * @return bool
     */
    public function myDelete()
    {
        $this->delete = 1;
        return $this->save(false, ['delete']);
    }

    /**
     * @return bool
     */
    public function unDelete()
    {
        $this->delete = 0;
        return $this->save(false, ['delete']);
    }

    /**
     * @return string
     */
    public function getStatusRent()
    {
        $freeCount = $this->getForRent()->orderBy('for_rent.check_out DESC')->notDeleted()->one();
 
        if ($freeCount !== null) {
            if (($freeCount->check_out > time())) {
                if ($freeCount->check_out > (time() + 3 * 24 * 60 * 60)) {
                    return self::STATUS_BUSY;
                }
                return self::STATUS_FREE3DAY;
            }
        }

        return self::STATUS_FREE;
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        switch ($this->getStatusRent()) {
            case self::STATUS_FREE:
                return '<span class="label label label-success">' .
                    (Yii::t("common", "Free")) .
                    '</span>';
                break;
            case self::STATUS_BUSY:
                return '<span class="label label-danger">' .
                    (Yii::t("common", "Busy")) .
                    '</span>';
                break;
            case self::STATUS_FREE3DAY:
                return '<span class="label label-warning">' .
                    (Yii::t("common", "Released after 3 days")) .
                    '</span>';
                break;
            default:
                return '<span class="label label-default">' .
                    (Yii::t("common", "Unknown")) .
                    '</span>';
        }
    }

    /**
     * @param $attributes
     * @param int $agencyId
     * @return array
     */
    public static function import($attributes, $agencyId = 0)
    {
        $model = new self();
        $model->load($attributes, '');
        $model->agency_id = $agencyId;
        $model->save();
        return $model->errors;
    }
}
