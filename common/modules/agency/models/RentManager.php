<?php

namespace common\modules\agency\models;

use modules\log\behaviors\LogBehavior;
use modules\users\models\backend\Users;
use Yii;

/**
 * This is the model class for table "rent_manager".
 *
 * @property integer $id
 * @property integer $rent_id
 * @property integer $manager_id
 *
 * @property Users $manager
 * @property Rent $rent
 */
class RentManager extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rent_manager';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rent_id', 'manager_id'], 'required'],
            [['rent_id', 'manager_id'], 'integer'],
            [
                ['manager_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Users::className(),
                'targetAttribute' => ['manager_id' => 'id']
            ],
            [
                ['rent_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Rent::className(),
                'targetAttribute' => ['rent_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'rent_id' => Yii::t('common', 'Rent ID'),
            'manager_id' => Yii::t('common', 'Manager ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Users::className(), ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRent()
    {
        return $this->hasOne(Rent::className(), ['id' => 'rent_id']);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\query\RentManagerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\agency\models\query\RentManagerQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'log' => [
                'class' => LogBehavior::className(),
                'types' => [
                    'update' => function ($model) {
                        /* @var $model self */
                        return "Изменил у объекта \"{$model->rent->address_text} ($model->rent_id)\" 
                        менеджера \"{$model->manager->name}\" ";
                    },
                    'insert' => function ($model) {
                        /* @var $model self */
                        return "Сделал \"{$model->manager->name}\" менеджером объекта 
                        \"{$model->rent->address_text} ($model->rent_id)\" ";
                    },
                    'delete' => function ($model) {
                        /* @var $model self */
                        return "Удалил у объекта \"{$model->rent->address_text} 
                        ($model->rent_id)\" менеджера \"{$model->manager->name}\" ";
                    }
                ]
            ]
        ];
    }
}
