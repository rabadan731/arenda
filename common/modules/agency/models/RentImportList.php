<?php

namespace common\modules\agency\models;

use common\modules\clients\models\Clients;
use Yii;
use common\modules\agency\models\query\RentImportListQuery;

/**
 * This is the model class for table "rent_import_list".
 *
 * @property integer $id
 * @property integer $rent_id
 * @property integer $client_id
 * @property integer $agency_id
 * @property integer $service_id
 * @property string $uid
 * @property integer $date_import
 * @property integer $date_from
 * @property integer $date_to
 *
 * @property Rent $rent
 * @property Clients $client
 * @property RentImportServices $service
 */
class RentImportList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rent_import_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['rent_id', 'agency_id', 'service_id', 'date_import', 'date_from', 'date_to'],
                'required'
            ],
            [
                [
                    'rent_id',
                    'agency_id',
                    'client_id',
                    'service_id',
                    'date_import',
                    'date_from',
                    'date_to'
                ],
                'integer'
            ],
            [['uid'], 'string'],
            [
                ['rent_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Rent::className(),
                'targetAttribute' => ['rent_id' => 'id']
            ],
            [
                ['service_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => RentImportServices::className(),
                'targetAttribute' => ['service_id' => 'service_id']
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'uid' => Yii::t('common', 'UID'),
            'rent_id' => Yii::t('common', 'Rent ID'),
            'agency_id' => Yii::t('common', 'Agency ID'),
            'service_id' => Yii::t('common', 'Service ID'),
            'date_import' => Yii::t('common', 'Date Import'),
            'date_from' => Yii::t('common', 'Date From'),
            'date_to' => Yii::t('common', 'Date To'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRent()
    {
        return $this->hasOne(Rent::className(), ['id' => 'rent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(RentImportServices::className(), ['service_id' => 'service_id']);
    }

    /**
     * @inheritdoc
     * @return RentImportListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RentImportListQuery(get_called_class());
    }
}
