<?php

namespace common\modules\agency\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\agency\models\Workers;
use yii\data\ArrayDataProvider;

/**
 * WorkersSearch represents the model behind the search form about `common\modules\agency\models\Workers`.
 */
class WorkersSearch extends Workers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'worker_type', 'balance', 'agency_id', 'status'], 'integer'],
            [['fio', 'photo', 'email', 'phone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Workers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'worker_type' => $this->worker_type,
            'balance' => $this->balance,
            'agency_id' => $this->agency_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }

    /**
     * @return ActiveDataProvider
     */
    public function searchDelete()
    {
        $query = Workers::find()->deleted();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->allDelete(),
            'pagination' => ['pageSize' => 10],
        ]);

        return $dataProvider;
    }
}
