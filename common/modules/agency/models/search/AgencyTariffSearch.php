<?php

namespace common\modules\agency\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\agency\models\AgencyTariff;
use yii\data\ArrayDataProvider;

/**
 * AgencyTariffSearch represents the model behind the search form about `common\modules\agency\models\AgencyTariff`.
 */
class AgencyTariffSearch extends AgencyTariff
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'month', 'status', 'delete'], 'integer'],
            [['title'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgencyTariff::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'month' => $this->month,
            'price' => $this->price,
            'status' => $this->status,
            'delete' => $this->delete,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }

    /**
     * @return ActiveDataProvider
     */
    public function searchDelete()
    {
        $query = AgencyTariff::find()->deleted();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->allDelete(),
            'pagination' => ['pageSize' => 10],
        ]);

        return $dataProvider;
    }
}
