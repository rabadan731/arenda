<?php

namespace common\modules\agency\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\agency\models\WorkerJobs;
use yii\data\ArrayDataProvider;

/**
 * WorkerJobsSearch represents the model behind the search form about `common\modules\agency\models\WorkerJobs`.
 */
class WorkerJobsSearch extends WorkerJobs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'worker_id', 'date_event', 'date_sent', 'status'], 'integer'],
            [['address_text', 'address_map', 'need_to'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WorkerJobs::find();
        $query->orderBy('date_event DESC');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'worker_id' => $this->worker_id,
            'price' => $this->price,
            'date_event' => $this->date_event,
            'date_sent' => $this->date_sent,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'address_text', $this->address_text])
            ->andFilterWhere(['like', 'address_map', $this->address_map])
            ->andFilterWhere(['like', 'need_to', $this->need_to]);

        return $dataProvider;
    }

    /**
     * @return ActiveDataProvider
     */
    public function searchDelete()
    {
        $query = WorkerJobs::find()->deleted();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->allDelete(),
            'pagination' => ['pageSize' => 10],
        ]);

        return $dataProvider;
    }
}
