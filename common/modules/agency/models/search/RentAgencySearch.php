<?php

namespace common\modules\agency\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\agency\models\RentAgency;

/**
 * RentAgencySearch represents the model behind the search form about `common\modules\agency\models\RentAgency`.
 */
class RentAgencySearch extends RentAgency
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'rent_id', 'agency_id', 'access_type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RentAgency::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'rent_id' => $this->rent_id,
            'agency_id' => $this->agency_id,
            'access_type' => $this->access_type,
        ]);

        return $dataProvider;
    }
}
