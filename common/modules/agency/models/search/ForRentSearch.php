<?php

namespace common\modules\agency\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\agency\models\ForRent;
use yii\data\ArrayDataProvider;

/**
 * ForRentSearch represents the model behind the search form about `common\modules\agency\models\ForRent`.
 */
class ForRentSearch extends ForRent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'client_id',
                    'rent_id',
                    'check_in',
                    'check_out',
                    'price_all',
                    'price_day',
                    'price_deposit',
                    'worker_agent',
                    'worker_driver',
                    'worker_cleaner',
                    'agency_id',
                    'import',
                    'status'
                ],
                'integer'
            ],
            [['comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ForRent::find();
        $query->orderBy('check_out ASC');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'for_rent.id' => $this->id,
            'for_rent.rent_id' => $this->rent_id,
            'client_id' => $this->client_id,
            'check_in' => $this->check_in,
            'check_out' => $this->check_out,
            'price_all' => $this->price_all,
            'price_day' => $this->price_day,
            'price_deposit' => $this->price_deposit,
            'worker_agent' => $this->worker_agent,
            'worker_driver' => $this->worker_driver,
            'worker_cleaner' => $this->worker_cleaner,
            'for_rent.agency_id' => $this->agency_id,
            'import' => $this->import,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }


    /**
     * @return ActiveDataProvider
     */
    public function searchDelete()
    {
        $query = ForRent::find()->deleted();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->allDelete(),
            'pagination' => ['pageSize' => 10],
        ]);

        return $dataProvider;
    }

    /**
     * @return ActiveDataProvider
     */
    public function searchReservationsDelete($id)
    {
        $query = ForRent::find()->reservationsDelete();
        $query->andWhere([
            'for_rent.rent_id' => $id,
        ]);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->allDelete(),
            'pagination' => ['pageSize' => 10],
        ]);

        return $dataProvider;
    }
}
