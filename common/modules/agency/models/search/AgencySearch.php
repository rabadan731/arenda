<?php

namespace common\modules\agency\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\agency\models\Agency;
use yii\data\ArrayDataProvider;

/**
 * AgencySearch represents the model behind the search form about `common\modules\agency\models\Agency`.
 */
class AgencySearch extends Agency
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'balance', 'status', 'date_begin', 'date_end'], 'integer'],
            [['title', 'logo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @return ActiveDataProvider
     */
    public function searchDelete()
    {
        $query = Agency::find()->deleted();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->allDelete(),
            'pagination' => ['pageSize' => 10],
        ]);

        return $dataProvider;
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Agency::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'balance' => $this->balance,
            'status' => $this->status,
            'date_begin' => $this->date_begin,
            'date_end' => $this->date_end,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'logo', $this->logo]);

        return $dataProvider;
    }
}
