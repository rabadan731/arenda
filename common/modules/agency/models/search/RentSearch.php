<?php

namespace common\modules\agency\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\agency\models\Rent;
use yii\data\ArrayDataProvider;

/**
 * RentSearch represents the model behind the search form about `common\modules\agency\models\Rent`.
 */
class RentSearch extends Rent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'contract_date_begin',
                    'contract_date_end',
                    'contract_month_price',
                    'price_week', 'price_day', 'agency_id',
                    'import',
                    'status',
                    'status_buy',
                    'status_rent',
                ],
                'integer'
            ],
            [
                [
                    'description',
                    'address_text',
                    'address_map',
                    'fio',
                    'photo',
                    'email',
                    'phone'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchMy($params)
    {
        $query = Rent::find();
        $query->andWhere(['=', 'rent.agency_id', Yii::$app->user->identity->agency_id]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {

            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contract_date_begin' => $this->contract_date_begin,
            'contract_date_end' => $this->contract_date_end,
            'contract_month_price' => $this->contract_month_price,
            'price_week' => $this->price_week,
            'price_day' => $this->price_day,
            'import' => $this->import,
            'status' => $this->status,
            'status_buy' => $this->status_buy,
            'status_rent' => $this->status_rent,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'address_text', $this->address_text])
            ->andFilterWhere(['like', 'address_map', $this->address_map])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }

    public function searchOthers($params)
    {
        $query = Rent::find();
        if (!empty(Yii::$app->user->identity->agency_id)) {
            $query->andWhere(['<>', 'rent.agency_id', Yii::$app->user->identity->agency_id]);
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contract_date_begin' => $this->contract_date_begin,
            'contract_date_end' => $this->contract_date_end,
            'contract_month_price' => $this->contract_month_price,
            'price_week' => $this->price_week,
            'price_day' => $this->price_day,
            'import' => $this->import,
            'status' => $this->status,
            'status_buy' => $this->status_buy,
            'status_rent' => $this->status_rent,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'address_text', $this->address_text])
            ->andFilterWhere(['like', 'address_map', $this->address_map])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }

    public function search($params)
    {
        $query = Rent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contract_date_begin' => $this->contract_date_begin,
            'contract_date_end' => $this->contract_date_end,
            'contract_month_price' => $this->contract_month_price,
            'price_week' => $this->price_week,
            'price_day' => $this->price_day,
            'agency_id' => $this->agency_id,
            'import' => $this->import,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'address_text', $this->address_text])
            ->andFilterWhere(['like', 'address_map', $this->address_map])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }

    public function soon()
    {
        $query = Rent::find();
        
        $query->andFilterWhere([
            'contract_date_begin' => $this->contract_date_begin,
            'contract_date_end' => $this->contract_date_end,
            'contract_month_price' => $this->contract_month_price,
            'price_week' => $this->price_week,
            'price_day' => $this->price_day,
            'rent.agency_id' => $this->agency_id,
            'import' => $this->import,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        $query->nextDays(3);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        
        return $dataProvider;
    }

    public function free()
    {
        $query = Rent::find();
        
        $query->andFilterWhere([
            'contract_date_begin' => $this->contract_date_begin,
            'contract_date_end' => $this->contract_date_end,
            'contract_month_price' => $this->contract_month_price,
            'price_week' => $this->price_week,
            'price_day' => $this->price_day,
            'rent.agency_id' => $this->agency_id,
            'import' => $this->import,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        $query->free();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        
        return $dataProvider;
    }


    /**
     * @return ArrayDataProvider
     */
    public function searchDelete()
    {
        $query = Rent::find()->deleted();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->allDelete(),
            'pagination' => ['pageSize' => 10],
        ]);

        return $dataProvider;
    }
}
