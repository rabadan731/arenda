<?php

namespace common\modules\agency\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\agency\models\RentAgency]].
 *
 * @see \common\modules\agency\models\RentAgency
 */
class RentAgencyQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\RentAgency[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\RentAgency|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function cm($rent_id, $agency_id)
    {
        $this->andWhere(['rent_id'=>$rent_id, 'agency_id'=>$agency_id]);
        return $this;
    }
}
