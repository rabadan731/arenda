<?php

namespace common\modules\agency\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\agency\models\WorkerJobs]].
 *
 * @see \common\modules\agency\models\WorkerJobs
 */
class WorkerJobsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\WorkerJobs[]|array
     */
    public function all($db = null)
    {
        $this->andWhere(['worker_jobs.delete'=>0]);
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\WorkerJobs|array|null
     */
    public function one($db = null)
    {
        if (!\Yii::$app->user->can("admin")) {
            $this->andWhere(['worker_jobs.delete'=>0]);
        }
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\Agency[]|array
     */
    public function allDelete($db = null)
    {
        return parent::all($db);
    }

    public function deleted()
    {
        $this->andWhere(['worker_jobs.delete'=>1]);
        return $this;
    }
}
