<?php

namespace common\modules\agency\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\agency\models\AgencyTariff]].
 *
 * @see \common\modules\agency\models\AgencyTariff
 */
class AgencyTariffQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\AgencyTariff[]|array
     */
    public function all($db = null)
    {
        $this->andWhere(['agency_tariff.delete'=>0]);
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\AgencyTariff|array|null
     */
    public function one($db = null)
    {
        if (\Yii::$app->id == 'app-console' || !\Yii::$app->user->can("admin")) {
            $this->andWhere(['agency_tariff.delete'=>0]);
        }
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\Agency[]|array
     */
    public function allDelete($db = null)
    {
        return parent::all($db);
    }

    public function deleted()
    {
        $this->andWhere(['agency_tariff.delete'=>1]);
        return $this;
    }
}
