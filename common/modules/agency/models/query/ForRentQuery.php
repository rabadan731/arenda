<?php

namespace common\modules\agency\models\query;

use common\modules\agency\models\ForRent;
use common\modules\main\models\KeyStorageItem;
use Yii;

/**
 * This is the ActiveQuery class for [[\common\modules\agency\models\ForRent]].
 *
 * @see \common\modules\agency\models\ForRent
 */
class ForRentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\ForRent[]|array
     */
    public function all($db = null)
    {
        $this->notDeleted();
        if (Yii::$app->user->identity->role == 'manager') {
            $this->manager()->thisAgency();
        }
        if (Yii::$app->user->identity->role == 'agency') {
            $this->thisAgency();
        }
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\ForRent|array|null
     */
    public function one($db = null)
    {
        if (!\Yii::$app->user->can("admin")) {
            //$this->andWhere(['for_rent.delete' => 0]);
            if (Yii::$app->user->identity->role == 'manager') {
                $this->manager()->thisAgency();
            }
            if (Yii::$app->user->identity->role == 'agency') {
                $this->thisAgency();
            }
        }

        return parent::one($db);
    }

    public function thisAgency()
    {
        $this->andWhere(['for_rent.agency_id' => Yii::$app->user->identity->agency_id]);
        return $this;
    }


    public function manager($id = null)
    {
        $id = is_null($id) ? (Yii::$app->user->id) : $id;
        $this->leftJoin("rent_manager", 'rent_manager.rent_id = for_rent.rent_id')
            ->andWhere(['rent_manager.manager_id' => $id]);
        return $this;
    }


    /**
     * @inheritdoc
     * @return \common\modules\agency\models\Agency[]|array
     */
    public function allDelete($db = null)
    {
        return parent::all($db);
    }

    /**
     * @return $this
     */
    public function deleted()
    {
        $this->andWhere(['for_rent.delete' => 1]);
        return $this;
    }

    /**
     * @return $this
     */
    public function reservationsDelete()
    {
        $this->andWhere(['for_rent.delete' => 0]);
        $this->andWhere(['for_rent.reservations' => ForRent::RESERVATIONS_DELETE]);
        return $this;
    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        $this->andWhere(['for_rent.delete' => 0]);
        $this->andWhere(['!=','for_rent.reservations', ForRent::RESERVATIONS_DELETE]);
        return $this;
    }

    public function period($start, $end)
    {
        $this->andWhere(['>=', 'for_rent.check_in', $start]);
        $this->andWhere(['<=', 'for_rent.check_in', $end]);
        return $this;
    }

    /**
     * @param $dateIn
     * @param $dateOut
     * @param bool $shiftDate
     * @return $this
     */
    public function date($dateIn, $dateOut, $shiftDate = false)
    {
        if (!is_null($dateIn)) {
            $dateIn = strtotime($dateIn);
            $dateOut = strtotime($dateOut);
            $this->datetime($dateIn, $dateOut, $shiftDate);
        }
        return $this;
    }

    /**
     * @param $dateIn
     * @param $dateOut
     * @param bool $shiftDate
     * @return $this
     */
    public function datetime($dateIn, $dateOut, $shiftDate = false)
    {
        if ($shiftDate) {
            $shiftDate = $this->getShiftDate();
            $dateIn = (int)$dateIn + (int)$shiftDate;
            $dateOut = (int)$dateOut - (int)$shiftDate;
        }

        $this->andWhere(['or',
            ['and', "for_rent.check_in <= {$dateIn}", "for_rent.check_out >= {$dateIn}"],
            ['and', "for_rent.check_in >= {$dateIn}", "for_rent.check_in <= {$dateOut}"],
            ['and', "for_rent.check_out >= {$dateIn}", "for_rent.check_out <= {$dateOut}"],
        ]);

        return $this;
    }

    
    /**
     * @return int|mixed
     */
    private function getShiftDate()
    {
        $cache = Yii::$app->getCache();
        $key = 'TheNumberSecondsOverlappingDates';
        $data = $cache->get($key);

        if ($data === false) {
            // $data нет в кэше, вычисляем заново
            $data = KeyStorageItem::getValue($key, 0);

            // Сохраняем значение $data в кэше. Данные можно получить в следующий раз.
            $cache->set($key, $data, (30 * 60));
        }

        return empty($data) ? 0 : $data;
    }
}
