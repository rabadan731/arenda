<?php

namespace common\modules\agency\models\query;

use common\modules\agency\models\ForRent;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[\common\modules\agency\models\Rent]].
 *
 * @see \common\modules\agency\models\Rent
 */
class RentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\Rent[]|array
     */
    public function all($db = null)
    {
        $this->andWhere(['rent.delete' => 0]);
        if (Yii::$app->user->identity->role != 'admin') {
            if (Yii::$app->user->identity->role == 'manager') {
                $this->manager()->thisAgency();
            }
            if (Yii::$app->user->identity->role == 'agency') {
                $this->thisAgency();
            }
        }

        return parent::all($db);
    }


    /**
     * @inheritdoc
     * @return \common\modules\agency\models\Rent|array|null
     */
    public function one($db = null)
    {
        if (Yii::$app->user->identity->role != 'admin') {
            $this->andWhere(['rent.delete' => 0]);
            if (Yii::$app->user->identity->role == 'manager') {
                $this->manager()->thisAgency();
            }
            if (Yii::$app->user->identity->role == 'agency') {
                $this->thisAgency();
            }
        }

        return parent::one($db);
    }


    /**
     * @inheritdoc
     * @return \common\modules\agency\models\Rent|array|null
     */
    public function oneDefault($db = null)
    {
        return parent::one($db);
    }


    public function agency($id)
    {
        $this->andWhere(['rent.agency_id' => $id]);
        return $this;
    }

    public function thisAgency()
    {
        $this->leftJoin("rent_agency", 'rent_agency.rent_id = rent.id');
        $this->andWhere([
            'or',
            ['rent.agency_id' => Yii::$app->user->identity->agency_id],
            ['rent_agency.agency_id' => Yii::$app->user->identity->agency_id]
        ]);

        return $this;
    }

    public function manager($id = null)
    {
        $id = is_null($id) ? (Yii::$app->user->id) : $id;
        $this->leftJoin("rent_manager", 'rent_manager.rent_id = rent.id')
            ->andWhere(['rent_manager.manager_id' => $id]);
        return $this;
    }

    public function nextDays($days = 3)
    {
        $this->leftJoin("for_rent", 'for_rent.rent_id = rent.id')
            ->andWhere(['<', 'for_rent.check_out', time() + ($days * 24 * 60 * 60)])
            ->andWhere(['>', 'for_rent.check_out', time()])
            ->andWhere(['for_rent.delete' => 0])
            ->andWhere(['!=','for_rent.reservations', ForRent::RESERVATIONS_DELETE]);
        return $this;
    }
    public function free()
    {
        $time = time();
        $maxRent = clone $this;
        $maxRent->leftJoin("for_rent", 'for_rent.rent_id = rent.id');
        $maxRent->andWhere(['>=', 'for_rent.check_out', $time])
            ->andWhere(['for_rent.delete' => 0])
            ->andWhere(['!=','for_rent.reservations', ForRent::RESERVATIONS_DELETE]);
        $r = ArrayHelper::getColumn($maxRent->select('for_rent.rent_id as id')->asArray()->all(), 'id');

        $this->andWhere(['not in', 'rent.id', $r]);

        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\Rent[]|array
     */
    public function allDelete($db = null)
    {
        return parent::all($db);
    }

    /**
     * @return $this
     */
    public function deleted()
    {
        $this->andWhere(['rent.delete' => 1]);
        return $this;
    }
}
