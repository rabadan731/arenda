<?php

namespace common\modules\agency\models\query;

use common\modules\main\models\KeyStorageItem;
use Yii;

/**
 * This is the ActiveQuery class for [[\common\modules\agency\models\RentImportList]].
 *
 * @see \common\modules\agency\models\RentImportList
 */
class RentImportListQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\RentImportList[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\RentImportList|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $dateIn
     * @param $dateOut
     * @param bool $shiftDate
     * @return $this
     */
    public function date($dateIn, $dateOut, $shiftDate = false)
    {
        if (!is_null($dateIn)) {
            $dateIn = strtotime($dateIn);
            $dateOut = strtotime($dateOut);
            $this->datetime($dateIn, $dateOut, $shiftDate);
        }
        return $this;
    }

    /**
     * @param $dateIn
     * @param $dateOut
     * @param bool $shiftDate
     * @return $this
     */
    public function datetime($dateIn, $dateOut, $shiftDate = false)
    {
        if ($shiftDate) {
            $shiftDate = $this->getShiftDate();
            $dateIn = (int)$dateIn + (int)$shiftDate;
            $dateOut = (int)$dateOut - (int)$shiftDate;
        }

        $this->andWhere(['or',
            ['and', "date_from <= {$dateIn}", "date_to >= {$dateIn}"],
            ['and', "date_from >= {$dateIn}", "date_from <= {$dateOut}"],
            ['and', "date_to >= {$dateIn}", "date_to <= {$dateOut}"],
        ]);

        return $this;
    }

    /**
     * @return int|mixed
     */
    private function getShiftDate()
    {
        $cache = Yii::$app->getCache();
        $key = 'TheNumberSecondsOverlappingDates';
        $data = $cache->get($key);

        if ($data === false) {
            // $data нет в кэше, вычисляем заново
            $data = KeyStorageItem::getValue($key, 0);

            // Сохраняем значение $data в кэше. Данные можно получить в следующий раз.
            $cache->set($key, $data, (30 * 60));
        }

        return empty($data) ? 0 : $data;
    }
}
