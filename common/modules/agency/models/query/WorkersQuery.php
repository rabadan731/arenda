<?php

namespace common\modules\agency\models\query;

use Yii;

/**
 * This is the ActiveQuery class for [[\common\modules\agency\models\Workers]].
 *
 * @see \common\modules\agency\models\Workers
 */
class WorkersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\Workers[]|array
     */
    public function all($db = null)
    {
        $this->andWhere(['workers.delete'=>0]);
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\Workers|array|null
     */
    public function one($db = null)
    {
        if (!\Yii::$app->user->can("admin")) {
            $this->andWhere(['workers.delete'=>0]);
        }
        return parent::one($db);
    }

    public function thisAgency()
    {
        $this->andWhere(['workers.agency_id'=>Yii::$app->user->identity->agency_id]);
        return $this;
    }

    public function agency($id)
    {
        $this->andWhere(['workers.agency_id'=>$id]);
        return $this;
    }

    public function type($type_id)
    {
        $this->andWhere(['workers.worker_type'=>$type_id]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\Agency[]|array
     */
    public function allDelete($db = null)
    {
        return parent::all($db);
    }

    public function deleted()
    {
        $this->andWhere(['workers.delete'=>1]);
        return $this;
    }
}
