<?php

namespace common\modules\agency\models\query;

use Yii;
use common\modules\agency\models\Agency;

/**
 * This is the ActiveQuery class for [[\common\modules\agency\models\Agency]].
 *
 * @see \common\modules\agency\models\Agency
 */
class AgencyQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['agency.status'=>Agency::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\Agency[]|array
     */
    public function all($db = null)
    {
        $this->andWhere(['agency.delete'=>0]);
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\Agency|array|null
     */
    public function one($db = null)
    {
        if (!\Yii::$app->user->can("admin")) {
            $this->andWhere(['agency.delete'=>0]);
        }
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\agency\models\Agency[]|array
     */
    public function allDelete($db = null)
    {
        return parent::all($db);
    }
    
    public function deleted()
    {
        $this->andWhere(['agency.delete'=>1]);
        return $this;
    }
}
