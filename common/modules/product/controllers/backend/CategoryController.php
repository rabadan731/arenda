<?php

namespace modules\product\controllers\backend;

use yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use modules\product\models\Category;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            Url::remember();
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['category_crud']
                    ]
                ],
            ]
        ];
    }
    
    /**
     * Lists all Category models.
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($id = 0)
    {
        $parent = $id ? $this->findModel($id) : null;
        $model = $parent ? $parent->children(1) : Category::find()->roots();

        $dataProvider = new ActiveDataProvider([
            'query' => $model->orderBy('id DESC'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'parent' => $parent
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionCreate($id = 0)
    {
        $model = new Category();
        $model->parent_id = $id;
        $parent = $id ?$this->findModel($id) : null;

        if ($model->load(Yii::$app->request->post()) && $model->createCategory()) {
            return $this->redirect(['index', 'id' => $model->parent_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'parent' => $parent
            ]);
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->updateCategory()) {
            Yii::$app->session->setFlash('success', Yii::t("common","Data saved successfully"));
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->deleteWithChildren();

        return $this->redirect(['index', 'id' => $model->parent_id]);
    }

    /**
     * Удаление нескольких записей
     * @return mixed
     */
    public function actionDeleteMultiple()
    {
        $selection = Yii::$app->request->post('selection', []);

        foreach ($selection as $id){
            $model = $this->findModel($id);
            $model->deleteWithChildren();
        }

        return $this->redirect(['index', 'id' => $model->parent_id]);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'ERROR_40'));
        }
    }
}
