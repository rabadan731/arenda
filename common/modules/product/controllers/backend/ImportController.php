<?php

namespace modules\product\controllers\backend;

use yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use modules\product\models\Import;
use modules\product\models\search\Import as ImportSearch;

/**
 * ImportController implements the CRUD actions for Import model.
 */
class ImportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            Url::remember();
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['import_view']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'test'],
                        'roles' => ['import_create']
                    ]
                ],
            ]
        ];
    }

    /**
     * Lists all Import models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ImportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Import model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Import();

        //if ($model->load(Yii::$app->request->post()) && $model->save()) {
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return 'Ещё не готово!';
            //return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionTest()
    {
        $xls = \PHPExcel_IOFactory::load(dirname(dirname(dirname(dirname(dirname(__DIR__))))) . '/hanmet.xls');

        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();

        echo "<table>";

        // Получили строки и обойдем их в цикле
        $rowIterator = $sheet->getRowIterator(20, 43);
        foreach ($rowIterator as $row) {
            // Получили ячейки текущей строки и обойдем их в цикле
            $cellIterator = $row->getCellIterator();

            echo "<tr>";

            foreach ($cellIterator as $cell) {
                echo "<td>" . $cell->getValue() . "</td>";
            }

            echo "</tr>";
        }
        echo "</table>";
    }
}
