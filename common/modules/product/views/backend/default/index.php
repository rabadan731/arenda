<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use modules\product\models\Import;
use modules\product\models\Category;

/* @var $this yii\web\View */
/* @var $searchModel modules\product\models\search\Product */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Каталог товаров';
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'cubes';
$this->params['place'] = 'product-default';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="jarviswidget">
    <div>
        <?php if (Yii::$app->user->can('product_crud')): ?>
            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-primary']) ?>
            </p>
        <?php endif; ?>

        <?php Pjax::begin(); ?>

        <?php
        $columns = [];

        $columns[] = [
            'attribute' => 'import_id',
            'filter' => (new Import())->valuesAll(),
            'format' => 'datetime',
            'value' => 'import.created_at'
        ];
        $columns[] = [
            'attribute' => 'category_id',
            'value' => 'category.name',
            'filter' => (new Category())->valuesAll(),
            'filterInputOptions' => [
                'encode' => false,
                'class' => 'form-control',
                'id' => null
            ]
        ];

        $columns[] = 'grade';
        $columns[] = 'quantity';
        $columns[] = 'price';

        if (Yii::$app->user->can('product_crud')){
            $columns[] = [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<div class="text-center">{view} {update} {delete}</div>'
            ];
        }
        ?>

        <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $columns,
            ]);
        ?>
        <?php Pjax::end(); ?>
    </div>
</div>