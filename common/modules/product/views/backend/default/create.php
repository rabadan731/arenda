<?php

/* @var $this yii\web\View */
/* @var $model modules\product\models\Product */

$this->title = 'Каталог товаров';
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'cubes';
$this->params['place'] = 'product-default';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Добавить';
?>

<div class="jarviswidget">
    <div>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
