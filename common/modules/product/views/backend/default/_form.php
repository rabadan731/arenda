<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\product\models\Category;

/* @var $this yii\web\View */
/* @var $model modules\product\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category_id')->dropDownList((new Category())->valuesAll(), [
        'prompt' => 'Не выбрана',
        'encode' => false
    ]) ?>

    <?= $form->field($model, 'material')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'grade')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'size')->input('number') ?>

    <?= $form->field($model, 'unit')->input('number') ?>

    <?= $form->field($model, 'quantity')->input('number') ?>

    <?= $form->field($model, 'price')->input('number') ?>

    <?= $form->field($model, 'discount')->input('number') ?>

    <?= $form->field($model, 'sum')->input('number') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
