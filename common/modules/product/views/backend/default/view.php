<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model modules\product\models\Product */

$this->title = 'Каталог товаров';
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'cubes';
$this->params['place'] = 'product-default';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->grade;
?>

<div class="jarviswidget">
    <div>

        <?php if (Yii::$app->user->can('product_crud')): ?>
            <p>
                <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Вы уверены, что хотите удалить данный продукт?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
        <?php endif; ?>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'category_id',
                'material',
                'grade',
                'size',
                'unit',
                'quantity',
                'price',
                'discount',
                'sum',
            ],
        ]) ?>
    </div>
</div>