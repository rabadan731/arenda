<?php

use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use modules\product\models\Import;
use modules\product\models\Category;

/* @var $this yii\web\View */
/* @var $searchModel modules\product\models\search\Product */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары на модерации';
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'cubes';
$this->params['place'] = 'product-moderation';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="jarviswidget">
    <div>

        <?php Pjax::begin(); ?>

        <?php
        $columns = [];

        if (Yii::$app->user->can('product_crud')){
            $columns[] = ['class' => 'yii\grid\CheckboxColumn'];
        }
        $columns[] = [
            'attribute' => 'import_id',
            'filter' => (new Import())->valuesAll(),
            'format' => 'datetime',
            'value' => 'import.created_at'
        ];
        $columns[] = [
            'attribute' => 'category_id',
            'value' => 'category.name',
            'filter' => (new Category())->valuesAll(),
            'filterInputOptions' => [
                'encode' => false,
                'class' => 'form-control',
                'id' => null
            ]
        ];
        $columns[] = 'grade';
        $columns[] = 'size';
        
        if (Yii::$app->user->can('product_crud')){
            $columns[] = [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<div class="text-center">{check} {view} {update} {delete}</div>',
                'buttons' => ArrayHelper::merge(Yii::$container->get('yii\grid\ActionColumn')->buttons, [
                    'check' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-check"></i>', $url, [
                            'class' => 'btn btn-xs btn-success',
                            'title' => 'Переместить товар в каталог',
                            'data-pjax' => 0,
                        ]);
                    }
                ])
            ];
        }
        ?>

        <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $columns,
            ]);
        ?>
        <?php Pjax::end(); ?>
    </div>
</div>