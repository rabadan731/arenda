<?php

/* @var $this yii\web\View */
/* @var $model modules\product\models\Category */

$this->title = 'Категории';
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'code-fork';
$this->params['place'] = 'product-category';
$this->params['breadcrumbs'] = $model->breadcrumbs(true, [
    'url' => ['index'],
    'label' => $this->title
]);
$this->params['breadcrumbs'][] = 'Изменить';
?>

<div class="jarviswidget">
    <div>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
