<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $parent \modules\product\models\Category */

$this->title = 'Категории';
$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'product-category';
$this->params['pageIcon'] = 'code-fork';
if ($parent){
    $this->params['breadcrumbs'] = $parent->breadcrumbs(false, [
        'url' => ['index'],
        'label' => $this->title
    ]);
}else{
    $this->params['breadcrumbs'][] = $this->title;
}
?>

<div class="jarviswidget">
    <div>
        <?= Html::beginForm(['delete-multiple']) ?>

        <p>
            <?= Html::a('Добавить', ['create', 'id' => $parent ? $parent->id : 0], [
                'class' => 'btn btn-primary'
            ]) ?>
            <?= Html::submitButton('Удалить', [
                'class' => 'btn btn-danger'
            ]) ?>
        </p>

        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'options' => [
                'class' => 'table'
            ],
            'columns' => [
                ['class' => 'yii\grid\CheckboxColumn'],

                'name',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '<div class="text-center">{view} {update} {delete}</div>',
                    'buttons' => ArrayHelper::merge(Yii::$container->get('yii\grid\ActionColumn')->buttons, [
                        'view' => function ($url, $model, $key) {
                            $url = Url::to(['index', 'id' => $model->id]);
                            return Html::a('<i class="fa fa-eye"></i>', $url, [
                                'class' => 'btn btn-xs btn-success',
                                'title' => 'Вложенные категории',
                                'data-pjax' => 0,
                            ]);
                        }
                    ])
                ],
            ],
        ]); ?>

        <?php Pjax::end(); ?>

        <?= Html::endForm() ?>
    </div>
</div>