<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model modules\product\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="box">
    <div class="box-body">
        <div class="col-md-5">
            <div class="row">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'parent_id')->dropDownList($model->valuesAll(), [
                    'encode' => false,
                    'prompt' => 'Не выбрана'
                ]) ?>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="form-group">
                    <?= Html::submitButton(
                        $model->isNewRecord ? 'Добавить' : 'Изменить', [
                            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
                        ]
                    ) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>