<?php

/* @var $this yii\web\View */
/* @var $model modules\product\models\Category */
/* @var $parent modules\product\models\Category */

$this->title = 'Категории';
$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'product-category';
$this->params['pageIcon'] = 'code-fork';

if ($parent){
    $this->params['breadcrumbs'] = $parent->breadcrumbs(true, [
        'url' => ['index'],
        'label' => $this->title
    ]);
}else{
    $this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
}

$this->params['breadcrumbs'][] = 'Добавить'
?>

<div class="jarviswidget">
    <div>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
