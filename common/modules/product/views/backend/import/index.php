<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use modules\product\models\Import;
use modules\users\models\backend\Users;

/* @var $this yii\web\View */
/* @var $searchModel modules\product\models\search\Import */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Импорт товаров';
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'download';
$this->params['place'] = 'product-import';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="jarviswidget">
    <div>

        <?php if (Yii::$app->user->can('import_create')): ?>
            <p>
                <?= Html::a('Импортировать', ['create'], ['class' => 'btn btn-primary']) ?>
            </p>
        <?php endif; ?>

        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'user_id',
                    'format' => 'html',
                    'filter' => (new Users())->valuesAll(),
                    'value' => function ($model){
                        /* @var $model Import */
                        return Html::a($model->user->name, ['/users/default/view', 'id' => $model->user_id]);
                    }
                ],
                [
                    'attribute' => 'quantity',
                    'value' => function ($model){
                        /* @var Import */
                        return \MessageFormatter::formatMessage('ru_RU', '{count, plural, one{# товар} few{# товара} many{# товаров} other{# товара}}', [
                            'count' => $model->quantity
                        ]);
                    }
                ],
                [
                    'attribute' => 'quantity_moderation',
                    'value' => function ($model){
                        /* @var Import */
                        return \MessageFormatter::formatMessage('ru_RU', '{count, plural, one{# товар} few{# товара} many{# товаров} other{# товара}}', [
                            'count' => $model->quantity_moderation
                        ]);
                    }
                ],
                'created_at:datetime',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '<div class="text-center">{moderation} {check}</div>',
                    'buttons' => [
                        'moderation' => function ($url, $model, $key) {
                            $url = ['/product/moderation/index', 'Product[import_id]' => $model->id];
                            return Html::a('<i class="fa fa-clock-o"></i>', $url, [
                                'class' => 'btn btn-xs btn-primary',
                                'title' => 'Просмотреть товары на модерации',
                                'data-pjax' => 0,
                            ]);
                        },
                        'check' => function ($url, $model, $key) {
                            $url = ['/product/default/index', 'Product[import_id]' => $model->id];
                            return Html::a('<i class="fa fa-check"></i>', $url, [
                                'class' => 'btn btn-xs btn-success',
                                'title' => 'Просмотреть товары из каталога',
                                'data-pjax' => 0,
                            ]);
                        }
                    ]
                ]
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
