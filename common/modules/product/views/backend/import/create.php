<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model modules\product\models\Import */

$this->title = Yii::t("common","Imports of goods");
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'download';
$this->params['place'] = 'product-import';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t("common","Import");
?>

<div class="jarviswidget">
    <div class="import-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="alert alert-warning fade in">
           <i class="fa-fw fa fa-warning"></i>
            <strong><?= Yii::t("common", "Attention"); ?>!</strong><br>
            <?= Yii::t("common", "After importing all the goods are sent for moderation"); ?>.
        </div>

        <?= $form->field($model, 'file')->fileInput() ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t("common","Import"), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
            ]) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
