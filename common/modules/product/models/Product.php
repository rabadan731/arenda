<?php

namespace modules\product\models;

use yii;
use modules\log\behaviors\LogBehavior;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $import_id
 * @property string $material
 * @property string $grade
 * @property integer $size
 * @property integer $unit
 * @property integer $quantity
 * @property integer $price
 * @property integer $discount
 * @property integer $sum
 * @property integer $status
 *
 * @property Category $category
 * @property Import $import
 */
class Product extends yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'grade', 'quantity', 'price', 'sum'], 'required'],
            [['category_id', 'import_id', 'size', 'unit', 'quantity', 'price', 'discount', 'sum', 'status'], 'integer'],
            [['material', 'grade'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'import_id' => 'Импорт',
            'material' => 'Материал',
            'grade' => 'Артикул',
            'size' => 'Размер',
            'unit' => 'Единица измерения',
            'quantity' => 'Количество',
            'price' => 'Цена',
            'discount' => 'Скидка',
            'sum' => 'Сумма',
            'status' => 'Статус',
        ];
    }

    public function behaviors()
    {
        return [
            'log' => [
                'class' => LogBehavior::className(),
                'types' => [
                    'update' => function ($model){
                        /* @var $model self */
                        return "Изменил товар \"$model->name\"";
                    },
                    'insert' => function ($model){
                        /* @var $model self */
                        return "Добавил товар \"$model->name\"";
                    },
                    'delete' => function ($model){
                        /* @var $model self */
                        return "Удалил товар \"$model->name\"";
                    }
                ]
            ]
        ];
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getImport()
    {
        return $this->hasOne(Import::className(), ['id' => 'import_id']);
    }

    /**
     * Перемещаем товар
     */
    public function moveCatalog()
    {
        if ($this->updateAttributes(['status' => 1])){
            $this->refreshImport();
        }
    }

    /**
     * Обновляем счётчики импорта
     */
    protected function refreshImport()
    {
        $quantity = self::find()->where([
            'import_id' => $this->import_id,
            'status' => 1
        ])->count();
        $quantity_moderation = self::find()->where([
            'import_id' => $this->import_id,
            'status' => 0
        ])->count();

        $import = $this->import;
        if ($import !== null){
            $import->quantity = $quantity + $quantity_moderation;
            $import->quantity_moderation = $quantity_moderation;
            $import->save(0);
        }
    }
}
