<?php

namespace modules\product\models\search;

use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\product\models\Import as ImportModel;

/**
 * Import represents the model behind the search form about `modules\product\models\Import`.
 */
class Import extends ImportModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'quantity', 'quantity_moderation', 'created_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ImportModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'quantity' => $this->quantity,
            'quantity_moderation' => $this->quantity_moderation,
            'created_at' => $this->created_at
        ]);

        return $dataProvider;
    }
}
