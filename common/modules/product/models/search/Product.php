<?php

namespace modules\product\models\search;

use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\product\models\Product as ProductModel;

/**
 * Product represents the model behind the search form about `modules\product\models\Product`.
 */
class Product extends ProductModel
{
    /**
     * Сценарий товаров на модерации
     */
    const SCENARIO_MODERATION = 'moderation';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'import_id', 'size', 'unit', 'quantity', 'price', 'discount', 'sum', 'status'], 'integer'],
            [['material', 'grade'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return [
            self::SCENARIO_DEFAULT => ['id', 'category_id', 'import_id', 'size', 'unit', 'quantity', 'price', 'discount', 'sum', 'status', 'material', 'grade'],
            self::SCENARIO_MODERATION => ['id', 'category_id', 'import_id', 'size', 'unit', 'quantity', 'price', 'discount', 'sum', 'status', 'material', 'grade']
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductModel::find()
            ->where(['status' => $this->scenario == self::SCENARIO_MODERATION ? 0 : 1])
            ->with(['category', 'import']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'import_id' => $this->import_id,
            'size' => $this->size,
            'unit' => $this->unit,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'discount' => $this->discount,
            'sum' => $this->sum,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'material', $this->material])
            ->andFilterWhere(['like', 'grade', $this->grade]);

        return $dataProvider;
    }
}
