<?php

namespace modules\product\models;

use yii;
use modules\users\models\frontend\Users;

/**
 * This is the model class for table "{{%product_import}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $quantity
 * @property integer $quantity_moderation
 * @property integer $created_at
 *
 * @property Users $user
 */
class Import extends yii\db\ActiveRecord
{
    /**
     * Импортируемый xls файл
     * @var
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_import}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file'], 'required'],
            [['file'], 'file', 'extensions' => ['xls']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quantity' => 'Импортировано',
            'quantity_moderation' => 'На модерации',
            'user_id' => 'Пользователь',
            'created_at' => 'Время',
            'file' => 'XLS Файл',
        ];
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return array
     */
    public function valuesAll()
    {
        $values = [];
        $all = self::find()->orderBy('id DESC')->all();

        foreach ($all as $item){
            /* @var $item Import */
            $values[$item->id] = Yii::$app->formatter->asDatetime($item->created_at);
        }

        return $values;
    }
}
