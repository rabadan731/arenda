<?php

namespace modules\product\models;

use yii;
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\Query;
use yii\helpers\ArrayHelper;
use modules\log\behaviors\LogBehavior;
use creocoder\nestedsets\NestedSetsBehavior;

/**
 * This is the model class for table "{{%product_category}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property integer $tree
 *
 * @mixin NestedSetsBehavior
 */
class Category extends yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50],
            [['name'], 'unique'],
            [['parent_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'name'      => Yii::t("common","Title"),
            'parent_id' => Yii::t("common","Category"),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'nestedSets' => [
                'class'             => NestedSetsBehavior::className(),
                'treeAttribute'     => 'tree',
                'leftAttribute'     => 'lft',
                'rightAttribute'    => 'rgt',
                'depthAttribute'    => 'depth',
            ],
            'log' => [
                'class' => LogBehavior::className(),
                'types' => [
                    'update' => function ($model){
                        /* @var $model self */
                        return "Изменил категорию \"$model->name\"";
                    },
                    'insert' => function ($model){
                        /* @var $model self */
                        return "Добавил категорию \"$model->name\"";
                    },
                    'delete' => function ($model){
                        /* @var $model self */
                        return "Удалил категорию \"$model->name\"";
                    }
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new Query(get_called_class());
    }

    /**
     * Получаем список  категорий для выбора родиля
     * @return array
     */
    public function valuesAll()
    {
        $arr = [];

        foreach (self::find()->where('id != :id', [':id' => (int) $this->id])->orderBy('tree')->all() as $category){
            $arr[$category->id] = str_repeat('&nbsp;', $category->depth * 8) . Html::encode($category->name);
        }

        return $arr;
    }

    /**
     * Получаем id дочерних категорий
     * @param bool $this_id Учитывать id текущей категории
     * @return array
     */
    public function childrenIds($this_id = false)
    {
        $ids = ArrayHelper::map($this->children()->all(), 'id', 'id');

        if ($this_id){
            $ids[] = $this->id;
        }

        return $ids;
    }

    /**
     * Изменяем категорию
     * @return bool
     * @throws \Exception
     */
    public function updateCategory()
    {
        if ($this->oldAttributes['parent_id'] != $this->parent_id && empty($this->parent_id)){
            return $this->makeRoot();
        }elseif ($this->oldAttributes['parent_id'] != $this->parent_id && ($parent = $this->findOne($this->parent_id))){
            return $this->prependTo($parent);
        }

        return parent::save();
    }

    /**
     * Создаём категорию
     * @return mixed
     */
    public function createCategory()
    {
        return $this->parent_id && ($parent = self::findOne($this->parent_id)) ?
            $this->prependTo($parent) : $this->makeRoot();
    }

    /**
     * Генерируем крошки
     * @param bool $lastLink
     * @param array $firstItem
     * @param array $urlOptions
     * @return array
     */
    public function breadcrumbs($lastLink = false, $firstItem = [], $urlOptions = [])
    {
        if (count($firstItem)){
            $breadcrumbs[] = $firstItem;
        }
        if (isset($urlOptions['path']) === false){
            $urlOptions['path'] = 'index';
        }
        if (isset($urlOptions['attributeUrl']) === false){
            $urlOptions['attributeUrl'] = 'id';
        }
        if (isset($urlOptions['attributeModel']) === false){
            $urlOptions['attributeModel'] = 'id';
        }

        foreach ($this->parents()->all() as $item){
            $url = [$urlOptions['path'], $urlOptions['attributeUrl'] => $item->{$urlOptions['attributeModel']}];
            $breadcrumbs[] = [
                'url' => Url::to($url),
                'label' => $item->name
            ];
        }

        if ($lastLink){
            $url = [$urlOptions['path'], $urlOptions['attributeUrl'] => $this->{$urlOptions['attributeModel']}];
            $breadcrumbs[] = [
                'url' => Url::to($url),
                'label' => $this->name
            ];
        }else{
            $breadcrumbs[] = $this->name;
        }

        return $breadcrumbs;
    }
}