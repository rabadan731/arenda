<?php

use yii\db\Migration;

/**
 * Handles the creation for table `product`.
 */
class m160507_133634_create_table_product extends Migration
{
    /**
     * @var string
     */
    public $table = '{{%product}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'import_id' => $this->integer(),
            'material' => $this->string(250),
            'grade' => $this->string(250)->notNull(),
            'size' => $this->string(50),
            'unit' => $this->integer(),
            'quantity' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
            'discount' => $this->integer(),
            'sum' => $this->integer()->notNull(),
            'status' => $this->integer(1),
        ]);

        $this->createIndex('import', $this->table, ['import_id']);
        $this->createIndex('status', $this->table, ['status']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->table);
    }
}
