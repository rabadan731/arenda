<?php

use yii\db\Migration;

/**
 * Handles the creation for table `product_import`.
 */
class m160510_162118_create_table_product_import extends Migration
{
    /**
     * @var string
     */
    public $table = '{{%product_import}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'quantity' => $this->integer()->defaultValue(0),
            'quantity_moderation' => $this->integer()->defaultValue(0),
            'created_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->table);
    }
}
