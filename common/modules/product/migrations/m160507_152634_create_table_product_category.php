<?php

use yii\db\Migration;

/**
 * Handles the creation for table `product_category`.
 */
class m160507_152634_create_table_product_category extends Migration
{
    /**
     * @var string
     */
    public $table = '{{%product_category}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'name' => $this->string(250),
            'parent_id' => $this->integer(),
            'lft' => $this->integer(),
            'rgt' => $this->integer(),
            'depth' => $this->integer(),
            'tree' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->table);
    }
}
