<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model modules\log\models\Log */

$this->title = Yii::t("common", "Logs action");
$this->params['pageTitle'] = $this->title;
$this->params['pageIcon'] = 'retweet';
$this->params['place'] = 'log';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = '#' . $model->id;
?>

<div class="jarviswidget">
    <div>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'user_id',
                    'format' => 'html',
                    'value' => $model->user !== null ?
                        Html::a($model->user->name, [
                            '/users/default/view',
                            'id' => $model->user_id
                        ]) : Yii::t("common", "Unknown"),
                ],
                [
                    'attribute' => 'type',
                    'format' => 'html',
                    'value' => isset($model->valueTypes()[$model->type]) ?
                        $model->valueTypes()[$model->type] : Yii::t("common", "Unknown"),
                ],
                'description',
                'ip',
                'ua',
                'url',
                [
                    'attribute' => 'text',
                    'format' => 'html',
                    'value' => ("<pre>" . ($model->text) . "</pre>")
                ],
                'created_at:datetime',
            ],
        ]) ?>
    </div>
</div>
