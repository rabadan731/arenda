<?php

use yii\db\Migration;

class m161017_201053_exchangeLog extends Migration
{
    /**
     * @var string
     */
    public $table = '{{%log_exchange}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'id'                => $this->primaryKey(),
            'description'       => $this->string(250),
            'text'              => $this->text(),
            'rent_id'           => $this->integer(),
            'service_id'        => $this->integer(),
            'ip'                => $this->string(25),
            'ua'                => $this->string(200),
            'url'               => $this->string(250),
            'created_at'        => $this->integer(),
        ]);

        $this->createIndex(  'idx_log_exchange_rent_id', $this->table, 'rent_id');
        $this->addForeignKey('fk_log_exchange_rent_id', $this->table, 'rent_id', "{{%rent}}", 'id', 'cascade', 'cascade');

        $this->createIndex(  'idx_log_exchange_rent_import_id', $this->table, 'service_id');
        $this->addForeignKey('fk_log_exchange_rent_import_id', $this->table, 'service_id', "{{%rent_import_services}}", 'service_id', 'cascade', 'cascade');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_log_exchange_rent_import_id', $this->table);
        $this->dropForeignKey('fk_log_exchange_rent_id', $this->table);

        $this->dropTable($this->table);
    }
}
