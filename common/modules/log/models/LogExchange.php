<?php

namespace common\modules\log\models;

use common\modules\agency\models\Rent;
use common\modules\agency\models\RentImportServices;
use Yii;

/**
 * This is the model class for table "log_exchange".
 *
 * @property integer $id
 * @property string $description
 * @property string $text
 * @property integer $rent_id
 * @property integer $service_id
 * @property string $ip
 * @property string $ua
 * @property string $url
 * @property integer $created_at
 *
 * @property Rent $rent
 * @property RentImportServices $service
 */
class LogExchange extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_exchange';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['rent_id', 'service_id', 'created_at'], 'integer'],
            [['description', 'url'], 'string', 'max' => 250],
            [['ip'], 'string', 'max' => 25],
            [['ua'], 'string', 'max' => 200],
            [
                ['rent_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Rent::className(),
                'targetAttribute' => ['rent_id' => 'id']
            ],
            [
                ['service_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => RentImportServices::className(),
                'targetAttribute' => ['service_id' => 'service_id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'description' => Yii::t('common', 'Description'),
            'text' => Yii::t('common', 'Text'),
            'rent_id' => Yii::t('common', 'Rent ID'),
            'service_id' => Yii::t('common', 'Service ID'),
            'ip' => Yii::t('common', 'Ip'),
            'ua' => Yii::t('common', 'Ua'),
            'url' => Yii::t('common', 'Url'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRent()
    {
        return $this->hasOne(Rent::className(), ['id' => 'rent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(RentImportServices::className(), ['service_id' => 'service_id']);
    }

    /**
     * @inheritdoc
     * @return \common\modules\log\models\query\LogExchangeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\log\models\query\LogExchangeQuery(get_called_class());
    }
}
