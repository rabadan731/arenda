<?php

namespace modules\log\models;

use yii;
use modules\users\models\backend\Users;

/**
 * This is the model class for table "{{%log}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property string $description
 * @property string $ip
 * @property string $text
 * @property string $ua
 * @property string $url
 * @property integer $created_at
 *
 * @property Users $user
 */
class Log extends yii\db\ActiveRecord
{
    /**
     * Тип действия: добавление
     */
    const TYPE_INSERT = 1;
    /**
     * Тип действия: изменение
     */
    const TYPE_UPDATE = 2;
    const TYPE_UPDATE_TARIFF = 21;
    /**
     * Тип действия: удаление
     */
    const TYPE_DELETE = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%log}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => Yii::t("common", "User"),
            'type' => Yii::t("common", "Action"),
            'description' => Yii::t("common", "Description"),
            'ip' => 'IP',
            'ua' => 'User Agent',
            'url' => 'URL',
            'created_at' => Yii::t("common", "Time"),
        ];
    }

    /**
     * Получаем пользователя который совершил действие.
     * @return yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * Получаем все типы действий в виде массива.
     * @return array
     */
    public function valueTypes()
    {
        return [
            self::TYPE_INSERT => Yii::t("common", "Adding"),
            self::TYPE_UPDATE => Yii::t("common", "Changing"),
            self::TYPE_UPDATE_TARIFF => Yii::t("common", "Tariff"),
            self::TYPE_DELETE => Yii::t("common", "Removal")
        ];
    }
}
