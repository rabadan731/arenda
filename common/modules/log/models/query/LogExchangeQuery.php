<?php

namespace common\modules\log\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\log\models\LogExchange]].
 *
 * @see \common\modules\log\models\LogExchange
 */
class LogExchangeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\log\models\LogExchange[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\log\models\LogExchange|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
