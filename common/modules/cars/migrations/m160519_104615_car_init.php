<?php

use yii\db\Migration;

class m160519_104615_car_init extends Migration
{

    /**
     * @var string
     */
    public $table = '{{%car}}';
    public $table_reserve = '{{%car_reserve}}';
    public $table_mark = '{{%car_mark}}';
    public $table_model = '{{%car_model}}';
    public $table_color = '{{%car_color}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {

        $this->createTable($this->table_mark, [
            'car_mark_id'   => $this->primaryKey(),
            'title'         => $this->string(256)->notNull(),
            'logo'          => $this->string(512),
            'status'        => $this->integer(1)->defaultValue(1),
            'delete'        => $this->integer(1)->defaultValue(0), //статус удален/не удален
        ]);

        $this->createTable($this->table_model, [
            'car_model_id'  => $this->primaryKey(),
            'car_mark_id'   => $this->integer(),
            'title'         => $this->string(256)->notNull(),
            'logo'          => $this->string(512),
            'status'        => $this->integer(1)->defaultValue(1),
            'delete'        => $this->integer(1)->defaultValue(0), //статус удален/не удален
        ]);

        $this->createIndex('idx_model_car_mark_id', $this->table_model, 'car_mark_id');
        $this->addForeignKey(
            'fk_model_car_mark_id',
            $this->table_model,
            'car_mark_id',
            $this->table_mark,
            'car_mark_id'
        );

        $this->createTable($this->table_reserve, [
            'reserve_id'    => $this->primaryKey(),
            'car_id'        => $this->integer()->notNull(),
            'phone'         => $this->string(256),
            'client_id'     => $this->integer()->notNull(),
            'information'   => $this->text(),
            'date_begin'    => $this->integer(),
            'date_end'      => $this->integer(),
            'status'        => $this->integer(1)->defaultValue(0),
            'delete'        => $this->integer(1)->defaultValue(0), //статус удален/не удален
            'created_by'    => $this->integer(),
            'updated_by'    => $this->integer(),
        ]);

        $this->createTable($this->table, [
            'car_id'        => $this->primaryKey(),
            'car_mark_id'   => $this->integer(),
            'car_model_id'  => $this->integer(),
            'type'          => $this->integer(),
            'year'          => $this->integer(4),
            'number'        => $this->string(16),
            'track'         => $this->integer(),
            'car_color_id'  => $this->string(7),
            'kpp'           => $this->integer(),
            'gazoline'      => $this->integer(),
            'gps_control'   => $this->integer(1)->defaultValue(0), //статус удален/не удален
            'volume'        => $this->float(),
            'status'        => $this->integer(1),
            'delete'        => $this->integer(1)->defaultValue(0), //статус удален/не удален
            'created_by'    => $this->integer(),
            'updated_by'    => $this->integer(),
            'created_at'    => $this->integer(),
            'updated_at'    => $this->integer(),
        ]);

        $this->createIndex('idx_car_reserve_client_id', $this->table_reserve, 'client_id');
        $this->addForeignKey(
            'fk_car_reserve_client_id',
            $this->table_reserve,
            'client_id',
            "{{%clients}}",
            'id'
        );

        $this->createIndex('idx_car_mark_id', $this->table, 'car_mark_id');
        $this->addForeignKey(
            'fk_car_mark_id',
            $this->table,
            'car_mark_id',
            $this->table_mark,
            'car_mark_id'
        );

        $this->createIndex('idx_car_model_id', $this->table, 'car_model_id');
        $this->addForeignKey(
            'fk_car_model_id',
            $this->table,
            'car_model_id',
            $this->table_model,
            'car_model_id'
        );

        $this->createIndex('idx_reserve_car_id', $this->table_reserve, 'car_id');
        $this->addForeignKey(
            'fk_reserve_car_id',
            $this->table_reserve,
            'car_id',
            $this->table,
            'car_id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_car_reserve_client_id', $this->table_reserve);
        $this->dropForeignKey('fk_model_car_mark_id', $this->table_model);
        $this->dropForeignKey('fk_reserve_car_id', $this->table_reserve);
        $this->dropForeignKey('fk_car_model_id', $this->table);
        $this->dropForeignKey('fk_car_mark_id', $this->table);

        $this->dropTable($this->table);
        $this->dropTable($this->table_reserve);
        $this->dropTable($this->table_model);
        $this->dropTable($this->table_mark);
    }

}
