<?php

use yii\db\Migration;

class m160519_104616_car_access extends Migration
{

    public $tableCarAccess = '{{%car_access}}';
    public $tableReserveAccess = '{{%car_reserve_access}}';
    public $tableAgency = '{{%agency}}';
    public $tableReserve = '{{%car_reserve}}';
    public $tableUsers = '{{%users}}';
    public $tableCar = '{{%car}}';

    public function safeUp()
    {


        $this->createTable($this->tableReserveAccess, [
            'car_reserve_access_id' => $this->primaryKey(),
            'reserve_id'            => $this->integer()->notNull(),
            'agency_id'             => $this->integer()->notNull(),
            'user_id'               => $this->integer()
        ]);


        $this->createIndex('idx_access_reserve_car_reserve_id', $this->tableReserveAccess, 'reserve_id');
        $this->addForeignKey(
            'fk_reserve_access_car_reserve_id',
            $this->tableReserveAccess,
            'reserve_id',
            $this->tableReserve,
            'reserve_id'
        );

        $this->createIndex('idx_access_reserve_agency_id', $this->tableReserveAccess, 'agency_id');
        $this->addForeignKey(
            'fk_reserve_access_agency_id',
            $this->tableReserveAccess,
            'agency_id',
            $this->tableAgency,
            'id'
        );

        $this->createIndex('idx_access_reserve_user_id', $this->tableReserveAccess, 'user_id');
        $this->addForeignKey(
            'fk_reserve_access_user_id',
            $this->tableReserveAccess,
            'user_id',
            $this->tableUsers,
            'id'
        );


        $this->createTable($this->tableCarAccess, [
            'car_access_id'     => $this->primaryKey(),
            'car_id'            => $this->integer()->notNull(),
            'agency_id'         => $this->integer()->notNull(),
            'user_id'           => $this->integer()
        ]);


        $this->createIndex('idx_car_access_reserve_car_car_id', $this->tableCarAccess, 'car_id');
        $this->addForeignKey(
            'fk_access_car_car_id',
            $this->tableCarAccess,
            'car_id',
            $this->tableCar,
            'car_id'
        );

        $this->createIndex('idx_access_agency_id', $this->tableCarAccess, 'agency_id');
        $this->addForeignKey(
            'fk_car_access_agency_id',
            $this->tableCarAccess,
            'agency_id',
            $this->tableAgency,
            'id'
        );

        $this->createIndex('idx_access_user_id', $this->tableCarAccess, 'user_id');
        $this->addForeignKey(
            'fk_car_access_user_id',
            $this->tableCarAccess,
            'user_id',
            $this->tableUsers,
            'id'
        );


    }

    public function safeDown()
    {

        $this->dropForeignKey('fk_car_access_user_id', $this->tableCarAccess);
        $this->dropForeignKey('fk_car_access_agency_id', $this->tableCarAccess);
        $this->dropForeignKey('fk_access_car_car_id', $this->tableCarAccess);

        $this->dropTable($this->tableCarAccess);

        $this->dropForeignKey('fk_reserve_access_user_id', $this->tableReserveAccess);
        $this->dropForeignKey('fk_reserve_access_agency_id', $this->tableReserveAccess);
        $this->dropForeignKey('fk_reserve_access_car_reserve_id', $this->tableReserveAccess);

        $this->dropTable($this->tableReserveAccess);

    }
}
