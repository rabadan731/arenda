<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\CarModel */

?>
<div class="car-model-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
