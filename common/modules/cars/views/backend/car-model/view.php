<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\CarModel */

$this->title = $model->title;
$this->params['pageIcon'] = 'car';
$this->params['pageTitle'] = $this->title;

?>
<div class="car-model-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'car_model_id',
            'title',
            'logo',
            'status',
            'delete',
        ],
    ]) ?>

</div>
