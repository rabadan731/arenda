<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\cars\models\CarMark;

/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\CarModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'car_mark_id')->dropDownList(CarMark::getList(), [
        'prompt' => Yii::t("common", "-- Select --")
    ]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton(Yii::t('cars', 'Save'), ['class' => 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
