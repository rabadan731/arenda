<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\CarReserve */

?>
<div class="car-reserve-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
