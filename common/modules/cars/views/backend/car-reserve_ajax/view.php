<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\CarReserve */
?>
<div class="car-reserve-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'reserve_id',
            'car_id',
            'phone',
            'name',
            'information:ntext',
            'date_begin',
            'date_end',
            'status',
            'delete',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
