<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\search\CarSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'car_id') ?>

    <?= $form->field($model, 'car_mark_id') ?>

    <?= $form->field($model, 'car_model_id') ?>

    <?= $form->field($model, 'year') ?>

    <?= $form->field($model, 'number') ?>

    <?php // echo $form->field($model, 'track') ?>

    <?php // echo $form->field($model, 'car_color_id') ?>

    <?php // echo $form->field($model, 'kpp') ?>

    <?php // echo $form->field($model, 'gazoline') ?>

    <?php // echo $form->field($model, 'gps_control') ?>

    <?php // echo $form->field($model, 'volume') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'delete') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('cars', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('cars', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
