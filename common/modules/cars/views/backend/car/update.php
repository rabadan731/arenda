<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\Car */

$this->title = Yii::t('cars', 'Update {modelClass}: ', [
    'modelClass' => 'Car',
]) . $model->car_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Cars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->car_id, 'url' => ['view', 'id' => $model->car_id]];
$this->params['breadcrumbs'][] = Yii::t('cars', 'Update');
$this->params['pageTitle'] = $this->title;
?>
<div class="jarviswidget">
    <div class="car-update">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
