<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\Car */

$this->title = $model->getCarTitle();
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Cars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="jarviswidget">
<div class="car-view">

    <?php if ($model->delete) { ?>
        <?php echo $this->render('/_delete', ['model' => $model]); ?>
    <?php } ?>

    <p>
        <?= Html::a(
            Yii::t('cars', 'Rent a car'),
            ['car-reserve/create', 'id' => $model->car_id],
            ['class' => 'btn btn-success']
        ) ?>
        <?= Html::a(
            Yii::t('cars', 'Update'),
            ['update', 'id' => $model->car_id],
            ['class' => 'btn btn-primary']
        ) ?>
        <?= Html::a(Yii::t('cars', 'Delete'), ['delete', 'id' => $model->car_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('cars', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'car_id',
            'carMark.title',
            'carModel.title',
            'year',
            'number',
            'track',
            'car_color_id',
            'kpp',
            'gazoline',
            'gps_control',
            'volume',
        ],
    ]) ?>

</div>
</div>
