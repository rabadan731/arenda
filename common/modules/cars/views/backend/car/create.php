<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\Car */

$this->title = Yii::t('common', 'Add')." ".Yii::t('cars', 'Car');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Cars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>

<div class="jarviswidget">
    <div class="car-create">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
