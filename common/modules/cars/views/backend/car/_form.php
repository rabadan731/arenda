<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\color\ColorInput;
use yii\widgets\ActiveForm;
use common\modules\cars\models\CarMark;
use kartik\depdrop\DepDrop;
use common\modules\agency\models\Agency;

/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\Car */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'car_mark_id')->dropDownList(CarMark::getList(), [
                'id' => 'car-mark-id',
                'prompt' => Yii::t("common", "-- Select --")
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'car_model_id')->widget(DepDrop::classname(), [
                'options' => ['id'=>'car-model-id'],
                'pluginOptions'=>[
                    'depends'=>['car-mark-id'],
                    'placeholder' => Yii::t("common", "-- Select --"),
                    'url' => Url::to(['mark-list'])
                ]
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'kpp')->dropDownList($model->getKppList(), [
                'prompt' => Yii::t("common", "-- Select --")
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'gazoline')->dropDownList($model->getGazolineList(), [
                'prompt' => Yii::t("common", "-- Select --")
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'volume')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'car_color_id')->widget(ColorInput::classname(), [
                'options' => ['placeholder' => 'Select color ...'],
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'year')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'track')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'gps_control')->textInput() ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'type')->dropDownList($model::getTypes(), [
                'prompt' => Yii::t("common", "-- Select --")
            ]) ?>
        </div>
        <div class="col-md-6">
            <?php if (Yii::$app->user->can('admin')) { ?>
                <?= $form->field($model, 'agency_id')->dropDownList(Agency::getAgencies(), [
                    'prompt' => Yii::t("common", "-- Select --")
                ]) ?>
            <?php } else { ?>
                <?= $form->field($model, 'agency_id')->hiddenInput([
                    'value'=>Yii::$app->user->identity->agency_id
                ])->label(false) ?>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('cars', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
