<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\cars\models\search\CarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cars', 'Cars');
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jarviswidget">
    <div class="car-index">
        <p>
            <?= Html::a(Yii::t('common', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'car_mark_id',
                        'value' => function ($data) {
                            return $data->carMark->title;
                        },
                        'filter' => \common\modules\cars\models\CarMark::getList()
                    ],
                    [
                        'attribute'=>'car_model_id',
                        'value' => function ($data) {
                            return $data->carModel->title;
                        },
                        'filter' => \common\modules\cars\models\CarModel::getList()
                    ],
                    'year',
                    'number',
                    // 'track',
                    // 'car_color_id',
                    // 'kpp',
                    // 'gazoline',
                    // 'gps_control',
                    // 'volume',
                    // 'status',
                    // 'delete',
                    // 'created_by',
                    // 'updated_by',
                    // 'created_at',
                    // 'updated_at',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'options' => ['style'=>'width:175px;'],
                        'template' => '<div class="text-center"> {view2} {update} {delete}</div>'
                    ],
                ],
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
