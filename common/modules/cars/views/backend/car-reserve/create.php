<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\CarReserve */

$this->title = Yii::t('cars', 'Car Reserve');
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Car Reserves'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jarviswidget">
    <div class="car-reserve-create">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
