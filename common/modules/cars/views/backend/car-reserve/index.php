<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\cars\models\search\CarReserveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cars', 'Provisions machines');
$this->params['pageIcon'] = 'car';
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="content" style="opacity: 1;">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="car-reserve-index">
                <p>
                    <?= Html::a(
                        "<span class=\"btn-label\"><i class=\"glyphicon glyphicon-plus\"></i></span>".Yii::t('common', 'Create'),
                        ['create'],
                        ['class' => 'btn btn-labeled btn-success']
                    ) ?>
                </p>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'options' => [
                        'class' => 'display projects-table table table-striped table-bordered table-hover dataTable no-footer'
                    ],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'car.carTitle',
                        'phone',
                        'date_begin:datetime',
                        'date_end:datetime',
                        // 'status',
                        // 'delete',
                        // 'created_by',
                        // 'updated_by',

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>