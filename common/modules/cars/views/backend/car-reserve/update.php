<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\CarReserve */

$this->title = Yii::t('cars', 'Update {modelClass}: ', [
    'modelClass' => 'Car Reserve',
]) . $model->car->getCarTitle();
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('cars', 'Car Reserves'), 'url' => ['index']
];
$this->params['breadcrumbs'][] = [
    'label' => $model->car->getCarTitle(),
    'url' => ['view', 'id' => $model->reserve_id]
];
$this->params['breadcrumbs'][] = Yii::t('cars', 'Update');
?>
<div class="jarviswidget">
    <div class="car-reserve-update">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
