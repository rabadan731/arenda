<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\CarReserve */

$this->title = $model->car->getCarTitle();
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cars', 'Car Reserves'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jarviswidget">
    <div class="car-reserve-view">

        <?php if ($model->delete) { ?>
            <?php echo $this->render('/_delete', ['model' => $model]); ?>
        <?php } ?>

        <p>
            <?= Html::a(
                Yii::t('cars', 'Update'),
                ['update', 'id' => $model->reserve_id],
                ['class' => 'btn btn-primary']
            ); ?>
            <?= Html::a(Yii::t('cars', 'Delete'), ['delete', 'id' => $model->reserve_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('cars', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'car.carTitle',
                'date_begin:datetime',
                'date_end:datetime',
                'information:html',
            ],
        ]) ?>

    </div>
</div>
