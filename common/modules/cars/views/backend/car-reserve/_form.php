<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\clients\models\Clients;
use common\modules\cars\models\Car;
use common\modules\agency\models\Agency;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\CarReserve */
/* @var $form yii\widgets\ActiveForm */

$agencyId = Yii::$app->user->identity->agency_id;

if (empty($model->date_begin)) {
    $model->date_begin = date("Y-m-d H:i");
} elseif (is_int($model->date_begin)) {
    $model->date_begin = date("Y-m-d H:i", $model->date_begin);
}

if (empty($model->date_end)) {
    $model->date_end = date("Y-m-d H:i", strtotime("+1 day"));
} elseif (is_int($model->date_end)) {
    $model->date_end = date("Y-m-d H:i", $model->date_end);
}

$js = <<< JS
    // Modal Link
    $('#add-client-form').click(function() {
        $("#dialog-message").dialog('open');
        return false;
    });
    
    $("#dialog-message").dialog({
        autoOpen : false,
        modal : true,
        title : 'Create client'
    });
    
    
    $('body').on('beforeSubmit', 'form#clients-form', function () {

     var form = $(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }
     //
     // alert(444);
     //  submit form
     $.ajax({
          url: '/backend/clients/clients/create/?save=1&agency_id={$agencyId}',
          type: 'post',
          data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
          contentType: false,       // The content type used when sending data to the server.
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false 
          success: function (response) {
               if (response.success) {
                    $("#dialog-message").dialog("close");
                    $("#carreserve-client_id").append(
                        $('<option value="'+response.at.id+'">'+response.at.fio+'</option>')
                    );
                    $("#carreserve-client_id [value='"+response.at.id+"']").attr("selected", "selected");
               } else {
                   alert("error save client");
               }
          }
     });
     return false;
});
    
JS;
$this->registerJs($js);

?>

<div class="car-reserve-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if (count($model->errors)) { ?>
        <?= $form->errorSummary($model); ?>
    <?php } ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'car_id')->dropDownList(Car::getList()) ?>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-8">
                    <?= $form->field($model, 'client_id')->dropDownList(
                        Clients::getList(),
                        ['prompt' => Yii::t("common", "-- Select --")]
                    ) ?>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <br />
                        <?= Html::button(Yii::t('common', 'Create new client'), [
                            'id' => 'add-client-form',
                            'class' => 'btn btn-success',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <?php echo DateTimePicker::widget([
                'model' => $model,
                'attribute' => 'date_begin',
                'options' => ['placeholder' => Yii::t("cars", "Date Begin")],
                'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd HH:ii',
                    'autoclose' => true,
                ],
                'pluginEvents' => [
                    "changeDate" => "function(e) { 
                        od = new Date($('#carreserve-date_end').val());
                        if (od < e.date) {
                            var year    = e.date.getUTCFullYear();
                            var month   = e.date.getUTCMonth() + 1;
                            var day     = e.date.getUTCDate();
                            var hours   = e.date.getUTCHours();
                            var minutes = e.date.getUTCMinutes();
                            $('#carreserve-date_end').val(year + \"-\" + month + \"-\" + day + 
                            \" \" + hours + \":\" + minutes);
                        } 
                    }",
                ]
            ]); ?>
        </div>
        <div class="col-md-6">
            <?php echo DateTimePicker::widget([
                'model' => $model,
                'attribute' => 'date_end',
                'options' => ['placeholder' => Yii::t("cars", "Date End")],
                'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd HH:ii',
                    'autoclose' => true,
                ],
                'pluginEvents' => [
                    "changeDate" => "function(e) { 
                        od = new Date($('#carreserve-date_begin').val());
                        if (od > e.date) {
                            var year    = e.date.getUTCFullYear();
                            var month   = e.date.getUTCMonth() + 1;
                            var day     = e.date.getUTCDate();
                            var hours   = e.date.getUTCHours();
                            var minutes = e.date.getUTCMinutes();
                            $('#carreserve-date_begin').val(
                                year + \"-\" + month + \"-\" + day + 
                                \" \" + hours + \":\" + minutes
                            );
                        } 
                    }",
                ]
            ]); ?>
        </div>
    </div>

    <br />

    <?php if (Yii::$app->user->can('admin')) { ?>
        <?= $form->field($model, 'agency_id')->dropDownList(Agency::getAgencies(), [
            'prompt' => Yii::t("common", "-- Select --")
        ]) ?>
    <?php } else { ?>
        <?= $form->field($model, 'agency_id')->hiddenInput([
            'value'=>Yii::$app->user->identity->agency_id
        ])->label(false) ?>
    <?php } ?>


    <?= $form->field($model, 'information')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div id="dialog-message" title="Create client">
    <?= $this->render('../../../../clients/views/backend/clients/_form', [
        'model' => new Clients()
    ]); ?>
</div><!-- #dialog-message -->
