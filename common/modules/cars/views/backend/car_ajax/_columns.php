<?php
use yii\helpers\Url;

return [
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'number',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=> 'car_mark_id',
        'value' => function ($data) {
            return $data->carMark->title;
        },
        'filter' => \common\modules\cars\models\CarMark::getList()
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'car_model_id',
        'value' => function ($data) {
            return $data->carModel->title;
        },
        'filter' => \common\modules\cars\models\CarModel::getList()
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'year',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'track',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'car_color_id',
    // ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'kpp',
         'value' => function ($data) {
             return empty($data->kpp)?null:$data->getKppList($data->kpp);
         },
         'filter' => \common\modules\cars\models\Car::getKppList()
     ],
//    // [
//        // 'class'=>'\kartik\grid\DataColumn',
//        // 'attribute'=>'gazoline',
//    // ],
//     [
//         'class'=>'\kartik\grid\BooleanColumn',
//         'attribute'=>'gps_control',
//     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'volume',
     ],
//    // [
//        // 'class'=>'\kartik\grid\DataColumn',
//        // 'attribute'=>'status',
//    // ],
//    // [
//        // 'class'=>'\kartik\grid\DataColumn',
//        // 'attribute'=>'delete',
//    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   