<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\Car */
?>
<div class="car-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
