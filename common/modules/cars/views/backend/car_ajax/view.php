<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\Car */
?>
<div class="car-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'car_id',
            'car_mark_id',
            'car_model_id',
            'year',
            'number',
            'track',
            'car_color_id',
            'kpp',
            'gazoline',
            'gps_control',
            'volume',
            'status',
            'delete',
        ],
    ]) ?>

</div>
