<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\CarMark */

?>
<div class="car-mark-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
