<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\CarMark */
?>
<div class="car-mark-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'car_mark_id',
            'title',
            'logo',
            'status',
            'delete',
        ],
    ]) ?>

</div>
