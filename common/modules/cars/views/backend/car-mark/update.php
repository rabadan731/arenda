<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\cars\models\CarMark */
?>
<div class="jarviswidget">
    <div class="car-mark-update">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
