<?php

namespace common\modules\cars\models;

use Yii;
use common\modules\cars\models\query\CarMarkQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "car_mark".
 *
 * @property integer $car_mark_id
 * @property string $title
 * @property string $logo
 * @property integer $status
 * @property integer $delete
 *
 * @property Car[] $cars
 */
class CarMark extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_mark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['status', 'delete'], 'integer'],
            [['title'], 'string', 'max' => 256],
            [['logo'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'car_mark_id' => Yii::t('cars', 'Car Mark ID'),
            'title' => Yii::t('cars', 'Title'),
            'logo' => Yii::t('cars', 'Logo'),
            'status' => Yii::t('cars', 'Status'),
            'delete' => Yii::t('cars', 'Delete'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['car_mark_id' => 'car_mark_id']);
    }

    /**
     * @inheritdoc
     * @return CarMarkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarMarkQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getList()
    {
        $items = self::find()->orderBy('title')->all();
        return ArrayHelper::map($items, 'car_mark_id', 'title');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModels()
    {
        return $this->hasMany(CarModel::className(), [
            'car_mark_id' => 'car_mark_id'
        ]);
    }


    /**
     * @return bool
     */
    public function myDelete()
    {
        $this->delete = 1;
        return $this->save(false, ['delete']);
    }

    /**
     * @return bool
     */
    public function unDelete()
    {
        $this->delete = 0;
        return $this->save(false, ['delete']);
    }
}
