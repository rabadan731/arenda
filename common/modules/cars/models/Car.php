<?php

namespace common\modules\cars\models;

use Yii;
use common\modules\cars\models\query\CarQuery;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "car".
 *
 * @property integer $car_id
 * @property integer $car_mark_id
 * @property integer $car_model_id
 * @property integer $type
 * @property integer $year
 * @property string $number
 * @property integer $track
 * @property integer $car_color_id
 * @property integer $kpp
 * @property integer $gazoline
 * @property integer $gps_control
 * @property double $volume
 * @property integer $status
 * @property integer $delete
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CarMark $carMark
 * @property CarModel $carModel
 * @property CarReserve[] $carReserves
 */
class Car extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $agency_id;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'car_mark_id',
                'car_model_id',
                'type',
                'year',
                'track',
                'kpp',
                'gazoline',
                'gps_control',
                'agency_id',
                'created_by',
                'updated_by',
                'created_at',
                'updated_at',
                'status',
                'delete'
            ], 'integer'],
            [['car_mark_id', 'car_model_id'], 'required'],
            [['volume'], 'number'],
            [['number'], 'string', 'max' => 16],
            [['car_color_id'], 'string', 'max' => 7],
            [
                ['car_mark_id'],
                'exist', 'skipOnError' => true,
                'targetClass' => CarMark::className(),
                'targetAttribute' => ['car_mark_id' => 'car_mark_id']
            ],
            [
                ['car_model_id'],
                'exist', 'skipOnError' => true,
                'targetClass' => CarModel::className(),
                'targetAttribute' => ['car_model_id' => 'car_model_id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'car_id' => Yii::t('cars', 'ID'),
            'car_mark_id' => Yii::t('cars', 'Mark'),
            'carMark.title' => Yii::t('cars', 'Mark'),
            'car_model_id' => Yii::t('cars', 'Model'),
            'carModel.title' => Yii::t('cars', 'Model'),
            'year' => Yii::t('cars', 'Year'),
            'number' => Yii::t('cars', 'Number'),
            'track' => Yii::t('cars', 'Track'),
            'car_color_id' => Yii::t('cars', 'Color'),
            'kpp' => Yii::t('cars', 'Kpp'),
            'gazoline' => Yii::t('cars', 'Gazoline'),
            'gps_control' => Yii::t('cars', 'Gps Control'),
            'volume' => Yii::t('cars', 'Volume'),
            'status' => Yii::t('cars', 'Status'),
            'delete' => Yii::t('cars', 'Delete'),
            'agency_id' => Yii::t('common', 'Agency'),
        ];
    }


    /**
     * @param null $key
     * @return array|mixed|null
     */
    public static function getKppList($key = null)
    {
        $array = [
            '1' => Yii::t("cars", "Automatic"),
            '3' => Yii::t("cars", "Manual"),
            '5' => Yii::t("cars", "Mixed"),
            '7' => Yii::t("cars", "Variator"),
        ];

        if (!is_null($key)) {
            if (isset($array[$key])) {
                return $array[$key];
            }
            return null;
        }

        return $array;
    }

    /**
     * @param null $key
     * @return array|mixed|null
     */
    public static function getTypes($key = null)
    {
        $array = [
            '100' => Yii::t("cars", "Car"),
            '102' => Yii::t("cars", "Moped"),
            '104' => Yii::t("cars", "Scooter"),
        ];

        if (!is_null($key)) {
            if (isset($array[$key])) {
                return $array[$key];
            }
            return null;
        }

        return $array;
    }

    /**
     * @param null $key
     * @return array|mixed|null
     */
    public static function getGazolineList($key = null)
    {
        $array = [
            '1' => Yii::t("cars", "Diesel"),
            '3' => Yii::t("cars", "Gasoline"),
            '5' => Yii::t("cars", "Gas / Gasoline"),
            '7' => Yii::t("cars", "Electric"),
        ];

        if (!is_null($key)) {
            if (isset($array[$key])) {
                return $array[$key];
            }
            return null;
        }

        return $array;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarMark()
    {
        return $this->hasOne(CarMark::className(), [
            'car_mark_id' => 'car_mark_id'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModel()
    {
        return $this->hasOne(CarModel::className(), [
            'car_model_id' => 'car_model_id'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarReserves()
    {
        return $this->hasMany(CarReserve::className(), [
            'car_id' => 'car_color_id'
        ]);
    }

    /**
     * @inheritdoc
     * @return CarQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function getList()
    {
        $find = self::find();
        $out = [];
        foreach ($find->all() as $item){
            $out[$item->car_id] = $item->getCarTitle();
        }
        return $out;
    }

    /**
     * @return string
     */
    public function getCarTitle()
    {
        return "{$this->carMark->title} {$this->carModel->title}
         {$this->volume} {$this->number}";
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if (CarAccess::find()->andWhere(['car_id' => $this->car_id])->count() < 1) {
            $access = new CarAccess();
            $access->car_id = $this->car_id;
            if (Yii::$app->user->can('admin')) {
                $access->agency_id = $this->agency_id;
            } else {
                $access->agency_id = Yii::$app->user->identity->agency_id;
            }
            $access->save();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        foreach ($this->carReserves as $item) {
            $item->delete();
        }

        CarAccess::deleteAll(['car_id' => $this->car_id]);

        return parent::beforeDelete();
    }

    /**
     * @return bool
     */
    public function myDelete()
    {
        $this->delete = 1;
        return $this->save(false, ['delete']);
    }

    /**
     * @return bool
     */
    public function unDelete()
    {
        $this->delete = 0;
        return $this->save(false, ['delete']);
    }
}
