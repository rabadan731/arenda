<?php

namespace common\modules\cars\models;

use Yii;
use common\modules\cars\models\query\CarModelQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "car_model".
 *
 * @property integer $car_model_id
 * @property integer $car_mark_id
 * @property string $title
 * @property string $logo
 * @property integer $status
 * @property integer $delete
 *
 * @property Car[] $cars
 * @property CarMark $carMark
 */
class CarModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_mark_id', 'status', 'delete'], 'integer'],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 256],
            [['logo'], 'string', 'max' => 512],
            [
                ['car_mark_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarMark::className(),
                'targetAttribute' => ['car_mark_id' => 'car_mark_id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'car_model_id' => Yii::t('cars', 'ID'),
            'car_mark_id' => Yii::t('cars', 'Mark'),
            'title' => Yii::t('cars', 'Title'),
            'logo' => Yii::t('cars', 'Logo'),
            'status' => Yii::t('cars', 'Status'),
            'delete' => Yii::t('cars', 'Delete'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), [
            'car_model_id' => 'car_model_id'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarMark()
    {
        return $this->hasOne(CarMark::className(), [
            'car_mark_id' => 'car_mark_id'
        ]);
    }

    /**
     * @inheritdoc
     * @return CarModelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarModelQuery(get_called_class());
    }


    /**
     * @return array
     */
    public static function getList($markID = null)
    {
        $items = self::find()->orderBy('title');

        if (!is_null($markID)) {
            $items->andWhere([
                'car_mark_id' => $markID
            ]);
        }

        return ArrayHelper::map($items->all(), 'car_model_id', 'title');
    }


    /**
     * @return bool
     */
    public function myDelete()
    {
        $this->delete = 1;
        return $this->save(false, ['delete']);
    }

    /**
     * @return bool
     */
    public function unDelete()
    {
        $this->delete = 0;
        return $this->save(false, ['delete']);
    }
}
