<?php

namespace common\modules\cars\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\cars\models\CarReserve]].
 *
 * @see \common\modules\cars\models\CarReserve
 */
class CarReserveQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\cars\models\CarReserve[]|array
     */
    public function all($db = null)
    {
        $this->andWhere(['car_reserve.delete' => 0]);
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\cars\models\CarReserve|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $dateBegin
     * @param $dateEnd
     * @return $this
     */
    public function date($dateBegin, $dateEnd)
    {
        if (!is_null($dateBegin)) {
            $dateBegin = strtotime($dateBegin);
            $dateEnd = strtotime($dateEnd);
            $this->datetime($dateBegin, $dateEnd);
        }
        return $this;
    }

    /**
     * @param $dateBegin
     * @param $dateEnd
     * @return $this
     */
    public function datetime($dateBegin, $dateEnd)
    {
        $this->andWhere(['or',
            [
                'and',
                "car_reserve.date_begin <= {$dateBegin}",
                "car_reserve.date_end >= {$dateBegin}"
            ],
            [
                'and',
                "car_reserve.date_begin >= {$dateBegin}",
                "car_reserve.date_begin <= {$dateEnd}"
            ],
            [
                'and',
                "car_reserve.date_end >= {$dateBegin}",
                "car_reserve.date_end <= {$dateEnd}"
            ],
        ]);

        return $this;
    }


    /**
     * @return $this
     */
    public function notDeleted()
    {
        $this->andWhere(['car_reserve.delete' => 0]);
        return $this;
    }

    /**
     * @param null $db
     * @return array|\yii\db\ActiveRecord[]
     */
    public function allDefault($db = null)
    {
        return parent::all($db);
    }

    public function deleted()
    {
        $this->andWhere(['car_reserve.delete' => 1]);
        return $this;
    }
}
