<?php

namespace common\modules\cars\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\cars\models\CarAccess]].
 *
 * @see \common\modules\cars\models\CarAccess
 */
class CarAccessQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\cars\models\CarAccess[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\cars\models\CarAccess|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
