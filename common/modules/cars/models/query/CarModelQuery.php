<?php

namespace common\modules\cars\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\cars\models\CarModel]].
 *
 * @see \common\modules\cars\models\CarModel
 */
class CarModelQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\cars\models\CarModel[]|array
     */
    public function all($db = null)
    {
        $this->andWhere(['car_model.delete' => 0]);
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\cars\models\CarModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
