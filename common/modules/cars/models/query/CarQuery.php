<?php

namespace common\modules\cars\models\query;

use Yii;

/**
 * This is the ActiveQuery class for [[\common\modules\cars\models\Car]].
 *
 * @see \common\modules\cars\models\Car
 */
class CarQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\cars\models\Car[]|array
     */
    public function all($db = null)
    {
        $this->andWhere(['car.delete' => 0]);
        if (!Yii::$app->user->can('admin')) {
            $this->checkAccess();
        }
        return parent::all($db);
    }

    /**
     * @return $this
     */
    public function checkAccess()
    {
        $this->leftJoin("car_access", 'car_access.car_id = car.car_id');
        $this->andWhere([
            'or',
            ['car_access.agency_id' => Yii::$app->user->identity->agency_id],
            ['car_access.user_id' => Yii::$app->user->id]
        ]);

        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\modules\cars\models\Car|array|null
     */
    public function one($db = null)
    {
        if (!Yii::$app->user->can('admin')) {
            $this->checkAccess();
        }
        return parent::one($db);
    }


    /**
     * @param null $db
     * @return array|\yii\db\ActiveRecord[]
     */
    public function allDefault($db = null)
    {
        return parent::all($db);
    }

    public function deleted()
    {
        $this->andWhere(['car.delete' => 1]);
        return $this;
    }
}
