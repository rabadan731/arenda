<?php

namespace common\modules\cars\models;

use common\modules\agency\models\Agency;
use modules\users\models\backend\Users;
use Yii;
use common\modules\cars\models\query\CarReserveAccessQuery;

/**
 * This is the model class for table "car_reserve_access".
 *
 * @property integer $car_reserve_access_id
 * @property integer $reserve_id
 * @property integer $agency_id
 * @property integer $user_id
 *
 * @property Agency $agency
 * @property CarReserve $reserve
 * @property Users $user
 */
class CarReserveAccess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_reserve_access';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reserve_id', 'agency_id'], 'required'],
            [['reserve_id', 'agency_id', 'user_id'], 'integer'],
            [
                ['agency_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Agency::className(),
                'targetAttribute' => ['agency_id' => 'id']
            ],
            [
                ['reserve_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CarReserve::className(),
                'targetAttribute' => ['reserve_id' => 'reserve_id']
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Users::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'car_reserve_access_id' => Yii::t('cars', 'Car Reserve Access ID'),
            'reserve_id' => Yii::t('cars', 'Reserve ID'),
            'agency_id' => Yii::t('cars', 'Agency ID'),
            'user_id' => Yii::t('cars', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'agency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReserve()
    {
        return $this->hasOne(CarReserve::className(), ['reserve_id' => 'reserve_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return CarReserveAccessQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarReserveAccessQuery(get_called_class());
    }
}
