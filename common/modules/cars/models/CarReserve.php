<?php

namespace common\modules\cars\models;

use Yii;
use common\modules\cars\models\query\CarReserveQuery;
use common\modules\clients\models\Clients;
use yii\behaviors\BlameableBehavior;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/**
 * This is the model class for table "car_reserve".
 *
 * @property integer $reserve_id
 * @property integer $car_id
 * @property string $phone
 * @property integer $client_id
 * @property string $information
 * @property integer $date_begin
 * @property integer $date_end
 * @property integer $status
 * @property integer $delete
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $agency_id
 *
 * @property Clients $client
 * @property Car $car
 */
class CarReserve extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $agency_id;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_reserve';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id'], 'required'],
            [['agency_id'], 'required', 'when' => function ($model) {
                return Yii::$app->user->can('admin');
            }],
            [[
                'car_id',
                'agency_id',
                'client_id',
                'status',
                'created_by',
                'updated_by',
                'delete'
            ], 'integer'],
            [['date_begin', 'date_end'], 'default', 'value' => null],
            [['date_begin', 'date_end'], 'validateReserve'],
            [['information'], 'string'],
            [['phone'], 'string', 'max' => 256],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Clients::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
            [
                ['car_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Car::className(),
                'targetAttribute' => ['car_id' => 'car_id']
            ],
        ];
    }


    /**
     * @param $attribute
     * @param $params
     */
    public function validateReserve($attribute, $params)
    {
        $find = self::find();
        if (!$this->isNewRecord) {
            $find->andWhere(['<>', 'car_reserve.reserve_id', $this->reserve_id]);
        }

        $result = $find->andWhere(['car_reserve.car_id' => $this->car_id])
            ->date($this->date_begin, $this->date_end)->notDeleted()->one();


        if (!is_null($result)) {
            $this->addError($attribute, Yii::t("common", "During this period, the object is busy"));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reserve_id' => Yii::t('cars', 'ID'),
            'car_id' => Yii::t('cars', 'Car'),
            'car.carTitle' => Yii::t('cars', 'Car'),
            'phone' => Yii::t('cars', 'Phone'),
            'client_id' => Yii::t('cars', 'Client'),
            'information' => Yii::t('cars', 'Information'),
            'date_begin' => Yii::t('cars', 'Date Begin'),
            'date_end' => Yii::t('cars', 'Date End'),
            'status' => Yii::t('cars', 'Status'),
            'delete' => Yii::t('cars', 'Delete'),
            'agency_id' => Yii::t('common', 'Agency'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return string
     */
    public function getCarNumber()
    {
        return $this->car->number;
    }

    /**
     * @inheritdoc
     * @return CarReserveQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarReserveQuery(get_called_class());
    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $this->dateToTime();

        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if (CarReserveAccess::find()->andWhere(['reserve_id' => $this->reserve_id])->count() < 1) {
            $access = new CarReserveAccess();
            $access->reserve_id = $this->reserve_id;
            if (Yii::$app->user->can('admin')) {
                $access->agency_id = $this->agency_id;
            } else {
                $access->agency_id = Yii::$app->user->identity->agency_id;
            }
            $access->save();
        }

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }


    /**
     *
     */
    public function dateToTime()
    {
        $this->date_begin = is_int($this->date_begin) ?
            $this->date_begin : strtotime($this->date_begin);
        $this->date_end = is_int($this->date_end) ?
            $this->date_end : strtotime($this->date_end);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        CarReserveAccess::deleteAll(['reserve_id' => $this->reserve_id]);

        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }


    /**
     * @return bool
     */
    public function myDelete()
    {
        $this->delete = 1;
        return $this->save(false, ['delete']);
    }

    /**
     * @return bool
     */
    public function unDelete()
    {
        $this->delete = 0;
        return $this->save(false, ['delete']);
    }
}
