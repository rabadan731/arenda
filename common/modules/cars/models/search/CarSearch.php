<?php

namespace common\modules\cars\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\cars\models\Car;
use yii\data\ArrayDataProvider;

/**
 * CarSearch represents the model behind the search form about `common\modules\cars\models\Car`.
 */
class CarSearch extends Car
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_id', 'car_mark_id', 'car_model_id', 'year', 'track', 'kpp', 'gazoline', 'gps_control', 'status', 'delete', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['number', 'car_color_id'], 'safe'],
            [['volume'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Car::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'car_id' => $this->car_id,
            'car_mark_id' => $this->car_mark_id,
            'car_model_id' => $this->car_model_id,
            'year' => $this->year,
            'track' => $this->track,
            'kpp' => $this->kpp,
            'gazoline' => $this->gazoline,
            'gps_control' => $this->gps_control,
            'volume' => $this->volume,
            'status' => $this->status,
            'delete' => $this->delete,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'car_color_id', $this->car_color_id]);

        return $dataProvider;
    }

    /**
     * @return ArrayDataProvider
     */
    public function searchDelete()
    {
        $query = Car::find()->deleted();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->allDefault(),
            'pagination' => ['pageSize' => 10],
        ]);

        return $dataProvider;
    }

}
