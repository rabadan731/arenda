<?php

namespace common\modules\cars\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\cars\models\CarReserve;
use yii\data\ArrayDataProvider;

/**
 * CarReserveSearch represents the model behind the search form about `common\modules\cars\models\CarReserve`.
 */
class CarReserveSearch extends CarReserve
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'reserve_id',
                'car_id',
                'client_id',
                'date_begin',
                'date_end',
                'status',
                'delete',
                'created_by',
                'updated_by'
            ], 'integer'],
            [['phone', 'information'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarReserve::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'reserve_id' => $this->reserve_id,
            'car_id' => $this->car_id,
            'client_id' => $this->client_id,
            'date_begin' => $this->date_begin,
            'date_end' => $this->date_end,
            'status' => $this->status,
            'delete' => $this->delete,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'information', $this->information]);

        return $dataProvider;
    }

    /**
     * @return ArrayDataProvider
     */
    public function searchDelete()
    {
        $query = CarReserve::find()->deleted();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->allDefault(),
            'pagination' => ['pageSize' => 10],
        ]);

        return $dataProvider;
    }
}
