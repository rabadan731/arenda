<?php

namespace common\modules\cars\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\cars\models\CarModel;

/**
 * CarModelSearch represents the model behind the search form about `common\modules\cars\models\CarModel`.
 */
class CarModelSearch extends CarModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_model_id', 'car_mark_id', 'status', 'delete'], 'integer'],
            [['title', 'logo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'car_mark_id' => $this->car_mark_id,
            'car_model_id' => $this->car_model_id,
            'status' => $this->status,
            'delete' => $this->delete,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'logo', $this->logo]);

        return $dataProvider;
    }
}
