<?php

namespace common\modules\cars\models;

use common\modules\cars\models\query\CarAccessQuery;
use common\modules\agency\models\Agency;
use modules\users\models\backend\Users;
use Yii;

/**
 * This is the model class for table "car_access".
 *
 * @property integer $car_access_id
 * @property integer $car_id
 * @property integer $agency_id
 * @property integer $user_id
 *
 * @property Car $car
 * @property Agency $agency
 * @property Users $user
 */
class CarAccess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_access';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_id', 'agency_id'], 'required'],
            [['car_id', 'agency_id', 'user_id'], 'integer'],
            [
                ['car_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Car::className(),
                'targetAttribute' => ['car_id' => 'car_id']
            ],
            [
                ['agency_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Agency::className(),
                'targetAttribute' => ['agency_id' => 'id']
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Users::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'car_access_id' => Yii::t('cars', 'Car Access ID'),
            'car_id' => Yii::t('cars', 'Car ID'),
            'agency_id' => Yii::t('cars', 'Agency ID'),
            'user_id' => Yii::t('cars', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'agency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return CarAccessQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarAccessQuery(get_called_class());
    }
}
