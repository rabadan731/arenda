<?php

namespace modules\cars;

use Yii;

/**
 * Class Module
 * @package modules\main
 */
class Module extends \yii\base\Module
{
    /**
     * @var bool
     */
    public $isBackend = false;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->isBackend === true) {
            $this->setViewPath('@common/modules/cars/views/backend');
            $this->setLayoutPath('@backend/views/layouts');
            $this->controllerNamespace = $this->controllerNamespace === null ?
                'common\modules\cars\controllers\backend' : $this->controllerNamespace;
        } else {
            $this->setViewPath('@common/modules/cars/views/frontend');
            $this->setLayoutPath('@frontend/views/layouts');
            $this->controllerNamespace = $this->controllerNamespace === null ?
                'common\modules\cars\controllers\frontend' : $this->controllerNamespace;
        }

        self::registerTranslations();
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function registerTranslations()
    {
        $translations = Yii::$app->i18n->translations;
        if (!isset($translations['cars']) && !isset($translations['cars/*'])) {
            Yii::$app->i18n->translations['cars'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@common/modules/cars/messages',
                'forceTranslation' => true,
                'fileMap' => [
                    'cars' => 'cars.php'
                ]
            ];
        }
    }
}
