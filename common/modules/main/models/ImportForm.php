<?php

namespace common\modules\main\models;

use common\modules\agency\models\Rent;
use common\modules\eav\models\EavParam;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\web\UploadedFile;


class ImportForm extends Model
{
    /**
     * @var UploadedFile
     */

    public $delimiter = ';';
    public $enclosure = '"';
    public $escape = '\\';
    public $row = 200;
    
    public $keys = [];
    public $typeImport = 0;
    public $scheme = null;
    public $agency = null;

    public $firstLineHeader = true;

    public $csvFile;
    public $md5;
    public $dir = "@runtime/import_file/";

    const TYPE_NEW_ADD_OLD_SAFE = 0;
    const TYPE_NEW_ADD_OLD_REWRITE = 3;
    const TYPE_ALL_ADD_OLD_SAFE = 6;

    public function getTypeImport($type = null)
    {
        $types = [
            self::TYPE_NEW_ADD_OLD_SAFE => 'Добавлять только новые, старые без изменеия',
            self::TYPE_NEW_ADD_OLD_REWRITE => 'Добавить новые, старые перезаписать',
            self::TYPE_ALL_ADD_OLD_SAFE => 'Добавить все к имеющимся, старые без изменеия',
        ];

        if (!is_null($type)) {
            if (isset($types[$type])) {
                return $types[$type];
            } else {
                return null;
            }
        }

        return $types;
    }

    public function rules()
    {
        return [
            [['csvFile'], 'file', 'skipOnEmpty' => false],
        ];
    }

    public function attributeLabels()
    {
        return [
            'csvFile' => "Загрузить новый файл",
            'firstLineHeader' => "Первая строка является заголовками столбцов",
        ];
    }



    public function getSchemes()
    {
        return [
            'rent' => "Объект сдачи(Rent)",
        ];
    }
    public function getSchemeObject() {
        switch ($this->scheme) {
            case 'rent' : return new Rent();
            default : return null;
        }
    }

    public function getScheme()
    {
        switch ($this->scheme) {
            case 'rent' :
                return [
                    'title' => "",
                    'description' => "",
                    'address_text' => "",
                    'latitude' => "",
                    'longitude' => "",
                    'fio' => "",
                    'email' => "",
                    'phone' => "",
                    'contract_date_begin' => "",
                    'contract_date_end' => "",
                    'contract_month_price' => "",
                    'contract_week_price' => "",
                    'contract_day_price' => "",
                    'price_month' => "",
                    'price_week' => "",
                    'price_day' => "", 
                ];
            break;

            default : return [];
        }
    }


    public function getFileKeys()
    {
        $keys = null;

        if (($handle = fopen(\Yii::getAlias($this->dir) . $this->md5, 'r')) !== FALSE) {
            if (($data = fgetcsv($handle, 0, $this->delimiter, $this->enclosure, $this->escape)) !== FALSE) {
                $encoding = $this->detect_encoding(implode($data, ' '));

                if ($encoding != 'utf-8') {
                    $keys = array_map(function ($item) use ($encoding) {
                        return iconv($encoding, "UTF-8", $item);
                    }, $data);
                }
            }
            fclose($handle);
        }

        return $keys;
    }


    public function parseTest($lines = 10)
    {
        $row = 1;
        $result = [];
        $encoding = 'utf-8';
        //Если файл существует
        if (($handle = fopen(\Yii::getAlias($this->dir) . $this->md5, 'r')) !== FALSE) {

            if ($this->firstLineHeader) {
                fgetcsv($handle, 0, $this->delimiter, $this->enclosure, $this->escape);
            }

            //обходим
            while (($data = fgetcsv($handle, 0, $this->delimiter, $this->enclosure, $this->escape)) !== FALSE) {


                if ($row == 1) {
                    $encoding = $this->detect_encoding(implode($data, ' '));
                }

                if ($encoding != 'utf-8') {
                    $data = array_map(function ($item) use ($encoding) {
                        return iconv($encoding, "UTF-8", $item);
                    }, $data);
                }


                foreach ($this->keys as $key => $val) {
                    $result[$row][$key] = $data[$val];
                }

                $row++;

                if ($row == $lines) {
                    fclose($handle);
                    return $result;
                }
            }
            fclose($handle);
            return $result;
        }

        return "ERROR_LOAD";
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->md5 = Inflector::slug($this->csvFile->baseName, '_') . '_' . time() . '.' . $this->csvFile->extension;
            if (FileHelper::createDirectory(\Yii::getAlias($this->dir))) {
                $this->csvFile->saveAs($this->getAlias());
            } else {
                throw new Exception("ERROR CREATE FOLDER");
            }
            return true;
        } else {
            return false;
        }
    }


    public function parse($pos = 0)
    {

        $row = 1;
        $encoding = 'utf-8';
        //Если файл существует
        if (($handle = fopen(\Yii::getAlias($this->dir) . $this->md5, 'r')) !== FALSE) {

            //если задана позиция, начинаем с нее. Иначе с нуля
            if ($pos > 0) {
                fseek($handle, $pos);
            } else {
                if ($this->firstLineHeader) {
                    fgetcsv($handle, 0, $this->delimiter, $this->enclosure, $this->escape);
                }
            }

            //обходим
            while (($data = fgetcsv($handle, 0, $this->delimiter, $this->enclosure, $this->escape)) !== FALSE) {

                if ($row == 1) {
                    $encoding = $this->detect_encoding(implode($data, ' '));
                }

                if ($encoding != 'utf-8') {
                    $data = array_map(function ($item) use ($encoding) {
                        return iconv($encoding, "UTF-8", $item);
                    }, $data);
                }

                $attributes = [];
                foreach ($this->keys as $key => $val) {
                    $attributes[$key] = $data[$val];
                }
 
                $model = $this->getSchemeObject();
                if (!empty($resultImport = $model->import($attributes, $this->agency))) {
                    return "ERROR_LOAD:".print_r($resultImport, 1);
                }
                // Company::import($this->parsingData($data));

                $row++;

                if ($row == $this->row) {
                    $endRow = ftell($handle);
                    fclose($handle);
                    return ("POS_" . $endRow);
                }
            }
            fclose($handle);
            return "END";
        }

        return "ERROR_LOAD";

    }


    public static function detect_encoding($string, $pattern_size = 50)
    {
        $list = array('cp1251', 'utf-8', 'ascii', '855', 'KOI8R', 'ISO-IR-111', 'CP866', 'KOI8U');
        $c = strlen($string);
        if ($c > $pattern_size) {
            $string = substr($string, floor(($c - $pattern_size) / 2), $pattern_size);
            $c = $pattern_size;
        }

        $reg1 = '/(\xE0|\xE5|\xE8|\xEE|\xF3|\xFB|\xFD|\xFE|\xFF)/i';
        $reg2 = '/(\xE1|\xE2|\xE3|\xE4|\xE6|\xE7|\xE9|\xEA|\xEB|\xEC|\xED|\xEF|\xF0|\xF1|\xF2|\xF4|\xF5|\xF6|\xF7|\xF8|\xF9|\xFA|\xFC)/i';

        $mk = 10000;
        $enc = 'ascii';
        foreach ($list as $item) {
            $sample1 = @iconv($item, 'cp1251', $string);
            $gl = @preg_match_all($reg1, $sample1, $arr);
            $sl = @preg_match_all($reg2, $sample1, $arr);
            if (!$gl || !$sl) continue;
            $k = abs(3 - ($sl / $gl));
            $k += $c - $gl - $sl;
            if ($k < $mk) {
                $enc = $item;
                $mk = $k;
            }
        }
        return $enc;
    }
    
    public function getAlias()
    {
        if (!is_dir(\Yii::getAlias($this->dir))) {
            FileHelper::createDirectory(rtrim("" . \Yii::getAlias($this->dir), "/"));
        }
        return \Yii::getAlias("{$this->dir}{$this->md5}");
    }

    public function deleteFile()
    {
        if (is_file($this->getAlias())) {
            unlink($this->getAlias());
        }
    }


}