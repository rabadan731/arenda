<?php

namespace common\modules\main\models;

use common\modules\main\models\query\MailQuery;
use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "arenda_mail".
 *
 * @property integer $id
 * @property string $from
 * @property string $to
 * @property string $subject
 * @property string $html
 * @property integer $status
 * @property integer $data_try_count
 * @property integer $data_try_sent
 * @property integer $data_sent
 */
class Mail extends \yii\db\ActiveRecord
{

    const STATUS_DRAFT = 0;
    const STATUS_TRY_SENT = 3;
    const STATUS_ERROR_SENT = 5;
    const STATUS_SENT = 7;

    /**
     * @param null $status
     * @return array|mixed|null
     */
    public static function getStatusItem($status = null)
    {
        $statusList = [
            self::STATUS_DRAFT => Yii::t("common", "Draft"),
            self::STATUS_TRY_SENT => Yii::t("common", "Attempt to send"),
            self::STATUS_ERROR_SENT => Yii::t("common", "Error sent"),
            self::STATUS_SENT => Yii::t("common", "Sent"),
        ];

        if (is_null($status)) {
            return $statusList;
        } else {
            if (array_key_exists($status, $statusList)) {
                return $statusList[$status];
            }
            return $status;
        }
    }

    /**
     * @return array|mixed|null
     */
    public function getStatusText()
    {
        return $this->getStatusItem($this->status);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'arenda_mail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'required'],
            [['from', 'to'], 'email'],
            [['html'], 'string'],
            [['status', 'data_try_count', 'data_try_sent', 'data_sent', 'created_by', 'updated_by'], 'integer'],
            [['from', 'to', 'subject'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'from' => Yii::t('common', 'From'),
            'to' => Yii::t('common', 'To'),
            'subject' => Yii::t('common', 'Subject'),
            'html' => Yii::t('common', 'Html'),
            'status' => Yii::t('common', 'Status'),
            'statusText' => Yii::t('common', 'Status'),
            'data_try_count' => Yii::t('common', 'Attempt to send'),
            'data_try_sent' => Yii::t('common', 'Data Try Sent'),
            'data_sent' => Yii::t('common', 'Data sent'),
            'created_by' => Yii::t('common', 'Created By'),
            'updated_by' => Yii::t('common', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     * @return MailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MailQuery(get_called_class());
    }

    /**
     * @return bool
     */
    public function beforeSentMail()
    {
        $this->data_try_count += 1;
        $this->data_try_sent = time();
        $this->status = self::STATUS_TRY_SENT;
        return $this->save(false, ['data_try_count', 'data_try_sent', 'status']);
    }

    /**
     * @return bool
     */
    public function goodSentMail()
    {
        $this->data_sent = time();
        $this->status = self::STATUS_SENT;
        return $this->save(false, ['data_sent', 'status']);
    }

    /**
     * @return bool
     */
    public function badSentMail()
    { 
        $this->status = self::STATUS_ERROR_SENT;
        return $this->save(false, ['status']); 
    }

    /**
     * @return bool
     */
    public function sent()
    {
        return Yii::$app->mailer->compose()
            ->setTo($this->to)
            ->setFrom(KeyStorageItem::getValue("email.mail", 'email'))
            ->setReplyTo(KeyStorageItem::getValue("email.mail", 'email'))
            ->setSubject($this->subject)
            ->setHtmlBody($this->html)
            ->send();
    }
}
