<?php

namespace common\modules\main\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\main\models\Mail]].
 *
 * @see \common\modules\main\models\Mail
 */
class MailQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\main\models\Mail[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\main\models\Mail|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
