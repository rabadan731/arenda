<?php

namespace common\modules\main\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "key_storage_item".
 *
 * @property integer $key
 * @property integer $value
 */
class KeyStorageItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%key_storage_item}}';
    }

    public function behaviors()
    {
        return [
            [
              'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'value'], 'required'],
            [['key'], 'string', 'max'=>128],
            [['value', 'comment'], 'safe'],
            [['key'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'key' => Yii::t('common', 'Key'),
            'value' => Yii::t('common', 'Value'),
            'comment' => Yii::t('common', 'Comment'),
        ];
    }

    /**
     * @param $key
     * @param $defaultValue
     * @return int
     */
    public static function getValue($key, $defaultValue = null)
    {
//        echo '<pre>'.print_r($key,1).'</pre>';
//        die;
        $model = self::find()->andWhere(['key' => $key])->one();
        if (is_null($model)) {
            $model = new self([
                'key' => $key,
                'comment' => $key,
                'value' => $defaultValue
            ]);
            $model->save();
        }
        return $model->value;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        Yii::$app->cache->delete($this->key);

        $mas =  array (
            'class' => 'yii\\swiftmailer\\Mailer',
            'useFileTransport' => false,
            'messageConfig' =>
                array (
                    'charset' => 'UTF-8',
                    'from' => KeyStorageItem::getValue("email.from", 'from'),
                ),
            'transport' =>
                array (
                    'class' => 'Swift_SmtpTransport',
                    'host' => KeyStorageItem::getValue("email.host", 'host'),
                    'username' => KeyStorageItem::getValue("email.username", 'username'),
                    'password' => KeyStorageItem::getValue("email.password", 'password'),
                    'port' => KeyStorageItem::getValue("email.port", '465'),
                    'encryption' => KeyStorageItem::getValue("email.encryption", 'ssl'),
                ),
        );
        file_put_contents(
            Yii::getAlias("@common/config/mailer.php"),
            "<?php \n return ".var_export($mas,1).";"
        );

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}
