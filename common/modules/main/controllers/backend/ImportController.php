<?php

namespace modules\main\controllers\backend;

use common\modules\agency\models\Rent;
use Yii;
use backend\components\Controller;
use common\modules\main\models\ImportForm;
use yii\helpers\FileHelper;
use yii\base\Exception;
use yii\helpers\Html;
use yii\web\UploadedFile;


/**
 * Class DefaultController
 * @package modules\main\controllers\backend
 */
class ImportController extends Controller
{
    /**
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $model = new ImportForm();

        if (Yii::$app->request->isPost) {
            $model->csvFile = UploadedFile::getInstance($model, 'csvFile');
            if ($model->upload()) {
                // file is uploaded successfully
                return $this->redirect(['import-file', 'md5' => $model->md5]);
            }
        }

        $dir = $model->getAlias();
        $oldFiles = FileHelper::findFiles(rtrim($dir, '/'));
        $oldFiles = array_map(function ($path) {
            return basename($path);
        }, $oldFiles);

        return $this->render('index', ['model' => $model, 'oldFiles' => $oldFiles]);
    }


    public function actionImportFile($md5, $import = false, $scheme = null)
    {
        $model = new ImportForm();
        $model->md5 = $md5;

        $rent = new Rent();
        $post = Yii::$app->request->post();

        if (Yii::$app->request->isPost) {
            $attrs = [
                'delimiter',
                'enclosure',
                'escape',
                'row',
                'typeImport',
                'agency'
            ];

            foreach ($attrs as $attr) {
                if (isset($post['ImportForm'][$attr]) && !empty($post['ImportForm'][$attr])) {
                    $model->{$attr} = $post['ImportForm'][$attr];
                }
            }

            if (isset($post['ImportForm']['firstLineHeader'])) {
                $model->firstLineHeader = $post['ImportForm']['firstLineHeader'];
            } else {
                $model->firstLineHeader = false;
            }
        }

        $model->scheme = $scheme;
        $model->keys = $model->getScheme($model->scheme);

        $fileKeys = $model->getFileKeys();

        foreach ($model->keys as $k => $title) {
            if ($v = array_search($k, $fileKeys)) {
                $model->keys[$k] = $v;
            } elseif ($v = array_search($rent->getAttributeLabel($k), $fileKeys)) {
                $model->keys[$k] = $v;
            }
        }

        if (isset($post['keys'])) {
            foreach ($model->keys as $key => $val) {
                if (isset($post['keys'][$key])) {
                    $model->keys[$key] = $post['keys'][$key];
                }
            }
        }

        $file = $model->getAlias();

        if ($import && Yii::$app->request->isAjax) {
            return $model->parse(Yii::$app->request->post("position"));
        }

        if (!file_exists($file)) {
            throw new Exception("File not exists");
        }

        return $this->render('import-file', [
            'file' => $file,
            'md5' => $md5,
            'model' => $model
        ]);
    }

    public function actionImportDelete($md5)
    {
        $model = new ImportForm();
        $model->md5 = $md5;
        $model->deleteFile();
        return $this->redirect(['index']);
    }


    public function actionExport()
    {
        $agencyId = 0;
        $rent = new Rent();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $agencyId = Yii::$app->request->post('agency_id');
            $rent->load($post);

            $rentKeys = array_keys(array_filter($rent->attributes, function($item) {
               return $item>0;
            }));


            # modify the name somewhat
            $name = "export_" . time() . ".csv";

            # Set the headers we need for this to work
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=' . $name);

            # Start the ouput
            $output = fopen('php://output', 'w');


            # Create the headers
            fputs($output, pack('H*','EFBBBF'));
            fputcsv($output, $rentKeys, ";", '"');


            $rentsFind = Rent::find();
            if (!is_null($agencyId) && $agencyId > 0) {
                $rentsFind->agency($agencyId);
            }
            # Then loop through the rows
            foreach ($rentsFind->all() as $row) {
                # Add the rows to the body
                fputcsv($output, $row->getAttributes($rentKeys), ";", '"');
            }

            fclose($output);
            exit();
        }

        return $this->render('export-rent-form', [
            'rent' => $rent,
            'agencyId' => $agencyId,
        ]);
    }

}
