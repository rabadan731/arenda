<?php

/* @var $this yii\web\View */
/* @var $model common\modules\main\models\Mail */

?>
<div class="arenda-mail-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
