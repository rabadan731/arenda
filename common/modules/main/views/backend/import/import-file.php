<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Progress;
use yii\bootstrap\ActiveForm;
use common\modules\agency\models\Agency;

/* @var $model \common\modules\main\models\ImportForm */
/* @var $file string */
/* @var $md5 string */
/* @var $this yii\web\View; */

$this->title = Yii::t('common', 'Import') . " :'{$md5}'";
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Import'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $md5;

list(, $urlForm) = Yii::$app->assetManager->publish('@bower/jquery-form/jquery.form.js');
$this->registerJsFile($urlForm, ['depends' => 'yii\web\YiiAsset']);

$testModel = new \common\modules\eav\models\EavParam();

$lines = count(file($file));
$importUrl = Url::to(['import-file', "md5" => $md5, 'import' => true,'scheme' => 'rent']);

$imported = true;

?>
<?php $form = ActiveForm::begin([
    'id' => 'import_number_form',
    'options' => ['enctype' => 'multipart/form-data']
]); ?>
<div class="padding-10">
    <div class="well">
        <p>Размер файла: <b><?= number_format(filesize($file) / 1024 / 1024, 2, '.', ' '); ?> Мб</b></p>
        <p>Дата загрузки: <b><?= date("d/m/Y h:i", filemtime($file)); ?></b></p>
        <p>Количество строк в файле: <b id="lines_count"><?= $lines; ?></b></p>

    </div>

    <h3>Схема импорта</h3>
    <?php if (!empty($model->scheme)) {
        $modelScheme = $model->getSchemeObject();
    ?>

    <strong>Импорт</strong>
    <div class="well">
        <div class="row">
            <div class="col-md-6">
                <table class="table">
                    <?php foreach ($model->keys as $key => $val) { ?>
                        <tr>
                            <td class="col-md-7">
                                <?= Html::activeLabel($modelScheme, $key); ?>
                                (<?= Html::label($key); ?>)
                            </td>
                            <td class="col-md-5">
                                <?= Html::dropDownList(
                                    "keys[{$key}]",
                                    $val,
                                    $model->getFileKeys(),
                                    ['prompt' => "--не выбрано--"]
                                ); ?>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td class="col-md-7">
                            <?= Html::activeLabel($model, 'agency_id'); ?>
                            (<?= Html::label('agency_id'); ?>)
                        </td>
                        <td class="col-md-5">
                            <?= $form->field($model, 'agency')->dropDownList(
                                Agency::getAgencies(),
                                ['prompt' => "--не выбрано--",]
                            )->label(false); ?>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <strong>Демо просмотр</strong>
                    </div>
                    <div class="box-body">
                        <?php
                        $attributes = $model->parseTest(4);
                        foreach ($attributes as $key => $attribute) {
                            echo "<h3 style='margin-bottom: 0'>Test Object №{$key}</h3>";
                            $modelScheme = $model->getSchemeObject();
                            /** @var $modelScheme \common\modules\agency\models\Rent */
                            $modelScheme->load($attribute, '');
                            $modelScheme->agency_id = $model->agency;
                            $modelScheme->validate();
                            echo "<h4>Заполненые аттрибуты</h4><pre>";
                            foreach ($modelScheme->attributes as $attrTitle => $attrValue) {
                                if (empty($attrValue)) continue;
                                echo "<strong>{$attrTitle}</strong>: {$attrValue} <br />";
                            }
                            echo "</pre>";
                            if ($modelScheme->hasErrors()) {
                                $imported = false;
                                echo "<h4>Ошибки</h4><pre style='background: #ff5f61'>";
                                foreach ($modelScheme->errors as $attrTitle => $attrErrors) {
                                    foreach ($attrErrors as $attrValue) {
                                        if (empty($attrValue)) continue;
                                        echo "<strong>{$attrTitle}</strong>: {$attrValue} <br />";
                                    }
                                }
                                echo "</pre>";
                            }
                            ?>
                            <hr />
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php } ?>
    <hr/>
    <div class="well">
        <div>
            <?= $form->field($model, 'firstLineHeader')->checkbox() ?>
        </div>
        <div class="row">
            <?= $form->field($model, 'delimiter', ['options' => ['class' => 'col-md-3']]) ?>
            <?= $form->field($model, 'enclosure', ['options' => ['class' => 'col-md-3']]) ?>
            <?= $form->field($model, 'escape', ['options' => ['class' => 'col-md-3']]) ?>
            <?= $form->field($model, 'row', ['options' => ['class' => 'col-md-3']]) ?>
        </div>
        <br />
        <?= Progress::widget([
            'id' => 'progress_bar',
            'percent' => 0,
            'barOptions' => ['class' => 'bg-color-purple'],
            'options' => ['class' => 'progress-striped active']
        ]); ?>

        <?= Html::submitButton('Предпросмотр', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Сброс', ['import-file', 'md5' => $md5, 'scheme' => $model->scheme], ['class' => 'btn btn-default']) ?>
        <?= Html::button('Импортировать', [
            'id' => 'button_import',
            'class' => 'btn btn-success'.($imported?"":" disabled")
        ]) ?>
    </div>
</div>

<?php $form->end(); ?>
<?php
if ($imported) {
    $js = <<< JS

var count_lines = "{$lines}";
var importBtn = $("#button_import");
var progressBar = $(".progress-bar");
var count_step = Number("{$model->row}");
var import_url = "{$importUrl}";

$('body').on('click', '#button_import', function () {
    $("#import_number_form").ajaxSubmit({
        url: import_url,
        data: {
            position: 0,
        },
        beforeSubmit: function() {
            importBtn.prop("disabled", true);
        },
        success: function(data) {
            subimportform(data, count_step);
        }
    });
});


function subimportform(data, loadsss) {
    if (data=="ERROR_CSV") {
        alert("Загружать можно только CSV файл!!!");
        importBtn.prop("disabled", false);
        importBtn.html("Импортировать");
    } else {
        if (data=="ERROR_LOAD" && data=="ERROR_POST") {
            alert("Ошибка загрузки файла на сервер: "+data);
            importBtn.prop("disabled", false );
            importBtn.html("Импортировать");
        } else {
            if (data=="END") {
                progressBar.css("width","100%");
                progressBar.parent().removeClass("active");
                importBtn.prop("disabled", false );
                importBtn.html("Импортировать");
                alert("Импорт успешно завершен.");
            }

            if (data.indexOf("POS_")>-1 ) {
                var return_fseek = data.substr(4);
                $("#import_number_form").ajaxSubmit({
                    url: import_url,
                    data: {
                        position: return_fseek,
                    },
                    beforeSubmit: function() {
                        importBtn.prop("disabled", true );
                        importBtn.html("Загружено "+loadsss+" строк, продолжаем...");
                        progressBar.css("width",""+(Number(loadsss) * 100 / count_lines)+"%");
                    },
                    success: function(data) {
                        subimportform(data, (Number(loadsss) + Number(count_step)));
                    }
                });
            }

        }
    }
}
JS;
    $this->registerJs($js);
}
