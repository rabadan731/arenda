<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $oldFiles [] */
/* @var $model \common\modules\main\models\ImportForm */

$this->title = Yii::t('common', 'Import');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('

');
?>
<div class="padding-10">
    <h1><?= $this->title; ?></h1>
    <div class="well">


        <strong>Ранее загруженые файлы:</strong>

        <?php foreach ($oldFiles as $oldFile) {
            //   echo '<pre>'.print_r($oldFile,1).'</pre>';die;
            ?>
            <p>
                <b>
                    <?= date("d/m/Y h:i", filemtime(Yii::getAlias($model->dir . $oldFile))); ?>
                </b>
                :
                <?= Html::a($oldFile, ['import-file', 'md5' => $oldFile, 'scheme' => 'rent'], ['class' => 'btn btn-default']); ?>
                <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['import-delete', 'md5' => $oldFile], [
                    'title' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-method' => 'post',
                    'class' => 'btn btn-danger'
                ]); ?>
            </p>
        <?php } ?>
    </div>


    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'options' => ['enctype' => 'multipart/form-data', 'class' => "smart-form"]
    ]); ?>
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'csvFile')->fileInput([
        'onchange' => "$('.fileShow').val(this.value);",
        'accept' => "text/csv",
    ]) ?>
    <br />
    <button class="btn btn-default padding-5" type="submit" style="">
        Загрузить файл
    </button>

    <?php ActiveForm::end(); ?>
</div>