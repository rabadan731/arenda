<?php

/* @var $this \yii\web\View */
/* @var $agencyId integer */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('common', 'Export');
$this->params['breadcrumbs'][] = $this->title;
 
?>
<?php $form = ActiveForm::begin([
    'id' => 'export_form',
]); ?>

<div class="padding-10">
    <div class="well">
        <h2 style="margin-top: 0;padding-top: 0;font-weight: bold"><?= Yii::t("common", "Attributes"); ?></h2>
        <div class="row">
            <?php foreach ($rent->attributes as $attribute => $value) { ?>
                <div class="col-md-2">
                    <?= $form
                        ->field($rent, $attribute)
                        ->label(true)
                        ->widget(\kartik\switchinput\SwitchInput::classname()); ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="padding-10">
    <div class="well">
        <h2 style="margin-top: 0;padding-top: 0;font-weight: bold"><?= Yii::t("common", "Condition"); ?></h2>
        <?=Html::dropDownList('agency_id', $agencyId, \common\modules\agency\models\Agency::getAgencies(), ['class'=>'form-control']); ?>
    </div>
</div>
<div class="padding-10">
    <div class="well">
        <?=Html::submitButton(Yii::t("common","Save"), ['class'=>'btn btn-success']);?>
    </div>
</div>
<?php ActiveForm::end(); ?>