<?php

namespace common\modules\clients\models;

use common\modules\clients\models\query\ClientAgencyQuery;
use common\modules\agency\models\Agency;
use Yii;

/**
 * This is the model class for table "client_agency".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $agency_id
 * @property integer $access_type
 *
 * @property Agency $agency
 * @property Clients $client
 */
class ClientAgency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_agency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'agency_id'], 'required'],
            [['client_id', 'agency_id', 'access_type'], 'integer'],
            [
                ['agency_id'],
                'exist', 'skipOnError' => true,
                'targetClass' => Agency::className(),
                'targetAttribute' => ['agency_id' => 'id']
            ],
            [
                ['client_id'],
                'exist', 'skipOnError' => true,
                'targetClass' => Clients::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'client_id' => Yii::t('common', 'Client ID'),
            'agency_id' => Yii::t('common', 'Agency ID'),
            'access_type' => Yii::t('common', 'Access Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'agency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @inheritdoc
     * @return ClientAgencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClientAgencyQuery(get_called_class());
    }
}
