<?php

namespace common\modules\clients\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\clients\models\ClientPayments;
use yii\data\ArrayDataProvider;

/**
 * ClientPaymentsSearch represents the model behind the search form about `common\modules\clients\models\ClientPayments`.
 */
class ClientPaymentsSearch extends ClientPayments
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'date_invoice', 'paid','for_rent_id'], 'integer'],
            [['title'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $order = 'date_invoice DESC')
    {
        $query = ClientPayments::find();
        $query->orderBy($order);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'for_rent_id' => $this->for_rent_id,
            'date_invoice' => $this->date_invoice,
            'price' => $this->price,
            'paid' => $this->paid,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }


    /**
     * @return ActiveDataProvider
     */
    public function searchDelete()
    {
        $query = ClientPayments::find()->deleted();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->allDelete(),
            'pagination' => ['pageSize' => 10],
        ]);

        return $dataProvider;
    }
}
