<?php

namespace common\modules\clients\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\clients\models\Clients;
use yii\data\ArrayDataProvider;

/**
 * ClientsSearch represents the model behind the search
 * form about `common\modules\clients\models\Clients`.
 */
class ClientsSearch extends Clients
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['balance', 'agency_id'], 'integer'],
            [['fio', 'email', 'phone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Clients::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'clients.id' => $this->id,
            'clients.balance' => $this->balance,
            'clients.agency_id' => $this->agency_id,
            'clients.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'clients.fio', $this->fio])
            ->andFilterWhere(['like', 'clients.photo', $this->photo])
            ->andFilterWhere(['like', 'clients.email', $this->email])
            ->andFilterWhere(['like', 'clients.phone', $this->phone]);
        
        return $dataProvider;
    }

    /**
     * @return ArrayDataProvider
     */
    public function searchDelete()
    {
        $query = Clients::find()->deleted();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->allDelete(),
            'pagination' => ['pageSize' => 10],
        ]);

        return $dataProvider;
    }
}
