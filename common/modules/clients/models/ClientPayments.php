<?php

namespace common\modules\clients\models;

use common\modules\clients\models\query\ClientPaymentsQuery;
use common\modules\agency\models\ForRent;
use common\modules\agency\models\Rent;
use modules\log\behaviors\LogBehavior;
use Yii;

/**
 * This is the model class for table "client_payments".
 *
 * @property integer $id
 * @property integer $client_id
 * @property string $title
 * @property integer $date_invoice
 * @property string $price
 * @property integer $for_rent_id
 * @property integer $paid
 * @property string $delete
 *
 * @property Clients $client
 */
class ClientPayments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'title'], 'required'],
            [['client_id', 'date_invoice', 'paid', 'for_rent_id', 'delete'], 'integer'],
            [['price'], 'number'],
            [['price'], 'default', 'value' => 0],
            [['title'], 'string', 'max' => 512],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Clients::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'client_id' => Yii::t('common', 'Client ID'),
            'title' => Yii::t('common', 'Title'),
            'date_invoice' => Yii::t('common', 'Date Invoice'),
            'price' => Yii::t('common', 'Price'),
            'paid' => Yii::t('common', 'Paid'),
            'for_rent_id' => Yii::t('common', 'For Rent'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForRent()
    {
        return $this->hasOne(ForRent::className(), ['id' => 'for_rent_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRent()
    {
        return $this->hasOne(
            Rent::className(),
            ['id' => 'rent_id']
        )->viaTable(
            'for_rent',
            ['id' => 'for_rent_id']
        );
    }


    /**
     * @inheritdoc
     * @return ClientPaymentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClientPaymentsQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'log' => [
                'class' => LogBehavior::className(),
                'types' => [
                    'update' => function ($model) {
                        /* @var $model self */
                        return "Изменил данные оплаты \"{$model->title}\" 
                        ID:{$model->id} сумма: {$model->price}";
                    },
                    'insert' => function ($model) {
                        /* @var $model self */
                        return "Добавил оплату \"{$model->title}\" 
                        ID:{$model->id} сумма: {$model->price}";
                    },
                    'delete' => function ($model) {
                        /* @var $model self */
                        return "Удалил оплату \"{$model->title}\" 
                        ID:{$model->id} сумма: {$model->price}";
                    }
                ]
            ]
        ];
    }

    public function myDelete()
    {
        $this->delete = 1;
        return $this->save(false, ['delete']);
    }

    public function unDelete()
    {
        $this->delete = 0;
        return $this->save(false, ['delete']);
    }
}
