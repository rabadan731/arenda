<?php

namespace common\modules\clients\models\query;

use common\modules\agency\models\Agency;

/**
 * This is the ActiveQuery class for [[\common\modules\clients\models\ClientPayments]].
 *
 * @see \common\modules\clients\models\ClientPayments
 */
class ClientPaymentsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\clients\models\ClientPayments[]|array
     */
    public function all($db = null)
    {
        $this->andWhere(['client_payments.delete'=>0]);
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\clients\models\ClientPayments|array|null
     */
    public function one($db = null)
    {
        if (!\Yii::$app->user->can("admin")) {
            $this->andWhere(['client_payments.delete'=>0]);
        }
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return Agency[]|array
     */
    public function allDelete($db = null)
    {
        return parent::all($db);
    }

    public function deleted()
    {
        $this->andWhere(['client_payments.delete'=>1]);
        return $this;
    }
}
