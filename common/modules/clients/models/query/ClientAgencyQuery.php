<?php

namespace common\modules\clients\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\clients\models\ClientAgency]].
 *
 * @see \common\modules\clients\models\ClientAgency
 */
class ClientAgencyQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\clients\models\ClientAgency[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\clients\models\ClientAgency|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


    public function cm($rent_id, $agency_id)
    {
        $this->andWhere(['client_id'=>$rent_id, 'agency_id'=>$agency_id]);
        return $this;
    }
}
