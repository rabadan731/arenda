<?php

namespace common\modules\clients\models\query;

use common\modules\agency\models\Agency;
use Yii;

/**
 * This is the ActiveQuery class for [[\common\modules\clients\models\Clients]].
 *
 * @see \common\modules\clients\models\Clients
 */
class ClientsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\clients\models\Clients[]|array
     */
    public function all($db = null)
    {
        $this->andWhere(['clients.delete' => 0]);
        if (Yii::$app->user->identity->role != 'admin') {
            if (Yii::$app->user->identity->role == 'manager') {
                $this->manager()->thisAgency();
            }
            if (Yii::$app->user->identity->role == 'agency') {
                $this->thisAgency();
            }
        }
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\clients\models\Clients|array|null
     */
    public function one($db = null)
    {

        if (Yii::$app->user->identity->role != 'admin') {
            $this->andWhere(['clients.delete' => 0]);
            if (Yii::$app->user->identity->role == 'manager') {
                $this->manager()->thisAgency();
            }
            if (Yii::$app->user->identity->role == 'agency') {
                $this->thisAgency();
            }
        }

        return parent::one($db);
    }

    public function thisAgency()
    {
        $this->leftJoin("client_agency", 'client_agency.client_id = clients.id');
        $this->andWhere([
            'or',
            ['clients.agency_id' => Yii::$app->user->identity->agency_id],
            ['client_agency.agency_id' => Yii::$app->user->identity->agency_id]
        ]);
        return $this;
    }

    public function agency($id)
    {
        $this->andWhere(['clients.agency_id' => $id]);
        return $this;
    }

    public function manager($id = null)
    {
        $id = is_null($id) ? (Yii::$app->user->id) : $id;
        $this->leftJoin("client_manager", 'client_manager.client_id = clients.id')
            ->andWhere(['client_manager.manager_id' => $id]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return Agency[]|array
     */
    public function allDelete($db = null)
    {
        return parent::all($db);
    }

    public function deleted()
    {
        $this->andWhere(['clients.delete' => 1]);
        return $this;
    }
}
