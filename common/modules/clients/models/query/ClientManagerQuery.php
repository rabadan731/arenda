<?php

namespace common\modules\clients\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\clients\models\ClientManager]].
 *
 * @see \common\modules\clients\models\ClientManager
 */
class ClientManagerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\clients\models\ClientManager[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\clients\models\ClientManager|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    /**
     * @inheritdoc
     */
    public function cm($client_id, $user_id)
    {
        $this->andWhere(['client_id'=>$client_id, 'manager_id'=>$user_id]);
        return $this;
    }
}
