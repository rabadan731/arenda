<?php

namespace common\modules\clients\models;

use common\modules\clients\models\query\ClientManagerQuery;
use modules\log\behaviors\LogBehavior;
use modules\users\models\backend\Users;
use Yii;

/**
 * This is the model class for table "client_manager".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $manager_id
 *
 * @property Clients $client
 * @property Users $manager
 */
class ClientManager extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_manager';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'manager_id'], 'required'],
            [['client_id', 'manager_id'], 'integer'],
            [
                ['client_id'],
                'exist', 'skipOnError' => true,
                'targetClass' => Clients::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
            [
                ['manager_id'],
                'exist', 'skipOnError' => true,
                'targetClass' => Users::className(),
                'targetAttribute' => ['manager_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'client_id' => Yii::t('common', 'Client ID'),
            'manager_id' => Yii::t('common', 'Manager ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Users::className(), ['id' => 'manager_id']);
    }

    /**
     * @inheritdoc
     * @return ClientManagerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClientManagerQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'log' => [
                'class' => LogBehavior::className(),
                'types' => [
                    'update' => function ($model) {
                        /* @var $model self */
                        return "Изменил у клиента \"{$model->client->fio}\" 
                        менеджера \"{$model->manager->name}\" ";
                    },
                    'insert' => function ($model) {
                        /* @var $model self */
                        return "Сделал \"{$model->manager->name}\" 
                        менеджером клиента \"{$model->client->fio}\" ";
                    },
                    'delete' => function ($model) {
                        /* @var $model self */
                        return "Удалил у клиента \"{$model->client->fio}\" 
                        менеджера \"{$model->manager->name}\" ";
                    }
                ]
            ]
        ];
    }
}
