<?php

namespace common\modules\clients\models;

use abeautifulsite\SimpleImage;
use common\modules\agency\models\Agency;
use common\modules\agency\models\ForRent;
use common\modules\clients\models\query\ClientsQuery;
use modules\log\behaviors\LogBehavior;
use modules\users\models\backend\Users;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "clients".
 *
 * @property integer $id
 * @property string $fio
 * @property string $photo
 * @property string $email
 * @property string $phone
 * @property string $delete
 * @property string $import_md5
 * @property float $balance
 * @property integer $agency_id
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $status
 *
 * @property ClientManager[] $managers
 * @property ClientPayments[] $clientPayments
 * @property Agency $agency
 * @property ForRent[] $forRents
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * Путь для сохранения файлов (аватарок)
     */
    const AVATAR_PATH = '@files/clients/avatars/';

    /**
     * URL директории с аватарками
     */
    const AVATAR_URL = '@urlFiles/clients/avatars/';

    /**
     * URL к аватарке по умолчанию
     */
    const DEFAULT_AVATAR_URL = '@urlFiles/clients/avatar.jpg';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'agency_id'], 'required'],
            [['email'], 'email'],
            [['agency_id', 'status', 'created_by', 'updated_by', 'delete'], 'integer'],
            [['balance'], 'number'],
            [['balance'], 'default', 'value' => 0],
            [['fio'], 'string', 'max' => 512],
            [['import_md5'], 'string', 'max' => 128],
            [['email', 'phone'], 'string', 'max' => 256],
            ['photo', 'file', 'mimeTypes' => ['image/png', 'image/gif', 'image/jpeg']],
            [
                ['agency_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Agency::className(),
                'targetAttribute' => ['agency_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'fio' => Yii::t('common', 'Fio'),
            'photo' => Yii::t('common', 'Photo'),
            'email' => Yii::t('common', 'Email'),
            'phone' => Yii::t('common', 'Phone'),
            'balance' => Yii::t('common', 'Balance'),
            'agency_id' => Yii::t('common', 'Agency ID'),
            'created_by' => Yii::t('common', 'Created By'),
            'updated_by' => Yii::t('common', 'Updated By'),
            'status' => Yii::t('common', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientPayments()
    {
        return $this->hasMany(ClientPayments::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'agency_id']);
    }

    /**
     * @inheritdoc
     * @return ClientsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClientsQuery(get_called_class());
    }


    /**
     * Полный путь к аватарке
     * @param bool $small Уменьшиная копия
     * @param bool $exists Делать проверку на наличие файла или нет
     * @return string
     */
    public function getPathAvatar($small = false, $exists = true)
    {
        $path = Yii::getAlias(self::AVATAR_PATH) .
            ($small ? 'small_' : '') .
            ($this->photo ? $this->photo : $this->getOldAttr('photo'));
        return is_file($path) || $exists === false ? $path : null;
    }

    public function getOldAttr($attribute)
    {
        return isset($this->oldAttributes[$attribute])?$this->oldAttributes[$attribute]:null;
    }

    /**
     * URL к аватарке
     * @param bool $small Уменьшиная копия
     * @return string
     */
    public function getUrlAvatar($small = false)
    {
        if (is_file($this->getPathAvatar($small))) {
            return Yii::getAlias(self::AVATAR_URL) . ($small ? 'small_' : '') . $this->photo;
        }
        return Yii::getAlias(self::DEFAULT_AVATAR_URL);
    }

    /**
     * Удаление аватара
     */
    public function deleteAvatar($save = false)
    {
        if (is_file($this->getPathAvatar())) {
            unlink($this->getPathAvatar());
        }
        if (is_file($this->getPathAvatar(true))) {
            unlink($this->getPathAvatar(true));
        }
        $this->photo = null;
        if ($save) {
            $this->save(0);
        }
    }

    /**
     * Загружаем аватар
     * @throws \Exception
     */
    private function uploadAvatar()
    {
        FileHelper::createDirectory(Yii::getAlias(self::AVATAR_PATH));
        $avatar = UploadedFile::getInstance($this, 'photo');
        if ($avatar !== null) {
            $this->deleteAvatar();
            $this->photo = uniqid() . '.jpg';
            if ($avatar->saveAs($this->getPathAvatar(false, false))) {
                $small = new SimpleImage($this->getPathAvatar());
                $small->thumbnail(120, 150);
                $small->save($this->getPathAvatar(true, false), 100, $avatar->extension);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!Yii::$app->user->can("admin")) {
            $this->agency_id = Yii::$app->user->identity->agency_id;
        }

        if (!empty(UploadedFile::getInstance($this, 'photo'))) {
            $this->uploadAvatar();
        } else {
            $this->photo = $this->getOldAttr('photo');
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForRents()
    {
        return $this->hasMany(ForRent::className(), ['client_id' => 'id']);
    }

    /**
     * Лист клиентов
     * @return array
     */
    public static function getList()
    {
        $find = self::find();
        return ArrayHelper::map($find->all(), 'id', 'fio');
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManagers()
    {
        return $this->hasMany(
            Users::className(),
            ['id' => 'manager_id']
        )->viaTable(
            'client_manager',
            ['client_id' => 'id']
        );
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            'log' => [
                'class' => LogBehavior::className(),
                'types' => [
                    'update' => function ($model) {
                        /* @var $model self */
                        return "Изменил клиента \"{$model->fio}\" ID:{$model->id}";
                    },
                    'insert' => function ($model) {
                        /* @var $model self */
                        return "Добавил клиента \"{$model->fio}\" ID:{$model->id}";
                    },
                    'delete' => function ($model) {
                        /* @var $model self */
                        return "Удалил клиента \"{$model->fio}\" ID:{$model->id}";
                    }
                ]
            ]
        ];
    }

    /**
     * @return bool
     */
    public function myDelete()
    {
        $this->delete = 1;
        return $this->save(false, ['delete']);
    }

    /**
     * @return bool
     */
    public function unDelete()
    {
        $this->delete = 0;
        return $this->save(false, ['delete']);
    }
}
