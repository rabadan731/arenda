<?php

namespace modules\clients\controllers\backend;

use Yii;
use common\modules\clients\models\ClientPayments;
use common\modules\clients\models\search\ClientPaymentsSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientPaymentsController implements the CRUD actions for ClientPayments model.
 */
class ClientPaymentsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ClientPayments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientPaymentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ClientPayments model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ClientPayments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ClientPayments();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ClientPayments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        $oldPrice = $model->price;
        $client = $model->client;

        if ($model->load($post)) {
            if (!is_int($model->date_invoice)) {
                $model->date_invoice = strtotime($model->date_invoice);
            }

            $r = $model->price - $oldPrice;

            if ($model->save()) {
                $client->balance = $client->balance + $r;
                $client->save(false, ['balance']);
                return $this->redirect(Url::previous());
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ClientPayments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $for_rent_id = $model->for_rent_id;
        $oldPrice = $model->price;
        $client = $model->client;
        if ($model->myDelete()) {
            $client->balance = $client->balance - $oldPrice;
            $client->save(false, ['balance']);
        }

        return $this->redirect(Url::previous());
    }

    public function actionUndelete($id)
    {
        $model = $this->findModel($id);
        $for_rent_id = $model->for_rent_id;
        $oldPrice = $model->price;
        $client = $model->client;
        if ($model->unDelete()) {
            $client->balance = $client->balance + $oldPrice;
            $client->save(false, ['balance']);
        }

        Yii::$app->session->setFlash('success', Yii::t("common", "The object is restored"));

        return $this->redirect(['for-rent/view', 'id' => $for_rent_id]);
    }

    /**
     * Finds the ClientPayments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClientPayments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ClientPayments::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
