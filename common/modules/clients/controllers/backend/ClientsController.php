<?php

namespace modules\clients\controllers\backend;

use common\modules\agency\models\Agency;
use common\modules\clients\models\ClientAgency;
use common\modules\clients\models\ClientManager;
use common\modules\clients\models\search\ClientAgencySearch;
use common\modules\clients\models\search\ClientPaymentsSearch;
use common\modules\agency\models\search\ForRentSearch;
use modules\users\models\backend\Users;
use Yii;
use common\modules\clients\models\Clients;
use common\modules\clients\models\search\ClientsSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * ClientsController implements the CRUD actions for Clients model.
 */
class ClientsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            Url::remember();
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'undelete' => ['POST'],
                    'unset-manager' => ['POST'],
                    'set-manager' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Clients models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientsSearch();

        $params = Yii::$app->request->queryParams;
        if (!Yii::$app->user->can('admin')) {
            $params['ClientsSearch']['agency_id'] = Yii::$app->user->identity->agency_id;
        }
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Clients model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $queryManagers = $model->getManagers();
        $managersProvider = new ActiveDataProvider([
            'query' => $queryManagers
        ]);

        $queryUsers = Users::find();
        $queryUsers->andWhere([
            'agency_id' => $model->agency_id,
            'role' => 'manager',
        ]);
        $ids = ArrayHelper::getColumn($queryManagers->asArray()->all(), 'id');
        $queryUsers->andWhere(['not in', 'id', $ids]);


        $searchModel = new ClientAgencySearch();
        $searchModel->client_id = $model->id;
        $othersAgencyProvider = $searchModel->search([]);

        $idList = ArrayHelper::getColumn($othersAgencyProvider->query->all(), 'agency_id');
        $idList[] = $model->agency_id;

        $othersAgencyQuery = Agency::find()->select('id,title')->where(['not in', 'id', $idList]);
        $othersAgency = ArrayHelper::map($othersAgencyQuery->all(), 'id', 'title');

        $searchModelPayments = new ClientPaymentsSearch();
        $searchModelPayments->client_id = $model->id;
        $paymentsProvider = $searchModelPayments->search([]);


        $forRentSearch = new ForRentSearch();
        $forRentSearch->client_id = $id;
        $forRentProvider = $forRentSearch->search([]);


        return $this->render('view', [
            'model' => $model,
            'managersProvider' => $managersProvider,
            'noManagers' => ArrayHelper::map($queryUsers->all(), 'id', 'name'),
            'othersAgency' => $othersAgency,
            'othersAgencyProvider' => $othersAgencyProvider,
            'paymentsProvider' => $paymentsProvider,
            'forRentProvider' => $forRentProvider,
        ]);
    }

    /**
     * Displays a single Clients model.
     * @param integer $id
     * @return mixed
     */
    public function actionUnsetManager($id, $user_id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $this->findModel($id);
        $model = ClientManager::find()->cm($id, $user_id)->one();
        if (!is_null($model)) {
            if ($model->delete()) {
                return ['success' => true];
            } else {
                return ['success' => false, 'text' => Yii::t("common", "Error saving data")];
            }
        }
    }

    /**
     * @param integer $id ,$user_id
     * @return mixed
     */
    public function actionSetManager($id, $manager_id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $this->findModel($id);
        $model = ClientManager::find()->cm($id, $manager_id)->one();
        if (is_null($model)) {
            $model = new ClientManager();
            $model->client_id = $id;
            $model->manager_id = $manager_id;
            if ($model->save()) {
                return ['success' => true];
            } else {
                return ['success' => false, 'text' => Yii::t("common", "Error saving data")];
            }
        }
    }

    /**
     * Displays a single Clients model.
     * @param integer $id
     * @return mixed
     */
    public function actionUnsetAgency($id, $agency_id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (Yii::$app->user->identity->role == 'manager') {
            return ['success' => false, 'text' => Yii::t("common", "Access is denied")];
        } else {
            $rent = $this->findModel($id);
            $model = ClientAgency::find()->cm($id, $agency_id)->one();
            if (is_null($model)) {
                return ['success' => false, 'text' => Yii::t("common", "No access")];
            } else {
                if ($model->delete()) {
                    return ['success' => true];
                } else {
                    return ['success' => false, 'text' => Yii::t("common", "Error saving data")];
                }
            }
        }
    }

    /**
     * @param integer $id ,$user_id
     * @return mixed
     */
    public function actionSetAgency($id, $agency_id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (Yii::$app->user->identity->role == 'manager') {
            return [
                'success' => false,
                'text' => Yii::t("common", "Access is denied")
            ];
        } else {
            $model = ClientAgency::find()->cm($id, $agency_id)->one();

            if (is_null($model)) {
                $model = new ClientAgency();
                $model->client_id = $id;
                $model->agency_id = $agency_id;
                if ($model->save()) {
                    return ['success' => true];
                } else {
                    return ['success' => false, 'text' => Yii::t("common", "Error saving data")];
                }
            }
        }
        return ['success' => false, 'text' => Yii::t("common", "Unknown error")];
    }


    /**
     * Creates a new Clients model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($save = 0, $agency_id = null)
    {
        $model = new Clients();
        if (is_null($agency_id)) {
            $agency_id = Yii::$app->user->identity->agency_id;
        }

        if (!is_null($agency_id) && Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($save == 0) {
                return ActiveForm::validate($model);
            } else {
                $model->agency_id = Yii::$app->user->identity->agency_id;
                $model->load(Yii::$app->request->post());
                $md5 = md5("client".print_r($model->attributes, 1));
                $findMd5 = Clients::find()->andWhere(['import_md5' => $md5])->one();
                if (is_null($findMd5)) {
                    $model->import_md5 = $md5;
                    if ($model->save()) {
                        if (Yii::$app->user->identity->role == 'manager') {
                            $rm = new ClientManager();
                            $rm->client_id = $model->id;
                            $rm->manager_id = Yii::$app->user->id;
                            $rm->save();
                        }
                        return ['success' => true, 'find'=> false, ' errors' => $model->errors, 'at' => $model->attributes];
                    } else {
                        return ['success' => false, 'find'=> false, ' errors' => $model->errors, 'at' => $model->attributes];
                    }
                } else {
                    return ['success' => false, 'find'=> true, 'at' => $findMd5->attributes];
                }
            }
        }


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->user->identity->role == 'manager') {
                $rm = new ClientManager();
                $rm->client_id = $model->id;
                $rm->manager_id = Yii::$app->user->id;
                $rm->save();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing Clients model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Clients model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->myDelete();

        return $this->redirect(['index']);
    }

    public function actionUndelete($id)
    {
        $this->findModel($id)->unDelete();

        Yii::$app->session->setFlash('success', Yii::t("common", "The object is restored"));

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Finds the Clients model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clients the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Clients::find()->where(['clients.id' => $id])->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Удаление аватара
     * @return \yii\web\Response
     */
    public function actionDeleteAvatar($id)
    {
        $model = $this->findModel($id);
        $model->deleteAvatar(true);
        return $this->redirect(['update', 'id' => $id]);
    }
}
