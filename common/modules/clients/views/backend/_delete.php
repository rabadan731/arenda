<?php

use yii\helpers\Html;

?>
<div class="alert alert-danger" role="alert">
    <?= Yii::t("common", "This object has been deleted"); ?>...
    &nbsp;
    <?php echo Html::a(Yii::t('common', 'To restore the object'), ['undelete', 'id' => $model->id], [
        'class' => 'btn btn-danger btn-sm ',
        'data' => [
            'confirm' => Yii::t('common', 'To restore the object'),
            'method' => 'post',
        ],
    ]) ?>
</div>