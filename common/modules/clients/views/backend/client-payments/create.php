<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\clients\models\ClientPayments */

$this->title = Yii::t('common', 'Create Client Payments');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Client Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jarviswidget">
    <div class="client-payments-create">
        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
