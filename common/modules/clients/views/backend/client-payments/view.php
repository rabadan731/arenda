<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\clients\models\ClientPayments */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Client Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'client-payments';

?>
<div class="jarviswidget">

    <?php if ($model->delete) { ?>
        <?php echo $this->render('/_delete', ['model'=>$model]); ?>
    <?php } ?>

    <div class="client-payments-view">

        <p>
            <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'client_id',
                'title',
                'date_invoice',
                'price',
                'paid',
            ],
        ]) ?>
    </div>
</div>
