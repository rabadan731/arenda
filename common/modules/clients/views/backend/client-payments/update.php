<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\clients\models\ClientPayments */

$this->title = Yii::t('common', 'Update {modelClass}: ', [
    'modelClass' => 'Client Payments',
]) . $model->title;


$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'client-payments';

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Client Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="jarviswidget">
    <div class="client-payments-update">
 
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
