<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\clients\models\Clients;
use common\modules\agency\models\ForRent;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\modules\clients\models\ClientPayments */
/* @var $form yii\widgets\ActiveForm */

$model->date_invoice = $model->date_invoice = date("Y-m-d H:i", is_null($model->date_invoice) ? time() :
    (is_int($model->date_invoice) ? $model->date_invoice : strtotime($model->date_invoice)));

?>

<div class="client-payments-form col-md-6">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'for_rent_id')->dropDownList(ForRent::getList($model), [
        'disabled'=>!Yii::$app->user->can('admin')
    ]) ?>
    <?= $form->field($model, 'client_id')->dropDownList(Clients::getList(), [
        'prompt' => Yii::t("common", "-- Select --"),
        'disabled'=>!Yii::$app->user->can('admin')
    ]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= Html::activeLabel($model, 'date_invoice'); ?>
    <?= DateTimePicker::widget([
        'model' => $model,
        'attribute' => 'date_invoice',
        'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd HH:ii',
            'autoclose' => true,
        ],
        'pluginEvents' => []
    ]); ?>

    <br />

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
