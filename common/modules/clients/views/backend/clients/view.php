<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\modules\clients\models\Clients */

$this->title = $model->fio;
$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'сlients';
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$confirmDelete = Yii::t("common", "Are you sure you want to unlink this user?");

$errorText = Yii::t("common", "Error saving data");

$js = <<< JS
$('body').on('click', '#manager-add', function () {
var managerId = parseInt($("#no-manager-list").val());
    if (!isNaN(managerId)) {
        $.ajax({
          url: '/backend/clients/clients/set-manager/?id={$model->id}&manager_id='+managerId,
          type: 'post',
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false 
          success: function (response) {
               if (response.success) {
                   $.pjax.reload({container:"#list-managers"}); 
               } else {
                   alert('$errorText');
               }
          }
     });
    }
});

$('body').on('click', '.unset-manager', function () {
     if (confirm("$confirmDelete")) {
        $.ajax({
           url: $(this).data('url'),
           type: 'post',
           cache: false,             // To unable request pages to be cached
           processData:false,        // To send DOMDocument or non processed data file it is set to false 
           success: function (response) {
           console.log(response);
              if (response.success) {
                 $.pjax.reload({container:"#list-managers"}); 
              } else {
                 alert(response.text);
              }
           }
        });
     }
});


$('body').on('click', '#agency-add', function () {
    var agencyId = parseInt($("#no-agency-list").val());
    if (!isNaN(agencyId)) {
        $.ajax({
          url: '/backend/clients/clients/set-agency/?id={$model->id}&agency_id='+agencyId,
          type: 'post',
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false 
          success: function (response) {
          console.log(response);
               if (response.success) {
                $.pjax.reload({container:"#list-managers"}); 
               } else {
                   alert(response.text);
               }
          }
     });
    }
});


$('body').on('click', '.unset-agency', function () {
     if (confirm("$confirmDelete")) {
        $.ajax({
           url: $(this).data('url'),
           type: 'post',
           cache: false,             // To unable request pages to be cached
           processData:false,        // To send DOMDocument or non processed data file it is set to false 
           success: function (response) {
           console.log(response);
              if (response.success) {
                 $.pjax.reload({container:"#list-managers"}); 
              } else {
                 alert(response.text);
              }
           }
        });
     }
});


JS;
$this->registerJs($js);
//
//

?>


<div class="rent-form" id="content">

    <?php if ($model->delete) { ?>
        <?php echo $this->render('/_delete', ['model' => $model]); ?>
    <?php } ?>

    <div class="jarviswidget" data-widget-togglebutton="false" data-widget-editbutton="false"
         data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false"
         role="widget">


        <header role="heading">

            <ul class="nav nav-tabs in" id="myTab">
                <li class="active">
                    <a data-toggle="tab" href="#s1" aria-expanded="true">
                        <i class="fa fa-building"></i>
                        <span class="hidden-mobile hidden-tablet">
                            <?= Yii::t("common", "Information about client"); ?>
                        </span>
                    </a>
                </li>

                <li class="">
                    <a data-toggle="tab" href="#s2" aria-expanded="false">
                        <i class="fa fa-calendar"></i>
                        <span class="hidden-mobile hidden-tablet">
                            <?= Yii::t("common", "History removal"); ?>
                        </span>
                    </a>
                </li>


                <li class="">
                    <a data-toggle="tab" href="#s3" aria-expanded="false">
                        <i class="fa fa-eur"></i>
                        <span class="hidden-mobile hidden-tablet">
                            <?= Yii::t("common", "Payment transaction"); ?>
                        </span>
                    </a>
                </li>

                <li class="">
                    <a data-toggle="tab" href="#s4" aria-expanded="false">
                        <i class="fa fa-cogs"></i>
                        <span class="hidden-mobile hidden-tablet">
                            <?= Yii::t("common", "Permissions"); ?>
                        </span>
                    </a>
                </li>

            </ul>

            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
        </header>

        <!-- widget div-->
        <div class="no-padding" role="content">
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">

                test
            </div>
            <!-- end widget edit box -->

            <div class="widget-body">
                <!-- content -->
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in padding-10 no-padding-bottom" id="s1">

                        <?php if ($model->getUrlAvatar(true) !== null) : ?>
                            <img src="<?= $model->getUrlAvatar(true) ?>" alt="<?= $model->fio ?>"/>
                        <?php endif; ?>
                        <br/><br/>
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'fio',
                                'email:email',
                                'phone',
                                'balance',
                            ],
                        ]) ?>
                        <br/><br/>
                        <p>
                            <?= Html::a(
                                Yii::t('common', 'Update'),
                                ['update', 'id' => $model->id],
                                ['class' => 'btn btn-primary']
                            ) ?>

                            <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </p>

                    </div>
                    <div class="tab-pane fade" id="s2">
                        <div class="padding-10">
                            <?= \yii\grid\GridView::widget([
                                'dataProvider' => $forRentProvider,
                                'columns' => [

                                    'check_in:date',
                                    'check_out:date',
                                    'price_all',
                                    'price_day',
                                    'price_deposit',
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'urlCreator' => function ($action, $model, $key, $index) {
                                            return ['for-rent/' . $action, 'id' => $model->id];
                                        },
                                        'template' => '<div class="text-center"> {view2} {update} {delete}</div>'
                                    ],
                                ],
                            ]); ?>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="s3">
                        <div class="padding-10">

                            <?= $this->render('_payments', [
                                'model' => $model,
                                'paymentsProvider' => $paymentsProvider
                            ]) ?>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="s4">
                        <?php \yii\widgets\Pjax::begin(['id' => 'list-managers', 'timeout' => 10000]); ?>
                        <div class="padding-10">
                            <div class="row">
                                <div class="col-md-7">
                                    <?= $this->render('_users', [
                                        'model' => $model,
                                        'dataProvider' => $managersProvider
                                    ]) ?>


                                    <div class="row">
                                        <div class="col-md-8">
                                            <!-- end widget content -->
                                            <?= Html::dropDownList('noManager', null, $noManagers, [
                                                'prompt' => Yii::t("common", "Select Manager"),
                                                'class' => 'form-control',
                                                'id' => 'no-manager-list'
                                            ]); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?= Html::button(Yii::t("common", "Add"), [
                                                'id' => 'manager-add',
                                                'class' => 'form-control btn btn-success'
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h3 class="no-margin"><?= Yii::t("common", "Other agencies"); ?></h3>
                                    <?= GridView::widget([
                                        'dataProvider' => $othersAgencyProvider,
                                        'columns' => [
                                            'agency.title',
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'urlCreator' => function ($action, $model, $key, $index) {
                                                    return ['/users/default/' . $action, 'id' => $model['id']];
                                                },
                                                'template' => '<div class="text-center">{unlink} {view}</div>',
                                                'buttons' => [
                                                    'view' => function ($url, $model, $key) {
                                                        return Html::a('<i class="fa fa-eye"></i>', $url, [
                                                            'class' => 'btn btn-xs btn-info',
                                                            'title' => Yii::t("common", "View"),
                                                            'data-pjax' => 0,
                                                            'target' => '_blank'
                                                        ]);
                                                    },
                                                    'unlink' => function ($url, $manager, $key) use ($model) {
                                                        return Html::a(
                                                            '<i class="fa fa-trash-o"></i> '.
                                                            Yii::t("common", "To decouple"),
                                                            'javascript:void(0);',
                                                            [
                                                                'class' => 'btn btn-xs btn-danger unset-agency',
                                                                'data-pjax' => 0,
                                                                'data-url' => \yii\helpers\Url::to([
                                                                    'unset-agency',
                                                                    'id' => $model->id,
                                                                    'agency_id' => $manager['agency_id']
                                                                ])
                                                            ]
                                                        );
                                                    }
                                                ]
                                            ]
                                        ],
                                    ]); ?>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <!-- end widget content -->
                                            <?= Html::dropDownList('noAgency', null, $othersAgency, [
                                                'prompt' => Yii::t("common", "Choose the Agency"),
                                                'class' => 'form-control',
                                                'id' => 'no-agency-list'
                                            ]); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?= Html::button(Yii::t("common", "Add"), [
                                                'id' => 'agency-add',
                                                'class' => 'form-control btn btn-success'
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php \yii\widgets\Pjax::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
