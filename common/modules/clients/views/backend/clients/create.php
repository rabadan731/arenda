<?php

/* @var $this yii\web\View */
/* @var $model common\modules\clients\models\Clients */

$this->title = Yii::t('common', 'Add client');

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'сlients';

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="jarviswidget">
    <div class="clients-create"  id="content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
