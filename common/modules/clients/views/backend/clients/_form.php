<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\modules\agency\models\Agency;

/* @var $this yii\web\View */
/* @var $model common\modules\clients\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(['id'=>'clients-form']); ?>

    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?php if (!$model->isNewRecord && $model->getUrlAvatar(true) !== null) : ?>
            <img src="<?= $model->getUrlAvatar(true) ?>" alt="<?= $model->fio ?>"/>
            <div>
                &raquo;
                <a href="<?= Url::to(['delete-avatar','id'=>$model->id]) ?>">
                    <?= Yii::t("common", "Remove avatar"); ?>
                </a>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'photo')->fileInput() ?>
    </div>


    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?php if (Yii::$app->user->can('admin')) : ?>

        <?= $form->field($model, 'balance')->textInput() ?>

        <?= $form->field($model, 'agency_id')->dropDownList(Agency::getAgencies(), [
            'prompt' => Yii::t("common", "-- Select --")
        ]) ?>

    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Save'), [
            'class'=>'btn btn-primary',
            'id' => 'clients-form-button'
        ]); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
