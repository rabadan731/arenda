<?php

/* @var $paymentsProvider yii\data\ActiveDataProvider */

use yii\widgets\Pjax;
use yii\grid\GridView;

?>

<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $paymentsProvider,
    'columns' => [
        'title',
        'rent.address_text',
        'date_invoice:date',
        'price',
        [
            'class' => 'yii\grid\ActionColumn',
            'urlCreator' => function ($action, $model, $key, $index) {
                return ['client-payments/' . $action, 'id' => $model->id];
            },
            'template' => '<div class="text-center">  {update} {delete}</div>'
        ],
    ],
]); ?>
<?php Pjax::end();
