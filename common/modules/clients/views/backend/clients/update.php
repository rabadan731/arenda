<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\clients\models\Clients */

$this->title = Yii::t('common', 'Update {modelClass}: ', [
        'modelClass' => 'Clients',
    ]) . $model->id;

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'сlients';

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="jarviswidget">
    <div class="clients-update"  id="content">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
