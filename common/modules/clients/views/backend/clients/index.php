<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\clients\models\search\ClientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Clients');

$this->params['pageTitle'] = $this->title;
$this->params['place'] = 'сlients';

$this->params['breadcrumbs'][] = $this->title;


?>
<div class="jarviswidget">
    <div class="clients-index">

        <p>
            <?= Html::a(Yii::t('common', 'Add client'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php Pjax::begin(); ?>    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'photo',
                    'filter' => false,
                    'format' => 'html',
                    'value' => function ($data) {
                        return Html::img($data->getUrlAvatar(true), ['style'=>'width:50px']);
                    },
                    'options'=>['style'=>'width:50px']
                ],
                'fio',
                'email:email',
                'phone',
                'balance',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '<div class="text-center"> {view2} {update} {delete}</div>'
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
