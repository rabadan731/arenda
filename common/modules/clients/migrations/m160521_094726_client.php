<?php

use yii\db\Migration;

class m160521_094726_client extends Migration
{

    /**
     * @var string
     */
    public $table = '{{%clients}}';
    public $client_men = '{{%client_manager}}';
    public $agency = '{{%agency}}';
    public $table_payments = '{{%client_payments}}';
    public $table_agency = '{{%client_agency}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id'            => $this->primaryKey(),
            'fio'           => $this->string(512)->notNull(),
            'photo'         => $this->string(512),
            'email'         => $this->string(256),
            'phone'         => $this->string(256),
            'balance'       => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'agency_id'     => $this->integer(11),
            'created_by'    => $this->integer(),
            'updated_by'    => $this->integer(),
            'status'        => $this->integer(1),
            'delete'        => $this->integer(1)->defaultValue(0), //статус удален/не удален
        ]);

        $this->createIndex('idx_clients_agency_id', $this->table, 'agency_id');
        $this->addForeignKey('fk_clients_agency_items', $this->table, 'agency_id', $this->agency, 'id', 'cascade', 'cascade');

        $this->createTable($this->table_payments, [
            'id'            => $this->primaryKey(),
            'client_id'     => $this->integer(11)->notNull(),
            'title'         => $this->string(512)->notNull(),
            'date_invoice'  => $this->integer(11),
            'price'         => $this->decimal(10,2)->notNull()->defaultValue(0.00),
            'paid'          => $this->integer(1)->defaultValue(0),
            'for_rent_id'   => $this->integer(11),
            'delete'        => $this->integer(1)->defaultValue(0), //статус удален/не удален
        ]);

        $this->createIndex('idx_client_payments_client_id', $this->table_payments, 'client_id');
        $this->addForeignKey('fk_client_payments_client_items', $this->table_payments, 'client_id', $this->table, 'id', 'cascade', 'cascade');


        $this->createTable($this->client_men, [
            'id'             => $this->primaryKey(),
            'client_id'      => $this->integer()->notNull(),
            'manager_id'     => $this->integer()->notNull(),
            'access_type'    => $this->integer(1)->defaultValue(1),
        ]);

        $this->createIndex('idx_client_manager_client_id', $this->client_men, 'client_id');
        $this->addForeignKey('fk_client_manager_client_items', $this->client_men, 'client_id', $this->table, 'id', 'cascade', 'cascade');
        $this->createIndex('idx_client_manager_manager_id', $this->client_men, 'manager_id');
        $this->addForeignKey('fk_client_manager_manager_items', $this->client_men, 'manager_id', '{{%users}}', 'id', 'cascade', 'cascade');

        $this->createTable($this->table_agency, [
            'id'             => $this->primaryKey(),
            'client_id'      => $this->integer()->notNull(),
            'agency_id'      => $this->integer()->notNull(),
            'access_type'    => $this->integer(1)->defaultValue(1),
        ]);

        $this->createIndex(  'idx_client_agency_client_tc_id', $this->table_agency, 'client_id');
        $this->addForeignKey('fk_client_agency_client_tc_items', $this->table_agency, 'client_id', $this->table, 'id', 'cascade', 'cascade');
        $this->createIndex(  'idx_client_agency_client_ta_id', $this->table_agency, 'agency_id');
        $this->addForeignKey('fk_client_agency_client_ta_items', $this->table_agency, 'agency_id', $this->agency, 'id', 'cascade', 'cascade');


    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_client_agency_client_tc_items', $this->table_agency);
        $this->dropForeignKey('fk_client_agency_client_ta_items', $this->table_agency);
        $this->dropTable($this->table_agency);

        $this->dropForeignKey('fk_client_manager_client_items', $this->client_men);
        $this->dropForeignKey('fk_client_manager_manager_items', $this->client_men);

        $this->dropForeignKey('fk_clients_agency_items', $this->table);
        $this->dropForeignKey('fk_client_payments_client_items', $this->table_payments);

        $this->dropTable($this->client_men);
        $this->dropTable($this->table_payments);
        $this->dropTable($this->table);
    }

}
