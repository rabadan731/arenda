<?php

use yii\db\Migration;

class m555555_555556_demo_clients extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {




            $this->batchInsert('{{%clients}}', [
                'fio', 'email', 'phone', 'balance', 'agency_id', 'created_by', 'updated_by', 'status',
            ], [
                ['Клиент Фамилия Отчество 1', 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['Клиент Фамилия Отчество 2', 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['Клиент Фамилия Отчество 3', 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['Клиент Фамилия Отчество 4', 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['Клиент Фамилия Отчество 5', 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['Клиент Фамилия Отчество 6', 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['Клиент Фамилия Отчество 7', 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['Клиент Фамилия Отчество 8', 'email@email.ru', '112', '0', $i, '2', '2', '1'],
                ['Клиент Фамилия Отчество 9', 'email@email.ru', '112', '0', $i, '2', '2', '1'],
            ]);

    }
}
