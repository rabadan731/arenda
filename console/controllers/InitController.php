<?php

namespace console\controllers;

use Yii;
use common\helpers\VarDumper;
use modules\users\models\backend\Users;
use yii\console\Controller;

class InitController extends Controller
{
    public function actionRegenerateAssignments()
    {
        $assignments = [];
        foreach (Users::find()->all() as $item) {
            /** @var Users $item */
            $assignments[$item->id] = [$item->role];
        }

        file_put_contents(
            Yii::getAlias(Yii::$app->authManager->assignmentFile),
            "<?php\nreturn " . VarDumper::export($assignments) . ";\n",
            LOCK_EX
        );
    }
}
