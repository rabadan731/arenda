<?php

namespace console\controllers;

use common\modules\agency\models\RentImportList;
use common\modules\agency\models\RentImportServices;
use yii\console\Controller;
use common\modules\agency\models\Agency;
use common\modules\agency\models\AgencyPayLog;
use yii\helpers\Console;

class CronController extends Controller
{
    public function actionTest()
    {
        echo \Yii::$app->id;
    }

    public function actionExchangeRentsCalendars()
    {
        $items = RentImportServices::find()->all();
        foreach ($items as $item) {
            /** @var RentImportServices $item */
            $item->importCalendar();
        }
    }

    // arenda.rabadan.ru/public_html/yii cron/payments
    public function actionPayments()
    {
        /* @var $model Agency */

        //$time = time();

        $time = strtotime(date("d-m-Y",time()));
        array_map(function($model) use ($time){
            /** @var $model Agency */
            $model->balanceChanged();
            if ($model->date_begin < $time && $time <= $model->date_end) {
                if (is_null(AgencyPayLog::find()->where(['date_pay'=>$time])->one())) {

                    if ($model->balance < $model->tariff->getPriceDay()){
                        $model->status = $model::STATUS_END_PAY;
                        $model->tariff_id = null;
                        $model->save(false,['status','tariff_id']);
                        Console::output("Save ".$model->id);
                    } else {
                        $balance_log = new AgencyPayLog();
                        $balance_log->date_pay = $time;
                        $balance_log->agency_id = $model->id;
                        $balance_log->tariff_id = $model->tariff_id;
                        $balance_log->price = $model->tariff->getPriceDay();
                        if ($balance_log->save()) {
                            $model->balance = $model->balance - $balance_log->price;
                            $model->save(false,['balance']);
                            Console::output("Save ".$model->id);
                        } else {
                            Console::output(print_r($balance_log->errors,1));
                        }
                    }

                }
            }

        },Agency::find()->active()->all());
    }

}
