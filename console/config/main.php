<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerMap' => [
        'migrate'=>[
            'class'=>'app\commands\MigrateController',
            'migrationLookup'=>[
                '@common/modules/log/migrations',
                '@common/modules/users/migrations',
                '@common/modules/agency/migrations',
                '@common/modules/eav/migrations',
                '@common/modules/main/migrations',
                '@common/modules/cars/migrations',
            ]
        ]
    ],
    'controllerNamespace' => 'console\controllers',
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'user' => [
            'class' => 'modules\users\components\User',
            'identityClass' => 'modules\users\models\backend\Users',
            'enableAutoLogin' => true,
            'loginUrl' => ['/users/guest/login'],
        ],
    ],
    'params' => $params,
];
